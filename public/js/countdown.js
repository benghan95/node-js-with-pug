$(document).ready(function(){

  var clock;
  // Grab the current date
  var currentDate = new Date();
  // Set some date in the future. In this case, it's always Jan 1
  var futureDate  = moment.tz('2018-05-15 09:00:00', 'Singapore');
  // Calculate the difference in seconds between the future and current date
  var diff = futureDate / 1000 - currentDate.getTime() / 1000;

if (diff <= 0) {
  $('#time-countdown').html('<p class="lead text-center"><strong>Celebrate our biggest milestone now!</strong></p>');
} else {
  // Instantiate a countdown FlipClock
  clock = $('#time-countdown').FlipClock(diff, {
    clockFace: 'DailyCounter',
    clockFaceOptions:  {
      showSeconds: true,
      countdown: true,
    },
    onStop: function() {
      $('#time-countdown').addClass('hide').append('<p class="lead text-center"><strong>Celebrate our biggest milestone now!</strong></p>');
    }
  });
}
var clockChild = $('#time-countdown').children();
for(var i = 0; i < clockChild.length; i+=3) {
  clockChild.slice(i, i+3).wrapAll("<div class='flipclock-wrap'></div>");
}

});