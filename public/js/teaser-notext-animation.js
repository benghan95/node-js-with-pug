(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
    }


(lib.wave = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2.1,0.1).curveTo(1.7,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.3,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.6,0.1).curveTo(-4,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5.1,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.3,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.9,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.4,0.5,2.9,0.5).curveTo(2.4,0.5,2.1,0.1).closePath();
    this.shape.setTransform(296.4,0,6.317,6.318);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,-0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2.1,0.1).curveTo(-2.4,0.5,-2.9,0.5).curveTo(-3.4,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.9,0.2).curveTo(-2.5,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-1,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,-0,0.2).curveTo(0.3,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.2,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(4,-0.2,3.6,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.3,0.5,2,0.1).closePath();
    this.shape_1.setTransform(242.7,0,6.317,6.318);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.4,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.4,0.5,2,0.1).closePath();
    this.shape_2.setTransform(188.7,0,6.317,6.318);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.4,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.4,0.5,2,0.1).closePath();
    this.shape_3.setTransform(134.7,0,6.317,6.318);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2.1,0.1).curveTo(1.7,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.3,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.6,0.1).curveTo(-4,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5.1,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.3,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.9,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.4,0.5,2.9,0.5).curveTo(2.4,0.5,2.1,0.1).closePath();
    this.shape_4.setTransform(80.9,0,6.317,6.318);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,-0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2.1,0.1).curveTo(-2.4,0.5,-2.9,0.5).curveTo(-3.4,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.9,0.2).curveTo(-2.5,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-1,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,-0,0.2).curveTo(0.3,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.2,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(4,-0.2,3.6,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.3,0.5,2,0.1).closePath();
    this.shape_5.setTransform(27.2,0,6.317,6.318);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.4,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.4,0.5,2,0.1).closePath();
    this.shape_6.setTransform(-26.8,0,6.317,6.318);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(2,0.1).curveTo(1.8,-0.2,1.4,-0.2).curveTo(1.1,-0.2,0.8,0.1).curveTo(0.5,0.5,0,0.5).curveTo(-0.5,0.5,-0.8,0.1).curveTo(-1,-0.2,-1.4,-0.2).curveTo(-1.8,-0.2,-2,0.1).curveTo(-2.4,0.5,-2.8,0.5).curveTo(-3.3,0.5,-3.7,0.1).curveTo(-3.9,-0.2,-4.3,-0.2).curveTo(-4.6,-0.2,-4.9,0.1).curveTo(-4.9,0.2,-4.9,0.2).curveTo(-4.9,0.2,-5,0.2).curveTo(-5,0.2,-5,0.2).curveTo(-5,0.2,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0.1).curveTo(-5.1,0.1,-5.1,0).curveTo(-5.1,0,-5.1,-0).curveTo(-5.1,-0.1,-5.1,-0.1).curveTo(-4.8,-0.5,-4.3,-0.5).curveTo(-3.8,-0.5,-3.5,-0.1).curveTo(-3.2,0.2,-2.8,0.2).curveTo(-2.4,0.2,-2.2,-0.1).curveTo(-1.9,-0.4,-1.4,-0.4).curveTo(-0.9,-0.4,-0.6,-0.1).curveTo(-0.4,0.2,0,0.2).curveTo(0.4,0.2,0.6,-0.1).curveTo(0.9,-0.5,1.4,-0.5).curveTo(1.9,-0.5,2.2,-0.1).curveTo(2.5,0.2,2.8,0.2).curveTo(3.3,0.2,3.5,-0.1).curveTo(3.8,-0.5,4.3,-0.5).curveTo(4.8,-0.5,5.1,-0.1).curveTo(5.1,-0.1,5.1,-0).curveTo(5.1,0,5.1,0).curveTo(5.1,0.1,5.1,0.1).curveTo(5.1,0.1,5.1,0.1).curveTo(5,0.2,5,0.2).curveTo(5,0.2,5,0.2).curveTo(4.9,0.2,4.9,0.2).curveTo(4.9,0.2,4.9,0.1).curveTo(4.6,-0.2,4.3,-0.2).curveTo(3.9,-0.2,3.7,0.1).curveTo(3.3,0.5,2.8,0.5).curveTo(2.4,0.5,2,0.1).closePath();
    this.shape_7.setTransform(-80.8,0,6.317,6.318);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.wave, new cjs.Rectangle(-113.2,-3,442,6), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(22.8,-12.7).curveTo(19,-12.9,15.1,-12).curveTo(11.3,-11.2,7.7,-9.5).curveTo(6.7,-9,6.3,-8.8).curveTo(5.5,-8.5,4.9,-8.5).curveTo(4.2,-8.4,2.6,-8.8).lineTo(-14,-12.9).curveTo(-16.5,-13.5,-18,-13.6).curveTo(-20.2,-13.7,-21.7,-12.8).curveTo(-22.6,-12.3,-23,-11.4).curveTo(-23.5,-10.4,-23,-9.6).curveTo(-22.8,-9.3,-22,-8.7).lineTo(-10.3,-1.3).lineTo(-17.4,2.7).curveTo(-18.1,3.1,-18.6,3.1).curveTo(-19,3.1,-19.7,2.8).curveTo(-22.1,1.3,-23.3,0.6).curveTo(-25.5,-0.5,-27.2,-0.4).curveTo(-28.2,-0.3,-28.9,0.2).curveTo(-29.8,0.8,-29.7,1.6).curveTo(-29.7,2.1,-29.2,2.8).lineTo(-25.9,8.5).curveTo(-28.3,10.5,-27.8,12).curveTo(-27.5,12.8,-26.5,13.2).curveTo(-25.7,13.6,-24.6,13.6).curveTo(-22.5,13.6,-18.8,12.8).curveTo(-9.5,10.8,1.7,7.3).curveTo(11.9,4,18.8,0.8).curveTo(24.3,-1.8,27.3,-4.5).curveTo(28.9,-6,29.4,-7.3).curveTo(29.8,-8.2,29.7,-9).curveTo(29.6,-10,29.1,-10.6).curveTo(28.6,-11.3,27.7,-11.6).curveTo(27.1,-11.9,26,-12.2).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.3,-15.3,67.9,33.3);


(lib.Tween1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(4.6,-2).lineTo(-4.6,2);
    this.shape.setTransform(-42.1,18.8);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.5,-1.1).lineTo(-2.5,1.1);
    this.shape_1.setTransform(-52.7,23.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

    // Layer 1
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(22.8,-12.7).curveTo(19,-12.9,15.1,-12).curveTo(11.3,-11.2,7.7,-9.5).curveTo(6.7,-9,6.3,-8.8).curveTo(5.5,-8.5,4.9,-8.5).curveTo(4.2,-8.4,2.6,-8.8).lineTo(-14,-12.9).curveTo(-16.5,-13.5,-18,-13.6).curveTo(-20.2,-13.7,-21.7,-12.8).curveTo(-22.6,-12.3,-23,-11.4).curveTo(-23.5,-10.4,-23,-9.6).curveTo(-22.8,-9.3,-22,-8.7).lineTo(-10.3,-1.3).lineTo(-17.4,2.7).curveTo(-18.1,3.1,-18.6,3.1).curveTo(-19,3.1,-19.7,2.8).curveTo(-22.1,1.3,-23.3,0.6).curveTo(-25.5,-0.5,-27.2,-0.4).curveTo(-28.2,-0.3,-28.9,0.2).curveTo(-29.8,0.8,-29.7,1.6).curveTo(-29.7,2.1,-29.2,2.8).lineTo(-25.9,8.5).curveTo(-28.3,10.5,-27.8,12).curveTo(-27.5,12.8,-26.5,13.2).curveTo(-25.7,13.6,-24.6,13.6).curveTo(-22.5,13.6,-18.8,12.8).curveTo(-9.5,10.8,1.7,7.3).curveTo(11.9,4,18.8,0.8).curveTo(24.3,-1.8,27.3,-4.5).curveTo(28.9,-6,29.4,-7.3).curveTo(29.8,-8.2,29.7,-9).curveTo(29.6,-10,29.1,-10.6).curveTo(28.6,-11.3,27.7,-11.6).curveTo(27.1,-11.9,26,-12.2).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.2,-15.3,86.9,40.8);


(lib.Symbol3 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(33.5,-40.2).curveTo(31.8,-38.8,28.9,-39.4).curveTo(26.9,-39.6,25.2,-41.9).curveTo(23.4,-44,24.1,-46.6).curveTo(24.4,-48.9,26.6,-50.6).curveTo(28.9,-52,31.6,-51.5).curveTo(34.1,-50.9,35.3,-48.9).curveTo(37,-46.6,36.1,-44.3).curveTo(35.5,-41.9,33.5,-40.2).closePath().moveTo(15.1,-67.6).curveTo(11.3,-58.7,14.8,-47.4).curveTo(16.6,-41.9,19.1,-38.2).curveTo(20.3,-36.5,19.7,-34.5).curveTo(19.4,-32.4,17.7,-31.3).curveTo(13.9,-28.4,10.8,-32.4).curveTo(7.3,-37.6,5.3,-44.8).curveTo(1,-59.2,6.2,-71.3).curveTo(6.8,-73.4,8.8,-73.9).curveTo(10.5,-74.8,12.5,-74.2).curveTo(17.1,-72.2,15.1,-67.6).closePath().moveTo(57.5,-37).lineTo(65.2,-37.3).lineTo(69.3,-43.7).lineTo(71.8,-36.5).lineTo(79.1,-34.7).lineTo(73,-30.1).lineTo(73.6,-22.6).lineTo(67.2,-26.7).lineTo(60.4,-23.8).lineTo(62.4,-31.3).closePath().moveTo(-19.2,-45.4).curveTo(-20.1,-44.5,-22.4,-44.5).curveTo(-24.1,-44.8,-25.2,-46.6).curveTo(-26.1,-48,-25.8,-49.7).curveTo(-25.2,-51.5,-24.1,-52.3).curveTo(-22.1,-53.2,-20.6,-52.9).curveTo(-19.2,-52.9,-18.1,-51.2).curveTo(-17.1,-49.7,-17.4,-48).curveTo(-17.8,-46.3,-19.2,-45.4).closePath().moveTo(-20.1,-11.1).lineTo(-22.4,-10.5).curveTo(-34.8,-9.4,-47.5,-19.5).curveTo(-51.1,-22.6,-48,-26.7).curveTo(-46.5,-28.1,-44.5,-28.4).curveTo(-42.5,-28.4,-41.1,-27.3).curveTo(-31.9,-19.8,-23.2,-20.3).curveTo(-21.2,-20.6,-19.8,-19.2).curveTo(-18.1,-18,-18.1,-16).curveTo(-17.8,-13.1,-20.1,-11.1).closePath().moveTo(-37.6,5.9).curveTo(-37.4,3.9,-35.3,2.4).curveTo(-33.6,1,-31.3,1.6).curveTo(-28.7,2.1,-27.9,3.9).curveTo(-26.4,5.6,-26.9,8.2).curveTo(-27.6,10.2,-29.2,11.6).curveTo(-31.3,12.5,-33.3,12.2).curveTo(-35.3,12.2,-36.8,9.9).curveTo(-38.2,8.2,-37.6,5.9).closePath().moveTo(-42.5,28.4).curveTo(-29.9,27.5,-17.4,33.8).curveTo(-13.4,35.9,-15.4,40.5).curveTo(-15.8,41.9,-17.1,42.2).curveTo(-19.2,43.9,-22.1,42.5).curveTo(-31.9,37.6,-42,38.2).curveTo(-46.5,38.2,-47.1,33.5).curveTo(-47.1,28.4,-42.5,28.4).closePath().moveTo(-68.5,38.2).lineTo(-68.2,43.3).lineTo(-72.5,40.5).lineTo(-77.1,42.2).lineTo(-75.7,37.3).lineTo(-79.1,33.5).lineTo(-74,33.3).lineTo(-71,28.9).lineTo(-69.3,33.8).lineTo(-64.5,35).closePath().moveTo(-56.4,67).curveTo(-55.8,64.7,-53.8,62.9).curveTo(-51.8,61.8,-49.1,62.1).curveTo(-46.9,62.4,-45.1,64.7).curveTo(-43.7,67,-44.2,69.3).curveTo(-44.9,72.2,-46.9,73.3).curveTo(-49.5,75,-51.5,74.2).curveTo(-54.4,73.6,-55.5,71.6).curveTo(-57,69.9,-56.4,67).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-80.1,-75.4,160.2,150.9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(9,-26.2).curveTo(10.5,-25.4,12.2,-25.7).curveTo(13.9,-25.9,14.8,-27.4).curveTo(15.6,-29.4,15.4,-30.6).curveTo(15.4,-32.3,13.6,-33.4).curveTo(12.2,-34.3,10.5,-34).curveTo(9,-33.7,7.6,-32.3).curveTo(6.7,-30.8,7,-29.1).curveTo(7.6,-27.1,9,-26.2).closePath().moveTo(23.7,-16.7).lineTo(23.4,-13).lineTo(26.6,-15).lineTo(29.8,-13.8).lineTo(28.9,-17).lineTo(30.9,-19.9).lineTo(27.5,-19.9).lineTo(25.7,-23.1).lineTo(24.3,-19.6).lineTo(20.8,-18.7).closePath().moveTo(29.5,-1.4).lineTo(30.9,-0.9).curveTo(31.2,-0.9,36.4,-1.4).curveTo(43,-2.6,48.5,-7.2).curveTo(51.1,-9.2,48.8,-12.1).curveTo(46.8,-14.7,43.9,-12.7).curveTo(39.8,-9.2,35.5,-8.1).curveTo(32.1,-7.8,31.8,-7.8).curveTo(28,-7.8,28,-4.6).curveTo(27.5,-2.3,29.5,-1.4).closePath().moveTo(62.6,0.9).curveTo(62,-0.9,60.9,-1.7).curveTo(59.2,-2.9,57.7,-2.3).curveTo(56,-2.3,54.8,-0.6).curveTo(54,0.9,54.3,2.6).curveTo(54.8,4.6,56,5.5).curveTo(57.4,6.3,59.2,6).curveTo(61.2,5.5,62,4.3).curveTo(62.9,2.3,62.6,0.9).closePath().moveTo(46.5,15.3).curveTo(41,15,34.9,17).curveTo(30.3,18.4,29.5,19.3).curveTo(26.3,21,27.7,23.9).lineTo(28.9,25.1).curveTo(30.6,26.2,32.4,25.4).curveTo(32.6,25.4,36.7,23.6).curveTo(41.9,21.9,46.2,22.2).curveTo(49.4,22.8,49.9,19).curveTo(49.9,15.8,46.5,15.3).closePath().moveTo(54.5,38.3).curveTo(54.5,36.9,52.8,35.7).curveTo(51.7,34.9,49.6,34.9).curveTo(47.9,35.4,46.8,36.9).curveTo(45.9,38.3,46.2,40).curveTo(46.5,41.5,47.9,42.9).curveTo(49.9,43.8,51.4,43.5).curveTo(53.4,42.9,54,41.5).curveTo(54.8,40,54.5,38.3).closePath().moveTo(-52.7,-31.7).lineTo(-56.1,-32).lineTo(-58.1,-34.9).lineTo(-59.3,-31.7).lineTo(-62.7,-30.9).lineTo(-59.9,-28.6).lineTo(-60.1,-25.1).lineTo(-57.3,-27.1).lineTo(-53.8,-25.7).lineTo(-55,-29.1).closePath().moveTo(-6.5,-38.6).curveTo(-2.5,-28.5,-9.4,-18.2).curveTo(-10.9,-15.3,-8.6,-13.3).curveTo(-5.7,-11.5,-3.7,-14.1).curveTo(-1.4,-17.9,0.4,-22.8).curveTo(3.3,-32.9,-0.2,-41.2).curveTo(-1.6,-44.4,-4.8,-43.2).curveTo(-8,-41.5,-6.5,-38.6).closePath().moveTo(-24.7,-14.7).curveTo(-23.8,-13.8,-21.5,-13.8).curveTo(-19.5,-14.4,-18.6,-15.9).curveTo(-17.8,-17.3,-18.1,-19).curveTo(-18.4,-20.8,-19.8,-21.6).curveTo(-20.7,-22.5,-23,-22.5).curveTo(-25,-21.9,-25.9,-20.5).curveTo(-26.7,-19,-26.4,-17.3).curveTo(-25.9,-15.6,-24.7,-14.7).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-63.7,-44.5,127.4,89.1), null);


(lib.star = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2.3,-2.3).lineTo(5.6,-0.5).curveTo(6.4,0.3,5.6,0.3).lineTo(2.3,2.1).lineTo(2.1,2.3).lineTo(0.3,5.6).curveTo(0.3,6.4,-0.5,5.6).lineTo(-2,2.3).lineTo(-2.3,2.1).lineTo(-5.6,0.3).curveTo(-6.4,0.3,-5.6,-0.5).lineTo(-2.3,-2.3).lineTo(-2,-2.3).lineTo(-0.5,-5.6).curveTo(0.3,-6.4,0.3,-5.6).lineTo(2.1,-2.3).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.star, new cjs.Rectangle(-7,-7,14,14), null);


(lib.o = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-4.8).curveTo(1.9,-4.8,3.4,-3.4).curveTo(4.8,-1.9,4.8,0).curveTo(4.8,1.9,3.4,3.4).curveTo(1.9,4.8,0,4.8).curveTo(-1.9,4.8,-3.4,3.4).curveTo(-4.8,1.9,-4.8,0).curveTo(-4.8,-1.9,-3.4,-3.4).curveTo(-1.9,-4.8,0,-4.8).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.o, new cjs.Rectangle(-5.7,-5.7,11.5,11.5), null);


(lib.mountains = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7.8,13.6).lineTo(-1.9,9.6).lineTo(3.6,13.6).moveTo(-20.1,2.2).lineTo(-7.3,-6.8).lineTo(11.4,6.5).moveTo(-20.1,2.2).lineTo(-14.2,6).lineTo(-6.8,0.9).lineTo(-3.8,2.5).lineTo(1.2,-0.4).moveTo(3.9,13.6).lineTo(19.3,2.3).lineTo(35.1,13.6).moveTo(113.8,13).lineTo(93.7,-1.7).lineTo(72.9,13).moveTo(145.1,-1.9).lineTo(137.4,3.1).lineTo(127.7,-3.6).lineTo(123.8,-1.5).lineTo(117.3,-5.2).moveTo(129,13).lineTo(121.4,7.9).lineTo(114.2,13).moveTo(145.1,-1.9).lineTo(128.3,-13.6).lineTo(104,3.8).moveTo(166.5,13).lineTo(145.1,-1.9).moveTo(196.1,12.9).lineTo(172.7,-4.9).lineTo(157.8,6.5).moveTo(-196.1,12.9).lineTo(-172.7,-4.9).lineTo(-157.8,6.5).moveTo(-166.5,13).lineTo(-145.1,-1.9).lineTo(-137.4,3.1).lineTo(-127.7,-3.6).lineTo(-123.8,-1.5).lineTo(-117.3,-5.2).moveTo(-129,13).lineTo(-121.4,7.9).lineTo(-114.2,13).moveTo(-145.1,-1.9).lineTo(-128.3,-13.6).lineTo(-104,3.8).moveTo(-113.8,13).lineTo(-93.7,-1.7).lineTo(-72.9,13).moveTo(-59,13.5).lineTo(-41.1,-0.1).lineTo(-29.9,8.6).moveTo(-36.5,13.6).lineTo(-20.1,2.2);
    this.shape.setTransform(21,-16.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mountains, new cjs.Rectangle(-176.1,-31.1,394.2,29.1), null);


(lib.hand2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(0.9,-66.5).lineTo(0.9,-75.6).curveTo(0.9,-79.3,-2,-82).curveTo(-5,-84.6,-9.2,-84.6).curveTo(-13.4,-84.6,-16.3,-82).curveTo(-19.2,-79.3,-19.2,-75.6).lineTo(-19.3,-66.5).curveTo(-19.4,-70.1,-22.2,-72.7).curveTo(-25.1,-75.3,-29.3,-75.3).curveTo(-33.5,-75.3,-36.4,-72.7).curveTo(-39.4,-70,-39.4,-66.3).lineTo(-39.4,-43.9).curveTo(-39.5,-46.6,-41.1,-48.7).curveTo(-41.6,-49.4,-42.3,-50.1).curveTo(-45.3,-52.7,-49.5,-52.7).curveTo(-53.6,-52.7,-56.6,-50.1).curveTo(-57.3,-49.4,-57.9,-48.7).curveTo(-59.5,-46.5,-59.5,-43.7).lineTo(-59.5,41.2).curveTo(-59.5,45.7,-56.6,57.1).curveTo(-54.5,65.8,-46.5,70.8).lineTo(-46.5,85.6).moveTo(16.4,85.6).lineTo(16.4,70.3).curveTo(27.7,62.1,57.1,13.2).curveTo(59.7,10.6,59.5,6.6).curveTo(59.3,2.7,56.4,-0.2).curveTo(53.4,-3.2,49.5,-3.4).curveTo(45.5,-3.6,42.9,-1).lineTo(20.6,21.3).curveTo(21,20,21,18.6).lineTo(21,-66.3).curveTo(21,-70,18.1,-72.7).curveTo(15.1,-75.3,11,-75.3).curveTo(6.8,-75.3,3.9,-72.7).curveTo(1,-70.1,0.9,-66.5).moveTo(0.9,-7.5).lineTo(0.9,-66.5).moveTo(-39.4,-43.9).lineTo(-39.4,0).moveTo(-19.3,-66.5).lineTo(-19.2,-5);
    this.shape.setTransform(0,-0.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hand2, new cjs.Rectangle(-64.6,-89.8,125.7,176), null);


(lib.hand1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(39.4,-43.9).curveTo(39.5,-46.5,41.1,-48.7).curveTo(41.6,-49.4,42.3,-50.1).curveTo(45.3,-52.7,49.5,-52.7).curveTo(53.6,-52.7,56.6,-50.1).curveTo(57.3,-49.4,57.9,-48.7).curveTo(59.5,-46.5,59.5,-43.7).lineTo(59.5,41.1).curveTo(59.5,45.6,56.6,57.1).curveTo(54.5,65.8,46.5,70.8).lineTo(46.5,85.6).moveTo(-0.9,-66.5).lineTo(-0.9,-75.5).curveTo(-0.9,-79.3,2,-82).curveTo(5,-84.6,9.2,-84.6).curveTo(13.4,-84.6,16.3,-82).curveTo(19.2,-79.3,19.2,-75.5).lineTo(19.3,-66.5).curveTo(19.4,-70.2,22.2,-72.7).curveTo(25.1,-75.3,29.3,-75.3).curveTo(33.5,-75.3,36.4,-72.7).curveTo(39.4,-70,39.4,-66.3).lineTo(39.4,-43.9).lineTo(39.4,0).moveTo(19.3,-66.5).lineTo(19.2,-5).moveTo(-16.4,85.6).lineTo(-16.4,70.3).curveTo(-27.7,62.1,-57.1,13.2).curveTo(-59.7,10.6,-59.5,6.7).curveTo(-59.3,2.7,-56.4,-0.2).curveTo(-53.4,-3.2,-49.5,-3.4).curveTo(-45.5,-3.7,-42.9,-1).lineTo(-20.6,21.3).curveTo(-21,20,-21,18.6).lineTo(-21,-66.3).curveTo(-21,-70,-18.1,-72.7).curveTo(-15.1,-75.3,-11,-75.3).curveTo(-6.8,-75.3,-3.9,-72.7).curveTo(-1,-70.1,-0.9,-66.5).moveTo(-0.9,-7.6).lineTo(-0.9,-66.5);
    this.shape.setTransform(0,-0.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#EC2C2C").beginStroke().moveTo(-16.4,85.1).lineTo(-16.4,69.8).curveTo(-27.7,61.7,-57.1,12.8).curveTo(-59.7,10.1,-59.5,6.1).curveTo(-59.3,2.2,-56.4,-0.7).curveTo(-53.4,-3.7,-49.5,-3.9).curveTo(-45.5,-4.2,-42.9,-1.5).lineTo(-20.6,20.8).curveTo(-21,19.5,-21,18.1).lineTo(-21,-66.8).curveTo(-21,-70.5,-18.1,-73.2).curveTo(-15.1,-75.8,-11,-75.8).curveTo(-6.8,-75.8,-3.9,-73.2).curveTo(-1,-70.6,-0.9,-67).lineTo(-0.9,-8).lineTo(-0.9,-67).lineTo(-0.9,-76.1).curveTo(-0.9,-79.8,2,-82.4).curveTo(5,-85.1,9.2,-85.1).curveTo(13.4,-85.1,16.3,-82.4).curveTo(19.2,-79.8,19.2,-76.1).lineTo(19.3,-67).curveTo(19.4,-70.6,22.2,-73.2).curveTo(25.1,-75.8,29.3,-75.8).curveTo(33.5,-75.8,36.4,-73.2).curveTo(39.4,-70.5,39.4,-66.8).lineTo(39.4,-44.4).lineTo(39.4,-0.5).lineTo(39.4,-44.4).curveTo(39.5,-47,41.1,-49.2).lineTo(42.3,-50.6).curveTo(45.3,-53.2,49.5,-53.2).curveTo(53.6,-53.2,56.6,-50.6).lineTo(57.9,-49.2).curveTo(59.5,-47,59.5,-44.2).lineTo(59.5,40.7).curveTo(59.5,45.1,56.6,56.6).curveTo(54.5,65.2,46.5,70.3).lineTo(46.5,85.1).closePath().moveTo(19.2,-5.5).lineTo(19.3,-67).closePath();

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.hand1, new cjs.Rectangle(-60.5,-89.8,125.2,176), null);


(lib.graSteam = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Isolation Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape.setTransform(13.5,51.2);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_1.setTransform(13.5,104.7);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_2.setTransform(-13,50.8);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_3.setTransform(-13,104.3);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_4.setTransform(13.2,-55.6);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_5.setTransform(13.2,-2.1);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_6.setTransform(-13.3,-56);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.8,32.2).lineTo(-0.9,32.1).curveTo(-1.2,31.5,-1.1,31.1).curveTo(-1,30.6,-0.6,30.4).curveTo(0.4,28.8,0.4,26.9).curveTo(0.4,24.9,-0.6,23.4).curveTo(-2.5,21,-2.5,17.9).curveTo(-2.5,15,-0.6,12.7).curveTo(0.4,11,0.4,9.1).curveTo(0.4,7,-0.6,5.3).curveTo(-2.5,2.9,-2.5,0.1).curveTo(-2.5,-3.3,-0.6,-5.2).curveTo(0.4,-6.7,0.4,-9).curveTo(0.4,-11,-0.6,-12.5).curveTo(-2.5,-14.9,-2.5,-17.8).curveTo(-2.5,-20.8,-0.6,-23.2).curveTo(0.4,-24.7,0.4,-26.8).curveTo(0.4,-28.6,-0.6,-30.2).curveTo(-1.2,-30.6,-1.2,-31).curveTo(-1.4,-31.5,-0.8,-32).curveTo(-0.4,-32.8,0,-32.7).curveTo(0.5,-32.8,0.9,-32).lineTo(0.9,-31.9).curveTo(2.5,-30,2.5,-26.8).curveTo(2.5,-23.2,0.9,-21.2).lineTo(0.8,-21.2).curveTo(-0.4,-20,-0.4,-17.8).curveTo(-0.4,-15.7,0.9,-14.2).curveTo(2.5,-12.1,2.5,-8.7).curveTo(2.5,-5.6,0.9,-3.5).lineTo(0.8,-3.4).curveTo(-0.4,-2.2,-0.4,0.1).curveTo(-0.4,2.3,0.8,3.6).lineTo(0.9,3.6).curveTo(2.5,5.5,2.5,9.1).curveTo(2.5,12.5,0.9,14.4).lineTo(0.9,14.4).curveTo(-0.4,16.1,-0.4,17.9).curveTo(-0.4,20.1,0.8,21.4).lineTo(0.9,21.4).curveTo(2.5,23.3,2.5,26.9).curveTo(2.5,30.2,0.9,32.1).curveTo(0.7,32.8,0.1,32.7).lineTo(0.1,32.7).curveTo(-0.3,32.7,-0.8,32.2).closePath();
    this.shape_7.setTransform(-13.3,-2.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graSteam, new cjs.Rectangle(-15.8,-88.6,31.8,226.1), null);


(lib.graIcon2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(4,1,1).moveTo(37.5,105).lineTo(56.8,-92.2).curveTo(57.5,-96.8,54.6,-100.6).curveTo(51.8,-104.2,47.2,-104.9).curveTo(42.6,-105.5,38.9,-102.7).curveTo(35.2,-100,34.5,-95.3).lineTo(22.8,-14.6).moveTo(-23.1,-14.9).curveTo(-21.3,-17.9,-18.7,-20.5).curveTo(-11.1,-28.1,-0.2,-28.1).curveTo(10.7,-28.1,18.4,-20.5).curveTo(21.1,-17.7,22.8,-14.6).curveTo(26,-8.9,26,-1.9).curveTo(26,9,18.4,16.7).curveTo(10.7,24.4,-0.2,24.4).curveTo(-11.1,24.4,-18.7,16.7).curveTo(-26.5,9,-26.5,-1.9).curveTo(-26.5,-9,-23.2,-14.7).curveTo(-23.1,-14.8,-23.1,-14.9).closePath().moveTo(-37.5,105).lineTo(-56.8,-92.2).curveTo(-57.4,-96.8,-54.6,-100.6).curveTo(-51.8,-104.2,-47.2,-104.9).curveTo(-42.6,-105.5,-38.9,-102.7).curveTo(-35.2,-100,-34.5,-95.3).lineTo(-23.1,-14.9);
    this.shape.setTransform(0,-45.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#EC2C2C").beginStroke().moveTo(-37.5,105).lineTo(-56.8,-92.2).curveTo(-57.4,-96.8,-54.6,-100.6).curveTo(-51.8,-104.2,-47.2,-104.9).curveTo(-42.6,-105.5,-38.9,-102.7).curveTo(-35.2,-100,-34.5,-95.3).lineTo(-23.1,-14.9).lineTo(-23.2,-14.7).curveTo(-26.5,-9,-26.5,-1.9).curveTo(-26.5,9,-18.7,16.7).curveTo(-11.1,24.4,-0.2,24.4).curveTo(10.7,24.4,18.4,16.7).curveTo(26,9,26,-1.9).curveTo(26,-8.9,22.8,-14.6).lineTo(34.5,-95.3).curveTo(35.2,-100,38.9,-102.7).curveTo(42.6,-105.5,47.2,-104.9).curveTo(51.8,-104.2,54.6,-100.6).curveTo(57.5,-96.8,56.8,-92.2).lineTo(37.5,105).closePath().moveTo(-18.7,16.7).curveTo(-26.5,9,-26.5,-1.9).curveTo(-26.5,-9,-23.2,-14.7).lineTo(-23.1,-14.9).curveTo(-21.3,-17.9,-18.7,-20.5).curveTo(-11.1,-28.1,-0.2,-28.1).curveTo(10.7,-28.1,18.4,-20.5).curveTo(21.1,-17.7,22.8,-14.6).curveTo(26,-8.9,26,-1.9).curveTo(26,9,18.4,16.7).curveTo(10.7,24.4,-0.2,24.4).curveTo(-11.1,24.4,-18.7,16.7).closePath().moveTo(22.8,-14.6).lineTo(22.8,-14.6).closePath();
    this.shape_1.setTransform(0,-45.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graIcon2, new cjs.Rectangle(-58.9,-152.5,117.8,214.1), null);


(lib.graIcon1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(1.8,1,1).moveTo(15.6,8.7).lineTo(44.5,18.5).lineTo(38,-5.6).curveTo(37,-9.1,38.9,-12.3).curveTo(40.5,-15.2,43.8,-16.2).curveTo(44,-16.3,44.1,-16.4).curveTo(47.6,-17.2,50.8,-15.5).curveTo(51.9,-14.8,52.7,-14.1).curveTo(54.2,-12.4,54.9,-10.2).lineTo(65.6,29.8).curveTo(66.5,33.2,64.7,36.4).curveTo(62.9,39.5,59.4,40.5).curveTo(57,41.1,54.8,40.5).lineTo(54.8,40.5).lineTo(30.2,33.4).lineTo(44,80.2).moveTo(6.3,-27.6).curveTo(9.7,-25.9,12.4,-23.1).curveTo(19.8,-15.8,19.8,-5.4).curveTo(19.8,2.5,15.6,8.7).curveTo(14.2,10.6,12.4,12.3).curveTo(5.2,19.6,-5.2,19.6).curveTo(-15.6,19.6,-22.9,12.3).curveTo(-23.5,11.7,-24,11.2).curveTo(-30.2,4.2,-30.2,-5.4).curveTo(-30.2,-11.3,-27.8,-16.3).curveTo(-26,-19.9,-22.9,-23.1).curveTo(-15.6,-30.4,-5.2,-30.4).curveTo(1.1,-30.4,6.3,-27.6).closePath().moveTo(43.8,-16.2).lineTo(53.2,-41.1).lineTo(73,-25.4).lineTo(52.7,-14.1).moveTo(-30.8,-68.7).curveTo(-31.1,-68.7,-31.5,-68.7).curveTo(-34.5,-68.7,-36.6,-70.8).curveTo(-38.8,-72.9,-38.8,-75.9).curveTo(-38.8,-78.9,-36.6,-81).curveTo(-34.5,-83.2,-31.5,-83.2).curveTo(-28.5,-83.2,-26.4,-81).curveTo(-24.2,-78.9,-24.2,-75.9).curveTo(-24.2,-72.9,-26.4,-70.8).curveTo(-26.8,-70.5,-27.1,-70.2).curveTo(-28.7,-68.8,-30.8,-68.7).lineTo(-29.1,-39.2).lineTo(-28.2,-23.1).lineTo(-27.8,-16.3).moveTo(-27.1,-70.2).lineTo(-16.2,-56.4).lineTo(-29.1,-39.2).moveTo(-65.4,27.2).curveTo(-68.3,26.9,-70.5,24.6).curveTo(-73,22.1,-73,18.5).lineTo(-73,-22.9).curveTo(-73,-26.5,-70.5,-29.1).curveTo(-67.9,-31.6,-64.2,-31.6).curveTo(-60.6,-31.6,-58.1,-29.1).curveTo(-55.5,-26.5,-55.5,-22.9).lineTo(-55.5,11.2).lineTo(-24,11.2).moveTo(-8.8,-47).lineTo(-28.2,-23.1).moveTo(-16.2,-56.4).lineTo(-8.8,-47).lineTo(6.3,-27.6).moveTo(-65.4,27.2).lineTo(-63,27.1).curveTo(-63.6,27.2,-64.2,27.2).curveTo(-64.8,27.2,-65.4,27.2).closePath().moveTo(-63,27.1).lineTo(-18.5,27.1).lineTo(-11.2,83.2);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#EF3224").beginStroke().moveTo(-1.2,-0).lineTo(1.2,-0).lineTo(-0,0.1).lineTo(-1.2,-0).closePath();
    this.shape_1.setTransform(-64.2,27.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graIcon1, new cjs.Rectangle(-74,-84.1,148,168.3), null);


(lib.graGPS = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(18.7,-7.2).curveTo(18.7,-4.4,18,-1.9).curveTo(16.4,3.2,14.3,7.6).curveTo(12,12.5,9.2,16.6).curveTo(5.6,21.9,1.1,25.9).curveTo(-2.2,23.2,-5,20.4).curveTo(-15.2,10.3,-17.9,-1.8).curveTo(-18.7,-4.4,-18.7,-7.2).curveTo(-18.7,-14.9,-13.2,-20.4).curveTo(-7.8,-25.9,0,-25.9).curveTo(7.8,-25.9,13.2,-20.4).curveTo(18.7,-14.9,18.7,-7.2).closePath().moveTo(9.4,-7).curveTo(9.4,-3.1,6.7,-0.4).curveTo(3.9,2.4,0,2.4).curveTo(-3.9,2.4,-6.7,-0.4).curveTo(-9.4,-3.1,-9.4,-7).curveTo(-9.4,-10.9,-6.7,-13.7).curveTo(-3.9,-16.4,0,-16.4).curveTo(3.9,-16.4,6.7,-13.7).curveTo(9.4,-10.9,9.4,-7).closePath();
    this.shape.setTransform(0,-25.9);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#EC2C2C").beginStroke().moveTo(-5,20.4).curveTo(-15.2,10.3,-17.9,-1.8).curveTo(-18.7,-4.4,-18.7,-7.2).curveTo(-18.7,-14.9,-13.2,-20.4).curveTo(-7.8,-25.9,0,-25.9).curveTo(7.8,-25.9,13.2,-20.4).curveTo(18.7,-14.9,18.7,-7.2).curveTo(18.7,-4.4,18,-1.9).curveTo(16.4,3.2,14.3,7.6).curveTo(12,12.5,9.2,16.6).curveTo(5.6,21.9,1.2,25.9).curveTo(-2.3,23.2,-5,20.4).closePath().moveTo(-9.4,-7).curveTo(-9.4,-3.1,-6.7,-0.4).curveTo(-3.9,2.4,0,2.4).curveTo(3.9,2.4,6.7,-0.4).curveTo(9.4,-3.1,9.4,-7).curveTo(9.4,-10.9,6.7,-13.7).curveTo(3.9,-16.4,0,-16.4).curveTo(-3.9,-16.4,-6.7,-13.7).curveTo(-9.4,-10.9,-9.4,-7).lineTo(-9.4,-7).closePath().moveTo(-6.7,-0.4).curveTo(-9.4,-3.1,-9.4,-7).curveTo(-9.4,-10.9,-6.7,-13.7).curveTo(-3.9,-16.4,0,-16.4).curveTo(3.9,-16.4,6.7,-13.7).curveTo(9.4,-10.9,9.4,-7).curveTo(9.4,-3.1,6.7,-0.4).curveTo(3.9,2.4,0,2.4).curveTo(-3.9,2.4,-6.7,-0.4).closePath().moveTo(9.4,-7).lineTo(9.4,-7).closePath();
    this.shape_1.setTransform(0,-25.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graGPS, new cjs.Rectangle(-19.7,-52.7,39.4,53.8), null);


(lib.aniMilestone = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // main
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.2,1.1).lineTo(-0.2,0.2).lineTo(-1.1,0.2).curveTo(-1.1,0.2,-1.2,0.2).curveTo(-1.2,0.2,-1.2,0.1).curveTo(-1.2,0.1,-1.3,0.1).curveTo(-1.3,0.1,-1.3,0).curveTo(-1.3,-0,-1.3,-0.1).curveTo(-1.2,-0.1,-1.2,-0.1).curveTo(-1.2,-0.2,-1.2,-0.2).curveTo(-1.1,-0.2,-1.1,-0.2).lineTo(-0.2,-0.2).lineTo(-0.2,-1.1).curveTo(-0.2,-1.1,-0.2,-1.2).curveTo(-0.1,-1.2,-0.1,-1.2).curveTo(-0.1,-1.2,-0.1,-1.3).curveTo(-0,-1.3,0,-1.3).curveTo(0.1,-1.3,0.1,-1.3).curveTo(0.1,-1.2,0.1,-1.2).curveTo(0.2,-1.2,0.2,-1.2).curveTo(0.2,-1.1,0.2,-1.1).lineTo(0.2,-0.2).lineTo(1.1,-0.2).curveTo(1.1,-0.2,1.2,-0.2).curveTo(1.2,-0.2,1.2,-0.1).curveTo(1.3,-0.1,1.3,-0.1).curveTo(1.3,-0,1.3,0).curveTo(1.3,0.1,1.3,0.1).curveTo(1.3,0.1,1.2,0.1).curveTo(1.2,0.2,1.2,0.2).curveTo(1.1,0.2,1.1,0.2).lineTo(0.2,0.2).lineTo(0.2,1.1).curveTo(0.2,1.1,0.2,1.2).curveTo(0.2,1.2,0.1,1.2).curveTo(0.1,1.2,0.1,1.3).curveTo(0.1,1.3,0,1.3).curveTo(-0,1.3,-0.1,1.3).curveTo(-0.1,1.2,-0.1,1.2).curveTo(-0.1,1.2,-0.2,1.2).curveTo(-0.2,1.1,-0.2,1.1).closePath();
    this.shape.setTransform(21.4,-50.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_1.setTransform(150.4,-50.5);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_2.setTransform(150.4,-37);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.4).curveTo(-1.9,0.8,-1.9,0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,0).curveTo(1.9,0.8,1.3,1.4).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_3.setTransform(150.4,-23.6);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_4.setTransform(150.4,-10.1);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_5.setTransform(150.4,3.4);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.4).curveTo(-1.9,0.8,-1.9,0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,0).curveTo(1.9,0.8,1.3,1.4).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_6.setTransform(150.4,16.8);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_7.setTransform(150.4,30.3);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_8.setTransform(150.4,43.7);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_9.setTransform(150.4,57.2);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_10.setTransform(-126.5,-50.6);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_11.setTransform(-126.5,-37.1);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.4).curveTo(-1.9,0.8,-1.9,0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,0).curveTo(1.9,0.8,1.3,1.4).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_12.setTransform(-126.5,-23.7);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.4).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.4).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_13.setTransform(-126.5,-10.2);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_14.setTransform(-126.5,3.2);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_15.setTransform(-126.5,16.7);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.4).curveTo(-1.9,0.8,-1.9,0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,0).curveTo(1.9,0.8,1.3,1.4).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_16.setTransform(-126.5,30.2);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_17.setTransform(-126.5,43.6);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0,1.9).curveTo(-0.8,1.9,-1.3,1.3).curveTo(-1.9,0.8,-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.4).curveTo(-0.8,-1.9,-0,-1.9).lineTo(0,-1.9).curveTo(0.8,-1.9,1.3,-1.4).curveTo(1.9,-0.8,1.9,-0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).closePath();
    this.shape_18.setTransform(-126.5,57.1);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_19.setTransform(138.3,-61.1);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_20.setTransform(124.8,-61.1);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_21.setTransform(111.3,-61.1);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_22.setTransform(97.9,-61.1);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_23.setTransform(84.4,-61.1);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_24.setTransform(70.9,-61.1);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_25.setTransform(57.5,-61.1);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_26.setTransform(44,-61.1);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_27.setTransform(30.5,-61.1);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_28.setTransform(17.1,-61.1);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_29.setTransform(3.6,-61.1);

    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_30.setTransform(-9.8,-61.1);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_31.setTransform(-23.3,-61.1);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_32.setTransform(-36.8,-61.1);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_33.setTransform(-50.2,-61.1);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_34.setTransform(-63.7,-61.1);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_35.setTransform(-77.2,-61.1);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_36.setTransform(-90.6,-61.1);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_37.setTransform(-104.1,-61.1);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_38.setTransform(-117.6,-61.1);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_39.setTransform(139,68.9);

    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_40.setTransform(125.5,68.9);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_41.setTransform(112,68.9);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_42.setTransform(98.6,68.9);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_43.setTransform(85.1,68.9);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_44.setTransform(71.6,68.9);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_45.setTransform(58.2,68.9);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_46.setTransform(44.7,68.9);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_47.setTransform(31.2,68.9);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_48.setTransform(17.8,68.9);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_49.setTransform(4.3,68.9);

    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_50.setTransform(-9.2,68.9);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_51.setTransform(-22.6,68.9);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_52.setTransform(-36.1,68.9);

    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_53.setTransform(-49.6,68.9);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_54.setTransform(-63,68.9);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_55.setTransform(-76.5,68.9);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_56.setTransform(-90,68.9);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.4,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.4,-1.3).curveTo(-0.8,-1.9,-0,-1.9).curveTo(0.8,-1.9,1.3,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.3,1.3).curveTo(0.8,1.9,-0,1.9).curveTo(-0.8,1.9,-1.4,1.3).closePath();
    this.shape_57.setTransform(-103.4,68.9);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.3,1.3).curveTo(-1.9,0.8,-1.9,0).lineTo(-1.9,-0).curveTo(-1.9,-0.8,-1.3,-1.3).curveTo(-0.8,-1.9,0,-1.9).curveTo(0.8,-1.9,1.4,-1.3).curveTo(1.9,-0.8,1.9,-0).lineTo(1.9,0).curveTo(1.9,0.8,1.4,1.3).curveTo(0.8,1.9,0,1.9).curveTo(-0.8,1.9,-1.3,1.3).closePath();
    this.shape_58.setTransform(-116.9,68.9);

    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_59.setTransform(118.8,0.8);

    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_60.setTransform(94.2,0.8);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_61.setTransform(69.6,0.8);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-3.5,4.4).curveTo(-3.5,3.5,-2.5,3.3).curveTo(0.2,2.6,0.1,0.1).curveTo(-0.9,-0.2,-1.6,-0.9).curveTo(-2.3,-1.7,-2.3,-2.7).lineTo(-2.3,-2.8).curveTo(-2.3,-4,-1.5,-4.7).curveTo(-0.7,-5.5,0.5,-5.5).curveTo(1.9,-5.5,2.7,-4.5).curveTo(3.5,-3.5,3.5,-1.8).lineTo(3.5,-1.6).curveTo(3.5,1.9,1.5,3.9).curveTo(-0.1,5.5,-2.2,5.5).curveTo(-3.5,5.5,-3.5,4.4).closePath();
    this.shape_62.setTransform(51.8,12.6);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_63.setTransform(35.3,0.8);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_64.setTransform(10.7,0.8);

    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_65.setTransform(-13.9,0.8);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-3.5,4.4).curveTo(-3.5,3.5,-2.5,3.3).curveTo(0.2,2.6,0.1,0.1).curveTo(-0.9,-0.2,-1.6,-0.9).curveTo(-2.3,-1.7,-2.3,-2.7).lineTo(-2.3,-2.8).curveTo(-2.3,-4,-1.5,-4.7).curveTo(-0.7,-5.5,0.5,-5.5).curveTo(1.9,-5.5,2.7,-4.5).curveTo(3.5,-3.5,3.5,-1.8).lineTo(3.5,-1.6).curveTo(3.5,1.9,1.5,3.9).curveTo(-0.1,5.5,-2.2,5.5).curveTo(-3.5,5.5,-3.5,4.4).closePath();
    this.shape_66.setTransform(-31.7,12.6);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_67.setTransform(-48.1,0.8);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,8.7).curveTo(-10.5,5.3,-10.5,0.1).lineTo(-10.5,0).curveTo(-10.5,-5.2,-7.6,-8.7).curveTo(-4.6,-12.2,0,-12.2).curveTo(4.6,-12.2,7.6,-8.7).curveTo(10.5,-5.3,10.5,-0.1).lineTo(10.5,0).curveTo(10.5,5.2,7.6,8.7).curveTo(4.6,12.2,-0,12.2).curveTo(-4.7,12.2,-7.6,8.7).closePath().moveTo(-3.8,-5.4).curveTo(-5.2,-3.3,-5.2,-0.1).lineTo(-5.2,0).curveTo(-5.2,3.3,-3.8,5.4).curveTo(-2.3,7.5,0,7.5).curveTo(2.3,7.5,3.8,5.4).curveTo(5.2,3.3,5.2,0.1).lineTo(5.2,0).curveTo(5.2,-3.2,3.7,-5.4).curveTo(2.3,-7.5,-0,-7.5).curveTo(-2.3,-7.5,-3.8,-5.4).closePath();
    this.shape_68.setTransform(-72.7,0.8);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,9.8).curveTo(-8.6,9.1,-8.6,7.8).curveTo(-8.6,6.8,-7.9,6.1).curveTo(-7.2,5.3,-6.2,5.3).curveTo(-5.4,5.3,-4.8,5.8).curveTo(-2.6,7.3,-0.3,7.3).curveTo(1.4,7.3,2.5,6.5).curveTo(3.5,5.6,3.5,4).lineTo(3.5,4).curveTo(3.5,2.5,2.4,1.6).curveTo(1.2,0.8,-0.6,0.8).curveTo(-1.8,0.8,-2.9,1.1).lineTo(-4.2,1.4).curveTo(-5.1,1.4,-6.6,0.3).curveTo(-7.6,-0.4,-7.5,-1.8).lineTo(-7,-9.8).curveTo(-7,-10.7,-6.3,-11.3).curveTo(-5.7,-12,-4.8,-12).lineTo(5.5,-12).curveTo(6.4,-12,7.1,-11.3).curveTo(7.8,-10.6,7.8,-9.7).curveTo(7.8,-8.8,7.1,-8.1).curveTo(6.4,-7.4,5.5,-7.4).lineTo(-2.5,-7.4).lineTo(-2.7,-3.4).curveTo(-1.1,-3.8,0.4,-3.8).curveTo(4.1,-3.8,6.3,-2).curveTo(8.6,-0,8.6,3.8).lineTo(8.6,3.9).curveTo(8.6,7.6,6.2,9.8).curveTo(3.8,12,-0.2,12).curveTo(-4.5,12,-7.6,9.8).closePath();
    this.shape_69.setTransform(-95.7,1);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(127.5,57.7).lineTo(-127.5,57.7).curveTo(-129.1,57.7,-130.3,56.5).curveTo(-131.4,55.3,-131.4,53.7).lineTo(-131.4,-53.7).curveTo(-131.4,-55.3,-130.3,-56.5).curveTo(-129.1,-57.7,-127.5,-57.7).lineTo(127.5,-57.7).curveTo(129.1,-57.7,130.3,-56.5).curveTo(131.4,-55.3,131.4,-53.7).lineTo(131.4,53.7).curveTo(131.4,55.3,130.3,56.5).curveTo(129.1,57.7,127.5,57.7).closePath().moveTo(123.9,53.5).lineTo(-124,53.5).curveTo(-125.6,53.5,-126.7,52.3).curveTo(-127.9,51.2,-127.9,49.6).lineTo(-127.9,-49.6).curveTo(-127.9,-51.2,-126.7,-52.3).curveTo(-125.6,-53.5,-124,-53.5).lineTo(123.9,-53.5).curveTo(125.5,-53.5,126.7,-52.3).curveTo(127.9,-51.2,127.9,-49.6).lineTo(127.9,49.6).curveTo(127.9,51.2,126.7,52.3).curveTo(125.5,53.5,123.9,53.5).closePath();
    this.shape_70.setTransform(11.4,3.4);

    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(141.1,73.6).lineTo(-141.1,73.6).curveTo(-143.4,73.6,-145.1,71.9).curveTo(-146.7,70.3,-146.7,68).lineTo(-146.7,-68).curveTo(-146.7,-70.3,-145.1,-71.9).curveTo(-143.4,-73.6,-141.1,-73.6).lineTo(141.1,-73.6).curveTo(143.4,-73.6,145.1,-71.9).curveTo(146.7,-70.3,146.7,-68).lineTo(146.7,68).curveTo(146.7,70.3,145.1,71.9).curveTo(143.4,73.6,141.1,73.6).closePath();
    this.shape_71.setTransform(11.4,3.4);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.beginFill("#EC2C2C").beginStroke().moveTo(-141.1,73.6).curveTo(-143.4,73.6,-145.1,71.9).curveTo(-146.7,70.3,-146.7,68).lineTo(-146.7,-68).curveTo(-146.7,-70.3,-145.1,-71.9).curveTo(-143.4,-73.6,-141.1,-73.6).lineTo(141.1,-73.6).curveTo(143.4,-73.6,145.1,-71.9).curveTo(146.7,-70.3,146.7,-68).lineTo(146.7,68).curveTo(146.7,70.3,145.1,71.9).curveTo(143.5,73.6,141.1,73.6).closePath();
    this.shape_72.setTransform(11.4,3.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[]},23).wait(3));

    // lines
    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(122.2,-62.6).lineTo(127.6,-66.1).moveTo(134.7,-47.5).lineTo(136.9,-48.6).moveTo(129.6,-45.2).lineTo(132.5,-46.6).moveTo(118.7,-40.4).lineTo(126.7,-43.9).moveTo(123.3,-23).lineTo(128.1,-24.6).moveTo(129.2,-25).lineTo(132.7,-26.1).moveTo(110.9,-55.3).lineTo(118.3,-60.1).moveTo(123.4,0.1).lineTo(134.3,0.8).moveTo(127.8,19.3).lineTo(130.7,19.8).moveTo(122,18.5).lineTo(127.4,19.3).moveTo(118,35.8).lineTo(136.6,44).moveTo(122,62.4).lineTo(127.3,65.9).moveTo(114.3,57.4).lineTo(120.5,61.4).moveTo(-123.3,-63.3).lineTo(-128.1,-66.4).moveTo(-110.7,-55.1).lineTo(-121.5,-62.1).moveTo(-122.9,-1.2).lineTo(-129.5,-1.1).moveTo(-123.3,-22.9).lineTo(-128.1,-24.4).moveTo(-131,-1.1).lineTo(-136.4,-1).moveTo(-128.8,-24.6).lineTo(-132.8,-26).moveTo(-119.1,-40.5).lineTo(-137,-48.6).moveTo(-128.1,19.3).lineTo(-130.8,19.8).moveTo(-120.9,18).lineTo(-126.6,19).moveTo(-110.7,55.3).lineTo(-120,61.7).moveTo(-122.9,63.7).lineTo(-127,66.4).moveTo(-118.6,35.3).lineTo(-137,43.3);
    this.shape_73.setTransform(10,0.3);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(113.4,-56.6).lineTo(121.1,-61.5).moveTo(125.1,-64).lineTo(130.6,-67.6).moveTo(121.5,-41.3).lineTo(129.6,-44.9).moveTo(126.1,-23.6).lineTo(131.1,-25.2).moveTo(132.2,-25.6).lineTo(135.8,-26.7).moveTo(132.6,-46.3).lineTo(135.6,-47.7).moveTo(137.8,-48.6).lineTo(140.1,-49.7).moveTo(120.7,36.7).lineTo(139.7,45.1).moveTo(124.8,19).lineTo(130.4,19.8).moveTo(126.3,0.1).lineTo(137.4,0.8).moveTo(130.7,19.8).lineTo(133.8,20.2).moveTo(116.9,58.8).lineTo(123.3,62.9).moveTo(124.8,63.9).lineTo(130.2,67.4).moveTo(-131.1,-68).lineTo(-126.2,-64.8).moveTo(-124.3,-63.6).lineTo(-113.3,-56.4).moveTo(-139.6,-1).lineTo(-134,-1.1).moveTo(-135.9,-26.6).lineTo(-131.8,-25.2).moveTo(-131.1,-24.9).lineTo(-126.2,-23.4).moveTo(-140.2,-49.7).lineTo(-121.9,-41.5).moveTo(-132.5,-1.1).lineTo(-125.8,-1.2).moveTo(-129.5,19.5).lineTo(-123.7,18.4).moveTo(-129.9,68).lineTo(-125.8,65.2).moveTo(-122.8,63.2).lineTo(-113.3,56.6).moveTo(-140.2,44.3).lineTo(-121.3,36.1).moveTo(-133.8,20.2).lineTo(-131.1,19.7);
    this.shape_74.setTransform(9.7,0.3);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(121.2,-60.4).lineTo(129.3,-65.6).moveTo(133.6,-68.4).lineTo(139.5,-72.2).moveTo(147.3,-51.9).lineTo(149.7,-53.1).moveTo(129.8,-44.1).lineTo(138.5,-48).moveTo(134.8,-25.2).lineTo(140.1,-26.9).moveTo(141.3,-27.3).lineTo(145.1,-28.5).moveTo(141.7,-49.4).lineTo(144.9,-50.9).moveTo(129,39.2).lineTo(149.3,48.1).moveTo(133.3,20.3).lineTo(139.3,21.1).moveTo(134.9,0.1).lineTo(146.8,0.9).moveTo(139.7,21.1).lineTo(142.9,21.6).moveTo(124.9,62.8).lineTo(131.7,67.2).moveTo(133.3,68.2).lineTo(139.1,72).moveTo(-139.9,-72.6).lineTo(-134.8,-69.2).moveTo(-149.1,-1.1).lineTo(-143.1,-1.2).moveTo(-145.1,-28.4).lineTo(-140.7,-26.9).moveTo(-141.5,-1.2).lineTo(-134.3,-1.3).moveTo(-139.9,-26.6).lineTo(-134.8,-25).moveTo(-149.7,-53.1).lineTo(-130.2,-44.3).moveTo(-132.7,-67.9).lineTo(-121,-60.2).moveTo(-142.9,21.6).lineTo(-139.9,21.1).moveTo(-138.3,20.8).lineTo(-132.1,19.7).moveTo(-138.7,72.6).lineTo(-134.3,69.7).moveTo(-149.7,47.3).lineTo(-129.6,38.6).moveTo(-131.2,67.5).lineTo(-121,60.5);
    this.shape_75.setTransform(8.8,0.3);

    this.shape_76 = new cjs.Shape();
    this.shape_76.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(134.1,-66.8).lineTo(143.1,-72.6).moveTo(147.8,-75.7).lineTo(154.4,-79.9).moveTo(143.6,-48.8).lineTo(153.2,-53.1).moveTo(156.8,-54.7).lineTo(160.3,-56.3).moveTo(162.9,-57.5).lineTo(165.6,-58.7).moveTo(156.3,-30.2).lineTo(160.5,-31.6).moveTo(149.1,-27.8).lineTo(155,-29.8).moveTo(147.5,22.4).lineTo(154.1,23.4).moveTo(138.2,69.4).lineTo(145.7,74.3).moveTo(147.5,75.5).lineTo(153.9,79.7).moveTo(142.7,43.3).lineTo(165.2,53.3).moveTo(149.3,0.2).lineTo(162.5,1).moveTo(154.5,23.4).lineTo(158.1,23.9).moveTo(-154.8,-80.3).lineTo(-149.1,-76.5).moveTo(-146.9,-75.1).lineTo(-133.9,-66.6).moveTo(-164.9,-1.2).lineTo(-158.3,-1.3).moveTo(-160.5,-31.4).lineTo(-155.7,-29.8).moveTo(-156.6,-1.3).lineTo(-148.6,-1.4).moveTo(-154.8,-29.5).lineTo(-149.1,-27.6).moveTo(-165.6,-58.7).lineTo(-144,-49).moveTo(-158.1,23.9).lineTo(-154.8,23.3).moveTo(-153,23).lineTo(-146.2,21.7).moveTo(-153.5,80.3).lineTo(-148.6,77.1).moveTo(-145.1,74.6).lineTo(-133.9,66.9).moveTo(-165.6,52.4).lineTo(-143.3,42.7);
    this.shape_76.setTransform(7.2,0.3);

    this.shape_77 = new cjs.Shape();
    this.shape_77.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(167.7,-85.9).lineTo(175.1,-90.6).moveTo(162.9,-55.4).lineTo(173.8,-60.3).moveTo(177.8,-62.1).lineTo(181.8,-63.9).moveTo(184.8,-65.2).lineTo(187.9,-66.6).moveTo(177.3,-34.3).lineTo(182.1,-35.9).moveTo(169.1,-31.6).lineTo(175.8,-33.8).moveTo(152.1,-75.9).lineTo(162.3,-82.4).moveTo(167.4,25.4).lineTo(174.8,26.5).moveTo(175.3,26.5).lineTo(179.4,27.1).moveTo(169.3,0.1).lineTo(184.3,1.1).moveTo(167.4,85.6).lineTo(174.6,90.4).moveTo(161.9,49.1).lineTo(187.4,60.4).moveTo(156.8,78.7).lineTo(165.3,84.3).moveTo(-169.1,-86.9).lineTo(-175.6,-91.1).moveTo(-169.1,-31.4).lineTo(-175.6,-33.5).moveTo(-176.6,-33.8).lineTo(-182.1,-35.6).moveTo(-179.6,-1.5).lineTo(-187.1,-1.4).moveTo(-168.6,-1.6).lineTo(-177.6,-1.5).moveTo(-151.9,-75.6).lineTo(-166.6,-85.2).moveTo(-163.4,-55.6).lineTo(-187.9,-66.6).moveTo(-151.9,75.9).lineTo(-164.6,84.6).moveTo(-175.6,26.4).lineTo(-179.4,27.1).moveTo(-168.6,87.4).lineTo(-174.1,91.1).moveTo(-165.9,24.6).lineTo(-173.6,26.1).moveTo(-162.6,48.4).lineTo(-187.9,59.4);
    this.shape_77.setTransform(5.1,0.3);

    this.shape_78 = new cjs.Shape();
    this.shape_78.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(163.3,-55.5).lineTo(174.2,-60.4).moveTo(168.1,-86.1).lineTo(175.5,-90.8).moveTo(178.2,-62.2).lineTo(182.3,-64).moveTo(185.3,-65.4).lineTo(188.3,-66.8).moveTo(169.5,-31.7).lineTo(176.2,-33.9).moveTo(177.7,-34.4).lineTo(182.6,-36).moveTo(152.5,-76.1).lineTo(162.7,-82.6).moveTo(167.8,25.4).lineTo(175.2,26.5).moveTo(169.7,0.1).lineTo(184.8,1.1).moveTo(175.7,26.6).lineTo(179.8,27.2).moveTo(157.2,78.9).lineTo(165.7,84.5).moveTo(162.3,49.2).lineTo(187.8,60.5).moveTo(167.8,85.8).lineTo(175,90.6).moveTo(-176,-91.3).lineTo(-169.5,-87.1).moveTo(-187.6,-1.4).lineTo(-180.1,-1.5).moveTo(-182.6,-35.7).lineTo(-177,-33.9).moveTo(-178,-1.5).lineTo(-169,-1.6).moveTo(-176,-33.6).lineTo(-169.5,-31.4).moveTo(-167,-85.4).lineTo(-152.2,-75.8).moveTo(-188.3,-66.8).lineTo(-163.8,-55.8).moveTo(-165,84.8).lineTo(-152.2,76.1).moveTo(-179.8,27.2).lineTo(-176,26.5).moveTo(-174.5,91.3).lineTo(-169,87.6).moveTo(-188.3,59.5).lineTo(-163,48.5).moveTo(-174,26.1).lineTo(-166.3,24.7);
    this.shape_78.setTransform(5.1,0.3);

    this.shape_79 = new cjs.Shape();
    this.shape_79.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(163.7,-55.6).lineTo(174.7,-60.6).moveTo(168.5,-86.3).lineTo(176,-91.1).moveTo(178.7,-62.4).lineTo(182.7,-64.2).moveTo(185.7,-65.5).lineTo(188.8,-66.9).moveTo(169.9,-31.8).lineTo(176.7,-34).moveTo(178.2,-34.5).lineTo(183,-36).moveTo(152.8,-76.2).lineTo(163.1,-82.8).moveTo(168.2,25.5).lineTo(175.7,26.6).moveTo(170.1,0.1).lineTo(185.2,1.1).moveTo(176.2,26.7).lineTo(180.2,27.3).moveTo(157.6,79.1).lineTo(166.1,84.7).moveTo(162.6,49.4).lineTo(188.3,60.7).moveTo(168.2,86).lineTo(175.5,90.8).moveTo(-176.5,-91.6).lineTo(-169.9,-87.3).moveTo(-188,-1.4).lineTo(-180.5,-1.5).moveTo(-183,-35.8).lineTo(-177.5,-34).moveTo(-178.5,-1.5).lineTo(-169.4,-1.6).moveTo(-176.5,-33.6).lineTo(-169.9,-31.5).moveTo(-188.8,-66.9).lineTo(-164.2,-55.9).moveTo(-167.4,-85.6).lineTo(-152.6,-76).moveTo(-165.4,85).lineTo(-152.6,76.2).moveTo(-180.2,27.3).lineTo(-176.5,26.6).moveTo(-175,91.6).lineTo(-169.4,87.8).moveTo(-188.8,59.7).lineTo(-163.4,48.6).moveTo(-174.5,26.2).lineTo(-166.7,24.7);
    this.shape_79.setTransform(5.1,0.3);

    this.shape_80 = new cjs.Shape();
    this.shape_80.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(164,-55.8).lineTo(175.1,-60.7).moveTo(168.9,-86.5).lineTo(176.4,-91.3).moveTo(179.1,-62.5).lineTo(183.1,-64.3).moveTo(186.1,-65.7).lineTo(189.2,-67.1).moveTo(170.3,-31.8).lineTo(177.1,-34.1).moveTo(178.6,-34.6).lineTo(183.4,-36.1).moveTo(153.2,-76.4).lineTo(163.5,-83).moveTo(168.6,25.6).lineTo(176.1,26.7).moveTo(170.5,0.1).lineTo(185.6,1.1).moveTo(176.6,26.7).lineTo(180.7,27.3).moveTo(157.9,79.3).lineTo(166.5,84.9).moveTo(163,49.5).lineTo(188.7,60.8).moveTo(168.6,86.2).lineTo(175.9,91).moveTo(-176.9,-91.8).lineTo(-170.3,-87.5).moveTo(-188.5,-1.4).lineTo(-180.9,-1.5).moveTo(-183.4,-35.9).lineTo(-177.9,-34.1).moveTo(-178.9,-1.5).lineTo(-169.8,-1.6).moveTo(-176.9,-33.7).lineTo(-170.3,-31.6).moveTo(-189.2,-67.1).lineTo(-164.5,-56).moveTo(-167.8,-85.8).lineTo(-153,-76.2).moveTo(-165.8,85.2).lineTo(-153,76.4).moveTo(-180.7,27.3).lineTo(-176.9,26.6).moveTo(-175.4,91.8).lineTo(-169.8,88).moveTo(-189.2,59.8).lineTo(-163.8,48.7).moveTo(-174.9,26.3).lineTo(-167.1,24.8);
    this.shape_80.setTransform(5.1,0.3);

    this.shape_81 = new cjs.Shape();
    this.shape_81.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(164.4,-55.9).lineTo(175.5,-60.8).moveTo(169.3,-86.7).lineTo(176.8,-91.5).moveTo(179.5,-62.7).lineTo(183.6,-64.5).moveTo(186.6,-65.8).lineTo(189.7,-67.3).moveTo(170.7,-31.9).lineTo(177.5,-34.1).moveTo(179,-34.6).lineTo(183.9,-36.2).moveTo(153.6,-76.6).lineTo(163.9,-83.2).moveTo(169,25.6).lineTo(176.5,26.7).moveTo(170.9,0.1).lineTo(186.1,1.1).moveTo(177,26.8).lineTo(181.1,27.4).moveTo(158.3,79.5).lineTo(166.9,85.1).moveTo(163.4,49.6).lineTo(189.2,61).moveTo(169,86.4).lineTo(176.3,91.2).moveTo(-177.3,-92).lineTo(-170.7,-87.7).moveTo(-188.9,-1.4).lineTo(-181.3,-1.5).moveTo(-183.9,-36).lineTo(-178.3,-34.1).moveTo(-179.3,-1.5).lineTo(-170.2,-1.6).moveTo(-177.3,-33.8).lineTo(-170.7,-31.7).moveTo(-189.7,-67.3).lineTo(-164.9,-56.1).moveTo(-168.2,-86).lineTo(-153.3,-76.3).moveTo(-166.2,85.4).lineTo(-153.3,76.6).moveTo(-181.1,27.4).lineTo(-177.3,26.7).moveTo(-175.3,26.3).lineTo(-167.5,24.9).moveTo(-175.8,92).lineTo(-170.2,88.2).moveTo(-189.7,59.9).lineTo(-164.2,48.8);
    this.shape_81.setTransform(5.1,0.3);

    this.shape_82 = new cjs.Shape();
    this.shape_82.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(164.8,-56).lineTo(175.9,-61).moveTo(169.7,-86.9).lineTo(177.2,-91.7).moveTo(179.9,-62.8).lineTo(184,-64.6).moveTo(187,-66).lineTo(190.1,-67.4).moveTo(171.1,-32).lineTo(177.9,-34.2).moveTo(179.4,-34.7).lineTo(184.3,-36.3).moveTo(153.9,-76.8).lineTo(164.3,-83.4).moveTo(169.4,25.7).lineTo(176.9,26.8).moveTo(171.3,0.1).lineTo(186.5,1.1).moveTo(177.4,26.8).lineTo(181.5,27.4).moveTo(158.7,79.7).lineTo(167.3,85.3).moveTo(163.8,49.7).lineTo(189.6,61.1).moveTo(169.4,86.6).lineTo(176.7,91.5).moveTo(-177.7,-92.2).lineTo(-171.1,-87.9).moveTo(-189.4,-1.4).lineTo(-181.8,-1.5).moveTo(-184.3,-36).lineTo(-178.7,-34.2).moveTo(-179.7,-1.5).lineTo(-170.6,-1.6).moveTo(-177.7,-33.9).lineTo(-171.1,-31.7).moveTo(-190.1,-67.4).lineTo(-165.3,-56.3).moveTo(-168.6,-86.2).lineTo(-153.7,-76.5).moveTo(-166.6,85.6).lineTo(-153.7,76.8).moveTo(-181.5,27.4).lineTo(-177.7,26.7).moveTo(-175.7,26.4).lineTo(-167.9,24.9).moveTo(-176.2,92.2).lineTo(-170.6,88.4).moveTo(-190.1,60.1).lineTo(-164.6,49);
    this.shape_82.setTransform(5.1,0.3);

    this.shape_83 = new cjs.Shape();
    this.shape_83.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(165.2,-56.2).lineTo(176.3,-61.1).moveTo(170.1,-87.1).lineTo(177.6,-91.9).moveTo(180.4,-63).lineTo(184.4,-64.8).moveTo(187.5,-66.2).lineTo(190.6,-67.6).moveTo(171.5,-32.1).lineTo(178.3,-34.3).moveTo(179.9,-34.8).lineTo(184.7,-36.4).moveTo(154.3,-77).lineTo(164.6,-83.6).moveTo(169.8,25.7).lineTo(177.3,26.9).moveTo(171.7,0.1).lineTo(187,1.1).moveTo(177.8,26.9).lineTo(181.9,27.5).moveTo(159.1,79.9).lineTo(167.7,85.5).moveTo(164.2,49.8).lineTo(190,61.2).moveTo(169.8,86.9).lineTo(177.1,91.7).moveTo(-178.1,-92.4).lineTo(-171.5,-88.1).moveTo(-189.8,-1.4).lineTo(-182.2,-1.5).moveTo(-184.7,-36.1).lineTo(-179.2,-34.3).moveTo(-180.2,-1.5).lineTo(-171,-1.6).moveTo(-178.1,-33.9).lineTo(-171.5,-31.8).moveTo(-190.6,-67.6).lineTo(-165.7,-56.4).moveTo(-169,-86.4).lineTo(-154,-76.7).moveTo(-167,85.8).lineTo(-154,77).moveTo(-181.9,27.5).lineTo(-178.1,26.8).moveTo(-176.1,26.4).lineTo(-168.2,25).moveTo(-176.6,92.4).lineTo(-171,88.6).moveTo(-190.6,60.2).lineTo(-165,49.1);
    this.shape_83.setTransform(5.1,0.3);

    this.shape_84 = new cjs.Shape();
    this.shape_84.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(165.6,-56.3).lineTo(176.7,-61.3).moveTo(170.5,-87.3).lineTo(178,-92.1).moveTo(180.8,-63.1).lineTo(184.9,-64.9).moveTo(187.9,-66.3).lineTo(191,-67.7).moveTo(171.9,-32.1).lineTo(178.8,-34.4).moveTo(180.3,-34.9).lineTo(185.2,-36.5).moveTo(154.7,-77.1).lineTo(165,-83.8).moveTo(170.2,25.8).lineTo(177.7,26.9).moveTo(172.1,0.1).lineTo(187.4,1.1).moveTo(178.2,27).lineTo(182.4,27.6).moveTo(159.4,80).lineTo(168.1,85.7).moveTo(164.6,49.9).lineTo(190.5,61.4).moveTo(170.2,87.1).lineTo(177.5,91.9).moveTo(-178.6,-92.6).lineTo(-171.9,-88.3).moveTo(-190.2,-1.4).lineTo(-182.6,-1.5).moveTo(-185.2,-36.2).lineTo(-179.6,-34.4).moveTo(-180.6,-1.5).lineTo(-171.4,-1.6).moveTo(-178.6,-34).lineTo(-171.9,-31.9).moveTo(-191,-67.7).lineTo(-166.1,-56.5).moveTo(-169.4,-86.6).lineTo(-154.4,-76.9).moveTo(-182.4,27.6).lineTo(-178.6,26.9).moveTo(-176.5,26.5).lineTo(-168.6,25).moveTo(-177,92.6).lineTo(-171.4,88.8).moveTo(-191,60.4).lineTo(-165.3,49.2).moveTo(-167.4,86).lineTo(-154.4,77.1);
    this.shape_84.setTransform(5.1,0.3);

    this.shape_85 = new cjs.Shape();
    this.shape_85.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(166,-56.4).lineTo(177.1,-61.4).moveTo(170.9,-87.5).lineTo(178.5,-92.3).moveTo(181.2,-63.3).lineTo(185.3,-65.1).moveTo(188.3,-66.5).lineTo(191.5,-67.9).moveTo(172.3,-32.2).lineTo(179.2,-34.5).moveTo(180.7,-35).lineTo(185.6,-36.6).moveTo(155,-77.3).lineTo(165.4,-84).moveTo(170.6,25.9).lineTo(178.2,27).moveTo(172.5,0.1).lineTo(187.8,1.1).moveTo(178.7,27).lineTo(182.8,27.6).moveTo(159.8,80.2).lineTo(168.5,85.9).moveTo(165,50.1).lineTo(190.9,61.5).moveTo(170.6,87.3).lineTo(178,92.1).moveTo(-179,-92.9).lineTo(-172.3,-88.5).moveTo(-190.7,-1.4).lineTo(-183.1,-1.5).moveTo(-185.6,-36.3).lineTo(-180,-34.5).moveTo(-181,-1.5).lineTo(-171.8,-1.6).moveTo(-179,-34.1).lineTo(-172.3,-32).moveTo(-191.5,-67.9).lineTo(-166.5,-56.7).moveTo(-169.8,-86.8).lineTo(-154.8,-77.1).moveTo(-182.8,27.6).lineTo(-179,26.9).moveTo(-176.9,26.6).lineTo(-169,25.1).moveTo(-177.4,92.9).lineTo(-171.8,89.1).moveTo(-191.5,60.5).lineTo(-165.7,49.3).moveTo(-167.8,86.2).lineTo(-154.8,77.3);
    this.shape_85.setTransform(5.1,0.3);

    this.shape_86 = new cjs.Shape();
    this.shape_86.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(166.4,-56.6).lineTo(177.6,-61.6).moveTo(171.3,-87.7).lineTo(178.9,-92.6).moveTo(181.6,-63.4).lineTo(185.7,-65.2).moveTo(188.8,-66.6).lineTo(191.9,-68).moveTo(172.7,-32.3).lineTo(179.6,-34.5).moveTo(181.1,-35.1).lineTo(186,-36.6).moveTo(155.4,-77.5).lineTo(165.8,-84.2).moveTo(171,25.9).lineTo(178.6,27.1).moveTo(172.9,0.1).lineTo(188.3,1.1).moveTo(179.1,27.1).lineTo(183.2,27.7).moveTo(160.2,80.4).lineTo(168.9,86.1).moveTo(165.3,50.2).lineTo(191.4,61.7).moveTo(171,87.5).lineTo(178.4,92.3).moveTo(-179.4,-93.1).lineTo(-172.7,-88.7).moveTo(-191.1,-1.4).lineTo(-183.5,-1.5).moveTo(-186,-36.4).lineTo(-180.4,-34.5).moveTo(-181.4,-1.5).lineTo(-172.2,-1.6).moveTo(-179.4,-34.2).lineTo(-172.7,-32).moveTo(-191.9,-68).lineTo(-166.9,-56.8).moveTo(-170.2,-87).lineTo(-155.1,-77.2).moveTo(-183.2,27.7).lineTo(-179.4,27).moveTo(-177.4,26.6).lineTo(-169.4,25.2).moveTo(-177.9,93.1).lineTo(-172.2,89.3).moveTo(-191.9,60.7).lineTo(-166.1,49.4).moveTo(-168.2,86.4).lineTo(-155.1,77.5);
    this.shape_86.setTransform(5.1,0.3);

    this.shape_87 = new cjs.Shape();
    this.shape_87.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(166.8,-56.7).lineTo(178,-61.7).moveTo(171.7,-87.9).lineTo(179.3,-92.8).moveTo(182.1,-63.6).lineTo(186.2,-65.4).moveTo(189.2,-66.8).lineTo(192.4,-68.2).moveTo(173.1,-32.4).lineTo(180,-34.6).moveTo(181.5,-35.1).lineTo(186.5,-36.7).moveTo(155.7,-77.7).lineTo(166.2,-84.4).moveTo(171.4,26).lineTo(179,27.1).moveTo(173.3,0.1).lineTo(188.7,1.1).moveTo(179.5,27.2).lineTo(183.6,27.8).moveTo(160.6,80.6).lineTo(169.3,86.3).moveTo(165.7,50.3).lineTo(191.8,61.8).moveTo(171.4,87.7).lineTo(178.8,92.5).moveTo(-192.4,-68.2).lineTo(-167.3,-56.9).moveTo(-179.8,-93.3).lineTo(-173.1,-88.9).moveTo(-191.6,-1.4).lineTo(-183.9,-1.5).moveTo(-186.5,-36.5).lineTo(-180.8,-34.6).moveTo(-181.9,-1.5).lineTo(-172.6,-1.6).moveTo(-179.8,-34.3).lineTo(-173.1,-32.1).moveTo(-170.6,-87.2).lineTo(-155.5,-77.4).moveTo(-183.6,27.8).lineTo(-179.8,27.1).moveTo(-177.8,26.7).lineTo(-169.8,25.2).moveTo(-178.3,93.3).lineTo(-172.6,89.5).moveTo(-192.4,60.8).lineTo(-166.5,49.5).moveTo(-168.6,86.6).lineTo(-155.5,77.7);
    this.shape_87.setTransform(5.1,0.3);

    this.shape_88 = new cjs.Shape();
    this.shape_88.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(167.1,-56.8).lineTo(178.4,-61.8).moveTo(172.1,-88.1).lineTo(179.7,-93).moveTo(182.5,-63.7).lineTo(186.6,-65.6).moveTo(189.7,-66.9).lineTo(192.8,-68.4).moveTo(173.6,-32.4).lineTo(180.4,-34.7).moveTo(182,-35.2).lineTo(186.9,-36.8).moveTo(156.1,-77.9).lineTo(166.6,-84.6).moveTo(171.7,26.1).lineTo(179.4,27.2).moveTo(173.8,0.1).lineTo(189.1,1.2).moveTo(179.9,27.2).lineTo(184.1,27.8).moveTo(160.9,80.8).lineTo(169.6,86.5).moveTo(166.1,50.4).lineTo(192.3,62).moveTo(171.7,87.9).lineTo(179.2,92.8).moveTo(-192.8,-68.4).lineTo(-167.6,-57.1).moveTo(-180.2,-93.5).lineTo(-173.6,-89.2).moveTo(-192,-1.4).lineTo(-184.3,-1.5).moveTo(-186.9,-36.5).lineTo(-181.3,-34.7).moveTo(-182.3,-1.5).lineTo(-173.1,-1.7).moveTo(-180.2,-34.3).lineTo(-173.6,-32.2).moveTo(-171,-87.5).lineTo(-155.9,-77.6).moveTo(-184.1,27.8).lineTo(-180.2,27.1).moveTo(-178.2,26.8).lineTo(-170.2,25.3).moveTo(-178.7,93.5).lineTo(-173.1,89.7).moveTo(-192.8,60.9).lineTo(-166.9,49.7).moveTo(-168.9,86.9).lineTo(-155.9,77.9);
    this.shape_88.setTransform(5.1,0.3);

    this.shape_89 = new cjs.Shape();
    this.shape_89.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(167.5,-56.9).lineTo(178.8,-62).moveTo(172.5,-88.3).lineTo(180.1,-93.2).moveTo(182.9,-63.8).lineTo(187,-65.7).moveTo(190.1,-67.1).lineTo(193.2,-68.5).moveTo(174,-32.5).lineTo(180.9,-34.8).moveTo(182.4,-35.3).lineTo(187.3,-36.9).moveTo(156.5,-78).lineTo(167,-84.8).moveTo(172.1,26.1).lineTo(179.8,27.2).moveTo(174.2,0.1).lineTo(189.6,1.2).moveTo(180.3,27.3).lineTo(184.5,27.9).moveTo(161.3,81).lineTo(170,86.7).moveTo(166.5,50.5).lineTo(192.7,62.1).moveTo(172.1,88.1).lineTo(179.6,93).moveTo(-193.2,-68.5).lineTo(-168,-57.2).moveTo(-180.7,-93.7).lineTo(-174,-89.4).moveTo(-192.5,-1.4).lineTo(-184.8,-1.5).moveTo(-187.3,-36.6).lineTo(-181.7,-34.8).moveTo(-182.7,-1.5).lineTo(-173.5,-1.7).moveTo(-180.7,-34.4).lineTo(-174,-32.3).moveTo(-171.4,-87.7).lineTo(-156.2,-77.8).moveTo(-184.5,27.9).lineTo(-180.7,27.2).moveTo(-178.6,26.8).lineTo(-170.6,25.3).moveTo(-193.2,61.1).lineTo(-167.3,49.8).moveTo(-179.1,93.7).lineTo(-173.5,89.9).moveTo(-169.3,87.1).lineTo(-156.2,78);
    this.shape_89.setTransform(5.1,0.3);

    this.shape_90 = new cjs.Shape();
    this.shape_90.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(167.9,-57.1).lineTo(179.2,-62.1).moveTo(172.9,-88.5).lineTo(180.5,-93.4).moveTo(183.3,-64).lineTo(187.5,-65.9).moveTo(190.6,-67.2).lineTo(193.7,-68.7).moveTo(174.4,-32.6).lineTo(181.3,-34.8).moveTo(182.8,-35.4).lineTo(187.8,-37).moveTo(156.8,-78.2).lineTo(167.3,-85).moveTo(172.5,26.2).lineTo(180.2,27.3).moveTo(174.6,0.1).lineTo(190,1.2).moveTo(180.7,27.4).lineTo(184.9,28).moveTo(161.7,81.2).lineTo(170.4,86.9).moveTo(166.9,50.6).lineTo(193.2,62.3).moveTo(172.5,88.3).lineTo(180,93.2).moveTo(-193.7,-68.7).lineTo(-168.4,-57.3).moveTo(-181.1,-93.9).lineTo(-174.4,-89.6).moveTo(-192.9,-1.4).lineTo(-185.2,-1.5).moveTo(-187.8,-36.7).lineTo(-182.1,-34.8).moveTo(-183.1,-1.5).lineTo(-173.9,-1.7).moveTo(-181.1,-34.5).lineTo(-174.4,-32.3).moveTo(-171.8,-87.9).lineTo(-156.6,-77.9).moveTo(-184.9,28).lineTo(-181.1,27.3).moveTo(-179,26.9).lineTo(-171,25.4).moveTo(-193.7,61.2).lineTo(-167.7,49.9).moveTo(-179.5,93.9).lineTo(-173.9,90.1).moveTo(-169.7,87.3).lineTo(-156.6,78.2);
    this.shape_90.setTransform(5.1,0.3);

    this.shape_91 = new cjs.Shape();
    this.shape_91.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(157.2,-78.4).lineTo(167.7,-85.2).moveTo(168.3,-57.2).lineTo(179.6,-62.3).moveTo(173.3,-88.7).lineTo(181,-93.6).moveTo(183.8,-64.1).lineTo(187.9,-66).moveTo(191,-67.4).lineTo(194.1,-68.8).moveTo(174.8,-32.7).lineTo(181.7,-34.9).moveTo(183.2,-35.5).lineTo(188.2,-37.1).moveTo(172.9,26.2).lineTo(180.7,27.4).moveTo(175,0.1).lineTo(190.5,1.2).moveTo(181.2,27.4).lineTo(185.3,28).moveTo(162.1,81.4).lineTo(170.8,87.1).moveTo(167.3,50.8).lineTo(193.6,62.4).moveTo(172.9,88.5).lineTo(180.5,93.4).moveTo(-194.1,-68.8).lineTo(-168.8,-57.5).moveTo(-181.5,-94.2).lineTo(-174.8,-89.8).moveTo(-193.4,-1.4).lineTo(-185.6,-1.5).moveTo(-188.2,-36.8).lineTo(-182.5,-34.9).moveTo(-183.6,-1.5).lineTo(-174.3,-1.7).moveTo(-181.5,-34.6).lineTo(-174.8,-32.4).moveTo(-172.2,-88.1).lineTo(-156.9,-78.1).moveTo(-185.3,28).lineTo(-181.5,27.3).moveTo(-179.4,26.9).lineTo(-171.4,25.5).moveTo(-194.1,61.4).lineTo(-168.1,50).moveTo(-179.9,94.2).lineTo(-174.3,90.3).moveTo(-170.1,87.5).lineTo(-156.9,78.4);
    this.shape_91.setTransform(5.1,0.3);

    this.shape_92 = new cjs.Shape();
    this.shape_92.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(157.6,-78.6).lineTo(168.1,-85.4).moveTo(168.7,-57.3).lineTo(180,-62.4).moveTo(173.7,-88.9).lineTo(181.4,-93.8).moveTo(184.2,-64.3).lineTo(188.3,-66.2).moveTo(191.4,-67.5).lineTo(194.6,-69).moveTo(175.2,-32.7).lineTo(182.1,-35).moveTo(183.6,-35.5).lineTo(188.6,-37.2).moveTo(173.3,26.3).lineTo(181.1,27.4).moveTo(175.4,0.1).lineTo(190.9,1.2).moveTo(181.6,27.5).lineTo(185.8,28.1).moveTo(162.4,81.5).lineTo(171.2,87.3).moveTo(167.7,50.9).lineTo(194.1,62.6).moveTo(173.3,88.7).lineTo(180.9,93.6).moveTo(-194.6,-69).lineTo(-169.2,-57.6).moveTo(-181.9,-94.4).lineTo(-175.2,-90).moveTo(-193.8,-1.4).lineTo(-186.1,-1.5).moveTo(-188.6,-36.9).lineTo(-182.9,-35).moveTo(-184,-1.5).lineTo(-174.7,-1.7).moveTo(-181.9,-34.7).lineTo(-175.2,-32.5).moveTo(-172.6,-88.3).lineTo(-157.3,-78.3).moveTo(-185.8,28.1).lineTo(-181.9,27.4).moveTo(-179.8,27).lineTo(-171.8,25.5).moveTo(-194.6,61.5).lineTo(-168.4,50.1).moveTo(-180.3,94.4).lineTo(-174.7,90.5).moveTo(-170.5,87.7).lineTo(-157.3,78.6);
    this.shape_92.setTransform(5.1,0.3);

    this.shape_93 = new cjs.Shape();
    this.shape_93.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(157.9,-78.8).lineTo(168.5,-85.5).moveTo(169.1,-57.5).lineTo(180.5,-62.6).moveTo(174.1,-89.2).lineTo(181.8,-94.1).moveTo(184.6,-64.4).lineTo(188.8,-66.3).moveTo(191.9,-67.7).lineTo(195,-69.1).moveTo(175.6,-32.8).lineTo(182.5,-35.1).moveTo(184.1,-35.6).lineTo(189.1,-37.2).moveTo(173.7,26.4).lineTo(181.5,27.5).moveTo(175.8,0.1).lineTo(191.3,1.2).moveTo(182,27.6).lineTo(186.2,28.2).moveTo(162.8,81.7).lineTo(171.6,87.5).moveTo(168,51).lineTo(194.5,62.7).moveTo(173.7,88.9).lineTo(181.3,93.8).moveTo(-195,-69.1).lineTo(-169.6,-57.7).moveTo(-182.3,-94.6).lineTo(-175.6,-90.2).moveTo(-194.2,-1.4).lineTo(-186.5,-1.5).moveTo(-189.1,-37).lineTo(-183.4,-35.1).moveTo(-184.4,-1.5).lineTo(-175.1,-1.7).moveTo(-182.3,-34.7).lineTo(-175.6,-32.6).moveTo(-173,-88.5).lineTo(-157.7,-78.5).moveTo(-186.2,28.2).lineTo(-182.3,27.5).moveTo(-180.3,27.1).lineTo(-172.2,25.6).moveTo(-195,61.7).lineTo(-168.8,50.2).moveTo(-180.8,94.6).lineTo(-175.1,90.7).moveTo(-170.9,87.9).lineTo(-157.7,78.8);
    this.shape_93.setTransform(5.1,0.3);

    this.shape_94 = new cjs.Shape();
    this.shape_94.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(158.3,-79).lineTo(168.9,-85.7).moveTo(169.5,-57.6).lineTo(180.9,-62.7).moveTo(174.5,-89.4).lineTo(182.2,-94.3).moveTo(185,-64.6).lineTo(189.2,-66.5).moveTo(192.3,-67.9).lineTo(195.5,-69.3).moveTo(176,-32.9).lineTo(182.9,-35.2).moveTo(184.5,-35.7).lineTo(189.5,-37.3).moveTo(174.1,26.4).lineTo(181.9,27.6).moveTo(176.2,0.1).lineTo(191.8,1.2).moveTo(182.4,27.6).lineTo(186.6,28.2).moveTo(163.2,81.9).lineTo(172,87.7).moveTo(168.4,51.1).lineTo(194.9,62.8).moveTo(174.1,89.1).lineTo(181.7,94.1).moveTo(-195.5,-69.3).lineTo(-170,-57.9).moveTo(-182.7,-94.8).lineTo(-176,-90.4).moveTo(-194.7,-1.4).lineTo(-186.9,-1.5).moveTo(-189.5,-37).lineTo(-183.8,-35.2).moveTo(-184.8,-1.5).lineTo(-175.5,-1.7).moveTo(-182.7,-34.8).lineTo(-176,-32.6).moveTo(-173.4,-88.7).lineTo(-158,-78.7).moveTo(-186.6,28.2).lineTo(-182.7,27.5).moveTo(-180.7,27.1).lineTo(-172.6,25.6).moveTo(-195.5,61.8).lineTo(-169.2,50.4).moveTo(-181.2,94.8).lineTo(-175.5,90.9).moveTo(-171.3,88.1).lineTo(-158,79);
    this.shape_94.setTransform(5.1,0.3);

    this.shape_95 = new cjs.Shape();
    this.shape_95.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(158.6,-79.1).lineTo(169.3,-85.9).moveTo(169.9,-57.7).lineTo(181.3,-62.8).moveTo(174.9,-89.6).lineTo(182.6,-94.5).moveTo(185.5,-64.7).lineTo(189.6,-66.6).moveTo(192.8,-68).lineTo(195.9,-69.5).moveTo(176.4,-33).lineTo(183.4,-35.2).moveTo(184.9,-35.8).lineTo(189.9,-37.4).moveTo(174.5,26.5).lineTo(182.3,27.6).moveTo(176.6,0.1).lineTo(192.2,1.2).moveTo(182.8,27.7).lineTo(187,28.3).moveTo(163.6,82.1).lineTo(172.4,87.9).moveTo(168.8,51.2).lineTo(195.4,63).moveTo(174.5,89.3).lineTo(182.1,94.3).moveTo(-195.9,-69.5).lineTo(-170.4,-58).moveTo(-183.2,-95).lineTo(-176.4,-90.6).moveTo(-195.1,-1.4).lineTo(-187.3,-1.5).moveTo(-189.9,-37.1).lineTo(-184.2,-35.2).moveTo(-185.3,-1.5).lineTo(-175.9,-1.7).moveTo(-183.2,-34.9).lineTo(-176.4,-32.7).moveTo(-173.8,-88.9).lineTo(-158.4,-78.8).moveTo(-187,28.3).lineTo(-183.2,27.6).moveTo(-181.1,27.2).lineTo(-173,25.7).moveTo(-195.9,61.9).lineTo(-169.6,50.5).moveTo(-181.6,95).lineTo(-175.9,91.1).moveTo(-171.7,88.3).lineTo(-158.4,79.1);
    this.shape_95.setTransform(5.1,0.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_73}]}).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[]},1).wait(3));

    // Layer_6
    this.shape_96 = new cjs.Shape();
    this.shape_96.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,0).curveTo(-2.3,-1,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0).lineTo(2.3,0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.8,-1.6,-0).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.6,0).curveTo(1.6,-0.7,1.2,-1.2).curveTo(0.7,-1.8,0,-1.8).curveTo(-0.7,-1.8,-1.2,-1.2).closePath();
    this.shape_96.setTransform(82.6,56.8);

    this.shape_97 = new cjs.Shape();
    this.shape_97.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,0).curveTo(-2.3,-1,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.7,-1.6,-0.1).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.2).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.2).closePath();
    this.shape_97.setTransform(82.6,66.8);
    this.shape_97._off = true;

    this.shape_98 = new cjs.Shape();
    this.shape_98.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,1,-2.3,0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,0).curveTo(2.3,1,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.8,-1.6,-0.1).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.2).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.2).closePath();
    this.shape_98.setTransform(82.6,86.8);

    this.shape_99 = new cjs.Shape();
    this.shape_99.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.8,-1.6,-0.1).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.6,0).curveTo(1.6,-0.7,1.2,-1.3).curveTo(0.7,-1.7,0,-1.7).curveTo(-0.7,-1.7,-1.2,-1.3).closePath();
    this.shape_99.setTransform(82.6,96.8);

    this.shape_100 = new cjs.Shape();
    this.shape_100.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,0).curveTo(-2.3,-1,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.8,-1.6,-0.1).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.2).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.2).closePath();
    this.shape_100.setTransform(82.6,97.3);

    this.shape_101 = new cjs.Shape();
    this.shape_101.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,0).curveTo(-2.3,-1,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0).lineTo(2.3,0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.8,-1.6,-0).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.2).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.2).closePath();
    this.shape_101.setTransform(82.6,97.7);

    this.shape_102 = new cjs.Shape();
    this.shape_102.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,1,-2.3,-0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0).lineTo(2.3,-0).curveTo(2.3,1,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.8,-1.6,-0).lineTo(-1.6,-0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.3).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.3).closePath();
    this.shape_102.setTransform(82.6,98);

    this.shape_103 = new cjs.Shape();
    this.shape_103.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,-0).curveTo(-2.3,-1,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0).lineTo(2.3,-0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.7,-1.6,-0).lineTo(-1.6,-0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.3).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.3).closePath();
    this.shape_103.setTransform(82.6,98.2);

    this.shape_104 = new cjs.Shape();
    this.shape_104.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,1,-2.3,0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,0).curveTo(2.3,1,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.8,-1.6,-0.1).lineTo(-1.6,0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.3).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.3).closePath();
    this.shape_104.setTransform(82.6,99.4);

    this.shape_105 = new cjs.Shape();
    this.shape_105.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,1,-2.3,-0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.7).curveTo(2.3,-1,2.3,-0).lineTo(2.3,-0).curveTo(2.3,1,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.2).curveTo(-1.6,-0.8,-1.6,-0).lineTo(-1.6,-0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.2).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.2).closePath();
    this.shape_105.setTransform(82.6,99.9);

    this.shape_106 = new cjs.Shape();
    this.shape_106.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,1,-2.3,-0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0).lineTo(2.3,-0).curveTo(2.3,1,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.7,-1.6,-0).lineTo(-1.6,-0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.3).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.3).closePath();
    this.shape_106.setTransform(82.6,100.1);

    this.shape_107 = new cjs.Shape();
    this.shape_107.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.7,1.6).curveTo(-2.3,0.9,-2.3,-0).curveTo(-2.3,-0.9,-1.7,-1.6).curveTo(-0.9,-2.3,0,-2.3).curveTo(1,-2.3,1.6,-1.6).curveTo(2.3,-1,2.3,-0.1).lineTo(2.3,-0).curveTo(2.3,0.9,1.6,1.6).curveTo(1,2.3,0,2.3).curveTo(-1,2.3,-1.7,1.6).closePath().moveTo(-1.2,-1.3).curveTo(-1.6,-0.8,-1.6,-0.1).lineTo(-1.6,-0).curveTo(-1.6,0.7,-1.2,1.2).curveTo(-0.7,1.7,0,1.7).curveTo(0.7,1.7,1.2,1.2).curveTo(1.6,0.7,1.2,-1.3).curveTo(0.9,-1.5,0.3,-1.5).curveTo(-0.2,-1.5,-1.2,-1.3).closePath();
    this.shape_107.setTransform(82.6,100.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_96}]}).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98,p:{y:86.8}}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102,p:{y:98}}]},1).to({state:[{t:this.shape_103,p:{y:98.2}}]},1).to({state:[{t:this.shape_102,p:{y:98.4}}]},1).to({state:[{t:this.shape_103,p:{y:98.7}}]},1).to({state:[{t:this.shape_98,p:{y:98.9}}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_105,p:{y:99.9}}]},1).to({state:[{t:this.shape_106,p:{y:100.1}}]},1).to({state:[{t:this.shape_105,p:{y:100.3}}]},1).to({state:[{t:this.shape_106,p:{y:100.6}}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_97}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_97).wait(1).to({_off:false},0).wait(1).to({y:76.8},0).to({_off:true},1).wait(2).to({_off:false,y:97},0).to({_off:true},1).wait(1).to({_off:false,y:97.5},0).to({_off:true},1).wait(6).to({_off:false,y:99.2},0).to({_off:true},1).wait(1).to({_off:false,y:99.6},0).to({_off:true},1).wait(5).to({_off:false,y:101.1},0).to({_off:true},1).wait(3));

    // Layer_7
    this.shape_108 = new cjs.Shape();
    this.shape_108.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.2,3,2.1,2.2).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.2,-3.1,-0.1,-3.1).curveTo(-1.3,-3.1,-2.1,-2.2).closePath();
    this.shape_108.setTransform(-47.6,18.7);

    this.shape_109 = new cjs.Shape();
    this.shape_109.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.2,-2,2.2).curveTo(-1.2,3.1,0,3.1).curveTo(1.2,3.1,2.1,2.2).curveTo(2.9,1.2,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_109.setTransform(-47.6,38.7);

    this.shape_110 = new cjs.Shape();
    this.shape_110.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.2,-2,2.1).curveTo(-1.2,3.1,0,3).curveTo(1.2,3.1,2.1,2.1).curveTo(2.9,1.2,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.7,0.6,-2.7).curveTo(-0.4,-2.7,-2.1,-2.2).closePath();
    this.shape_110.setTransform(-47.6,58.7);

    this.shape_111 = new cjs.Shape();
    this.shape_111.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3,0,3).curveTo(1.2,3,2.1,2.1).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.7,0.6,-2.7).curveTo(-0.4,-2.7,-2.1,-2.2).closePath();
    this.shape_111.setTransform(-47.6,78.7);

    this.shape_112 = new cjs.Shape();
    this.shape_112.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.2,3,2.1,2.2).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_112.setTransform(-47.6,100.2);

    this.shape_113 = new cjs.Shape();
    this.shape_113.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,-0).curveTo(-2.9,1.2,-2,2.2).curveTo(-1.2,3.1,0,3.1).curveTo(1.2,3.1,2.1,2.2).curveTo(2.9,1.2,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_113.setTransform(-47.6,100.4);

    this.shape_114 = new cjs.Shape();
    this.shape_114.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.2,-2,2.1).curveTo(-1.2,3,0,3).curveTo(1.2,3,2.1,2.1).curveTo(2.9,1.2,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.7,0.6,-2.7).curveTo(-0.4,-2.7,-2.1,-2.2).closePath();
    this.shape_114.setTransform(-47.6,100.9);

    this.shape_115 = new cjs.Shape();
    this.shape_115.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3.1,0,3).curveTo(1.2,3.1,2.1,2.1).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.7,0.6,-2.7).curveTo(-0.4,-2.7,-2.1,-2.2).closePath();
    this.shape_115.setTransform(-47.6,101.1);

    this.shape_116 = new cjs.Shape();
    this.shape_116.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.2,-2,2.1).curveTo(-1.2,3.1,0,3).curveTo(1.2,3.1,2.1,2.1).curveTo(2.9,1.2,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_116.setTransform(-47.6,101.8);

    this.shape_117 = new cjs.Shape();
    this.shape_117.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.2,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.2,3,2.1,2.2).curveTo(2.9,1.2,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_117.setTransform(-47.6,102.1);

    this.shape_118 = new cjs.Shape();
    this.shape_118.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.8,-4.1,0,-4.1).curveTo(1.7,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0.1,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.2,-2,2.2).curveTo(-1.2,3.1,0,3).curveTo(1.2,3.1,2.1,2.2).curveTo(2.9,1.2,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_118.setTransform(-47.6,102.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108,p:{y:18.7}}]}).to({state:[{t:this.shape_109,p:{y:38.7}}]},1).to({state:[{t:this.shape_110,p:{y:58.7}}]},1).to({state:[{t:this.shape_111,p:{y:78.7}}]},1).to({state:[{t:this.shape_108,p:{y:98.7}}]},1).to({state:[{t:this.shape_111,p:{y:99}}]},1).to({state:[{t:this.shape_110,p:{y:99.2}}]},1).to({state:[{t:this.shape_111,p:{y:99.4}}]},1).to({state:[{t:this.shape_110,p:{y:99.7}}]},1).to({state:[{t:this.shape_109,p:{y:99.9}}]},1).to({state:[{t:this.shape_112,p:{y:100.2}}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_112,p:{y:100.6}}]},1).to({state:[{t:this.shape_114,p:{y:100.9}}]},1).to({state:[{t:this.shape_115,p:{y:101.1}}]},1).to({state:[{t:this.shape_114,p:{y:101.3}}]},1).to({state:[{t:this.shape_115,p:{y:101.6}}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117,p:{y:102.1}}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_117,p:{y:102.5}}]},1).to({state:[{t:this.shape_112,p:{y:102.8}}]},1).to({state:[{t:this.shape_111,p:{y:103}}]},1).to({state:[]},1).wait(3));

    // Layer_8
    this.shape_119 = new cjs.Shape();
    this.shape_119.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.8).lineTo(4.8,-0.3).curveTo(5,-0.2,5,0.1).curveTo(5,0.2,4.8,0.4).lineTo(2,1.9).lineTo(1.9,2).lineTo(0.3,4.8).curveTo(0.2,5,0,5).curveTo(-0.2,5,-0.3,4.8).lineTo(-1.9,2).lineTo(-2,1.9).lineTo(-4.8,0.4).curveTo(-5,0.2,-5,0.1).curveTo(-5,-0.2,-4.8,-0.3).lineTo(-2,-1.8).lineTo(-1.9,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).closePath();
    this.shape_119.setTransform(23,55.8);

    this.shape_120 = new cjs.Shape();
    this.shape_120.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5.3,0.4).curveTo(-5.3,0.1,-5.1,0).lineTo(-2.3,-1.5).lineTo(-0.6,-4.5).lineTo(1.6,-1.7).lineTo(4.5,0).lineTo(1.7,2.2).lineTo(0,5.1).curveTo(-0.5,5.3,-0.6,5.1).lineTo(-2.1,2.3).lineTo(-5.1,0.7).curveTo(-5.3,0.6,-5.3,0.4).closePath();
    this.shape_120.setTransform(23.3,65.5);
    this.shape_120._off = true;

    this.shape_121 = new cjs.Shape();
    this.shape_121.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.8).lineTo(4.8,-0.3).curveTo(5,-0.2,5,0).curveTo(5,0.2,4.8,0.4).lineTo(2,1.9).lineTo(1.9,2).lineTo(0.3,4.8).curveTo(0.2,5,0,5).curveTo(-0.2,5,-0.3,4.8).lineTo(-1.9,2).lineTo(-2,1.9).lineTo(-4.8,0.4).curveTo(-5,0.2,-5,0).curveTo(-5,-0.2,-4.8,-0.3).lineTo(-2,-1.8).lineTo(-1.9,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).closePath();
    this.shape_121.setTransform(23,95.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_119}]}).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_120}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_120).wait(1).to({_off:false},0).wait(1).to({y:75.5},0).wait(1).to({y:85.5},0).to({_off:true},1).wait(1).to({_off:false,y:95.7},0).wait(1).to({y:96},0).wait(1).to({y:96.2},0).wait(1).to({y:96.4},0).wait(1).to({y:96.7},0).wait(1).to({y:96.9},0).wait(1).to({y:97.1},0).wait(1).to({y:97.4},0).wait(1).to({y:97.6},0).wait(1).to({y:97.9},0).wait(1).to({y:98.1},0).wait(1).to({y:98.3},0).wait(1).to({y:98.6},0).wait(1).to({y:98.8},0).wait(1).to({y:99},0).wait(1).to({y:99.3},0).wait(1).to({y:99.5},0).wait(1).to({y:99.8},0).to({_off:true},1).wait(3));

    // Layer_9
    this.shape_122 = new cjs.Shape();
    this.shape_122.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.9).lineTo(4.8,-0.3).curveTo(5,-0.2,5,0).curveTo(5,0.2,4.8,0.3).lineTo(2,1.8).lineTo(1.9,2).lineTo(0.3,4.8).curveTo(0.2,5,0,5).curveTo(-0.2,5,-0.3,4.8).lineTo(-1.9,2).lineTo(-2,1.8).lineTo(-4.8,0.3).curveTo(-5,0.2,-5,0).curveTo(-5,-0.2,-4.8,-0.3).lineTo(-2,-1.9).curveTo(-1.9,-1.9,-1.9,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).curveTo(1.9,-1.9,2,-1.9).closePath();
    this.shape_122.setTransform(-117,63.4);

    this.shape_123 = new cjs.Shape();
    this.shape_123.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5.3,0.4).curveTo(-5.3,0.1,-5.1,0).lineTo(-2.3,-1.5).lineTo(-0.6,-4.5).lineTo(1.6,-1.7).lineTo(4.5,0).lineTo(1.7,2.2).lineTo(0,5.1).curveTo(-0.5,5.4,-0.6,5.1).lineTo(-2.2,2.3).lineTo(-2.3,2.2).lineTo(-5.1,0.7).curveTo(-5.3,0.6,-5.3,0.4).closePath();
    this.shape_123.setTransform(-116.7,73);
    this.shape_123._off = true;

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_122,p:{y:63.4}}]}).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_122,p:{y:103.4}}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_123}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_123).wait(1).to({_off:false},0).wait(1).to({y:83},0).wait(1).to({y:93},0).to({_off:true},1).wait(1).to({_off:false,y:103.3},0).wait(1).to({y:103.5},0).wait(1).to({y:103.7},0).wait(1).to({y:104},0).wait(1).to({y:104.2},0).wait(1).to({y:104.5},0).wait(1).to({y:104.7},0).wait(1).to({y:104.9},0).wait(1).to({y:105.2},0).wait(1).to({y:105.4},0).wait(1).to({y:105.6},0).wait(1).to({y:105.9},0).wait(1).to({y:106.1},0).wait(1).to({y:106.4},0).wait(1).to({y:106.6},0).wait(1).to({y:106.8},0).wait(1).to({y:107.1},0).wait(1).to({y:107.3},0).to({_off:true},1).wait(3));

    // Layer_10
    this.shape_124 = new cjs.Shape();
    this.shape_124.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.9).lineTo(4.8,-0.3).curveTo(5,-0.2,5,0).curveTo(5,0.2,4.8,0.3).lineTo(2,1.9).lineTo(1.8,2).lineTo(0.3,4.8).curveTo(0.2,5,0,5).curveTo(-0.2,5,-0.3,4.8).lineTo(-1.8,2).lineTo(-2,1.9).lineTo(-4.8,0.3).curveTo(-5,0.2,-5,0).curveTo(-5,-0.2,-4.8,-0.3).lineTo(-2,-1.9).lineTo(-1.8,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.8,-2).closePath();
    this.shape_124.setTransform(150.7,68.3);

    this.shape_125 = new cjs.Shape();
    this.shape_125.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5.3,0.4).curveTo(-5.3,0.1,-5.1,0).lineTo(-2.3,-1.5).lineTo(-0.6,-4.5).lineTo(1.6,-1.7).lineTo(4.5,0).lineTo(1.7,2.2).lineTo(0,5.2).curveTo(-0.5,5.4,-0.6,5.2).lineTo(-2.1,2.3).lineTo(-5.1,0.7).curveTo(-5.3,0.6,-5.3,0.4).closePath();
    this.shape_125.setTransform(152.2,78);
    this.shape_125._off = true;

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_124,p:{x:150.7,y:68.3}}]}).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_124,p:{x:155.7,y:108.3}}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_125}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_125).wait(1).to({_off:false},0).wait(1).to({x:153.4,y:88},0).wait(1).to({x:154.7,y:98},0).to({_off:true},1).wait(1).to({_off:false,x:155.9,y:108.2},0).wait(1).to({y:108.5},0).wait(1).to({y:108.7},0).wait(1).to({y:108.9},0).wait(1).to({y:109.2},0).wait(1).to({y:109.4},0).wait(1).to({y:109.6},0).wait(1).to({y:109.9},0).wait(1).to({y:110.1},0).wait(1).to({y:110.4},0).wait(1).to({y:110.6},0).wait(1).to({y:110.8},0).wait(1).to({y:111.1},0).wait(1).to({y:111.3},0).wait(1).to({y:111.5},0).wait(1).to({y:111.8},0).wait(1).to({y:112},0).wait(1).to({y:112.3},0).to({_off:true},1).wait(3));

    // Layer_11
    this.shape_126 = new cjs.Shape();
    this.shape_126.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,-0,-2.7).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.8,-0,2.8).curveTo(-0.4,2.8,-0.4,2.3).closePath();
    this.shape_126.setTransform(131.3,61.3);

    this.shape_127 = new cjs.Shape();
    this.shape_127.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.4).curveTo(-0.4,-2.7,-0,-2.7).curveTo(0.4,-2.7,0.4,-2.4).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.8,-0,2.8).curveTo(-0.4,2.8,-0.4,2.3).closePath();
    this.shape_127.setTransform(131.3,71.3);
    this.shape_127._off = true;

    this.shape_128 = new cjs.Shape();
    this.shape_128.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.4).curveTo(-0.4,-2.7,-0,-2.7).curveTo(0.4,-2.7,0.4,-2.4).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,-0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_128.setTransform(131.3,91.3);

    this.shape_129 = new cjs.Shape();
    this.shape_129.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.4).curveTo(-0.4,-2.8,-0,-2.8).curveTo(0.4,-2.8,0.4,-2.4).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,-0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_129.setTransform(131.3,101.3);

    this.shape_130 = new cjs.Shape();
    this.shape_130.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.8,-0,-2.8).curveTo(0.4,-2.8,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.7,-0,2.7).curveTo(-0.4,2.7,-0.4,2.3).closePath();
    this.shape_130.setTransform(131.3,102.5);

    this.shape_131 = new cjs.Shape();
    this.shape_131.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.8,-0,-2.8).curveTo(0.4,-2.8,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,-0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_131.setTransform(131.3,102.8);

    this.shape_132 = new cjs.Shape();
    this.shape_132.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.4).curveTo(-0.4,-2.7,-0,-2.7).curveTo(0.4,-2.7,0.4,-2.4).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.8,-0,2.8).curveTo(-0.4,2.8,-0.4,2.4).closePath();
    this.shape_132.setTransform(131.3,103.9);

    this.shape_133 = new cjs.Shape();
    this.shape_133.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,-0,-2.8).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.7,-0,2.8).curveTo(-0.4,2.7,-0.4,2.3).closePath();
    this.shape_133.setTransform(131.3,104.4);

    this.shape_134 = new cjs.Shape();
    this.shape_134.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,-0,-2.8).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.7,-0,2.8).curveTo(-0.4,2.7,-0.4,2.3).closePath();
    this.shape_134.setTransform(131.3,104.9);

    this.shape_135 = new cjs.Shape();
    this.shape_135.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.4).curveTo(-0.4,-2.7,-0,-2.8).curveTo(0.4,-2.7,0.4,-2.4).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,-0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_135.setTransform(131.3,105.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_126,p:{y:61.3}}]}).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128,p:{y:91.3}}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_126,p:{y:102.3}}]},1).to({state:[{t:this.shape_130,p:{y:102.5}}]},1).to({state:[{t:this.shape_131,p:{y:102.8}}]},1).to({state:[{t:this.shape_130,p:{y:103}}]},1).to({state:[{t:this.shape_131,p:{y:103.2}}]},1).to({state:[{t:this.shape_128,p:{y:103.5}}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_132}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_133}]},1).to({state:[{t:this.shape_131,p:{y:104.7}}]},1).to({state:[{t:this.shape_134}]},1).to({state:[{t:this.shape_131,p:{y:105.1}}]},1).to({state:[{t:this.shape_135}]},1).to({state:[{t:this.shape_127}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_127).wait(1).to({_off:false},0).wait(1).to({y:81.3},0).to({_off:true},1).wait(2).to({_off:false,y:101.6},0).wait(1).to({y:101.8},0).wait(1).to({y:102},0).to({_off:true},1).wait(6).to({_off:false,y:103.7},0).to({_off:true},1).wait(1).to({_off:false,y:104.2},0).to({_off:true},1).wait(5).to({_off:false,y:105.6},0).to({_off:true},1).wait(3));

    // Layer_12
    this.shape_136 = new cjs.Shape();
    this.shape_136.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.5).lineTo(-0.6,0.6).lineTo(-3.4,0.6).curveTo(-4.1,0.6,-4.1,-0).curveTo(-4.1,-0.6,-3.4,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.5).curveTo(-0.6,-4.1,0,-4.1).curveTo(0.6,-4.1,0.6,-3.5).lineTo(0.6,-0.6).lineTo(3.5,-0.6).curveTo(4.1,-0.6,4.1,-0).curveTo(4.1,0.6,3.5,0.6).lineTo(0.6,0.6).lineTo(0.6,3.5).curveTo(0.6,3.6,0.6,3.6).curveTo(0.6,3.7,0.6,3.8).curveTo(0.5,3.8,0.5,3.8).curveTo(0.5,3.9,0.4,3.9).curveTo(0.4,4,0.3,4).curveTo(0.3,4,0.3,4.1).curveTo(0.2,4.1,0.1,4.1).curveTo(0.1,4.1,0,4.1).curveTo(-0.6,4.1,-0.6,3.5).closePath();
    this.shape_136.setTransform(-126.4,-5.3);

    this.shape_137 = new cjs.Shape();
    this.shape_137.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.5).lineTo(-0.6,0.6).lineTo(-3.4,0.6).curveTo(-4.1,0.6,-4.1,-0).curveTo(-4.1,-0.6,-3.4,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.5).curveTo(-0.6,-4.1,0,-4.1).curveTo(0.6,-4.1,0.6,-3.5).lineTo(0.6,-0.6).lineTo(3.5,-0.6).curveTo(4.1,-0.6,4.1,-0).curveTo(4.1,0.6,3.5,0.6).lineTo(0.6,0.6).lineTo(0.6,3.5).curveTo(0.6,3.6,0.6,3.6).curveTo(0.6,3.7,0.6,3.8).curveTo(0.6,3.8,0.5,3.8).curveTo(0.5,3.9,0.4,3.9).curveTo(0.4,4,0.3,4).curveTo(0.3,4,0.2,4.1).curveTo(0.2,4.1,0.1,4.1).curveTo(0.1,4.1,0,4.1).curveTo(-0.6,4.1,-0.6,3.5).closePath();
    this.shape_137.setTransform(-143.9,-5.3);
    this.shape_137._off = true;

    this.shape_138 = new cjs.Shape();
    this.shape_138.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.5).lineTo(-0.6,0.6).lineTo(-3.4,0.6).curveTo(-4.1,0.6,-4.1,-0).curveTo(-4.1,-0.6,-3.4,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.5).curveTo(-0.6,-4.1,0,-4.1).curveTo(0.6,-4.1,0.6,-3.5).lineTo(0.6,-0.6).lineTo(3.5,-0.6).curveTo(4.1,-0.6,4.1,-0).curveTo(4.1,0.6,3.5,0.6).lineTo(0.6,0.6).lineTo(0.6,3.5).curveTo(0.6,3.6,0.6,3.6).curveTo(0.6,3.7,0.6,3.8).curveTo(0.6,3.8,0.5,3.8).curveTo(0.5,3.9,0.4,3.9).curveTo(0.4,4,0.3,4).curveTo(0.3,4,0.3,4.1).curveTo(0.2,4.1,0.1,4.1).curveTo(0.1,4.1,0,4.1).curveTo(-0.6,4.1,-0.6,3.5).closePath();
    this.shape_138.setTransform(-178.9,-5.3);
    this.shape_138._off = true;

    this.shape_139 = new cjs.Shape();
    this.shape_139.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.5).lineTo(-0.6,0.6).lineTo(-3.4,0.6).curveTo(-4.1,0.6,-4.1,-0).curveTo(-4.1,-0.6,-3.4,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.5).curveTo(-0.6,-4.1,0,-4.1).curveTo(0.6,-4.1,0.6,-3.5).lineTo(0.6,-0.6).lineTo(3.5,-0.6).curveTo(4.1,-0.6,4.1,-0).curveTo(4.1,0.6,3.5,0.6).lineTo(0.6,0.6).lineTo(0.6,3.5).curveTo(0.6,3.6,0.6,3.6).curveTo(0.6,3.7,0.6,3.8).curveTo(0.5,3.8,0.5,3.8).curveTo(0.5,3.9,0.4,3.9).curveTo(0.4,4,0.3,4).curveTo(0.3,4,0.2,4.1).curveTo(0.2,4.1,0.1,4.1).curveTo(0.1,4.1,0,4.1).curveTo(-0.6,4.1,-0.6,3.5).closePath();
    this.shape_139.setTransform(-196.4,-5.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_136}]}).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_139,p:{x:-196.4}}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_139,p:{x:-198.6}}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_139,p:{x:-199}}]},1).to({state:[{t:this.shape_137}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_138}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_139,p:{x:-200.5}}]},1).to({state:[{t:this.shape_137}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_136).to({_off:true},1).wait(1).to({_off:false,x:-161.4},0).to({_off:true},1).wait(6).to({_off:false,x:-197.6},0).wait(1).to({x:-197.9},0).wait(1).to({x:-198.1},0).wait(1).to({x:-198.3},0).to({_off:true},1).wait(5).to({_off:false,x:-199.8},0).to({_off:true},1).wait(1).to({_off:false,x:-200.2},0).to({_off:true},1).wait(5));
    this.timeline.addTween(cjs.Tween.get(this.shape_137).wait(1).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false,x:-196.7},0).to({_off:true},1).wait(1).to({_off:false,x:-197.1},0).to({_off:true},1).wait(6).to({_off:false,x:-198.8},0).to({_off:true},1).wait(1).to({_off:false,x:-199.3},0).to({_off:true},1).wait(5).to({_off:false,x:-200.7},0).to({_off:true},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_138).wait(3).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false,x:-196.9},0).to({_off:true},1).wait(1).to({_off:false,x:-197.4},0).to({_off:true},1).wait(8).to({_off:false,x:-199.5},0).to({_off:true},1).wait(1).to({_off:false,x:-200},0).to({_off:true},1).wait(6));

    // Layer_13
    this.shape_140 = new cjs.Shape();
    this.shape_140.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.2,0.4).curveTo(-2.6,0.4,-2.6,0).curveTo(-2.6,-0.4,-2.2,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.2).curveTo(-0.4,-2.6,-0,-2.6).curveTo(0.3,-2.6,0.4,-2.2).lineTo(0.4,-0.4).lineTo(2.2,-0.4).curveTo(2.6,-0.4,2.6,0).curveTo(2.6,0.4,2.2,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.4,0.3,2.4).curveTo(0.3,2.5,0.3,2.5).curveTo(0.2,2.6,0.2,2.6).curveTo(0.1,2.6,-0,2.6).curveTo(-0.4,2.6,-0.4,2.3).closePath();
    this.shape_140.setTransform(-83.2,28.9);

    this.timeline.addTween(cjs.Tween.get(this.shape_140).wait(1).to({y:48.9},0).wait(1).to({y:68.9},0).wait(1).to({y:88.9},0).wait(1).to({y:108.9},0).wait(1).to({y:109.1},0).wait(1).to({y:109.4},0).wait(1).to({y:109.6},0).wait(1).to({y:109.8},0).wait(1).to({y:110.1},0).wait(1).to({y:110.3},0).wait(1).to({y:110.5},0).wait(1).to({y:110.8},0).wait(1).to({y:111},0).wait(1).to({y:111.3},0).wait(1).to({y:111.5},0).wait(1).to({y:111.7},0).wait(1).to({y:112},0).wait(1).to({y:112.2},0).wait(1).to({y:112.4},0).wait(1).to({y:112.7},0).wait(1).to({y:112.9},0).wait(1).to({y:113.2},0).to({_off:true},1).wait(3));

    // Layer_14
    this.shape_141 = new cjs.Shape();
    this.shape_141.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3.1,0,3).curveTo(1.3,3.1,2.1,2.1).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.2,-3,-0,-3.1).curveTo(-1.3,-3,-2.1,-2.2).closePath();
    this.shape_141.setTransform(-4.2,-24.1);

    this.shape_142 = new cjs.Shape();
    this.shape_142.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3.1,0,3).curveTo(1.3,3.1,2.1,2.1).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_142.setTransform(-4.2,-45.3);
    this.shape_142._off = true;

    this.shape_143 = new cjs.Shape();
    this.shape_143.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3.1,0,3.1).curveTo(1.3,3.1,2.1,2.2).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_143.setTransform(-4.2,-66.6);

    this.shape_144 = new cjs.Shape();
    this.shape_144.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.2).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.3,3,2.1,2.2).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.2).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.2).closePath();
    this.shape_144.setTransform(-4.2,-87.8);

    this.shape_145 = new cjs.Shape();
    this.shape_145.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.1).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.3,3,2.1,2.2).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.1).curveTo(1.2,-3.1,-0,-3).curveTo(-1.3,-3.1,-2.1,-2.1).closePath();
    this.shape_145.setTransform(-4.2,-109.1);

    this.shape_146 = new cjs.Shape();
    this.shape_146.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,-0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0).lineTo(4.1,-0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.1).curveTo(-2.9,-1.3,-2.9,-0).lineTo(-2.9,-0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3,0,3).curveTo(1.3,3,2.1,2.1).curveTo(2.9,1.3,2.9,-0).curveTo(2.9,-1.3,2,-2.1).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.1).closePath();
    this.shape_146.setTransform(-4.2,-110.3);

    this.shape_147 = new cjs.Shape();
    this.shape_147.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.1).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.2).curveTo(-1.2,3,0,3.1).curveTo(1.3,3,2.1,2.2).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.1).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.1).closePath();
    this.shape_147.setTransform(-4.2,-110.5);

    this.shape_148 = new cjs.Shape();
    this.shape_148.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.9,2.9).curveTo(-4.1,1.7,-4.1,0).curveTo(-4.1,-1.7,-2.9,-2.9).curveTo(-1.7,-4.1,0,-4.1).curveTo(1.8,-4.1,2.9,-2.9).curveTo(4.1,-1.7,4.1,-0.1).lineTo(4.1,0).curveTo(4.1,1.7,2.9,2.9).curveTo(1.7,4.1,-0,4.1).curveTo(-1.8,4.1,-2.9,2.9).closePath().moveTo(-2.1,-2.1).curveTo(-2.9,-1.3,-2.9,-0.1).lineTo(-2.9,0).curveTo(-2.9,1.3,-2,2.1).curveTo(-1.2,3,0,3).curveTo(1.3,3,2.1,2.1).curveTo(2.9,1.3,2.9,0).curveTo(2.9,-1.3,2,-2.1).curveTo(1.6,-2.6,0.6,-2.6).curveTo(-0.4,-2.6,-2.1,-2.1).closePath();
    this.shape_148.setTransform(-4.2,-112.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_141}]}).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_143,p:{y:-66.6}}]},1).to({state:[{t:this.shape_144,p:{y:-87.8}}]},1).to({state:[{t:this.shape_145}]},1).to({state:[{t:this.shape_143,p:{y:-109.3}}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_143,p:{y:-109.8}}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_146,p:{y:-110.3}}]},1).to({state:[{t:this.shape_147,p:{y:-110.5}}]},1).to({state:[{t:this.shape_146,p:{y:-110.7}}]},1).to({state:[{t:this.shape_147,p:{y:-111}}]},1).to({state:[{t:this.shape_144,p:{y:-111.2}}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_144,p:{y:-111.7}}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_148,p:{y:-112.4}}]},1).to({state:[{t:this.shape_142}]},1).to({state:[{t:this.shape_148,p:{y:-112.9}}]},1).to({state:[{t:this.shape_147,p:{y:-113.1}}]},1).to({state:[{t:this.shape_143,p:{y:-113.4}}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_142).wait(1).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false,y:-109.6},0).to({_off:true},1).wait(1).to({_off:false,y:-110},0).to({_off:true},1).wait(5).to({_off:false,y:-111.5},0).to({_off:true},1).wait(1).to({_off:false,y:-111.9},0).wait(1).to({y:-112.2},0).to({_off:true},1).wait(1).to({_off:false,y:-112.6},0).to({_off:true},1).wait(6));

    // Layer_15
    this.shape_149 = new cjs.Shape();
    this.shape_149.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.9).lineTo(4.8,-0.3).curveTo(5,-0.2,5,0).curveTo(5,0.2,4.8,0.3).lineTo(2,1.9).curveTo(1.9,1.9,1.9,2).lineTo(0.3,4.8).curveTo(0.2,5,0,5).curveTo(-0.2,5,-0.3,4.8).lineTo(-1.8,2).lineTo(-2,1.9).lineTo(-4.8,0.3).curveTo(-5,0.2,-5,0).curveTo(-5,-0.2,-4.8,-0.3).lineTo(-2,-1.9).lineTo(-1.8,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).curveTo(1.9,-1.9,2,-1.9).closePath();
    this.shape_149.setTransform(114.4,-60.6);

    this.shape_150 = new cjs.Shape();
    this.shape_150.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5.3,0.4).curveTo(-5.3,0.1,-5.1,0).lineTo(-2.3,-1.5).lineTo(-0.6,-4.5).lineTo(1.6,-1.6).lineTo(4.5,0).lineTo(1.7,2.2).lineTo(0,5.2).curveTo(-0.5,5.4,-0.6,5.2).lineTo(-2.1,2.4).lineTo(-2.3,2.2).lineTo(-5.1,0.7).curveTo(-5.3,0.6,-5.3,0.4).closePath();
    this.shape_150.setTransform(114.7,-69.7);
    this.shape_150._off = true;

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_149,p:{y:-60.6}}]}).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_149,p:{y:-95.6}}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[{t:this.shape_150}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_150).wait(1).to({_off:false},0).wait(1).to({y:-78.5},0).wait(1).to({y:-87.2},0).to({_off:true},1).wait(1).to({_off:false,x:114.9,y:-96.2},0).wait(1).to({x:115.2,y:-96.5},0).wait(1).to({x:115.4,y:-96.7},0).wait(1).to({x:115.6,y:-96.9},0).wait(1).to({x:115.9,y:-97.2},0).wait(1).to({x:116.1,y:-97.4},0).wait(1).to({x:116.3,y:-97.6},0).wait(1).to({x:116.6,y:-97.9},0).wait(1).to({x:116.8,y:-98.1},0).wait(1).to({x:117.1,y:-98.4},0).wait(1).to({x:117.3,y:-98.6},0).wait(1).to({x:117.5,y:-98.8},0).wait(1).to({x:117.8,y:-99.1},0).wait(1).to({x:118,y:-99.3},0).wait(1).to({x:118.2,y:-99.5},0).wait(1).to({x:118.5,y:-99.8},0).wait(1).to({x:118.7,y:-100},0).wait(1).to({x:119,y:-100.3},0).to({_off:true},1).wait(3));

    // Layer_16
    this.shape_151 = new cjs.Shape();
    this.shape_151.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.9).lineTo(4.9,-0.3).curveTo(5.1,-0.2,5.1,0).curveTo(5.1,0.3,4.9,0.3).lineTo(2,1.9).lineTo(1.9,2).lineTo(0.3,4.9).curveTo(0.2,5.1,0,5.1).curveTo(-0.2,5.1,-0.3,4.9).lineTo(-1.9,2).lineTo(-2,1.9).lineTo(-4.9,0.3).curveTo(-5.1,0.3,-5.1,0).curveTo(-5.1,-0.2,-4.9,-0.3).lineTo(-2,-1.9).lineTo(-1.9,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).closePath();
    this.shape_151.setTransform(-75.6,-61.6);

    this.shape_152 = new cjs.Shape();
    this.shape_152.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5.4,0.3).curveTo(-5.4,0.1,-5.2,-0).lineTo(-2.3,-1.6).lineTo(-0.6,-4.5).lineTo(1.6,-1.7).lineTo(4.6,-0).lineTo(1.7,2.2).lineTo(0,5.2).curveTo(-0.5,5.4,-0.6,5.2).lineTo(-2.2,2.3).lineTo(-2.3,2.2).lineTo(-5.2,0.6).curveTo(-5.4,0.6,-5.4,0.3).closePath();
    this.shape_152.setTransform(-75.3,-69.5);
    this.shape_152._off = true;

    this.shape_153 = new cjs.Shape();
    this.shape_153.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2,-1.9).lineTo(4.9,-0.3).curveTo(5.1,-0.2,5.1,0).curveTo(5.1,0.2,4.9,0.3).lineTo(2,1.9).lineTo(1.9,2).lineTo(0.3,4.9).curveTo(0.2,5.1,0,5.1).curveTo(-0.2,5.1,-0.3,4.9).lineTo(-1.9,2).lineTo(-2,1.9).lineTo(-4.9,0.3).curveTo(-5.1,0.2,-5.1,0).curveTo(-5.1,-0.2,-4.9,-0.3).lineTo(-2,-1.9).lineTo(-1.9,-2).lineTo(-0.3,-4.8).curveTo(-0.2,-5,0,-5).curveTo(0.2,-5,0.3,-4.8).lineTo(1.9,-2).closePath();
    this.shape_153.setTransform(-75.6,-91.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_151}]}).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_153}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[{t:this.shape_152}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_152).wait(1).to({_off:false},0).wait(1).to({y:-77},0).wait(1).to({y:-84.5},0).to({_off:true},1).wait(1).to({_off:false,y:-92.2},0).wait(1).to({y:-92.5},0).wait(1).to({y:-92.7},0).wait(1).to({y:-92.9},0).wait(1).to({y:-93.2},0).wait(1).to({y:-93.4},0).wait(1).to({y:-93.6},0).wait(1).to({y:-93.9},0).wait(1).to({y:-94.1},0).wait(1).to({y:-94.4},0).wait(1).to({y:-94.6},0).wait(1).to({y:-94.8},0).wait(1).to({y:-95.1},0).wait(1).to({y:-95.3},0).wait(1).to({y:-95.5},0).wait(1).to({y:-95.8},0).wait(1).to({y:-96},0).wait(1).to({y:-96.3},0).to({_off:true},1).wait(3));

    // Layer_17
    this.shape_154 = new cjs.Shape();
    this.shape_154.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,0,-2.7).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_154.setTransform(24.6,22.4);

    this.shape_155 = new cjs.Shape();
    this.shape_155.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.4).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,0,-2.7).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.4).curveTo(0.4,2.7,0,2.7).curveTo(-0.4,2.7,-0.4,2.4).closePath();
    this.shape_155.setTransform(24.6,-6.3);

    this.shape_156 = new cjs.Shape();
    this.shape_156.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,-0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,0,-2.7).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,-0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.7,0,2.7).curveTo(-0.4,2.7,-0.4,2.3).closePath();
    this.shape_156.setTransform(24.6,-35.1);
    this.shape_156._off = true;

    this.shape_157 = new cjs.Shape();
    this.shape_157.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.4,2.3).lineTo(-0.4,0.4).lineTo(-2.3,0.4).curveTo(-2.7,0.4,-2.7,0).curveTo(-2.7,-0.4,-2.3,-0.4).lineTo(-0.4,-0.4).lineTo(-0.4,-2.3).curveTo(-0.4,-2.7,0,-2.7).curveTo(0.4,-2.7,0.4,-2.3).lineTo(0.4,-0.4).lineTo(2.3,-0.4).curveTo(2.7,-0.4,2.7,0).curveTo(2.7,0.4,2.3,0.4).lineTo(0.4,0.4).lineTo(0.4,2.3).curveTo(0.4,2.7,0,2.7).curveTo(-0.4,2.7,-0.4,2.3).closePath();
    this.shape_157.setTransform(24.6,-95.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_154}]}).to({state:[{t:this.shape_155,p:{y:-6.3}}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_155,p:{y:-95}}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_155,p:{y:-95.4}}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_157,p:{y:-95.9}}]},1).to({state:[{t:this.shape_154}]},1).to({state:[{t:this.shape_157,p:{y:-96.4}}]},1).to({state:[{t:this.shape_156}]},1).to({state:[{t:this.shape_156}]},1).to({state:[]},1).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.shape_154).to({_off:true},1).wait(2).to({_off:false,y:-63.8},0).to({_off:true},1).wait(2).to({_off:false,y:-93.1},0).to({_off:true},1).wait(1).to({_off:false,y:-93.5},0).wait(1).to({y:-93.8},0).to({_off:true},1).wait(1).to({_off:false,y:-94.2},0).to({_off:true},1).wait(5).to({_off:false,y:-95.7},0).to({_off:true},1).wait(1).to({_off:false,y:-96.1},0).to({_off:true},1).wait(6));
    this.timeline.addTween(cjs.Tween.get(this.shape_156).wait(2).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,y:-92.6},0).wait(1).to({y:-92.8},0).to({_off:true},1).wait(1).to({_off:false,y:-93.3},0).to({_off:true},1).wait(2).to({_off:false,y:-94},0).to({_off:true},1).wait(1).to({_off:false,y:-94.5},0).wait(1).to({y:-94.7},0).to({_off:true},1).wait(1).to({_off:false,y:-95.2},0).to({_off:true},1).wait(5).to({_off:false,y:-96.6},0).wait(1).to({y:-96.9},0).to({_off:true},1).wait(3));

    // Layer_18
    this.shape_158 = new cjs.Shape();
    this.shape_158.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.6).lineTo(-3.2,0.6).curveTo(-3.7,0.5,-3.7,0).curveTo(-3.7,-0.6,-3.2,-0.5).lineTo(-0.6,-0.5).lineTo(-0.6,-3.2).curveTo(-0.5,-3.7,-0,-3.8).curveTo(0.6,-3.7,0.5,-3.2).lineTo(0.5,-0.5).lineTo(3.1,-0.5).curveTo(3.7,-0.6,3.7,0).curveTo(3.7,0.5,3.1,0.6).lineTo(0.5,0.6).lineTo(0.5,3.2).curveTo(0.6,3.8,-0,3.7).curveTo(-0.5,3.8,-0.6,3.2).closePath();
    this.shape_158.setTransform(-40.8,-64.5);

    this.shape_159 = new cjs.Shape();
    this.shape_159.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.6).lineTo(-3.2,0.6).curveTo(-3.7,0.5,-3.7,0).curveTo(-3.7,-0.6,-3.2,-0.5).lineTo(-0.6,-0.5).lineTo(-0.6,-3.2).curveTo(-0.6,-3.7,-0,-3.8).curveTo(0.6,-3.7,0.6,-3.2).lineTo(0.6,-0.5).lineTo(3.1,-0.5).curveTo(3.7,-0.6,3.7,0).curveTo(3.7,0.5,3.1,0.6).lineTo(0.6,0.6).lineTo(0.6,3.2).curveTo(0.6,3.8,-0,3.7).curveTo(-0.6,3.8,-0.6,3.2).closePath();
    this.shape_159.setTransform(-112.1,-98.3);

    this.shape_160 = new cjs.Shape();
    this.shape_160.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.6).lineTo(-3.1,0.6).curveTo(-3.7,0.5,-3.7,0).curveTo(-3.7,-0.6,-3.1,-0.5).lineTo(-0.6,-0.5).lineTo(-0.6,-3.2).curveTo(-0.6,-3.7,0,-3.8).curveTo(0.6,-3.7,0.6,-3.2).lineTo(0.6,-0.5).lineTo(3.2,-0.5).curveTo(3.7,-0.6,3.7,0).curveTo(3.7,0.5,3.2,0.6).lineTo(0.6,0.6).lineTo(0.6,3.2).curveTo(0.6,3.8,0,3.7).curveTo(-0.6,3.8,-0.6,3.2).closePath();
    this.shape_160.setTransform(-135.8,-109.5);

    this.shape_161 = new cjs.Shape();
    this.shape_161.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.5).lineTo(-3.1,0.5).curveTo(-3.7,0.5,-3.7,-0).curveTo(-3.7,-0.5,-3.1,-0.5).lineTo(-0.6,-0.5).lineTo(-0.6,-3.2).curveTo(-0.6,-3.8,0,-3.8).curveTo(0.6,-3.8,0.6,-3.2).lineTo(0.6,-0.5).lineTo(3.2,-0.5).curveTo(3.7,-0.5,3.7,-0).curveTo(3.7,0.5,3.2,0.5).lineTo(0.6,0.5).lineTo(0.6,3.2).curveTo(0.6,3.8,0,3.8).curveTo(-0.6,3.8,-0.6,3.2).closePath();
    this.shape_161.setTransform(-135.8,-109.8);

    this.shape_162 = new cjs.Shape();
    this.shape_162.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.5).lineTo(-3.1,0.5).curveTo(-3.7,0.6,-3.7,-0).curveTo(-3.7,-0.5,-3.1,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.2).curveTo(-0.6,-3.8,0,-3.7).curveTo(0.6,-3.8,0.6,-3.2).lineTo(0.6,-0.6).lineTo(3.2,-0.6).curveTo(3.7,-0.5,3.7,-0).curveTo(3.7,0.6,3.2,0.5).lineTo(0.6,0.5).lineTo(0.6,3.2).curveTo(0.6,3.7,0,3.8).curveTo(-0.6,3.7,-0.6,3.2).closePath();
    this.shape_162.setTransform(-135.8,-110);

    this.shape_163 = new cjs.Shape();
    this.shape_163.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.6).lineTo(-3.1,0.6).curveTo(-3.7,0.6,-3.7,-0).curveTo(-3.7,-0.6,-3.1,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.2).curveTo(-0.6,-3.8,0,-3.7).curveTo(0.6,-3.8,0.6,-3.2).lineTo(0.6,-0.6).lineTo(3.2,-0.6).curveTo(3.7,-0.6,3.7,-0).curveTo(3.7,0.6,3.2,0.6).lineTo(0.6,0.6).lineTo(0.6,3.2).curveTo(0.6,3.7,0,3.7).curveTo(-0.6,3.7,-0.6,3.2).closePath();
    this.shape_163.setTransform(-135.8,-110.7);

    this.shape_164 = new cjs.Shape();
    this.shape_164.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.5).lineTo(-3.1,0.5).curveTo(-3.7,0.5,-3.7,0).curveTo(-3.7,-0.6,-3.1,-0.5).lineTo(-0.6,-0.5).lineTo(-0.6,-3.2).curveTo(-0.6,-3.7,0,-3.8).curveTo(0.6,-3.7,0.6,-3.2).lineTo(0.6,-0.5).lineTo(3.2,-0.5).curveTo(3.7,-0.6,3.7,0).curveTo(3.7,0.5,3.2,0.5).lineTo(0.6,0.5).lineTo(0.6,3.2).curveTo(0.6,3.8,0,3.8).curveTo(-0.6,3.8,-0.6,3.2).closePath();
    this.shape_164.setTransform(-135.8,-111.7);

    this.shape_165 = new cjs.Shape();
    this.shape_165.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.5).lineTo(-3.1,0.5).curveTo(-3.7,0.6,-3.7,-0).curveTo(-3.7,-0.5,-3.1,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.2).curveTo(-0.6,-3.8,0,-3.8).curveTo(0.6,-3.8,0.6,-3.2).lineTo(0.6,-0.6).lineTo(3.2,-0.6).curveTo(3.7,-0.5,3.7,-0).curveTo(3.7,0.6,3.2,0.5).lineTo(0.6,0.5).lineTo(0.6,3.2).curveTo(0.6,3.7,0,3.8).curveTo(-0.6,3.7,-0.6,3.2).closePath();
    this.shape_165.setTransform(-135.8,-111.9);

    this.shape_166 = new cjs.Shape();
    this.shape_166.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.5).lineTo(-3.1,0.5).curveTo(-3.7,0.6,-3.7,-0).curveTo(-3.7,-0.5,-3.1,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.2).curveTo(-0.6,-3.8,0,-3.7).curveTo(0.6,-3.8,0.6,-3.2).lineTo(0.6,-0.6).lineTo(3.2,-0.6).curveTo(3.7,-0.5,3.7,-0).curveTo(3.7,0.6,3.2,0.5).lineTo(0.6,0.5).lineTo(0.6,3.2).curveTo(0.6,3.7,0,3.7).curveTo(-0.6,3.7,-0.6,3.2).closePath();
    this.shape_166.setTransform(-135.8,-112.6);

    this.shape_167 = new cjs.Shape();
    this.shape_167.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.6,3.2).lineTo(-0.6,0.6).lineTo(-3.1,0.6).curveTo(-3.7,0.6,-3.7,0).curveTo(-3.7,-0.6,-3.1,-0.6).lineTo(-0.6,-0.6).lineTo(-0.6,-3.2).curveTo(-0.6,-3.7,0,-3.7).curveTo(0.6,-3.7,0.6,-3.2).lineTo(0.6,-0.6).lineTo(3.2,-0.6).curveTo(3.7,-0.6,3.7,0).curveTo(3.7,0.6,3.2,0.6).lineTo(0.6,0.6).lineTo(0.6,3.2).curveTo(0.6,3.8,0,3.7).curveTo(-0.6,3.8,-0.6,3.2).closePath();
    this.shape_167.setTransform(-135.8,-112.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_158,p:{x:-40.8,y:-64.5}}]}).to({state:[{t:this.shape_158,p:{x:-64.6,y:-75.8}}]},1).to({state:[{t:this.shape_158,p:{x:-88.3,y:-87}}]},1).to({state:[{t:this.shape_159}]},1).to({state:[{t:this.shape_160,p:{y:-109.5}}]},1).to({state:[{t:this.shape_161,p:{y:-109.8}}]},1).to({state:[{t:this.shape_162,p:{y:-110}}]},1).to({state:[{t:this.shape_161,p:{y:-110.2}}]},1).to({state:[{t:this.shape_162,p:{y:-110.5}}]},1).to({state:[{t:this.shape_163,p:{y:-110.7}}]},1).to({state:[{t:this.shape_160,p:{y:-111}}]},1).to({state:[{t:this.shape_163,p:{y:-111.2}}]},1).to({state:[{t:this.shape_160,p:{y:-111.4}}]},1).to({state:[{t:this.shape_164,p:{y:-111.7}}]},1).to({state:[{t:this.shape_165,p:{y:-111.9}}]},1).to({state:[{t:this.shape_164,p:{y:-112.1}}]},1).to({state:[{t:this.shape_165,p:{y:-112.4}}]},1).to({state:[{t:this.shape_166,p:{y:-112.6}}]},1).to({state:[{t:this.shape_167,p:{y:-112.9}}]},1).to({state:[{t:this.shape_166,p:{y:-113.1}}]},1).to({state:[{t:this.shape_167,p:{y:-113.3}}]},1).to({state:[{t:this.shape_160,p:{y:-113.6}}]},1).to({state:[{t:this.shape_161,p:{y:-113.8}}]},1).to({state:[]},1).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-136.3,-71.2,295.4,153.7);


(lib.aniDrum = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(2.4,2.1).lineTo(3.2,-1.7).moveTo(-1,1.3).lineTo(-3.4,-1.7);
    this.shape.setTransform(-0.5,-29.4);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-2.3,-1.3).lineTo(2.3,-3.9).moveTo(-1.2,3.2).lineTo(2.1,3.8);
    this.shape_1.setTransform(14.2,-14.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},2).to({state:[{t:this.shape_1}]},2).to({state:[]},2).wait(3));

    // Layer 3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-17.1,16.8).lineTo(17.1,-16.7);
    this.shape_2.setTransform(18,-28);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-17,5.5).lineTo(17,-5.5);
    this.shape_3.setTransform(20.2,-33.8);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-16.9,-5.9).lineTo(16.9,5.9);
    this.shape_4.setTransform(22.3,-39.5);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-16.8,-17.1).lineTo(16.8,17.1);
    this.shape_5.setTransform(24.5,-45.2);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-16.8,-10.4).lineTo(16.8,10.3);
    this.shape_6.setTransform(23.2,-41.8);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-16.9,-3.6).lineTo(16.9,3.6);
    this.shape_7.setTransform(21.9,-38.4);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-17,3.2).lineTo(17,-3.2);
    this.shape_8.setTransform(20.6,-34.9);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-17.1,10).lineTo(17.1,-10);
    this.shape_9.setTransform(19.3,-31.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2}]}).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_2}]},1).wait(1));

    // Layer 2
    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(15.6,13).lineTo(-15.7,-13);
    this.shape_10.setTransform(-24.7,-43.9);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-15.7,-13).lineTo(15.6,13);
    this.shape_11.setTransform(-24.7,-42.2);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(11.8,16.6).lineTo(-11.8,-16.6);
    this.shape_12.setTransform(-24.7,-28.3);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-15,-6.3).lineTo(15,6.3);
    this.shape_13.setTransform(-24.7,-38.5);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-17.2,1.2).lineTo(17.3,-1.1);
    this.shape_14.setTransform(-24.7,-45.7);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-18.7,5.5).lineTo(18.6,-5.6);
    this.shape_15.setTransform(-24.7,-50);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(19.1,-7).lineTo(-19.1,7);
    this.shape_16.setTransform(-24.7,-51.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10,p:{y:-43.9}}]}).to({state:[{t:this.shape_11,p:{y:-42.2}}]},1).to({state:[{t:this.shape_11,p:{y:-37}}]},1).to({state:[{t:this.shape_10,p:{y:-28.3}}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).wait(1));

    // Layer 1
    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-47,-22.4).lineTo(-47,-22.8).curveTo(-47,-23.1,-47,-23.3).curveTo(-46.9,-23.7,-46.9,-24.1).curveTo(-45.7,-30.3,-33.2,-34.9).curveTo(-19.5,-39.8,-0,-39.8).curveTo(7,-39.8,13.3,-39.1).curveTo(24.5,-38,33.2,-34.9).curveTo(45.7,-30.3,46.9,-24.1).curveTo(46.9,-23.7,47,-23.2).curveTo(47,-23.2,47,-23.1).lineTo(47,-22.8).curveTo(47,-22.8,47,-22.7).curveTo(47,-22.5,47,-22.4).curveTo(47,-22.3,47,-22.2).curveTo(47,-22.2,47,-22.1).curveTo(47,-22,47,-21.9).curveTo(47,-21.9,47,-21.8).curveTo(47,-21.7,47,-21.7).curveTo(47,-21.6,47,-21.5).curveTo(47,-21.5,47,-21.4).lineTo(47,-21.3).curveTo(47,-21.2,47,-21.1).lineTo(47,-21).curveTo(47,-20.9,47,-20.8).curveTo(47,-20.7,47,-20.7).curveTo(47,-20.6,47,-20.5).curveTo(47,-20.5,47,-20.4).curveTo(47,-20.3,47,-20.3).curveTo(47,-20.2,47,-20.2).curveTo(47,-20.1,47,-20).curveTo(47,-20,47,-19.9).curveTo(47,-19.8,47,-19.7).curveTo(47,-19.7,47,-19.6).curveTo(47,-19.5,47,-19.5).curveTo(47,-19.4,47,-19.4).curveTo(47,-19.3,47,-19.2).curveTo(47,-19.2,47,-19.1).lineTo(47,-18.9).curveTo(47,-18.8,47,-18.7).curveTo(47,-18.6,47,-18.6).lineTo(47,-18.5).curveTo(47,-18.4,47,-18.3).lineTo(47,-18.2).curveTo(47,-18.1,47,-18.1).curveTo(47,-18,47,-17.9).curveTo(47,-17.9,47,-17.8).curveTo(47,-17.7,47,-17.7).curveTo(47,-17.6,47,-17.6).lineTo(47,-17.5).curveTo(47,-17.4,47,-17.3).lineTo(47,-17.2).curveTo(47,-17.1,47,-17.1).curveTo(47,-17,47,-16.9).curveTo(47,-16.9,47,-16.8).curveTo(47,-16.7,47,-16.7).curveTo(47,-16.6,47,-16.6).lineTo(47,-16.5).curveTo(47,-16.4,47,-16.3).curveTo(47,-16.3,47,-16.2).curveTo(47,-16.2,47,-16.1).lineTo(47,-16).curveTo(47,-15.9,47,-15.9).curveTo(47,-15.8,47,-15.7).curveTo(47,-15.7,47,-15.6).curveTo(47,-15.6,47,-15.5).lineTo(47,-15.4).curveTo(47,-15.3,47,-15.3).curveTo(47,-15.2,47,-15.2).lineTo(47,-15.1).curveTo(47,-15,47,-14.9).curveTo(47,-14.9,47,-14.8).curveTo(47,-14.7,47,-14.7).curveTo(47,-14.6,47,-14.6).curveTo(47,-14.5,47,-14.5).curveTo(47,-14.4,47,-14.4).lineTo(47,-14.3).curveTo(47,-14.2,47,-14.1).lineTo(47,-14.1).curveTo(47,-14,47,-13.9).curveTo(47,-13.9,47,-13.8).curveTo(47,-13.7,47,-13.7).lineTo(47,-13.6).curveTo(47,-13.5,47,-13.5).curveTo(47,-13.4,47,-13.4).lineTo(47,-13.3).curveTo(47,-13.2,47,-13.2).lineTo(47,-13.1).curveTo(47,-13,47,-12.9).curveTo(47,-12.9,47,-12.8).curveTo(47,-12.8,47,-12.7).curveTo(47,-12.7,47,-12.6).curveTo(47,-12.6,47,-12.5).lineTo(47,-12.4).curveTo(47,-12.4,47,-12.3).lineTo(47,-12.2).curveTo(47,-12.2,47,-12.1).lineTo(47,-12).curveTo(47,-11.9,47,-11.9).lineTo(47,-11.8).curveTo(47,-11.7,47,-11.7).lineTo(47,-11.6).curveTo(47,-11.5,47,-11.5).lineTo(47,-11.4).curveTo(47,-11.3,47,-11.3).lineTo(47,-11.2).curveTo(47,-11.1,47,-11.1).lineTo(47,-11).curveTo(47,-10.9,47,-10.9).curveTo(47,-10.8,47,-10.8).curveTo(47,-10.7,47,-10.7).lineTo(47,-10.6).curveTo(47,-10.5,47,-10.5).lineTo(47,-10.2).curveTo(47,-10.2,47,-10.1).curveTo(47,-10.1,47,-10).curveTo(47,-10,47,-9.9).curveTo(47,-9.9,47,-9.8).curveTo(47,-9.8,47,-9.7).curveTo(47,-9.7,47,-9.6).lineTo(47,-9.3).curveTo(47,-9.2,47,-9.2).curveTo(47,-9.1,47,-9.1).curveTo(47,-9,47,-9).curveTo(47,-8.9,47,-8.9).lineTo(47,-8.7).curveTo(47,-8.7,47,-8.6).curveTo(47,-8.6,47,-8.5).lineTo(47,-8.3).curveTo(47,-8.2,47,-8.2).curveTo(47,-8.1,47,-8.1).lineTo(47,-8).curveTo(47,-8,47,-7.9).curveTo(47,-7.9,47,-7.8).lineTo(47,-7.7).curveTo(47,-7.6,47,-7.6).curveTo(47,-7.5,47,-7.5).lineTo(47,-7.2).curveTo(47,-7.2,47,-7.1).lineTo(47,-7.1).curveTo(47,-7,47,-7).curveTo(47,-6.9,47,-6.9).lineTo(47,-6.8).curveTo(47,-6.8,47,-6.7).curveTo(47,-6.6,47,-6.6).curveTo(47,-6.5,47,-6.5).lineTo(47,-6.3).curveTo(47,-6.2,47,-6.2).curveTo(47,-6.1,47,-6.1).lineTo(47,-5.9).curveTo(47,-5.9,47,-5.8).lineTo(47,-5.6).curveTo(47,-5.6,47,-5.5).lineTo(47,-5.4).curveTo(47,-5.3,47,-5.3).lineTo(47,-5.2).curveTo(47,-5.2,47,-5.1).lineTo(47,-4.9).curveTo(47,-4.9,47,-4.8).lineTo(47,-4.8).curveTo(47,-4.7,47,-4.7).lineTo(47,-4.3).curveTo(47,-4.2,47,-4.2).curveTo(47,-4.1,47,-4.1).curveTo(47,-4,47,-3.9).lineTo(47,-3.8).curveTo(47,-3.8,47,-3.7).lineTo(47,14.9).curveTo(47,15,47,15.1).curveTo(47,22.5,47,22.8).curveTo(47,29.9,33.2,34.8).curveTo(19.5,39.8,-0,39.8).curveTo(-19.5,39.8,-33.2,34.8).curveTo(-47,29.9,-47,22.8).curveTo(-47,22.5,-47,15.1).curveTo(-47,14.9,-47,14.9).curveTo(-47,14.3,-47,-10.9).curveTo(-47,-11.1,-47,-11.3).curveTo(-47,-11.5,-47,-11.7).curveTo(-47,-11.9,-47,-12.1).curveTo(-47,-12.3,-47,-12.5).curveTo(-47,-12.7,-47,-12.9).curveTo(-47,-13.2,-47,-13.4).curveTo(-47,-13.6,-47,-13.9).curveTo(-47,-14.1,-47,-14.3).curveTo(-47,-14.5,-47,-14.7).curveTo(-47,-14.9,-47,-15.2).curveTo(-47,-15.4,-47,-15.7).curveTo(-47,-15.9,-47,-16.1).curveTo(-47,-16.4,-47,-16.6).curveTo(-47,-16.9,-47,-17.1).curveTo(-47,-17.3,-47,-17.6).curveTo(-47,-17.9,-47,-18.1).curveTo(-47,-18.3,-47,-18.6).curveTo(-47,-18.9,-47,-19.1).curveTo(-47,-19.4,-47,-19.6).curveTo(-47,-19.9,-47,-20.2).curveTo(-47,-20.5,-47,-20.7).curveTo(-47,-21,-47,-21.3).curveTo(-47,-21.5,-47,-21.9).curveTo(-47,-22.1,-47,-22.4).closePath().moveTo(1.6,-5.8).curveTo(0.8,-5.8,-0,-5.8).curveTo(-14.1,-5.8,-25.1,-8.4).curveTo(-29.4,-9.4,-33.2,-10.8).curveTo(-46.6,-15.6,-47,-22.4).moveTo(1.6,31.8).curveTo(0.8,31.8,-0,31.8).curveTo(-14.1,31.8,-25.1,29.2).curveTo(-29.4,28.2,-33.2,26.9).curveTo(-46.8,21.9,-47,15.1).moveTo(-25.1,-8.4).lineTo(-25.1,29.2).moveTo(29.4,-9.5).curveTo(17.4,-6,1.6,-5.8).lineTo(1.6,31.8).moveTo(29.4,28.1).curveTo(17.4,31.7,1.6,31.8).moveTo(47,15.1).curveTo(46.7,22,33.2,26.9).curveTo(31.3,27.5,29.4,28.1).moveTo(29.4,-9.5).lineTo(29.4,28.1).moveTo(47,-22.4).curveTo(46.5,-15.6,33.2,-10.8).curveTo(31.3,-10.1,29.4,-9.5);
    this.shape_17.setTransform(0,15.7);

    this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48,-57.9,96,114.5);


(lib.Symbol = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-5.9).lineTo(0,0).lineTo(0,5.9).moveTo(-5.9,0).lineTo(0,0).lineTo(5.9,0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol, new cjs.Rectangle(-6.9,-6.9,13.8,13.8), null);


(lib.aniWaves = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(-46.5,27).curveTo(-47,25.2,-47,23).lineTo(-47,-27).lineTo(47,-27).lineTo(47,23).curveTo(47,25.2,46.5,27).closePath();
    mask.setTransform(40.3,3);

    // Layer 1
    this.instance = new lib.graSteam();
    this.instance.parent = this;
    this.instance.setTransform(0,0,1,1,90);

    var maskedShapeInstanceList = [this.instance];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).to({x:80.6},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-15.8,94,31.8);


(lib.aniWave = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_2 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(-109.7,20.2).lineTo(-109.7,-20.3).lineTo(109.7,-20.3).lineTo(109.7,20.2).closePath();
    mask.setTransform(-111,-10.4);

    // Layer_1
    this.instance = new lib.wave();
    this.instance.parent = this;
    this.instance.setTransform(-330,0);

    var maskedShapeInstanceList = [this.instance];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-107.7},99).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.7,-3,219.5,6);


(lib.aniStar = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.star();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,rotation:-360,x:-118.5},5,cjs.Ease.get(-1)).to({rotation:-5040,x:-127.5},24).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-92,2,7,7);


(lib.anio = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.o();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,x:-128.5},4,cjs.Ease.get(-1)).to({x:-137.5},25).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.4,2.6,5.8,5.8);


(lib.aniIcon5 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 2
    this.instance = new lib.hand1();
    this.instance.parent = this;
    this.instance.setTransform(-69.5,26.8,1,1,-15,0,0,0.6,-0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:-21.7,y:9.1},3,cjs.Ease.get(1)).to({x:-18.6},1).to({x:-33.9,y:21.3},3).to({rotation:-15,x:-69.5,y:26.8},3).to({rotation:0,x:-21.7,y:9.1},3,cjs.Ease.get(1)).to({x:-18.6},1).to({x:-33.9,y:21.3},3).to({rotation:-15,x:-69.5,y:26.8},3).to({rotation:0,x:-21.7,y:9.1},3,cjs.Ease.get(1)).to({x:-18.6},1).to({_off:true},2).wait(5));

    // Layer 1
    this.instance_1 = new lib.hand2();
    this.instance_1.parent = this;
    this.instance_1.setTransform(81.8,17,1,1,15,0,0,-0.6,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:0,x:17.9,y:0.9},3,cjs.Ease.get(1)).to({x:15.4,y:7},1).to({x:34,y:0.9},3).to({rotation:15,x:81.8,y:17},3).to({rotation:0,x:17.9,y:0.9},3,cjs.Ease.get(1)).to({x:15.4,y:7},1).to({x:34,y:0.9},3).to({rotation:15,x:81.8,y:17},3).to({rotation:0,x:17.9,y:0.9},3,cjs.Ease.get(1)).to({x:15.4,y:7},1).to({_off:true},2).wait(5));

    // Layer_7
    this.instance_2 = new lib.Symbol2();
    this.instance_2.parent = this;
    this.instance_2.setTransform(102.9,-63.9);
    this.instance_2.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:1.05,scaleY:1.05,x:108.3,y:-74.9,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:102.9,y:-63.9,alpha:0},0).to({scaleX:1.05,scaleY:1.05,x:108.3,y:-74.9,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:102.9,y:-63.9,alpha:0},0).to({scaleX:1.05,scaleY:1.05,x:108.3,y:-74.9,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(8));

    // Isolation Mode
    this.instance_3 = new lib.Symbol3();
    this.instance_3.parent = this;
    this.instance_3.setTransform(-99.4,-44.4);
    this.instance_3.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1.05,scaleY:1.05,x:-104,y:-54.4,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:-99.4,y:-44.4,alpha:0},0).to({scaleX:1.05,scaleY:1.05,x:-104,y:-54.4,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:-99.4,y:-44.4,alpha:0},0).to({scaleX:1.05,scaleY:1.05,x:-104,y:-54.4,alpha:1},4,cjs.Ease.get(1)).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-179.5,-119.8,346.1,245.8);


(lib.aniIcon1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-0.5,0.8).lineTo(0.5,-0.8);
    this.shape.setTransform(68.5,-41.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-3,1.7).lineTo(3,-1.7);
    this.shape_1.setTransform(77.9,-46.8);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-4.5,2.3).lineTo(4.5,-2.3);
    this.shape_2.setTransform(83.5,-49.9);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-5,2.5).lineTo(5,-2.5);
    this.shape_3.setTransform(85.4,-50.9);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-3.1,1.6).lineTo(3.1,-1.6);
    this.shape_4.setTransform(91.1,-54.7);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-1.2,0.8).lineTo(1.1,-0.8);
    this.shape_5.setTransform(96.8,-58.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},11).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[]},1).wait(28));

    // Layer_3
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-0.7,0.8).lineTo(0.7,-0.8);
    this.shape_6.setTransform(60.6,-47.5);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-2.1,3.7).lineTo(2.2,-3.7);
    this.shape_7.setTransform(70.7,-55.1);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-3,5.5).lineTo(3,-5.5);
    this.shape_8.setTransform(76.8,-59.6);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-3.3,6.1).lineTo(3.3,-6.1);
    this.shape_9.setTransform(78.8,-61.1);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-2,3.6).lineTo(2,-3.6);
    this.shape_10.setTransform(82,-67.4);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(-0.7,1).lineTo(0.7,-1);
    this.shape_11.setTransform(85.3,-73.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},11).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[]},1).wait(28));

    // Layer_2
    this.instance = new lib.graIcon1();
    this.instance.parent = this;
    this.instance.setTransform(-17.1,122.7,1.44,1.44,0,0,0,-0.1,83.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({scaleY:1.44,skewX:3.9},3).to({scaleY:1.41,skewX:-3.7,x:-16.9},3).to({scaleY:1.44,skewX:0,x:-17.1},5,cjs.Ease.get(1)).wait(29));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.3,-118.1,212.8,242.1);


(lib.aniGPS = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.instance = new lib.graGPS();
    this.instance.parent = this;
    this.instance.setTransform(-1.2,-1.3,0.1,0.1,0,0,0,0,-0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:0,scaleX:1.1,scaleY:1.1,y:-1.2},4,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1,y:-1.3},2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-6.5,3.9,5.4);


(lib.aniGlobe = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // aniGPS
    this.instance = new lib.aniGPS("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-47.5,24.1,0.849,0.849,-15);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(7).to({_off:false},0).wait(52));

    // aniGPS
    this.instance_1 = new lib.aniGPS("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(19.6,25,0.569,0.569,45,0,0,0.5,0.1);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).wait(54));

    // aniGPS
    this.instance_2 = new lib.aniGPS("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-4.3,-3.3,0.849,0.849,15);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({_off:false},0).wait(55));

    // aniGPS
    this.instance_3 = new lib.aniGPS("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-36.5,-39.1);
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off:false},0).wait(57));

    // globe
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(26.3,-92.8).lineTo(19.4,-78).curveTo(46.5,-66,58.1,-40).curveTo(62,-31.2,63.4,-22.6).curveTo(66.4,-3.6,58,15.1).curveTo(57.8,15.6,57.5,16.2).curveTo(49.1,34,33.5,45.2).curveTo(24.6,51.5,13.5,55.5).curveTo(1.5,60,-10.6,60.9).curveTo(-25,62,-39.4,58).lineTo(-45.6,71.2).moveTo(12.1,-62.5).lineTo(12.9,-62.1).curveTo(22.7,-57.5,23.7,-40.1).curveTo(24,-34.5,23.4,-28.7).lineTo(45.6,-19.3).curveTo(47.6,-5.1,41.9,8.9).curveTo(41.8,9.3,41.6,9.8).curveTo(35.9,23.1,25.2,31.5).moveTo(11.8,-62.6).lineTo(12.1,-62.5).lineTo(1.7,-37.9).lineTo(23.4,-28.7).curveTo(22,-16,16.1,-2).curveTo(16,-1.6,15.8,-1.2).curveTo(10.1,12.2,2.5,21.9).lineTo(25.2,31.5).curveTo(19.2,36.2,11.6,39.2).curveTo(-9.2,47.7,-30,39.1).lineTo(-30.8,38.8).curveTo(-40.8,34.3,-41.8,16.6).curveTo(-42.2,10.2,-41.2,3.4).curveTo(-39.6,-8.8,-33.9,-22.2).curveTo(-33.7,-22.7,-33.6,-23.1).lineTo(-9,-12.7).lineTo(-19.7,12.5).lineTo(-30.8,38.8).curveTo(-51.5,30,-60,8.9).curveTo(-63.1,1.3,-63.9,-6.2).curveTo(-65.4,-19.8,-59.7,-33.1).curveTo(-59.5,-33.6,-59.4,-34).curveTo(-56.1,-41.4,-51.2,-47.3).curveTo(-47,-52.3,-41.6,-56.2).curveTo(-36.2,-60,-29.6,-62.7).curveTo(-27.6,-63.5,-25.6,-64.2).curveTo(-6.9,-70.3,11.8,-62.6).closePath().moveTo(12.9,-62.1).curveTo(33.5,-53.2,42,-32.4).curveTo(44.6,-25.8,45.6,-19.3).moveTo(16.1,-2).lineTo(41.9,8.9).moveTo(-41.6,-56.2).lineTo(-19.4,-46.8).curveTo(-15.7,-51.4,-11.5,-55).curveTo(1.7,-66.3,11.8,-62.6).moveTo(-33.6,-23.1).curveTo(-27.6,-37,-19.4,-46.8).lineTo(1.7,-37.9).lineTo(-9,-12.7).lineTo(16.1,-2).moveTo(-59.4,-34).lineTo(-33.6,-23.1).moveTo(2.5,21.9).curveTo(-1.8,27.3,-6.6,31.5).curveTo(-19.9,43,-30,39.1).moveTo(-41.2,3.4).lineTo(-19.7,12.5).lineTo(2.5,21.9).moveTo(-10.6,82).lineTo(-21.9,82).moveTo(-10.6,60.9).lineTo(-10.6,82).lineTo(-10.6,92.8).lineTo(-39.4,92.8).moveTo(-63.9,-6.2).lineTo(-41.2,3.4).moveTo(1.7,82).lineTo(-10.6,82).moveTo(18.9,92.8).lineTo(-10.6,92.8);
    this.shape.setTransform(2.5,7.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-62.7,-86,130.4,187.6);


(lib.ani = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.Symbol();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,x:-118.5},5,cjs.Ease.get(-1)).to({x:-127.5},24,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-92,2,6.9,6.9);


(lib.aniPhone = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_10
    this.instance = new lib.anio("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(58.6,36.2,0.6,0.6,-130,0,0,-3.2,6);

    this.instance_1 = new lib.anio("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(109.1,-38.9,0.7,0.7,135,0,0,-3.2,5.8);

    this.instance_2 = new lib.anio("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-93.2,26.1,0.7,0.7,45,0,0,-3.2,5.9);

    this.instance_3 = new lib.ani("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(17.8,60.4,0.8,0.8,180,0,0,3.4,5.4);

    this.instance_4 = new lib.ani("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(-90.9,60.4,0.8,0.8,0,0,0,-88.5,5.5);

    this.instance_5 = new lib.ani("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(9.8,-44.8,0.8,0.8,180,0,0,3.5,5.5);

    this.instance_6 = new lib.ani("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(-82.9,-44.7,0.8,0.8,0,0,0,-88.5,5.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},1).to({state:[]},20).wait(26));

    // Layer 7
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape.setTransform(66.2,44.4);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_1.setTransform(69,47);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_2.setTransform(77.4,54.6);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_3.setTransform(91.3,67.3);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4,-0.4,-3.6).curveTo(0.2,-3.3,0.4,-3.4).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.2,-4.4).curveTo(2.5,-4.4,2.7,-4.4).curveTo(3.4,-4.3,4,-3.9).curveTo(4.2,-3.7,4.4,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,5,-1.1).curveTo(5,0.5,2.8,2.4).curveTo(2.6,2.6,2.4,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.9,2.3,-3,2.2).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_4.setTransform(91.6,67.6);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.4,-3.4).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.4).curveTo(2.4,-4.4,2.7,-4.4).curveTo(3.4,-4.4,4,-3.9).curveTo(4.2,-3.8,4.4,-3.6).curveTo(5,-2.9,5.1,-1.5).curveTo(5.1,-1.3,5.1,-1.1).curveTo(5,0.4,2.9,2.4).curveTo(2.6,2.6,2.4,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.9,2.3,-3,2.2).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_5.setTransform(92,67.9);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,-1.5).curveTo(-5,-3.2,-4.1,-3.9).curveTo(-0.9,-4.1,-0.5,-3.6).curveTo(0.1,-3.3,0.3,-3.4).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.4).curveTo(2.4,-4.5,2.7,-4.4).curveTo(3.4,-4.4,4,-3.9).curveTo(4.2,-3.8,4.4,-3.6).curveTo(5,-2.9,5.1,-1.6).curveTo(5.1,-1.4,5.1,-1.2).curveTo(5,0.4,2.9,2.4).curveTo(2.6,2.6,2.4,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.4,-0.2,4.4).curveTo(-0.3,4.4,-0.3,4.4).curveTo(-1.1,3.9,-2.8,2.5).curveTo(-2.9,2.3,-3,2.2).curveTo(-5.2,0.1,-5.1,-1.5).closePath();
    this.shape_6.setTransform(92.3,68.2);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,-1.5).curveTo(-5,-3.3,-4.1,-3.9).curveTo(-0.9,-4.1,-0.4,-3.7).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.6,-3.8,0.8,-3.9).curveTo(1.5,-4.4,2.1,-4.4).curveTo(2.4,-4.5,2.7,-4.5).curveTo(3.4,-4.4,4.1,-4).curveTo(4.3,-3.8,4.4,-3.7).curveTo(5,-3,5.1,-1.6).curveTo(5.1,-1.4,5.1,-1.2).curveTo(5.1,0.4,3,2.4).curveTo(2.7,2.6,2.5,2.8).curveTo(0.8,4.2,0.2,4.5).curveTo(-0.1,4.4,-0.2,4.4).curveTo(-0.3,4.4,-0.3,4.4).curveTo(-1.1,4,-2.8,2.5).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.2,0.1,-5.1,-1.5).closePath();
    this.shape_7.setTransform(92.6,68.5);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.2,-1.5).curveTo(-5,-3.2,-4.1,-4).curveTo(-1,-4.1,-0.5,-3.6).curveTo(0.1,-3.3,0.3,-3.5).curveTo(0.6,-3.8,0.8,-4).curveTo(1.4,-4.3,2.1,-4.5).curveTo(2.4,-4.5,2.7,-4.5).curveTo(3.4,-4.5,4.1,-4).curveTo(4.3,-3.8,4.4,-3.6).curveTo(5,-3,5.2,-1.6).curveTo(5.2,-1.4,5.2,-1.2).curveTo(5.1,0.4,3,2.4).curveTo(2.7,2.6,2.5,2.8).curveTo(0.9,4.2,0.2,4.5).curveTo(-0.2,4.5,-0.2,4.5).curveTo(-0.3,4.5,-0.3,4.5).curveTo(-1.1,4,-2.8,2.5).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.2,0.2,-5.2,-1.5).closePath();
    this.shape_8.setTransform(93,68.8);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.2,-1.6).curveTo(-5,-3.3,-4.1,-4).curveTo(-1,-4.2,-0.4,-3.7).curveTo(0.1,-3.4,0.3,-3.5).curveTo(0.6,-3.8,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.5).curveTo(3.4,-4.5,4.1,-4.1).curveTo(4.3,-3.9,4.5,-3.7).curveTo(5.1,-3,5.2,-1.7).curveTo(5.2,-1.5,5.2,-1.2).curveTo(5.2,0.4,3,2.4).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.3,4.5).curveTo(-0.1,4.5,-0.2,4.5).curveTo(-0.2,4.5,-0.3,4.5).curveTo(-1.1,4.1,-2.8,2.5).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.2,0.1,-5.2,-1.6).closePath();
    this.shape_9.setTransform(93.3,69.1);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.2,-1.6).curveTo(-5.1,-3.3,-4.2,-4).curveTo(-1,-4.2,-0.5,-3.8).curveTo(0.1,-3.4,0.3,-3.5).curveTo(0.5,-3.8,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.6).curveTo(3.4,-4.6,4.1,-4.1).curveTo(4.3,-4,4.5,-3.8).curveTo(5.1,-3.1,5.2,-1.7).curveTo(5.3,-1.5,5.3,-1.2).curveTo(5.2,0.3,3.1,2.3).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.2,4.5).curveTo(-0.1,4.5,-0.2,4.5).curveTo(-0.2,4.5,-0.3,4.5).curveTo(-1.1,4.1,-2.8,2.6).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.3,0.2,-5.2,-1.6).closePath();
    this.shape_10.setTransform(93.6,69.4);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.5).curveTo(-5.2,-3.3,-4.2,-4).curveTo(-1.1,-4.3,-0.5,-3.7).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.6,-4.6).curveTo(3.4,-4.6,4.1,-4.1).curveTo(4.3,-4,4.5,-3.8).curveTo(5.1,-3.1,5.3,-1.7).curveTo(5.3,-1.5,5.3,-1.2).curveTo(5.3,0.4,3.1,2.4).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.2,4.6).curveTo(-0.1,4.6,-0.2,4.6).curveTo(-0.2,4.6,-0.3,4.6).curveTo(-1.1,4.2,-2.8,2.6).curveTo(-3,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.5).closePath();
    this.shape_11.setTransform(94,69.7);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.5).curveTo(-5.2,-3.3,-4.2,-4).curveTo(-1,-4.3,-0.5,-3.8).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.6).curveTo(3.5,-4.6,4.1,-4.1).curveTo(4.3,-4,4.5,-3.8).curveTo(5.2,-3.1,5.3,-1.7).curveTo(5.3,-1.5,5.3,-1.3).curveTo(5.3,0.4,3.2,2.4).curveTo(2.9,2.6,2.7,2.8).curveTo(1,4.3,0.3,4.6).curveTo(-0.1,4.6,-0.1,4.6).curveTo(-0.2,4.6,-0.2,4.6).curveTo(-1,4.2,-2.8,2.7).curveTo(-2.9,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.5).closePath();
    this.shape_12.setTransform(94.3,70);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.6).curveTo(-5.2,-3.4,-4.3,-4.1).curveTo(-1.1,-4.4,-0.5,-3.9).curveTo(0,-3.5,0.3,-3.5).curveTo(0.5,-3.8,0.8,-4).curveTo(1.4,-4.5,2.1,-4.6).curveTo(2.4,-4.7,2.7,-4.7).curveTo(3.4,-4.7,4.1,-4.2).curveTo(4.3,-4.1,4.5,-3.9).curveTo(5.2,-3.2,5.3,-1.8).curveTo(5.4,-1.6,5.4,-1.4).curveTo(5.4,0.3,3.2,2.3).curveTo(2.9,2.6,2.7,2.8).curveTo(1,4.3,0.3,4.6).curveTo(-0.1,4.6,-0.1,4.6).curveTo(-0.2,4.6,-0.2,4.6).curveTo(-1,4.2,-2.9,2.6).curveTo(-3,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.6).closePath();
    this.shape_13.setTransform(94.6,70.3);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.3,-3.4,-4.3,-4.1).curveTo(-1.1,-4.4,-0.5,-3.8).curveTo(0,-3.5,0.2,-3.4).curveTo(0.5,-3.8,0.7,-4).curveTo(1.3,-4.5,2.1,-4.6).curveTo(2.4,-4.7,2.6,-4.7).curveTo(3.4,-4.7,4.1,-4.2).curveTo(4.3,-4.1,4.5,-3.9).curveTo(5.2,-3.2,5.4,-1.8).curveTo(5.4,-1.6,5.4,-1.3).curveTo(5.4,0.3,3.2,2.4).curveTo(2.9,2.6,2.7,2.8).curveTo(1,4.4,0.3,4.7).curveTo(-0.1,4.7,-0.2,4.7).curveTo(-0.2,4.7,-0.3,4.7).curveTo(-1.1,4.3,-2.9,2.7).curveTo(-3,2.6,-3.1,2.5).curveTo(-5.5,0.2,-5.4,-1.6).closePath();
    this.shape_14.setTransform(95,70.6);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.3,-3.4,-4.3,-4.1).curveTo(-1.1,-4.4,-0.5,-3.9).curveTo(0,-3.5,0.2,-3.4).curveTo(0.5,-3.8,0.7,-4).curveTo(1.4,-4.5,2.1,-4.6).curveTo(2.4,-4.7,2.7,-4.7).curveTo(3.5,-4.7,4.2,-4.3).curveTo(4.4,-4.1,4.6,-4).curveTo(5.3,-3.3,5.4,-1.8).curveTo(5.4,-1.6,5.4,-1.4).curveTo(5.5,0.3,3.3,2.4).curveTo(3,2.6,2.8,2.8).curveTo(1,4.4,0.3,4.7).curveTo(-0,4.7,-0.1,4.7).curveTo(-0.2,4.7,-0.2,4.7).curveTo(-1,4.4,-2.9,2.7).curveTo(-3,2.6,-3.1,2.5).curveTo(-5.5,0.2,-5.4,-1.6).closePath();
    this.shape_15.setTransform(95.3,70.9);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.3,-3.4,-4.4,-4.1).curveTo(-1.2,-4.5,-0.5,-3.9).curveTo(-0,-3.5,0.2,-3.4).curveTo(0.4,-3.8,0.7,-4).curveTo(1.3,-4.5,2.1,-4.7).curveTo(2.4,-4.7,2.6,-4.7).curveTo(3.5,-4.8,4.2,-4.3).curveTo(4.4,-4.2,4.6,-4).curveTo(5.3,-3.3,5.4,-1.8).curveTo(5.5,-1.6,5.5,-1.4).curveTo(5.5,0.3,3.3,2.4).curveTo(3,2.6,2.8,2.8).curveTo(1,4.4,0.3,4.7).curveTo(-0,4.8,-0.1,4.8).curveTo(-0.2,4.8,-0.2,4.8).curveTo(-1,4.4,-2.9,2.8).curveTo(-3,2.6,-3.1,2.5).curveTo(-5.5,0.2,-5.4,-1.6).closePath();
    this.shape_16.setTransform(95.6,71.2);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.5,-1.6).curveTo(-5.4,-3.5,-4.5,-4.2).curveTo(-1.2,-4.5,-0.5,-3.9).curveTo(-0,-3.6,0.2,-3.5).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2,-4.7).curveTo(2.3,-4.8,2.6,-4.8).curveTo(3.4,-4.8,4.2,-4.3).curveTo(4.4,-4.2,4.5,-4).curveTo(5.3,-3.3,5.5,-1.9).curveTo(5.5,-1.6,5.5,-1.4).curveTo(5.5,0.3,3.3,2.4).curveTo(3.1,2.6,2.8,2.8).curveTo(1.1,4.4,0.3,4.8).curveTo(-0.1,4.8,-0.1,4.8).curveTo(-0.2,4.8,-0.2,4.8).curveTo(-1,4.4,-2.9,2.8).curveTo(-3,2.7,-3.2,2.5).curveTo(-5.6,0.2,-5.5,-1.6).closePath();
    this.shape_17.setTransform(96,71.5);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.6,-1.6).curveTo(-5.4,-3.5,-4.5,-4.2).curveTo(-1.2,-4.6,-0.6,-4).curveTo(-0.1,-3.6,0.2,-3.5).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2,-4.7).curveTo(2.3,-4.8,2.6,-4.8).curveTo(3.4,-4.8,4.2,-4.4).curveTo(4.4,-4.3,4.5,-4.1).curveTo(5.3,-3.4,5.5,-1.9).curveTo(5.5,-1.7,5.5,-1.4).curveTo(5.6,0.2,3.3,2.4).curveTo(3.1,2.6,2.9,2.8).curveTo(1.1,4.4,0.3,4.8).curveTo(-0.1,4.8,-0.1,4.8).curveTo(-0.2,4.8,-0.2,4.8).curveTo(-1,4.5,-2.9,2.8).curveTo(-3.1,2.7,-3.2,2.6).curveTo(-5.6,0.2,-5.6,-1.6).closePath();
    this.shape_18.setTransform(96.3,71.8);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.6,-1.6).curveTo(-5.4,-3.5,-4.5,-4.2).curveTo(-1.2,-4.6,-0.5,-4).curveTo(-0.1,-3.6,0.2,-3.5).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2,-4.7).curveTo(2.3,-4.8,2.6,-4.8).curveTo(3.5,-4.9,4.2,-4.4).curveTo(4.4,-4.3,4.6,-4.1).curveTo(5.4,-3.4,5.5,-1.9).curveTo(5.6,-1.7,5.6,-1.5).curveTo(5.6,0.2,3.4,2.4).curveTo(3.2,2.6,2.9,2.8).curveTo(1.1,4.4,0.4,4.8).curveTo(-0,4.9,-0.1,4.9).curveTo(-0.1,4.9,-0.2,4.9).curveTo(-1,4.5,-2.9,2.8).curveTo(-3,2.7,-3.2,2.6).curveTo(-5.6,0.2,-5.6,-1.6).closePath();
    this.shape_19.setTransform(96.6,72.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[]},1).wait(26));

    // Layer 6
    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_20.setTransform(66.2,-108.9);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.8,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.9).curveTo(5,-1.7,4.9,-1.4).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_21.setTransform(69.8,-111.2);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_22.setTransform(80.7,-118.3);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_23.setTransform(98.8,-130.3);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4,-0.4,-3.6).curveTo(0.2,-3.3,0.4,-3.4).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.4).curveTo(2.4,-4.4,2.7,-4.4).curveTo(3.4,-4.3,4,-3.9).curveTo(4.2,-3.7,4.4,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,5,-1.1).curveTo(4.9,0.4,2.8,2.4).curveTo(2.6,2.6,2.4,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.8,-2.8,2.4).curveTo(-2.9,2.3,-3,2.2).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_24.setTransform(99.2,-130.8);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4,-0.4,-3.6).curveTo(0.2,-3.3,0.4,-3.4).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.2,-4.4).curveTo(2.5,-4.4,2.7,-4.4).curveTo(3.4,-4.3,4.1,-3.9).curveTo(4.3,-3.7,4.4,-3.5).curveTo(5,-2.8,5.1,-1.5).curveTo(5.1,-1.3,5.1,-1.1).curveTo(5,0.5,2.9,2.4).curveTo(2.7,2.6,2.4,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.4,-0.2,4.4).curveTo(-0.3,4.4,-0.3,4.4).curveTo(-1.1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-3,2.2).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_25.setTransform(99.6,-131.4);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,-1.5).curveTo(-5,-3.2,-4.1,-3.9).curveTo(-0.9,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.4,-3.4).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.4).curveTo(2.4,-4.5,2.7,-4.4).curveTo(3.4,-4.4,4,-3.9).curveTo(4.2,-3.8,4.4,-3.6).curveTo(5,-2.9,5.1,-1.6).curveTo(5.1,-1.4,5.1,-1.2).curveTo(5,0.4,2.9,2.4).curveTo(2.7,2.6,2.5,2.8).curveTo(0.8,4.2,0.2,4.4).curveTo(-0.2,4.4,-0.2,4.4).curveTo(-0.3,4.4,-0.3,4.4).curveTo(-1.1,3.9,-2.8,2.5).curveTo(-2.9,2.3,-3,2.2).curveTo(-5.2,0.1,-5.1,-1.5).closePath();
    this.shape_26.setTransform(100.1,-132);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,-1.5).curveTo(-5,-3.3,-4.1,-4).curveTo(-1,-4.2,-0.5,-3.7).curveTo(0.1,-3.4,0.3,-3.5).curveTo(0.6,-3.8,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.5,2.7,-4.5).curveTo(3.4,-4.5,4,-4).curveTo(4.2,-3.9,4.4,-3.7).curveTo(5,-3,5.1,-1.6).curveTo(5.1,-1.4,5.1,-1.2).curveTo(5.1,0.4,2.9,2.3).curveTo(2.7,2.6,2.5,2.8).curveTo(0.8,4.2,0.2,4.5).curveTo(-0.2,4.4,-0.2,4.4).curveTo(-0.3,4.4,-0.3,4.4).curveTo(-1.1,4,-2.8,2.5).curveTo(-2.9,2.4,-3,2.2).curveTo(-5.2,0.1,-5.1,-1.5).closePath();
    this.shape_27.setTransform(100.5,-132.5);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,-1.5).curveTo(-5,-3.3,-4.1,-3.9).curveTo(-1,-4.2,-0.4,-3.7).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.5,2.7,-4.5).curveTo(3.4,-4.5,4.1,-4).curveTo(4.3,-3.9,4.4,-3.7).curveTo(5.1,-3,5.2,-1.6).curveTo(5.2,-1.4,5.2,-1.2).curveTo(5.1,0.4,3,2.4).curveTo(2.7,2.6,2.5,2.8).curveTo(0.9,4.3,0.2,4.5).curveTo(-0.1,4.5,-0.2,4.5).curveTo(-0.2,4.5,-0.3,4.5).curveTo(-1.1,4,-2.8,2.5).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.2,0.2,-5.1,-1.5).closePath();
    this.shape_28.setTransform(100.9,-133.1);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.2,-1.6).curveTo(-5,-3.3,-4.1,-4).curveTo(-1,-4.2,-0.4,-3.7).curveTo(0.1,-3.4,0.3,-3.5).curveTo(0.6,-3.8,0.8,-4).curveTo(1.5,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.5).curveTo(3.5,-4.5,4.1,-4.1).curveTo(4.3,-3.9,4.5,-3.7).curveTo(5.1,-3,5.2,-1.7).curveTo(5.2,-1.5,5.2,-1.2).curveTo(5.2,0.4,3.1,2.4).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.3,4.5).curveTo(-0.1,4.5,-0.2,4.5).curveTo(-0.2,4.5,-0.3,4.5).curveTo(-1.1,4.1,-2.8,2.5).curveTo(-2.9,2.4,-3,2.3).curveTo(-5.2,0.1,-5.2,-1.6).closePath();
    this.shape_29.setTransform(101.3,-133.7);

    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.2,-1.5).curveTo(-5.1,-3.3,-4.2,-4).curveTo(-1,-4.2,-0.5,-3.7).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.5).curveTo(3.4,-4.5,4.1,-4.1).curveTo(4.3,-3.9,4.5,-3.8).curveTo(5.1,-3.1,5.2,-1.7).curveTo(5.3,-1.4,5.3,-1.2).curveTo(5.2,0.4,3.1,2.4).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.2,4.6).curveTo(-0.1,4.5,-0.2,4.5).curveTo(-0.2,4.5,-0.3,4.5).curveTo(-1.1,4.1,-2.8,2.6).curveTo(-2.9,2.5,-3,2.4).curveTo(-5.3,0.2,-5.2,-1.5).closePath();
    this.shape_30.setTransform(101.8,-134.3);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.6).curveTo(-5.2,-3.4,-4.2,-4).curveTo(-1.1,-4.3,-0.5,-3.8).curveTo(0,-3.5,0.3,-3.5).curveTo(0.5,-3.8,0.8,-4).curveTo(1.4,-4.5,2.1,-4.5).curveTo(2.4,-4.6,2.6,-4.6).curveTo(3.4,-4.6,4.1,-4.1).curveTo(4.3,-4,4.5,-3.8).curveTo(5.1,-3.1,5.2,-1.7).curveTo(5.3,-1.5,5.3,-1.3).curveTo(5.3,0.3,3.1,2.4).curveTo(2.8,2.6,2.6,2.8).curveTo(0.9,4.3,0.2,4.6).curveTo(-0.1,4.6,-0.2,4.6).curveTo(-0.2,4.6,-0.3,4.6).curveTo(-1.1,4.2,-2.9,2.6).curveTo(-3,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.6).closePath();
    this.shape_31.setTransform(102.2,-134.8);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.5).curveTo(-5.2,-3.3,-4.3,-4).curveTo(-1.1,-4.3,-0.5,-3.8).curveTo(0.1,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.8,-4).curveTo(1.4,-4.4,2.1,-4.5).curveTo(2.4,-4.6,2.7,-4.6).curveTo(3.4,-4.6,4.1,-4.1).curveTo(4.3,-4,4.5,-3.8).curveTo(5.2,-3.1,5.3,-1.7).curveTo(5.3,-1.5,5.3,-1.3).curveTo(5.3,0.4,3.1,2.4).curveTo(2.9,2.6,2.7,2.8).curveTo(1,4.3,0.3,4.6).curveTo(-0.1,4.6,-0.2,4.6).curveTo(-0.2,4.6,-0.3,4.6).curveTo(-1.1,4.2,-2.8,2.7).curveTo(-3,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.5).closePath();
    this.shape_32.setTransform(102.6,-135.4);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-1.6).curveTo(-5.2,-3.4,-4.3,-4.1).curveTo(-1.1,-4.4,-0.5,-3.8).curveTo(0.1,-3.5,0.3,-3.4).curveTo(0.5,-3.8,0.8,-4).curveTo(1.4,-4.5,2.1,-4.6).curveTo(2.4,-4.7,2.7,-4.6).curveTo(3.5,-4.6,4.2,-4.2).curveTo(4.4,-4.1,4.5,-3.9).curveTo(5.2,-3.2,5.4,-1.8).curveTo(5.4,-1.5,5.4,-1.3).curveTo(5.4,0.3,3.2,2.4).curveTo(3,2.6,2.7,2.8).curveTo(1,4.3,0.3,4.6).curveTo(-0.1,4.6,-0.1,4.6).curveTo(-0.2,4.6,-0.2,4.6).curveTo(-1,4.3,-2.8,2.7).curveTo(-2.9,2.5,-3.1,2.4).curveTo(-5.4,0.2,-5.3,-1.6).closePath();
    this.shape_33.setTransform(103,-136);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.2,-3.4,-4.3,-4.1).curveTo(-1.1,-4.4,-0.5,-3.9).curveTo(0,-3.5,0.2,-3.5).curveTo(0.5,-3.8,0.7,-4).curveTo(1.4,-4.5,2.1,-4.6).curveTo(2.4,-4.7,2.6,-4.7).curveTo(3.5,-4.7,4.2,-4.3).curveTo(4.4,-4.1,4.5,-3.9).curveTo(5.2,-3.2,5.4,-1.8).curveTo(5.4,-1.6,5.4,-1.4).curveTo(5.4,0.3,3.2,2.4).curveTo(3,2.6,2.7,2.8).curveTo(1,4.3,0.3,4.7).curveTo(-0.1,4.7,-0.1,4.7).curveTo(-0.2,4.7,-0.2,4.7).curveTo(-1,4.3,-2.9,2.7).curveTo(-3,2.6,-3.1,2.4).curveTo(-5.4,0.2,-5.4,-1.6).closePath();
    this.shape_34.setTransform(103.5,-136.5);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.3,-3.4,-4.4,-4.1).curveTo(-1.2,-4.4,-0.5,-3.9).curveTo(-0,-3.5,0.2,-3.4).curveTo(0.4,-3.8,0.7,-4).curveTo(1.3,-4.5,2,-4.6).curveTo(2.3,-4.7,2.6,-4.7).curveTo(3.4,-4.7,4.1,-4.3).curveTo(4.3,-4.1,4.5,-4).curveTo(5.2,-3.3,5.4,-1.8).curveTo(5.4,-1.6,5.4,-1.4).curveTo(5.4,0.3,3.2,2.4).curveTo(3,2.6,2.8,2.8).curveTo(1,4.4,0.3,4.7).curveTo(-0.1,4.7,-0.2,4.7).curveTo(-0.2,4.7,-0.3,4.7).curveTo(-1.1,4.4,-2.9,2.7).curveTo(-3,2.6,-3.1,2.5).curveTo(-5.5,0.2,-5.4,-1.6).closePath();
    this.shape_35.setTransform(103.9,-137.1);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,-1.6).curveTo(-5.3,-3.5,-4.4,-4.2).curveTo(-1.2,-4.5,-0.5,-3.9).curveTo(-0,-3.6,0.2,-3.5).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2.1,-4.7).curveTo(2.4,-4.8,2.6,-4.7).curveTo(3.5,-4.8,4.2,-4.3).curveTo(4.4,-4.2,4.6,-4).curveTo(5.3,-3.3,5.4,-1.9).curveTo(5.5,-1.6,5.5,-1.4).curveTo(5.5,0.3,3.3,2.4).curveTo(3,2.6,2.8,2.8).curveTo(1.1,4.4,0.3,4.7).curveTo(-0,4.7,-0.1,4.7).curveTo(-0.2,4.7,-0.2,4.7).curveTo(-1,4.4,-2.9,2.7).curveTo(-3,2.6,-3.1,2.5).curveTo(-5.5,0.2,-5.4,-1.6).closePath();
    this.shape_36.setTransform(104.3,-137.7);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.5,-1.6).curveTo(-5.4,-3.4,-4.5,-4.2).curveTo(-1.2,-4.6,-0.5,-3.9).curveTo(-0,-3.6,0.2,-3.4).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2,-4.7).curveTo(2.3,-4.8,2.6,-4.8).curveTo(3.4,-4.8,4.2,-4.3).curveTo(4.4,-4.2,4.5,-4.1).curveTo(5.3,-3.3,5.5,-1.8).curveTo(5.5,-1.7,5.5,-1.4).curveTo(5.5,0.2,3.3,2.3).curveTo(3.1,2.6,2.8,2.8).curveTo(1,4.4,0.3,4.8).curveTo(-0.1,4.8,-0.1,4.8).curveTo(-0.2,4.8,-0.2,4.8).curveTo(-1,4.4,-2.9,2.8).curveTo(-3,2.7,-3.2,2.6).curveTo(-5.6,0.2,-5.5,-1.6).closePath();
    this.shape_37.setTransform(104.8,-138.2);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.5,-1.6).curveTo(-5.4,-3.5,-4.5,-4.2).curveTo(-1.2,-4.6,-0.5,-4).curveTo(-0,-3.6,0.2,-3.5).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2.1,-4.7).curveTo(2.4,-4.8,2.6,-4.8).curveTo(3.5,-4.8,4.2,-4.4).curveTo(4.4,-4.3,4.6,-4.1).curveTo(5.4,-3.4,5.5,-1.9).curveTo(5.5,-1.7,5.5,-1.4).curveTo(5.6,0.2,3.4,2.4).curveTo(3.1,2.6,2.9,2.8).curveTo(1.1,4.4,0.4,4.8).curveTo(-0,4.8,-0.1,4.8).curveTo(-0.1,4.8,-0.2,4.8).curveTo(-1,4.5,-2.9,2.8).curveTo(-3,2.7,-3.2,2.6).curveTo(-5.6,0.2,-5.5,-1.6).closePath();
    this.shape_38.setTransform(105.2,-138.8);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.6,-1.6).curveTo(-5.5,-3.5,-4.5,-4.2).curveTo(-1.2,-4.6,-0.6,-4).curveTo(-0.1,-3.6,0.2,-3.4).curveTo(0.4,-3.8,0.7,-4.1).curveTo(1.3,-4.6,2,-4.8).curveTo(2.3,-4.8,2.6,-4.8).curveTo(3.4,-4.8,4.2,-4.4).curveTo(4.4,-4.3,4.6,-4.2).curveTo(5.4,-3.4,5.5,-1.9).curveTo(5.6,-1.7,5.6,-1.4).curveTo(5.6,0.2,3.4,2.3).curveTo(3.1,2.6,2.9,2.8).curveTo(1.1,4.5,0.3,4.8).curveTo(-0,4.8,-0.1,4.8).curveTo(-0.2,4.8,-0.2,4.8).curveTo(-1,4.6,-2.9,2.8).curveTo(-3.1,2.7,-3.2,2.6).curveTo(-5.7,0.2,-5.6,-1.6).closePath();
    this.shape_39.setTransform(105.6,-139.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[]},1).wait(26));

    // Layer 4
    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_40.setTransform(-67.2,-108.1);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.3,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-4,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2).curveTo(2.9,2.2,2.7,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_41.setTransform(-70.5,-110.5);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.3,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.7,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_42.setTransform(-80.6,-117.9);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_43.setTransform(-97.3,-130.3);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.8,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.3).curveTo(3.4,-4.3,4,-3.9).curveTo(4.2,-3.8,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_44.setTransform(-97.7,-130.4);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.6).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.8).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.8,-2.8,5,-1.5).curveTo(5,-1.2,4.9,-1.1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_45.setTransform(-98.1,-130.6);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.8,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.6,2.4,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.8,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_46.setTransform(-98.6,-130.7);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.6).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_47.setTransform(-99,-130.9);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.9,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.3).curveTo(-2.8,2.3,-2.8,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_48.setTransform(-99.5,-131.1);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.5).curveTo(0.8,4.1,0.1,4.3).curveTo(0,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_49.setTransform(-99.9,-131.3);

    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.7,-3.8).curveTo(1.3,-4.3,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_50.setTransform(-100.3,-131.4);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_51.setTransform(-100.8,-131.6);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_52.setTransform(-101.2,-131.8);

    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_53.setTransform(-101.6,-131.9);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_54.setTransform(-102.1,-132.1);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_55.setTransform(-102.5,-132.3);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_56.setTransform(-103,-132.4);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_57.setTransform(-103.4,-132.6);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,4.9,-1.3).curveTo(5,0.2,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_58.setTransform(-103.8,-132.8);

    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.5).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_59.setTransform(-104.3,-133);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[]},1).wait(26));

    // Layer 5
    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_60.setTransform(-65.6,75);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_61.setTransform(-68.4,77.7);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_62.setTransform(-76.8,85.5);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_63.setTransform(-90.8,98.5);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.8,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.3).curveTo(3.4,-4.3,4,-3.8).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.2,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.2,0.1,4.3).curveTo(-0,4.3,-0.2,4.2).curveTo(-0.2,4.2,-0.3,4.2).curveTo(-1.1,3.8,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_64.setTransform(-91.5,98.8);

    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.8,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.5).curveTo(0.7,4.1,0.1,4.3).curveTo(-0,4.3,-0.2,4.2).curveTo(-0.2,4.2,-0.3,4.2).curveTo(-1.1,3.8,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_65.setTransform(-92.2,99);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.8,-2.9,5,-1.5).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(-0,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_66.setTransform(-92.9,99.2);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.5,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.8,4.1,0.1,4.3).curveTo(-0,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_67.setTransform(-93.6,99.5);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_68.setTransform(-94.3,99.7);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2,-4.3).curveTo(3.3,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_69.setTransform(-95,100);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.2,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.6).curveTo(0.8,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_70.setTransform(-95.7,100.2);

    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_71.setTransform(-96.4,100.4);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_72.setTransform(-97.1,100.7);

    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_73.setTransform(-97.8,100.9);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_74.setTransform(-98.5,101.2);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1.1,-4.1,-0.4,-3.5).curveTo(0.2,-3.2,0.3,-3.3).curveTo(0.4,-3.5,0.7,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.2,-4.3,3.8,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.9,4.1,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_75.setTransform(-99.2,101.4);

    this.shape_76 = new cjs.Shape();
    this.shape_76.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1.1,-4.1,-0.4,-3.5).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.3,3.8,-3.9).curveTo(4,-3.8,4.1,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_76.setTransform(-99.9,101.6);

    this.shape_77 = new cjs.Shape();
    this.shape_77.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.2,0.3,-3.3).curveTo(0.4,-3.5,0.7,-3.7).curveTo(1.2,-4.1,1.9,-4.3).curveTo(3.2,-4.4,3.8,-3.9).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,5,-1.2).curveTo(5,0.3,3,2.2).curveTo(2.8,2.4,2.7,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_77.setTransform(-100.7,101.9);

    this.shape_78 = new cjs.Shape();
    this.shape_78.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.1,1.9,-4.3).curveTo(3.1,-4.4,3.8,-3.9).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_78.setTransform(-101.4,102.1);

    this.shape_79 = new cjs.Shape();
    this.shape_79.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.1,-4.1,-0.4,-3.5).curveTo(0.2,-3.2,0.2,-3.2).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.1,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_79.setTransform(-102.1,102.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[]},1).wait(26));

    // Isolation Mode
    this.shape_80 = new cjs.Shape();
    this.shape_80.graphics.beginFill("#FFFFFF").beginStroke().moveTo(3.7,3.4).curveTo(3.4,3.1,3.4,2.6).lineTo(3.4,-0.4).curveTo(3.4,-1.8,2.2,-1.8).curveTo(1,-1.8,1,-0.4).lineTo(1,2.6).curveTo(1,3.1,0.7,3.4).curveTo(0.4,3.7,0,3.7).curveTo(-0.4,3.7,-0.7,3.4).curveTo(-1,3.1,-1,2.6).lineTo(-1,-0.4).curveTo(-1,-1.8,-2.2,-1.8).curveTo(-3.4,-1.8,-3.4,-0.4).lineTo(-3.4,2.6).curveTo(-3.4,3.1,-3.7,3.4).curveTo(-4,3.7,-4.4,3.7).curveTo(-4.8,3.7,-5.1,3.4).curveTo(-5.4,3.1,-5.4,2.6).lineTo(-5.4,-2.6).curveTo(-5.4,-3,-5.1,-3.3).curveTo(-4.8,-3.6,-4.4,-3.6).curveTo(-4,-3.6,-3.7,-3.3).curveTo(-3.4,-3,-3.4,-2.6).lineTo(-3.4,-2.5).curveTo(-2.5,-3.7,-1.3,-3.7).curveTo(0.1,-3.7,0.7,-2.5).curveTo(1.7,-3.7,3,-3.7).curveTo(4.2,-3.7,4.8,-3).curveTo(5.4,-2.3,5.4,-1.1).lineTo(5.4,2.6).curveTo(5.4,3.1,5.1,3.4).curveTo(4.8,3.7,4.4,3.7).curveTo(4,3.7,3.7,3.4).closePath();
    this.shape_80.setTransform(30.6,12.2);

    this.shape_81 = new cjs.Shape();
    this.shape_81.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.7,2.7).curveTo(-3.9,1.6,-3.8,0.1).lineTo(-3.8,-0).curveTo(-3.9,-1.5,-2.7,-2.6).curveTo(-1.7,-3.7,0,-3.7).curveTo(1.6,-3.7,2.7,-2.6).curveTo(3.8,-1.6,3.8,-0).curveTo(3.8,1.5,2.7,2.6).curveTo(1.6,3.7,0,3.7).curveTo(-1.7,3.7,-2.7,2.7).closePath().moveTo(-1.3,-1.4).curveTo(-1.9,-0.9,-1.9,-0).curveTo(-1.9,0.8,-1.3,1.4).curveTo(-0.8,1.9,0,2).curveTo(0.9,1.9,1.4,1.4).curveTo(1.8,0.8,1.8,0.1).lineTo(1.8,-0).curveTo(1.8,-0.8,1.4,-1.4).curveTo(0.8,-1.9,0,-2).curveTo(-0.9,-2,-1.3,-1.4).closePath();
    this.shape_81.setTransform(19.9,12.3);

    this.shape_82 = new cjs.Shape();
    this.shape_82.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.2,2.7).curveTo(-3.3,1.6,-3.3,0.1).lineTo(-3.3,-0).curveTo(-3.3,-1.5,-2.2,-2.6).curveTo(-1.1,-3.7,0.5,-3.7).curveTo(2,-3.7,2.9,-2.9).curveTo(3.2,-2.7,3.2,-2.2).curveTo(3.2,-1.8,2.9,-1.5).curveTo(2.7,-1.3,2.3,-1.3).curveTo(2,-1.3,1.7,-1.5).curveTo(1.1,-2,0.4,-2).curveTo(-0.3,-2,-0.8,-1.4).curveTo(-1.3,-0.9,-1.3,-0).curveTo(-1.3,0.8,-0.8,1.4).curveTo(-0.3,1.9,0.5,2).curveTo(1.2,1.9,1.9,1.5).curveTo(2.1,1.3,2.4,1.3).curveTo(2.8,1.3,3,1.5).curveTo(3.3,1.8,3.3,2.1).curveTo(3.3,2.5,3,2.8).curveTo(2,3.7,0.4,3.7).curveTo(-1.1,3.7,-2.2,2.7).closePath();
    this.shape_82.setTransform(12.1,12.3);

    this.shape_83 = new cjs.Shape();
    this.shape_83.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.9,0.8).curveTo(-1.2,0.5,-1.1,0).lineTo(-1.1,-0.1).curveTo(-1.2,-0.5,-0.9,-0.9).curveTo(-0.5,-1.2,0,-1.2).curveTo(0.5,-1.2,0.8,-0.9).curveTo(1.2,-0.5,1.2,-0.1).lineTo(1.2,0).curveTo(1.2,0.5,0.8,0.8).curveTo(0.5,1.2,0,1.2).curveTo(-0.5,1.2,-0.9,0.8).closePath();
    this.shape_83.setTransform(6.6,14.8);

    this.shape_84 = new cjs.Shape();
    this.shape_84.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.6,3.1).curveTo(-3.3,2.5,-3.3,1.5).curveTo(-3.2,0.3,-2.5,-0.3).curveTo(-1.7,-0.8,-0.4,-0.8).curveTo(0.5,-0.8,1.3,-0.5).lineTo(1.3,-0.6).curveTo(1.3,-1.9,-0.2,-1.9).curveTo(-0.9,-1.9,-1.6,-1.7).lineTo(-1.9,-1.7).curveTo(-2.3,-1.7,-2.5,-1.9).curveTo(-2.7,-2.2,-2.7,-2.5).curveTo(-2.7,-3.1,-2.2,-3.3).curveTo(-1.2,-3.7,0.1,-3.7).curveTo(1.7,-3.7,2.5,-2.9).curveTo(3.3,-2.1,3.2,-0.6).lineTo(3.2,2.6).curveTo(3.2,3,3,3.3).curveTo(2.7,3.6,2.3,3.6).curveTo(1.9,3.6,1.6,3.3).curveTo(1.3,3.1,1.3,2.8).curveTo(0.5,3.7,-0.8,3.7).curveTo(-1.9,3.7,-2.6,3.1).closePath().moveTo(-1.3,1.4).curveTo(-1.3,1.8,-1,2).curveTo(-0.7,2.3,-0.2,2.3).curveTo(0.5,2.3,0.9,1.9).curveTo(1.4,1.6,1.3,1).lineTo(1.3,0.7).curveTo(0.8,0.4,0.1,0.4).curveTo(-1.3,0.4,-1.3,1.4).closePath();
    this.shape_84.setTransform(0.8,12.3);

    this.shape_85 = new cjs.Shape();
    this.shape_85.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.7,4.6).curveTo(-1,4.3,-1,3.9).lineTo(-1,-1.4).curveTo(-1,-1.8,-0.7,-2.1).curveTo(-0.4,-2.4,-0,-2.4).curveTo(0.4,-2.4,0.7,-2.1).curveTo(1,-1.8,1,-1.4).lineTo(1,3.9).curveTo(1,4.3,0.7,4.6).curveTo(0.4,4.9,-0,4.9).curveTo(-0.4,4.9,-0.7,4.6).closePath().moveTo(-0.8,-3.3).curveTo(-1.1,-3.5,-1.1,-4).curveTo(-1.1,-4.4,-0.8,-4.7).curveTo(-0.5,-4.9,-0,-4.9).curveTo(0.5,-4.9,0.8,-4.7).curveTo(1.1,-4.4,1.1,-4).curveTo(1.1,-3.5,0.8,-3.3).curveTo(0.5,-3,-0,-3).curveTo(-0.5,-3,-0.8,-3.3).closePath();
    this.shape_85.setTransform(-4.8,11);

    this.shape_86 = new cjs.Shape();
    this.shape_86.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.5,2.9).curveTo(-2.9,2.6,-2.9,2.2).curveTo(-2.9,1.8,-2.7,1.6).curveTo(-2.4,1.4,-2.1,1.4).lineTo(-1.7,1.5).curveTo(-0.7,2.1,0.2,2.1).curveTo(1.1,2.1,1.1,1.6).lineTo(1.1,1.5).curveTo(1.1,1.3,0.6,1.1).lineTo(-0.4,0.7).curveTo(-1.4,0.4,-1.8,0.1).curveTo(-2.5,-0.5,-2.5,-1.4).curveTo(-2.5,-2.5,-1.8,-3.1).curveTo(-1,-3.7,0.1,-3.7).curveTo(1.2,-3.7,2.3,-3.2).curveTo(2.8,-2.9,2.8,-2.4).curveTo(2.8,-2.1,2.5,-1.8).curveTo(2.3,-1.6,2,-1.6).lineTo(1.6,-1.7).curveTo(0.7,-2.1,0,-2.1).curveTo(-0.7,-2.1,-0.7,-1.7).lineTo(-0.7,-1.6).curveTo(-0.7,-1.4,-0.3,-1.1).lineTo(0.7,-0.8).curveTo(1.7,-0.4,2.2,-0.1).curveTo(2.9,0.5,2.9,1.3).lineTo(2.9,1.4).curveTo(2.9,2.5,2.1,3.1).curveTo(1.4,3.7,0.2,3.7).curveTo(-1.3,3.7,-2.5,2.9).closePath();
    this.shape_86.setTransform(-10.1,12.3);

    this.shape_87 = new cjs.Shape();
    this.shape_87.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.6,3.1).curveTo(-3.3,2.5,-3.3,1.5).curveTo(-3.2,0.3,-2.5,-0.3).curveTo(-1.7,-0.8,-0.4,-0.8).curveTo(0.5,-0.8,1.3,-0.5).lineTo(1.3,-0.6).curveTo(1.3,-1.9,-0.2,-1.9).curveTo(-0.9,-1.9,-1.6,-1.7).lineTo(-1.9,-1.7).curveTo(-2.3,-1.7,-2.5,-1.9).curveTo(-2.7,-2.2,-2.7,-2.5).curveTo(-2.7,-3.1,-2.2,-3.3).curveTo(-1.2,-3.7,0.1,-3.7).curveTo(1.7,-3.7,2.5,-2.9).curveTo(3.3,-2.1,3.2,-0.6).lineTo(3.2,2.6).curveTo(3.2,3,3,3.3).curveTo(2.7,3.6,2.3,3.6).curveTo(1.9,3.6,1.6,3.3).curveTo(1.3,3.1,1.3,2.8).curveTo(0.5,3.7,-0.8,3.7).curveTo(-1.9,3.7,-2.6,3.1).closePath().moveTo(-1.3,1.4).curveTo(-1.3,1.8,-1,2).curveTo(-0.7,2.3,-0.2,2.3).curveTo(0.5,2.3,0.9,1.9).curveTo(1.4,1.6,1.3,1).lineTo(1.3,0.7).curveTo(0.8,0.4,0.1,0.4).curveTo(-1.3,0.4,-1.3,1.4).closePath();
    this.shape_87.setTransform(-17.2,12.3);

    this.shape_88 = new cjs.Shape();
    this.shape_88.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.9,3.3).curveTo(-2.2,3,-2.2,2.6).lineTo(-2.2,-2.6).curveTo(-2.2,-3.1,-1.9,-3.3).curveTo(-1.6,-3.6,-1.2,-3.6).curveTo(-0.7,-3.6,-0.4,-3.3).curveTo(-0.2,-3.1,-0.2,-2.6).lineTo(-0.2,-2.3).curveTo(0.4,-3.6,1.2,-3.6).curveTo(1.7,-3.6,1.9,-3.3).curveTo(2.2,-3.1,2.2,-2.6).curveTo(2.2,-1.9,1.4,-1.7).curveTo(-0.2,-1.4,-0.2,0.7).lineTo(-0.2,2.6).curveTo(-0.2,3,-0.4,3.3).curveTo(-0.7,3.7,-1.2,3.7).curveTo(-1.6,3.7,-1.9,3.3).closePath();
    this.shape_88.setTransform(-23.1,12.3);

    this.shape_89 = new cjs.Shape();
    this.shape_89.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-0.7,4.6).curveTo(-1,4.3,-1,3.9).lineTo(-1,-1.4).curveTo(-1,-1.8,-0.7,-2.1).curveTo(-0.4,-2.4,-0,-2.4).curveTo(0.4,-2.4,0.7,-2.1).curveTo(1,-1.8,1,-1.4).lineTo(1,3.9).curveTo(1,4.3,0.7,4.6).curveTo(0.4,4.9,-0,4.9).curveTo(-0.4,4.9,-0.7,4.6).closePath().moveTo(-0.8,-3.3).curveTo(-1.1,-3.5,-1.1,-4).curveTo(-1.1,-4.4,-0.8,-4.7).curveTo(-0.5,-4.9,-0,-4.9).curveTo(0.5,-4.9,0.8,-4.7).curveTo(1.1,-4.4,1.1,-4).curveTo(1.1,-3.5,0.8,-3.3).curveTo(0.5,-3,-0,-3).curveTo(-0.5,-3,-0.8,-3.3).closePath();
    this.shape_89.setTransform(-28,11);

    this.shape_90 = new cjs.Shape();
    this.shape_90.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-2.6,3.1).curveTo(-3.3,2.5,-3.3,1.5).curveTo(-3.2,0.3,-2.5,-0.3).curveTo(-1.7,-0.8,-0.4,-0.8).curveTo(0.5,-0.8,1.3,-0.5).lineTo(1.3,-0.6).curveTo(1.3,-1.9,-0.2,-1.9).curveTo(-0.9,-1.9,-1.6,-1.7).lineTo(-1.9,-1.7).curveTo(-2.3,-1.7,-2.5,-1.9).curveTo(-2.7,-2.2,-2.7,-2.5).curveTo(-2.7,-3.1,-2.2,-3.3).curveTo(-1.2,-3.7,0.1,-3.7).curveTo(1.7,-3.7,2.5,-2.9).curveTo(3.3,-2.1,3.2,-0.6).lineTo(3.2,2.6).curveTo(3.2,3,3,3.3).curveTo(2.7,3.6,2.3,3.6).curveTo(1.9,3.6,1.6,3.3).curveTo(1.3,3.1,1.3,2.8).curveTo(0.5,3.7,-0.8,3.7).curveTo(-1.9,3.7,-2.6,3.1).closePath().moveTo(-1.3,1.4).curveTo(-1.3,1.8,-1,2).curveTo(-0.7,2.3,-0.2,2.3).curveTo(0.5,2.3,0.9,1.9).curveTo(1.4,1.6,1.3,1).lineTo(1.3,0.7).curveTo(0.8,0.4,0.1,0.4).curveTo(-1.3,0.4,-1.3,1.4).closePath();
    this.shape_90.setTransform(-33.9,12.3);

    this.shape_91 = new cjs.Shape();
    this.shape_91.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(11.9,-2.1).lineTo(5.1,-2.1).lineTo(-5,-12.4).curveTo(-5.2,-12.7,-5.4,-12.4).curveTo(-5.9,-12.2,-5.7,-12).lineTo(-2.5,-2.1).lineTo(-12,-2.1).lineTo(-12,-3.4).curveTo(-12,-4.6,-12.6,-4.6).curveTo(-14,-5,-14,-3.7).lineTo(-14,3.8).curveTo(-14,4.7,-13.1,4.7).curveTo(-12,5.1,-12,3.8).lineTo(-12,2.2).lineTo(-2.5,2.2).lineTo(-5.7,12.3).curveTo(-5.9,12.5,-5,12.5).lineTo(5.1,2.2).lineTo(11.9,2.2).curveTo(13,2.2,13.7,1.5).curveTo(14.1,0.6,13.9,-0.3).curveTo(13.5,-2.1,11.9,-2.1).closePath();
    this.shape_91.setTransform(3.6,-14.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80}]}).to({state:[]},21).to({state:[]},25).wait(1));

    // Layer 1
    this.shape_92 = new cjs.Shape();
    this.shape_92.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-43.4,-93.3).lineTo(43.3,-93.3).curveTo(52.5,-93.3,52.5,-85.2).lineTo(52.5,67.5).curveTo(52.5,75.6,43.3,75.6).lineTo(-43.4,75.6).curveTo(-52.6,75.6,-52.6,67.5).lineTo(-52.6,-85.2).curveTo(-52.6,-90.9,-48.1,-92.6).curveTo(-46.2,-93.3,-43.4,-93.3).closePath().moveTo(-49.9,-101.4).lineTo(49.9,-101.4).curveTo(60.4,-101.4,60.4,-91.7).lineTo(60.4,91.8).curveTo(60.4,101.4,49.9,101.4).lineTo(-49.9,101.4).curveTo(-60.4,101.4,-60.4,91.8).lineTo(-60.4,-91.7).curveTo(-60.4,-92.1,-60.4,-92.6).curveTo(-59.9,-101.4,-49.9,-101.4).closePath().moveTo(7.1,88.5).curveTo(7.1,91.5,5,93.5).curveTo(2.9,95.5,-0,95.5).curveTo(-2.9,95.5,-5,93.5).curveTo(-7.1,91.5,-7.1,88.5).curveTo(-7.1,85.5,-5,83.4).curveTo(-2.9,81.4,-0,81.4).curveTo(2.9,81.4,5,83.4).curveTo(7.1,85.5,7.1,88.5).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape_92).to({_off:true},21).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-61.3,-102.4,122.8,204.8);


(lib.aniIcon2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 2
    this.instance = new lib.graIcon2();
    this.instance.parent = this;
    this.instance.setTransform(-0.4,36.2,0.6,0.6,0,0,0,-0.1,70);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.61,scaleY:0.59},1,cjs.Ease.get(-1)).to({regY:70.1,scaleX:0.68,scaleY:0.52,y:36.3},2).to({regY:70.2,scaleX:0.52,scaleY:0.64,x:-0.5,y:36.4},2,cjs.Ease.get(1)).to({regY:70.1,scaleX:0.5,scaleY:0.66,x:-0.4,y:36.3},1).to({regY:70,scaleX:0.6,scaleY:0.56,y:36.2},4).to({scaleY:0.6},3).wait(1).to({scaleX:0.61,scaleY:0.59},1,cjs.Ease.get(-1)).to({regY:70.1,scaleX:0.68,scaleY:0.52,y:36.3},2).to({regY:70.2,scaleX:0.52,scaleY:0.64,x:-0.5,y:36.4},2,cjs.Ease.get(1)).to({regY:70.1,scaleX:0.5,scaleY:0.66,x:-0.4,y:36.3},1).to({regY:70,scaleX:0.6,scaleY:0.56,y:36.2},4).to({scaleY:0.6},3).wait(2));

    // Layer 3
    this.instance_1 = new lib.anio("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-7,-13.8,0.48,0.48,48.5,0,0,0,0.1);

    this.instance_2 = new lib.anio("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-3,-74.8,0.48,0.48,-106.5);

    this.instance_3 = new lib.anio("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-23.1,-105.2,0.48,0.48,161,0,0,0.1,0.1);

    this.instance_4 = new lib.anio("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(4.3,-57.5,0.48,0.48,71);

    this.instance_5 = new lib.ani("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(11.3,-22.8,0.6,0.6,11,0,0,0.1,-0.1);

    this.instance_6 = new lib.ani("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(-5.7,-23.9,0.6,0.6,101,0,0,-0.2,-0.1);

    this.instance_7 = new lib.ani("synched",0,false);
    this.instance_7.parent = this;
    this.instance_7.setTransform(-15.7,-19.1,0.6,0.6,151.7);

    this.instance_8 = new lib.ani("synched",0,false);
    this.instance_8.parent = this;
    this.instance_8.setTransform(24,-58,0.6,0.6,26,0,0,-0.1,-0.1);

    this.instance_9 = new lib.aniStar("synched",0,false);
    this.instance_9.parent = this;
    this.instance_9.setTransform(13.2,-16.5,0.6,0.6,150.5,0,0,-0.1,0.1);

    this.instance_10 = new lib.aniStar("synched",0,false);
    this.instance_10.parent = this;
    this.instance_10.setTransform(-11,-57.7,0.6,0.6,149.5);

    this.instance_11 = new lib.aniStar("synched",0,false);
    this.instance_11.parent = this;
    this.instance_11.setTransform(-19,-12.8,0.6,0.6,50.8);

    this.instance_12 = new lib.aniStar("synched",0,false);
    this.instance_12.parent = this;
    this.instance_12.setTransform(14.9,-69.7,0.6,0.6,41.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1}]}).wait(29));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-57.5,-123.6,117.9,154.8);


(lib.aniFireworkSpark = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 6
    this.instance = new lib.ani("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-2.2,1.5,0.5,0.5,-90,0,0,-0.1,-0.1);

    this.instance_1 = new lib.ani("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-2.2,-2.8,0.5,0.5,0,0,0,-0.1,-0.1);

    this.instance_2 = new lib.ani("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(2.8,2.8,0.5,0.5,180,0,0,-0.1,-0.1);

    this.instance_3 = new lib.ani("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(2.8,-8.5,0.5,0.5,90,0,0,-0.1,-0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},8).to({state:[]},7).wait(15));

    // Layer 7
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(3.5,0).curveTo(3.5,1.4,2.5,2.5).curveTo(1.4,3.5,0,3.5).curveTo(-1.4,3.5,-2.5,2.5).curveTo(-3.5,1.4,-3.5,0).curveTo(-3.5,-1.4,-2.5,-2.5).curveTo(-1.4,-3.5,0,-3.5).curveTo(1.4,-3.5,2.5,-2.5).curveTo(3.5,-1.4,3.5,0).closePath();

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4.7,0).curveTo(-4.7,-1.9,-3.3,-3.3).curveTo(-1.9,-4.7,0,-4.7).curveTo(1.9,-4.7,3.3,-3.3).curveTo(4.7,-1.9,4.7,0).curveTo(4.7,1.9,3.3,3.3).curveTo(1.9,4.7,0,4.7).curveTo(-1.9,4.7,-3.3,3.3).curveTo(-4.7,1.9,-4.7,0).closePath();

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.8,0).curveTo(-5.8,-2.5,-4.2,-4.2).curveTo(-2.5,-5.8,0,-5.8).curveTo(2.5,-5.8,4.2,-4.2).curveTo(5.8,-2.5,5.8,0).curveTo(5.8,2.5,4.2,4.2).curveTo(2.5,5.8,0,5.8).curveTo(-2.5,5.8,-4.2,4.2).curveTo(-5.8,2.5,-5.8,0).closePath();

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7,0).curveTo(-7,-3,-5,-5).curveTo(-3,-7,0,-7).curveTo(3,-7,5,-5).curveTo(7,-3,7,0).curveTo(7,3,5,5).curveTo(3,7,0,7).curveTo(-3,7,-5,5).curveTo(-7,3,-7,0).closePath();

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-8.2,0).curveTo(-8.2,-3.5,-5.8,-5.8).curveTo(-3.5,-8.2,0,-8.2).curveTo(3.5,-8.2,5.8,-5.8).curveTo(8.2,-3.5,8.2,0).curveTo(8.2,3.5,5.8,5.8).curveTo(3.5,8.2,0,8.2).curveTo(-3.5,8.2,-5.8,5.8).curveTo(-8.2,3.5,-8.2,0).closePath();

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.4,0).curveTo(-9.4,-4,-6.7,-6.7).curveTo(-4,-9.4,0,-9.4).curveTo(4,-9.4,6.8,-6.7).curveTo(9.4,-4,9.4,0).curveTo(9.4,4,6.8,6.8).curveTo(4,9.4,0,9.4).curveTo(-4,9.4,-6.7,6.8).curveTo(-9.4,4,-9.4,0).closePath();

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-10.7,0).curveTo(-10.7,-4.5,-7.6,-7.6).curveTo(-4.5,-10.7,0,-10.7).curveTo(4.5,-10.7,7.6,-7.6).curveTo(10.7,-4.5,10.7,0).curveTo(10.7,4.5,7.6,7.6).curveTo(4.5,10.7,0,10.7).curveTo(-4.5,10.7,-7.6,7.6).curveTo(-10.7,4.5,-10.7,0).closePath();

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-11.8,0).curveTo(-11.8,-5,-8.4,-8.4).curveTo(-5,-11.8,0,-11.8).curveTo(5,-11.8,8.4,-8.4).curveTo(11.8,-5,11.8,0).curveTo(11.8,5,8.4,8.4).curveTo(5,11.8,0,11.8).curveTo(-5,11.8,-8.4,8.4).curveTo(-11.8,5,-11.8,0).closePath();

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.1,0).curveTo(-13.1,-5.5,-9.3,-9.3).curveTo(-5.5,-13.1,0,-13.1).curveTo(5.5,-13.1,9.3,-9.3).curveTo(13.1,-5.5,13.1,0).curveTo(13.1,5.5,9.3,9.3).curveTo(5.5,13.1,0,13.1).curveTo(-5.5,13.1,-9.3,9.3).curveTo(-13.1,5.5,-13.1,0).closePath();

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-14.2,0).curveTo(-14.2,-6,-10.2,-10.2).curveTo(-6,-14.2,0,-14.2).curveTo(6,-14.2,10.2,-10.2).curveTo(14.2,-6,14.2,0).curveTo(14.2,6,10.2,10.2).curveTo(6,14.2,0,14.2).curveTo(-6,14.2,-10.2,10.2).curveTo(-14.2,6,-14.2,0).closePath();

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(15.4,0).curveTo(15.4,6.5,11,11).curveTo(6.5,15.4,0,15.4).curveTo(-6.5,15.4,-11,11).curveTo(-15.4,6.5,-15.4,0).curveTo(-15.4,-6.5,-11,-11).curveTo(-6.5,-15.4,0,-15.4).curveTo(6.5,-15.4,11,-11).curveTo(15.4,-6.5,15.4,0).closePath();

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},1).wait(15));

    // Layer 3
    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill("#EF3224").beginStroke().moveTo(-2.3,2.2).curveTo(-3.2,1.3,-3.2,0).curveTo(-3.2,-1.3,-2.3,-2.3).curveTo(-1.3,-3.2,0,-3.2).curveTo(1.3,-3.2,2.2,-2.3).curveTo(3.2,-1.3,3.1,0).curveTo(3.2,1.3,2.2,2.2).curveTo(1.3,3.2,0,3.1).curveTo(-1.3,3.2,-2.3,2.2).closePath();

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill("#EF3224").beginStroke().moveTo(-2.4,2.4).curveTo(-3.4,1.4,-3.4,0).curveTo(-3.4,-1.4,-2.4,-2.4).curveTo(-1.4,-3.4,0,-3.4).curveTo(1.4,-3.4,2.4,-2.4).curveTo(3.4,-1.4,3.4,0).curveTo(3.4,1.4,2.4,2.4).curveTo(1.4,3.4,0,3.4).curveTo(-1.4,3.4,-2.4,2.4).closePath();

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill("#EF3224").beginStroke().moveTo(-3,3).curveTo(-4.2,1.7,-4.1,0).curveTo(-4.2,-1.7,-3,-3).curveTo(-1.7,-4.2,0,-4.1).curveTo(1.7,-4.2,3,-3).curveTo(4.2,-1.7,4.1,0).curveTo(4.2,1.7,3,3).curveTo(1.7,4.2,0,4.1).curveTo(-1.7,4.2,-3,3).closePath();

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill("#EF3224").beginStroke().moveTo(-3.9,3.9).curveTo(-5.5,2.3,-5.5,0).curveTo(-5.5,-2.3,-3.9,-3.9).curveTo(-2.3,-5.5,0,-5.5).curveTo(2.3,-5.5,3.9,-3.9).curveTo(5.5,-2.3,5.5,0).curveTo(5.5,2.3,3.9,3.9).curveTo(2.3,5.5,0,5.5).curveTo(-2.3,5.5,-3.9,3.9).closePath();

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill("#EF3224").beginStroke().moveTo(-5.1,5.1).curveTo(-7.3,3,-7.3,0).curveTo(-7.3,-3,-5.1,-5.1).curveTo(-3,-7.3,0,-7.3).curveTo(3,-7.3,5.1,-5.1).curveTo(7.3,-3,7.3,0).curveTo(7.3,3,5.1,5.1).curveTo(3,7.3,0,7.3).curveTo(-3,7.3,-5.1,5.1).closePath();

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill("#EF3224").beginStroke().moveTo(-6.8,6.8).curveTo(-9.5,4,-9.5,0).curveTo(-9.5,-4,-6.8,-6.8).curveTo(-4,-9.5,0,-9.5).curveTo(4,-9.5,6.8,-6.8).curveTo(9.5,-4,9.5,0).curveTo(9.5,4,6.8,6.8).curveTo(4,9.5,0,9.5).curveTo(-4,9.5,-6.8,6.8).closePath();

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill("#EF3224").beginStroke().moveTo(-8.7,8.7).curveTo(-12.4,5.1,-12.3,0).curveTo(-12.4,-5.1,-8.7,-8.7).curveTo(-5.1,-12.4,0,-12.3).curveTo(5.1,-12.4,8.7,-8.7).curveTo(12.4,-5.1,12.3,0).curveTo(12.4,5.1,8.7,8.7).curveTo(5.1,12.4,0,12.3).curveTo(-5.1,12.4,-8.7,8.7).closePath();

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill("#EF3224").beginStroke().moveTo(-11.1,11.1).curveTo(-15.7,6.5,-15.7,0).curveTo(-15.7,-6.5,-11.1,-11.1).curveTo(-6.5,-15.7,0,-15.7).curveTo(6.5,-15.7,11.1,-11.1).curveTo(15.7,-6.5,15.7,0).curveTo(15.7,6.5,11.1,11.1).curveTo(6.5,15.7,0,15.7).curveTo(-6.5,15.7,-11.1,11.1).closePath();

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill("#EF3224").beginStroke().moveTo(-13.8,13.8).curveTo(-19.5,8.1,-19.5,0).curveTo(-19.5,-8.1,-13.8,-13.8).curveTo(-8.1,-19.5,0,-19.5).curveTo(8.1,-19.5,13.8,-13.8).curveTo(19.5,-8.1,19.5,0).curveTo(19.5,8.1,13.8,13.8).curveTo(8.1,19.5,0,19.5).curveTo(-8.1,19.5,-13.8,13.8).closePath();

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill("#EF3224").beginStroke().moveTo(-16.8,16.8).curveTo(-23.8,9.8,-23.9,0).curveTo(-23.8,-9.8,-16.8,-16.8).curveTo(-9.8,-23.8,0,-23.9).curveTo(9.8,-23.8,16.8,-16.8).curveTo(23.8,-9.8,23.9,0).curveTo(23.8,9.8,16.8,16.8).curveTo(9.8,23.8,0,23.9).curveTo(-9.8,23.8,-16.8,16.8).closePath();

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill("#EF3224").beginStroke().moveTo(-20.3,20.2).curveTo(-28.7,11.9,-28.7,0).curveTo(-28.7,-11.9,-20.3,-20.3).curveTo(-11.9,-28.7,0,-28.7).curveTo(11.9,-28.7,20.2,-20.3).curveTo(28.7,-11.9,28.7,0).curveTo(28.7,11.9,20.2,20.2).curveTo(11.9,28.7,0,28.7).curveTo(-11.9,28.7,-20.3,20.2).closePath();

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill("#EF3224").beginStroke().moveTo(-24,24).curveTo(-34.1,14,-34,0).curveTo(-34.1,-14,-24,-24).curveTo(-14,-34.1,0,-34).curveTo(14,-34.1,24,-24).curveTo(34.1,-14,34,0).curveTo(34.1,14,24,24).curveTo(14,34.1,0,34).curveTo(-14,34.1,-24,24).closePath();

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill("#EF3224").beginStroke().moveTo(-28.2,28.2).curveTo(-40,16.5,-40,0).curveTo(-40,-16.5,-28.2,-28.2).curveTo(-16.5,-40,0,-40).curveTo(16.5,-40,28.2,-28.2).curveTo(40,-16.5,40,0).curveTo(40,16.5,28.2,28.2).curveTo(16.5,40,0,40).curveTo(-16.5,40,-28.2,28.2).closePath();

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},2).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[]},9).wait(7));

    // Layer 5
    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(34.7,-13.5).lineTo(24.6,-9.5).moveTo(15,-34.2).lineTo(10.6,-24.2).moveTo(9.4,24.5).lineTo(13.4,34.8).moveTo(34.2,15.1).lineTo(24,10.7).moveTo(-24.2,-10.6).lineTo(-34.1,-14.9).moveTo(-13.6,-34.8).lineTo(-9.7,-24.6).moveTo(-10.6,24.1).lineTo(-15,34.1).moveTo(-24.5,9.6).lineTo(-34.7,13.5);
    this.shape_24.setTransform(0,-0.1);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0.1,0.1).lineTo(0.1,37.3).moveTo(0.1,0.1).lineTo(26.4,26.4).moveTo(0.1,0.1).lineTo(37.3,0.1).moveTo(26.5,-26.4).lineTo(0.1,0.1).moveTo(0.1,-37.3).lineTo(0.1,0.1).moveTo(-26.4,-26.4).lineTo(0.1,0.1).lineTo(-26.2,26.4).moveTo(-37.3,0.1).lineTo(0.1,0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_25},{t:this.shape_24}]},12).to({state:[]},11).wait(7));

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().moveTo(-3.7,3.7).curveTo(-5.2,2.2,-5.2,0).curveTo(-5.2,-2.2,-3.7,-3.7).curveTo(-2.2,-5.2,0,-5.2).curveTo(2.2,-5.2,3.7,-3.7).curveTo(5.2,-2.2,5.2,0).curveTo(5.2,2.2,3.7,3.7).curveTo(2.2,5.2,0,5.2).curveTo(-2.2,5.2,-3.7,3.7).closePath();
    var mask_graphics_1 = new cjs.Graphics().moveTo(-4,4).curveTo(-5.6,2.3,-5.6,0).curveTo(-5.6,-2.3,-4,-4).curveTo(-2.3,-5.6,0,-5.6).curveTo(2.3,-5.6,4,-4).curveTo(5.6,-2.3,5.6,0).curveTo(5.6,2.3,4,4).curveTo(2.3,5.6,0,5.6).curveTo(-2.3,5.6,-4,4).closePath();
    var mask_graphics_2 = new cjs.Graphics().moveTo(-4.9,4.9).curveTo(-6.9,2.9,-6.9,0).curveTo(-6.9,-2.9,-4.9,-4.9).curveTo(-2.9,-6.9,0,-6.9).curveTo(2.9,-6.9,4.9,-4.9).curveTo(6.9,-2.9,6.9,0).curveTo(6.9,2.9,4.9,4.9).curveTo(2.9,6.9,0,6.9).curveTo(-2.9,6.9,-4.9,4.9).closePath();
    var mask_graphics_3 = new cjs.Graphics().moveTo(-6.4,6.4).curveTo(-9,3.7,-9,0).curveTo(-9,-3.7,-6.4,-6.4).curveTo(-3.7,-9,0,-9).curveTo(3.7,-9,6.4,-6.4).curveTo(9,-3.7,9,0).curveTo(9,3.7,6.4,6.4).curveTo(3.7,9,0,9).curveTo(-3.7,9,-6.4,6.4).closePath();
    var mask_graphics_4 = new cjs.Graphics().moveTo(-8.5,8.5).curveTo(-11.9,4.9,-12,0).curveTo(-11.9,-5,-8.5,-8.5).curveTo(-5,-11.9,0,-12).curveTo(4.9,-11.9,8.5,-8.5).curveTo(11.9,-5,12,0).curveTo(11.9,4.9,8.5,8.5).curveTo(4.9,11.9,0,12).curveTo(-5,11.9,-8.5,8.5).closePath();
    var mask_graphics_5 = new cjs.Graphics().moveTo(-11.2,11.2).curveTo(-15.7,6.5,-15.8,0).curveTo(-15.7,-6.5,-11.2,-11.2).curveTo(-6.5,-15.7,0,-15.8).curveTo(6.5,-15.7,11.2,-11.2).curveTo(15.7,-6.5,15.8,0).curveTo(15.7,6.5,11.2,11.2).curveTo(6.5,15.7,0,15.8).curveTo(-6.5,15.7,-11.2,11.2).closePath();
    var mask_graphics_6 = new cjs.Graphics().moveTo(-14.4,14.4).curveTo(-20.3,8.5,-20.3,0).curveTo(-20.3,-8.4,-14.4,-14.4).curveTo(-8.4,-20.3,0,-20.3).curveTo(8.5,-20.3,14.4,-14.4).curveTo(20.3,-8.4,20.3,0).curveTo(20.3,8.5,14.4,14.4).curveTo(8.5,20.3,0,20.3).curveTo(-8.4,20.3,-14.4,14.4).closePath();
    var mask_graphics_7 = new cjs.Graphics().moveTo(-18.3,18.3).curveTo(-25.9,10.7,-25.8,0).curveTo(-25.9,-10.7,-18.3,-18.3).curveTo(-10.7,-25.9,0,-25.8).curveTo(10.7,-25.9,18.3,-18.3).curveTo(25.9,-10.7,25.8,0).curveTo(25.9,10.7,18.3,18.3).curveTo(10.7,25.9,0,25.8).curveTo(-10.7,25.9,-18.3,18.3).closePath();
    var mask_graphics_8 = new cjs.Graphics().moveTo(-22.8,22.8).curveTo(-32.2,13.3,-32.1,0).curveTo(-32.2,-13.3,-22.8,-22.8).curveTo(-13.3,-32.2,0,-32.1).curveTo(13.3,-32.2,22.8,-22.8).curveTo(32.2,-13.3,32.1,0).curveTo(32.2,13.3,22.8,22.8).curveTo(13.3,32.2,0,32.1).curveTo(-13.3,32.2,-22.8,22.8).closePath();
    var mask_graphics_9 = new cjs.Graphics().moveTo(-27.8,27.8).curveTo(-39.3,16.2,-39.3,0).curveTo(-39.3,-16.2,-27.8,-27.8).curveTo(-16.2,-39.3,0,-39.3).curveTo(16.2,-39.3,27.8,-27.8).curveTo(39.3,-16.2,39.3,0).curveTo(39.3,16.2,27.8,27.8).curveTo(16.2,39.3,0,39.3).curveTo(-16.2,39.3,-27.8,27.8).closePath();
    var mask_graphics_10 = new cjs.Graphics().moveTo(-33.5,33.5).curveTo(-47.3,19.6,-47.3,0).curveTo(-47.3,-19.6,-33.5,-33.5).curveTo(-19.6,-47.3,0,-47.3).curveTo(19.6,-47.3,33.5,-33.5).curveTo(47.3,-19.6,47.3,0).curveTo(47.3,19.6,33.5,33.5).curveTo(19.6,47.3,0,47.3).curveTo(-19.6,47.3,-33.5,33.5).closePath();
    var mask_graphics_11 = new cjs.Graphics().moveTo(-39.7,39.7).curveTo(-56.1,23.2,-56.2,0).curveTo(-56.1,-23.2,-39.7,-39.7).curveTo(-23.2,-56.1,0,-56.2).curveTo(23.2,-56.1,39.7,-39.7).curveTo(56.1,-23.2,56.2,0).curveTo(56.1,23.2,39.7,39.7).curveTo(23.2,56.1,0,56.2).curveTo(-23.2,56.1,-39.7,39.7).closePath();
    var mask_graphics_12 = new cjs.Graphics().moveTo(-46.5,46.5).curveTo(-65.9,27.2,-65.9,0).curveTo(-65.9,-27.2,-46.5,-46.5).curveTo(-27.2,-65.9,0,-65.9).curveTo(27.2,-65.9,46.5,-46.5).curveTo(65.9,-27.2,65.9,0).curveTo(65.9,27.2,46.5,46.5).curveTo(27.2,65.9,0,65.9).curveTo(-27.2,65.9,-46.5,46.5).closePath();

    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:0,y:0}).wait(1).to({graphics:mask_graphics_1,x:0,y:0}).wait(1).to({graphics:mask_graphics_2,x:0,y:0}).wait(1).to({graphics:mask_graphics_3,x:0,y:0}).wait(1).to({graphics:mask_graphics_4,x:0,y:0}).wait(1).to({graphics:mask_graphics_5,x:0,y:0}).wait(1).to({graphics:mask_graphics_6,x:0,y:0}).wait(1).to({graphics:mask_graphics_7,x:0,y:0}).wait(1).to({graphics:mask_graphics_8,x:0,y:0}).wait(1).to({graphics:mask_graphics_9,x:0,y:0}).wait(1).to({graphics:mask_graphics_10,x:0,y:0}).wait(1).to({graphics:mask_graphics_11,x:0,y:0}).wait(1).to({graphics:mask_graphics_12,x:0,y:0}).wait(11).to({graphics:null,x:0,y:0}).wait(7));

    // Layer 1
    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(34.7,-13.5).lineTo(24.6,-9.5).moveTo(15,-34.2).lineTo(10.6,-24.2).moveTo(9.4,24.5).lineTo(13.4,34.8).moveTo(34.2,15.1).lineTo(24,10.7).moveTo(-24.2,-10.6).lineTo(-34.1,-14.9).moveTo(-13.6,-34.8).lineTo(-9.7,-24.6).moveTo(-10.6,24.1).lineTo(-15,34.1).moveTo(-24.5,9.6).lineTo(-34.7,13.5);
    this.shape_26.setTransform(0,-0.1);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0.1,0.1).lineTo(0.1,37.3).moveTo(0.1,0.1).lineTo(26.4,26.4).moveTo(0.1,0.1).lineTo(37.3,0.1).moveTo(26.5,-26.4).lineTo(0.1,0.1).moveTo(0.1,-37.3).lineTo(0.1,0.1).moveTo(-26.4,-26.4).lineTo(0.1,0.1).lineTo(-26.2,26.4).moveTo(-37.3,0.1).lineTo(0.1,0.1);

    var maskedShapeInstanceList = [this.shape_26,this.shape_27];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).to({state:[]},23).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.2,-5.2,10.4,10.4);


(lib.phase2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // wave
    this.instance = new lib.aniWave("single",0);
    this.instance.parent = this;
    this.instance.setTransform(108,235.4);
    this.instance.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance).to({y:205.4,alpha:1,mode:"synched"},4).wait(56));

    // aniIcon1
    this.instance_1 = new lib.aniIcon1("single",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(4.4,27.3,0.9,0.9,0,0,0,0.1,-0.1);
    this.instance_1.alpha = 0;
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({y:-2.7,alpha:1,mode:"synched"},4,cjs.Ease.get(1)).wait(55));

    // aniIcon2
    this.instance_2 = new lib.aniIcon2("single",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(108.9,163.4,0.807,0.807,10.3,0,0,-0.2,0.1);
    this.instance_2.alpha = 0;
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).to({y:133.4,alpha:1,mode:"synched",loop:false},4,cjs.Ease.get(1)).wait(51));

    // aniIcon2
    this.instance_3 = new lib.aniIcon2("single",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-129.8,162.9,0.807,0.807,-10.7,0,0,-0.4,0.1);
    this.instance_3.alpha = 0;
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({y:132.9,alpha:1,mode:"synched",loop:false},4,cjs.Ease.get(1)).wait(50));

    // aniDrum
    this.instance_4 = new lib.aniDrum("single",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(223.8,163.5,1,1,31.5,0,0,-0.8,0);
    this.instance_4.alpha = 0;
    this.instance_4._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(11).to({_off:false},0).to({y:133.5,alpha:1,mode:"synched"},4,cjs.Ease.get(1)).wait(45));

    // aniDrum
    this.instance_5 = new lib.aniDrum("single",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(-239.2,163.4,1,1,-21.2,0,0,-0.8,-0.1);
    this.instance_5.alpha = 0;
    this.instance_5._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(12).to({_off:false},0).to({y:133.4,alpha:1,mode:"synched"},4,cjs.Ease.get(1)).wait(44));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-112.7,232.4,219.5,6);


(lib.ani_1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer_19
    this.instance = new lib.aniIcon5("single",0);
    this.instance.parent = this;
    this.instance.setTransform(-6.1,44.9);
    this.instance.alpha = 0;
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(128).to({_off:false},0).to({y:14.9,alpha:1,mode:"synched",loop:false},4,cjs.Ease.get(1)).wait(26));

    // milstone
    this.instance_1 = new lib.aniMilestone("single",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-10,45);
    this.instance_1.alpha = 0;
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(101).to({_off:false},0).to({y:30,alpha:1,mode:"synched",loop:false},4).to({_off:true},23).wait(30));

    // plane
    this.instance_2 = new lib.Tween1("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-173.2,92.2,0.8,0.8);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(84).to({_off:false},0).to({x:-88.8,y:61.4},16).to({_off:true},1).wait(57));

    // plane
    this.instance_3 = new lib.Tween1("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(103.2,30.1);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween1copy("synched",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(102.2,-51.9);
    this.instance_4._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(84).to({_off:false},0).to({x:228.7,y:-48.4},16).to({_off:true},1).wait(57));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(107).to({_off:false},0).to({x:228.7,y:-108.4},20).to({_off:true},1).wait(30));

    // plane
    this.instance_5 = new lib.Tween1("synched",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(-243.8,29.1);
    this.instance_5._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(84).to({_off:false},0).to({x:-138.3,y:-9.4},16).to({_off:true},1).wait(57));

    // globe
    this.instance_6 = new lib.aniGlobe();
    this.instance_6.parent = this;
    this.instance_6.setTransform(0,50);
    this.instance_6.alpha = 0;
    this.instance_6._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(84).to({_off:false},0).to({y:10,alpha:1,mode:"synched",startPosition:0,loop:false},4,cjs.Ease.get(1)).to({_off:true},13).wait(57));

    // phase3
    this.instance_7 = new lib.anio("synched",0,false);
    this.instance_7.parent = this;
    this.instance_7.setTransform(1.1,20,0.8,0.8,-37.4);

    this.instance_8 = new lib.anio("synched",0,false);
    this.instance_8.parent = this;
    this.instance_8.setTransform(1,20,0.8,0.8,-132.5);

    this.instance_9 = new lib.anio("synched",0,false);
    this.instance_9.parent = this;
    this.instance_9.setTransform(1.1,20,0.8,0.8,135);

    this.instance_10 = new lib.anio("synched",0,false);
    this.instance_10.parent = this;
    this.instance_10.setTransform(1,20,0.8,0.8,45);

    this.instance_11 = new lib.ani("synched",0,false);
    this.instance_11.parent = this;
    this.instance_11.setTransform(-5.5,25.7,1,1,-90);

    this.instance_12 = new lib.ani("synched",0,false);
    this.instance_12.parent = this;
    this.instance_12.setTransform(5.5,14.3,1,1,90);

    this.instance_13 = new lib.ani("synched",0,false);
    this.instance_13.parent = this;
    this.instance_13.setTransform(5.7,25.5,1,1,180);

    this.instance_14 = new lib.ani("synched",0,false);
    this.instance_14.parent = this;
    this.instance_14.setTransform(-5.7,14.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7}]},86).to({state:[]},15).wait(57));

    // Layer_11
    this.instance_15 = new lib.aniWaves("synched",0,false);
    this.instance_15.parent = this;
    this.instance_15.setTransform(109.2,60);
    this.instance_15.alpha = 0;
    this.instance_15._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(59).to({_off:false},0).to({y:30,alpha:1},4,cjs.Ease.get(1)).to({_off:true},21).wait(74));

    // lines
    this.instance_16 = new lib.aniWaves("synched",0,false);
    this.instance_16.parent = this;
    this.instance_16.setTransform(-189.8,60);
    this.instance_16.alpha = 0;
    this.instance_16._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(59).to({_off:false},0).to({y:30,alpha:1},4,cjs.Ease.get(1)).to({_off:true},21).wait(74));

    // phase2-
    this.instance_17 = new lib.aniPhone("single",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(0.1,59.7);
    this.instance_17.alpha = 0;
    this.instance_17._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(59).to({_off:false},0).to({y:29.7,alpha:1,mode:"synched",loop:false},4,cjs.Ease.get(1)).to({_off:true},21).wait(74));

    // Layer_9
    this.instance_18 = new lib.phase2("synched",0,false);
    this.instance_18.parent = this;
    this.instance_18.setTransform(10.7,-37.8);
    this.instance_18._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(33).to({_off:false},0).to({_off:true},26).wait(99));

    // star
    this.instance_19 = new lib.aniStar("synched",0,false);
    this.instance_19.parent = this;
    this.instance_19.setTransform(228.5,11.5,1,1,60,0,0,-88.5,5.4);
    this.instance_19._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(26).to({_off:false},0).to({_off:true},7).wait(125));

    // star
    this.instance_20 = new lib.aniStar("synched",0,false);
    this.instance_20.parent = this;
    this.instance_20.setTransform(-145.5,-24.5,1,1,60,0,0,-88.5,5.5);
    this.instance_20._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(23).to({_off:false},0).to({_off:true},10).wait(125));

    // Layer_4
    this.instance_21 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_21.parent = this;
    this.instance_21.setTransform(-272.5,65.5);
    this.instance_21._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(25).to({_off:false},0).to({_off:true},8).wait(125));

    // firework3
    this.instance_22 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_22.parent = this;
    this.instance_22.setTransform(244.5,29.5);

    this.instance_23 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_23.parent = this;
    this.instance_23.setTransform(-125.5,-14.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_23},{t:this.instance_22}]},18).to({state:[]},15).wait(125));

    // firework2
    this.instance_24 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_24.parent = this;
    this.instance_24.setTransform(98.5,8.5);
    this.instance_24._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(15).to({_off:false},0).to({_off:true},18).wait(125));

    // firework1
    this.instance_25 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_25.parent = this;
    this.instance_25.setTransform(-20,-48.4);
    this.instance_25._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(10).to({_off:false},0).to({_off:true},23).wait(125));

    // fireworkLine3
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,5.3).lineTo(0,-5.3);
    this.shape.setTransform(-273,168.3);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-9.6).lineTo(0,9.6);
    this.shape_1.setTransform(-273,157.4);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.9).lineTo(0,13.9);
    this.shape_2.setTransform(-273,146.4);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.3).lineTo(0,18.3);
    this.shape_3.setTransform(-273,135.5);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,22.6).lineTo(0,-22.6);
    this.shape_4.setTransform(-273,124.6);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.4).lineTo(0,18.4);
    this.shape_5.setTransform(-273,114.5);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-14.1).lineTo(0,14.2);
    this.shape_6.setTransform(-273,104.4);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-10).lineTo(0,10);
    this.shape_7.setTransform(-273,94.3);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-5.7).lineTo(0,5.7);
    this.shape_8.setTransform(-273,84.2);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,1.5).lineTo(0,-1.5);
    this.shape_9.setTransform(-273,74.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},17).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[]},1).wait(131));

    // fireworkLine3
    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_10.setTransform(243,164);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-12.8).lineTo(0,12.8);
    this.shape_11.setTransform(243,149.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.5).lineTo(0,18.5);
    this.shape_12.setTransform(243,135);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.2).lineTo(0,24.2);
    this.shape_13.setTransform(243,120.5);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,30).lineTo(0,-30);
    this.shape_14.setTransform(243,106);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.4).lineTo(0,24.4);
    this.shape_15.setTransform(243,92.6);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.8).lineTo(0,18.8);
    this.shape_16.setTransform(243,79.2);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_17.setTransform(243,65.8);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-7.6).lineTo(0,7.6);
    this.shape_18.setTransform(243,52.4);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_19.setTransform(243,39);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_10}]},10).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[]},1).wait(138));

    // fireworkLine3
    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_20.setTransform(-127,182);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-12.8).lineTo(0,12.8);
    this.shape_21.setTransform(-127,167.5);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.5).lineTo(0,18.5);
    this.shape_22.setTransform(-127,153);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.2).lineTo(0,24.2);
    this.shape_23.setTransform(-127,138.5);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,30).lineTo(0,-30);
    this.shape_24.setTransform(-127,124);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.4).lineTo(0,24.4);
    this.shape_25.setTransform(-127,103.4);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.8).lineTo(0,18.8);
    this.shape_26.setTransform(-127,82.8);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_27.setTransform(-127,62.2);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-7.6).lineTo(0,7.6);
    this.shape_28.setTransform(-127,41.6);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_29.setTransform(-127,21);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_20}]},10).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[]},1).wait(138));

    // fireworkLine2
    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_30.setTransform(97,172);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-12.8).lineTo(0,12.8);
    this.shape_31.setTransform(97,150);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.5).lineTo(0,18.5);
    this.shape_32.setTransform(97,128);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.2).lineTo(0,24.2);
    this.shape_33.setTransform(97,106);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,30).lineTo(0,-30);
    this.shape_34.setTransform(97,84);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.4).lineTo(0,24.4);
    this.shape_35.setTransform(97,70.6);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.8).lineTo(0,18.8);
    this.shape_36.setTransform(97,57.2);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_37.setTransform(97,43.8);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-7.6).lineTo(0,7.6);
    this.shape_38.setTransform(97,30.4);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_39.setTransform(97,17);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_30}]},7).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[]},1).wait(141));

    // fireworkLine1
    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2.5).lineTo(0,-2.5);
    this.shape_40.setTransform(0,169.5);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.6).lineTo(0,13.6);
    this.shape_41.setTransform(0,135.1);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.8).lineTo(0,24.8);
    this.shape_42.setTransform(0,100.8);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-35.9).lineTo(0,35.9);
    this.shape_43.setTransform(0,66.4);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,47).lineTo(0,-47);
    this.shape_44.setTransform(0,32);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-35.8).lineTo(0,35.8);
    this.shape_45.setTransform(0,10.8);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.5).lineTo(0,24.5);
    this.shape_46.setTransform(0,-10.5);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_47.setTransform(0,-31.7);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_48.setTransform(0,-53);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_40}]},4).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[]},1).wait(145));

    // phase1
    this.instance_26 = new lib.mountains();
    this.instance_26.parent = this;
    this.instance_26.setTransform(-25.4,208.4,1.21,1.21);
    this.instance_26.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_26).to({y:178.4,alpha:1},4,cjs.Ease.get(0.99)).to({_off:true},29).wait(125));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-316,-174,632,395);


// stage content:
(lib.teaser = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // aniLayer
    this.instance = new lib.ani_1();
    this.instance.parent = this;
    this.instance.setTransform(350,181);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(461.6,551.8,476.8,35.2);
// library properties:
lib.properties = {
    id: '2A9F3D2A489349BFA6576CCDE5DDDE1F',
    width: 700,
    height: 400,
    fps: 24,
    color: "#EC2C2C",
    opacity: 1.00,
    manifest: [],
    preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};

an.compositions = an.compositions || {};
an.compositions['2A9F3D2A489349BFA6576CCDE5DDDE1F'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}

an.getComposition = function(id) {
    return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;