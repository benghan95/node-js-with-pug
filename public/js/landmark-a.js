var jsonfile_a = "/public/landmark-a.json",
    fps = 60,
    width = 370,
    height = 160,
    AJAX_req_a;

AJAX_JSON_Req_a(jsonfile_a);

function handle_AJAX_Complete_a() {
    if( AJAX_req_a.readyState == 4 && AJAX_req_a.status == 200 )
    {
        json_a = JSON.parse(AJAX_req_a.responseText);
        // comp = new SVGAnim(
        //                json,
        //                width,
        //                height,
        //                fps
        //                );
        var container_a = document.getElementById("vector-container");
        var comp_a = new SVGAnim(json_a,width,height,fps);
        comp_a.s.node.setAttribute('id','landmark-a');
        container_a.appendChild(comp_a.s.node);
    }
}

function AJAX_JSON_Req_a( url )
{
    AJAX_req_a = new XMLHttpRequest();
    AJAX_req_a.open("GET", url, true);
    AJAX_req_a.setRequestHeader("Content-type", "application/json");

    AJAX_req_a.onreadystatechange = handle_AJAX_Complete_a;
    AJAX_req_a.send();
}