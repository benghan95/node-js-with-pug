$(document).ready(function() {
  var lang = $('input[name="lang"]').val();
  var isLogout = true;
  var firstTImeLoggedIn = false;

  $('#see-my-journey').attr('disabled', false);

  $('#see-my-journey').on('click', function() {
    try {
      aaWidget.authorizationByCookie().then(function(cookie) {
        if (cookie) {
          if ('userId' in cookie) {
            $('#loggedin-message').removeClass('hide');
            aaWidget.getUser().then(function(userData){
              if ('id' in userData) {
                let gender = "male";
                if (userData.title == 'MS') {
                  gender = "female";
                }
                $.ajax({
                  url: '/' + lang + '/login',
                  method: 'POST',
                  data: {
                    "accessToken": cookie.accessToken,
                    "name"       : userData.firstName,
                    "email"      : userData.username,
                    "gender"     : gender,
                    "userId"     : cookie.userId,
                    "bigId"      : userData.loyaltyId,
                    "lang"       : lang,
                  },
                  success: function(res) {
                    if (res.status === 'success') {
                      setTimeout(function() {
                        window.location.href = '/' + lang + '/loading/' + res.user.hexhash;
                      });
                    } else {
                      aaWidget.logout();
                      $('#loggedin-message').addClass('hide');
                      console.error(res.message);
                    }
                  },
                  error: function() {
                    console.error("Error occurred. Please try again later.");
                  }
                });
              }
            });
            
          }
        }
      });
      
    } catch (err) {
      
    }
  });


  function successCallback (data) {
    var url = window.location.href; 
    if (url.indexOf('?logout') !== -1 && isLogout) {
      console.log("Logged Out");
      isLogout = false;
      aaWidget.logout();
      reloadWidget();
    } else {
      console.log("Logged In");
      aaWidget.authorizationByCookie().then(function(cookie) {
        if (firstTImeLoggedIn) {
          if (cookie) {
            if ('userId' in cookie) {
              $('#loggedin-message').removeClass('hide');
              aaWidget.getUser().then(function(userData) {
                if ('id' in userData) {
                  let gender = "male";
                  if (userData.title == 'MS') {
                    gender = "female";
                  }
                  $.ajax({
                    url: '/' + lang + '/login',
                    method: 'POST',
                    data: {
                      "accessToken": cookie.accessToken,
                      "name"       : userData.firstName,
                      "email"      : userData.username,
                      "gender"     : gender,
                      "userId"     : cookie.userId,
                      "bigId"      : userData.loyaltyId,
                      "lang"       : lang,
                    },
                    success: function(res) {
                      if (res.status === 'success') {
                        setTimeout(function() {
                          window.location.href = '/' + lang + '/loading/' + res.user.hexhash;
                        });
                      } else {
                        aaWidget.logout();
                        $('#loggedin-message').addClass('hide');
                        console.error(res.message);
                      }
                    },
                    error: function() {
                      console.error("Error occurred. Please try again later.");
                    }
                  });
                }
              });
              
            }
          }
        }
      });
    }
  }

  function failureFunction(error) {
    isLogout = false;
    console.error('failureInitialization: ', error);
    firstTImeLoggedIn = true;
  }

  var widgetLang = "en-GB";
  switch (lang) {
    case 'id':
      widgetLang = 'id-ID';
      break;
    case 'th':
      widgetLang = 'en-TT';
      break;
    case 'cn-zh':
      widgetLang = 'zh-CN';
      break;
    case 'tw-zh':
      widgetLang = 'zh-TW';
      break;
    case 'hk-zh':
      widgetLang = 'zh-HK';
      break;
    case 'kr':
      widgetLang = 'ko-KR';
      break;
    case 'jp':
      widgetLang = 'ja-JP';
      break;
    case 'vn':
      widgetLang = 'vi-VN';
      break;
    default:
      break;
  }

  const configuration = {
    elementRoot  : 'airasia_widget',
    hideSignupTab: true,
    lang         : widgetLang,
    orientation  : 'horizontal'
  };

  function reloadWidget() {
    $('#airasia_widget').html('');
    // document.getElementById('airasia_widget').innerHTML = '';
    aaWidget.initialize({
      elementRoot    : configuration.elementRoot,
      hideSignupTab  : configuration.hideSignupTab,
      successCallback: successCallback,
      failureCallback: failureFunction,
      orientation    : configuration.orientation
    });
  }

  reloadWidget();
})