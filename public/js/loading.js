$(document).ready(function () {
  var lang      = $('input[name="lang"]').val();
  var hexhash   = $('input[name="hexhash"]').val();
  var render    = $('input[name="render"]').val();
  var thumbnail = $('input[name="thumbnail"]').val();
  var server    = $('input[name="server"]').val();

  var stuckTimes = 0;
  var currentPercentage = 0;
  var stillPending = true;
  var maxProgressCount = 5;

  var timer = setInterval(function() {
    getLog();
  }, 3000);

  function getLog () {
    if (stillPending) {
      $.ajax({
        url: 'https://halfabillion.airasia.com/en/check-status',
        method: 'POST',
        data: {
          'hexhash'    : hexhash,
        },
        success: function(res) {
          console.log(res);
          if (res.status === 'success') {
            stillPending = false;
            server = res.message;
            updateProgress(2, 25);
          }
        },
        error: function(err) {
          // $('#loadingModal').modal('show');
          window.location.href = '/' + lang + '/my-journey/' + hexhash;
          clearInterval(timer);
          console.error("Error occurred. Please try again later.");
        }
      });
    } else {
      let url = 'halfabillion.airasia.com';
      switch (server) {
        case '20.191.143.59':
          url = 'halfabillion.airasia.com';
          break;
        case '52.231.66.227':
          url = 'a1-halfabillion.airasia.com';
          break;
        case '104.215.182.222':
          url = 'a4-halfabillion.airasia.com';
          break;
        case '13.67.53.185':
          url = 'a6-halfabillion.airasia.com';
          break;
        case '137.116.140.88':
          url = 'a7-halfabillion.airasia.com';
          break;
        case '137.116.142.123':
          url = 'a8-halfabillion.airasia.com';
          break;
      }
      $.ajax({
        url: 'https://' + url + '/en/logging',
        method: 'POST',
        data: {
          'hexhash'    : hexhash,
          'renderId'   : render,
          'thumbnailId': thumbnail,
        },
        success: function(res) {
          console.log(res);
          if (res.status === 'completed') {
            updateProgress(maxProgressCount, 100);
            clearInterval(timer);
            setTimeout(function() {
              console.log('/' + lang + '/my-journey/' + hexhash);
              window.location.href = '/' + lang + '/my-journey/' + hexhash;
            }, 2000);
          } else if (res.status === 'rendering') {
            var percent = res.message;
            if (percent <= 0 ){
              percent = 5;
            }
            let counter = Math.floor(percent / 12.5);
            if (counter >= maxProgressCount) {
              counter = maxProgressCount;
            }
            updateProgress(counter, percent);
            if (currentPercentage == percent) {
              stuckTimes ++;
            } else {
              stuckTimes = 0;
              currentPercentage = percent;
            }
            if (stuckTimes >= 6) {
              $('#loadingModal').modal('show');
              clearInterval(timer);
            }
          } else if (res.status === 'pending') {
            updateProgress(2, 40);
          } else if (res.status === 'failed') {
            updateProgress(3, 60);
            $('#loadingModal').modal('show');
            clearInterval(timer);
          } else {
            updateProgress(4, 70);
            $('#loadingModal').modal('show');
            clearInterval(timer);
          }
        },
        error: function(err) {
          // window.location.href = '/' + lang + '/my-journey/' + hexhash;
          clearInterval(timer);
          console.error("Error occurred. Please try again later.");
        }
      });
    }
  }

  function updateProgress(counter, percent) {
    var progressText = $('#loading-section .progress-lead');
    var progressBar = $('#loading-section .progress-bar');
    progressText.html(loadingMsg[counter] + '...');
    progressBar.css('width', percent + '%').attr('aria-valuenow', percent);

    initProgressAnime();
  }

  function successCallback (data) {
  }

  function failureFunction(error) {
    window.location.href = 'https://halfabillion.airasia.com/' + lang;
  }

  var configuration = {
    elementRoot  : 'airasia_widget',
    hideSignupTab: true,
    lang         : lang,
    orientation  : 'horizontal'
  };

  function reloadWidget() {
    $('#airasia_widget').html('');
    // document.getElementById('airasia_widget').innerHTML = '';
    aaWidget.initialize({
      elementRoot    : configuration.elementRoot,
      hideSignupTab  : configuration.hideSignupTab,
      successCallback: successCallback,
      failureCallback: failureFunction,
      orientation    : configuration.orientation
    });
  }

  reloadWidget();

  var quote_prefix = "Did you know?";
  switch (lang) {
    case 'id':
      quote_prefix = "Tahukah Anda?";
      break;
    case 'th':
      quote_prefix = "รู้หรือไม่?";
      break;
    case 'cn-zh':
      quote_prefix = "你知道吗？";
      break;
    case 'tw-zh':
      quote_prefix = "您知道嗎?";
      break;
    case 'hk-zh':
      quote_prefix = "您知道嗎？";
      break;
    case 'kr':
      quote_prefix = "알고 계셨습니까?";
      break;
    case 'jp':
      quote_prefix = "ご存知ですか?";
      break;
    case 'vn':
      quote_prefix = "Bạn có biết?";
      break;
    default:
      break;
  }

  var quotes = [
    "Our best-selling food on board is Pak Nasser’s Nasi Lemak!",
    "Every 35 seconds an AirAsia aircraft is either taking off or landing somewhere in Asia.",
    "AirAsia's HQ has 4 adopted furry guardians, each named after our flight carrier codes: AK (AirAsia), D7 (AirAsia X), FD (Thai AirAsia), and QZ (Indonesia AirAsia)",
    "We fly to over 135 destinations across Asia, Australia and the USA.",
    "We were awarded the World’s Best Low-Cost Airline 9 times by Skytrax, and World’s Leading Low-Cost Airline 5 times by the World Travel Awards.",
    "As an Asean airline, we fly direct to all 10 Asean countries: Indonesia, Thailand, Philippines, Malaysia, Singapore, Vietnam, Myanmar, Cambodia, Laos, and Brunei.",
    "Our aircraft cover an average of 2.8 million kilometres a year; that’s over 3 times the distance from Earth to the moon (and back)!",
    "We introduced the Fast Airport Clearance Experience System (F.A.C.E.S.), Malaysia’s first airport facial recognition system, at Senai International Airport.",
  ];

  switch (lang) {
    case 'id':
      quotes = [
        "Makanan terlaris kami dalam pesawat adalah Nasi Lemak Pak Nasser!",
        "Setiap 35 detik sebuah pesawat AirAsia sedang lepas landas atau mendarat di suatu tempat di Asia.",
        "HQ AirAsia sudah mengadopsi 4 anjing penjaga, masing-masing diberi nama sesuai dengan kode penerbangan kami: AK (AirAsia), D7 (AirAsia X), FD (Thai AirAsia), dan QZ (Indonesia AirAsia)",
        "Kami terbang ke lebih dari 135 destinasi di seluruh penjuru Asia, Australia dan AS.",
        "Kami diberi penghargaan Maskapai Berbiaya Rendah Terbaik Dunia 9 kali oleh Skytrax, dan Maskapai Berbiaya Rendah Terbaik Dunia 5 kali oleh World Travel Awards.",
        "Sebagai maskapai penerbangan Asean, kami terbang langsung ke 10 negara Asean: Indonesia, Thailand, Filipina, Malaysia, Singapura, Vietnam, Myanmar, Kamboja, Laos, dan Brunei.",
        "Rata-rata pesawat kami terbang menempuh 2,8 juta kilometer setiap tahun; lebih dari 3 kali jarak Bumi ke bulan (pulang-pergi)!",
        "Kami memperkenalkan Fast Airport Clearance Experience System (F.A.C.E.S.), sistem pengenalan wajah di bandara yang pertama di Malaysia, di Bandara Internasional Senai, Johor Bahru.",
      ];
      break;
    case 'th':
      quotes = [
        "อาหารบนเครื่องที่มียอดขายดีที่สุดของเราคือ นาซิลามักสูตรปาคนาสเซอร์ (ข้าวอบใบเตยสไตล์มาเลเซีย)",
        "ทุกๆ 35 วินาที จะมีเครื่องบินแอร์เอเชีย 1 ลำที่บินขึ้นหรือลงจอดที่สนามบินสักแห่งในเอเชีย",
        "ที่อาคารสำนักงานใหญ่ของแอร์เอเชียจะมีสุนัขนำโชค 4 ตัวที่ถูกเลี้ยงอยู่ที่นั่น ซึ่งแต่ละตัวนั้นถูกตั้งชื่อตามรหัสเที่ยวบินของสายการบินในเครือ ได้แก่:AK (แอร์เอเชีย), D7 (แอร์เอเชียเอ็กซ์), FD (ไทยแอร์เอเชีย) และ QZ (อินโดนีเซียแอร์เอเชีย)",
        "เราบินสู่กว่า 135 ปลายทางทั่วทั้งเอเชีย ออสเตรเลีย และสหรัฐอเมริกา",
        "เราได้รับรางวัลสายการบินราคาประหยัดที่ดีที่สุดในโลก 9 สมัยจาก Skytrax และได้รับรางวัลสายการบินราคาประหยัดชั้นนำของโลก 5 สมัยจาก World Travel Awards",
        "ในฐานะของสายการบินแห่งอาเซียน เราให้บริการบินตรงสู่ 10 ประเทศทั่วอาเซียน ได้แก่:อินโดนีเซีย ไทย ฟิลิปปินส์ มาเลเซีย สิงคโปร์ เวียดนาม เมียนมา กัมพูชา ลาว และบรูไน",
        "เครื่องบินของเราแต่ละลำให้บริการโดยมีระยะทางการบินเฉลี่ยอยู่ที่ 2.8 ล้านกิโลเมตรตต่อปี คิดเป็น 3 เท่าของระยะทางในการเดินทางจากโลก ไป-กลับ ดวงจันทร์!",
        "เราเริ่มนำเอาระบบ Fast Airport Clearance Experience System (F.A.C.E.S.) ซึ่งเป็นการนำระบบจดจำใบหน้ามาใช้งานที่ท่าอากาศยานนานาชาติเซไน ซึ่งเป็นการนำมาใช้งานในสนามบินเป็นครั้งแรกของประเทศมาเลเซีย",
      ];
      break;
    case 'cn-zh':
      quotes = [
        "我们航班上最受欢迎的机餐是大马椰浆饭！",
        "每35秒便有一架亚航航班在亚洲某个机场起飞或降落。",
        "亚航总部收养了4只狗狗作为守护者，并以亚航子公司的航班代码命名它们。它们的名字分别是：AK（马来西亚亚航）、D7（亚航长途）、FD（泰国亚航）及QZ（印尼亚航）。",
        "我们飞往包括亚洲、澳洲及美国在内的130多个目的地。",
        "我们已连续9年荣获Skytrax“世界最佳低成本航空公司”大奖，并5次荣获 World Travel Awards（世界旅游大奖）“世界领先低成本航空公司”大奖。",
        "作为东盟的一家航空公司，我们的航班直接飞往东盟10国：印尼、泰国、菲律宾、马来西亚、新加坡、越南、缅甸、柬埔寨、老挝和文莱。",
        "我们的年航班总里程达280万公里，可往返月球3次。",
        "我们还在士乃国际机场引进了马来西亚第一套机场面部识别系统-机场快速通关系统(F.A.C.E.S.)。",
      ];
      break;
    case 'tw-zh':
      quotes = [
        "我們機上最暢銷的餐點是Pak Nasser的馬來西亞的椰漿飯!",
        "每35秒就有一班亞航飛機在亞洲某個地方起飛或著陸。",
        "亞航總部有 4隻領養的狗護衛，每一隻都以我們的飛機代碼: AK (馬亞航)、 D7 (全亞航)、FD (Thai AirAsia)和 QZ (Indonesia AirAsia)命名。",
        "我們飛往亞洲、澳洲和美國 超過135個目的地。",
        "我們連續9次奪得Skytrax世界最佳廉價航空公司以及在世界旅遊大賞中連續5次獲得世界最佳低成本航空公司獎。",
        "關於亞洲航線，我們直飛所有10個東協國家: 印度、泰國、菲律賓、馬來西亞、新加坡、越南、緬甸、柬埔寨、寮國和汶萊。",
        "我們的飛機平均一年飛行280萬公里; 也就是三倍從地球到月亮的距離 (以及返回)!",
        "我們引進臉部辨識系統 (F.A.C.E.S.)，是馬來西亞機場第一部臉部辨識系統，於士乃國際機場提供 。",
      ];
      break;
    case 'hk-zh':
      quotes = [
        "我們最暢銷的機艙食物是Pak Nasser的椰漿飯！",
        "每35秒，便有一架AirAsia客機在亞洲某一地方起飛或降落。",
        "AirAsia總部收養了四隻狗護衛，牠們都是以我們的航班編號來命名，分別是：AK (亞航), D7 (AirAsia X), FD (泰國亞航), 和QZ (印尼亞航)。",
        "我們飛往包括亞洲、澳洲和美國超過135個目的地。",
        "我們連續9年獲得Skytrax頒發的「世界最佳低成本航空公司」大獎 (World’s Best Low-Cost Airline)及連續5次獲得World Travel Awards頒發的「世界領先低成本航空公司」(World’s Leading Low-Cost Airline)大獎。",
        "作為一間東盟航空公司，我們的航班可直飛全部10個東盟國家，包括印尼、泰國、菲律賓、馬來西亞、新加坡、越南、緬甸、柬埔寨、老撾和文萊。",
        "我們的飛機每年平均飛行280萬公里，也是從地球往來月亮距離的三倍！",
        "我們在新山機場推出馬來西亞首個機場面部辨識系統FACES (The Fast Airport Clearance Experience System)。",
      ];
      break;
    case 'kr':
      quotes = [
        "기내에서가장인기있었던식사는팩나서나시레막(Pak Nasser’s Nasi Lemak)이랍니다!",
        "에어아시아 항공기는 이 시간에도 매 35초마다 아시아의 모든 장소에서 이륙하거나 착륙하고 있습니다.",
        "에어아시아 본사에는 각 에어아시아 항공사 코드인 AK(에어아시아), D7(에어아시아X), FD(태국 에어아시아) 및 QZ(인도네시아 에어아시아)의 이름을 딴 4마리의 귀여운 경비견들이 근무하고 있습니다.",
        "에어아시아는 아시아, 호주 및 미국 135개 이상의 취항지로 비행합니다.",
        "에어아시아는 세계 최고 저비용 항공사 상을 Skytrax로 부터 9회, World Travel Awards로 부터 5회 수상하였습니다.",
        "아세안 항공사인 에어아시아는 아세안 10개 국가인 인도네시아, 태국, 필리핀, 말레이시아, 싱가포르, 베트남, 미얀마, 캄보디아, 라오스, 브루나이로 직접 비행합니다.",
        "에어아시아의 비행기는 연평균 280만 킬로미터의 거리를 비행하는데, 이를 환산하면 지구에서 달까지의 거리의 3배 이상입니다!",
        "에어아시아는세나이(Senai)국제공항에말레이시아최초의공항안면인식시스템인F.A.C.E.S.(Fast Airport Clearance Experience System)를도입하였습니다.",
      ];
      break;
    case 'jp':
      quotes = [
        "「ナシレマ」は機内食のなかで売上No1!",
        "今日もアジアのどこかで35秒ごとにエアアジアの飛行機が離着陸しています。",
        "エアアジア本社ではふわふわの毛並みが人気の4匹の警備員が勤務しています。彼らの名前はエアアジアの便名と同じ、AK（エアアジア）、D7（エアアジアX）、FD（タイ・エアアジア）、QZ（インドネシア・エアアジア）です。",
        "アジア、オーストラリア、アメリカを含む135以上の地域に就航しています。",
        "スカイトラックス社の「ワールド・ベスト・ローコスト・エアライン」を9年連続で受賞し、ワールド・トラベル・アワードでは「ワールドリーディング・ローコストエアライン」に5年連続で受賞しました。",
        "ASEANのエアラインとして、10カ国全ての加盟国に直行便を運航しています。ASEAN加盟国：インドネシア、タイ、フィリピン、マレーシア、シンガポール、ベトナム、ミャンマー、カンボジア、ラオス、ブルネイ",
        "エアアジアの保有する飛行機の年間飛行距離は平均280万km。それは地球から月までの距離の3倍以上!",
        "エアアジアは、マレーシアで初めての顔認証搭乗システム(F.A.C.E.S.)をセナイ国際空港に導入しました。",
      ];
      break;
    case 'vn':
      quotes = [
        "Món ăn bán chạy nhất của chúng tôi trên máy bay là Nasi Lemak Pak Nasser!",
        "Cứ 35 giây lại có một máy bay AirAsia cất cánh hoặc hạ cánh tại đâu đó ở châu Á.",
        "Trụ sở chính của AirAsia đã nhận nuôi 4 chú chó bảo vệ, mỗi chú được đặt theo một mã hãng chuyên chở của chúng tôi: AK (AirAsia), D7 (AirAsia X), FD (Thai AirAsia), và QZ (Indonesia AirAsia)",
        "Chúng tôi có đường bay tới hơn 135 điểm đến khắp châu Á, Australia và Hoa Kỳ.",
        "Chúng tôi nhận được giải thưởng Hãng hàng không Giá rẻ Tốt nhất Thế giới của Skytrax 9 lần, và Hãng hàng không Giá rẻ Hàng đầu Thế giới của World Travel Awards 5 lần.",
        "Là một hãng hàng không tại Asean, chúng tôi có đường bay thẳng tới tất cả 10 nước Asean: Indonesia, Thái Lan, Philippine, Malaysia, Singapore, Việt Nam, Myanmar, Campuchia, Lào, và Brunei.",
        "Máy bay của chúng tôi trung bình mỗi năm bay 2,8 triệu km; gấp ba lần khoảng cách khi bay từ Trái đất đến mặt trăng (và quay về)!",
        "Chúng tôi đã đưa vào Hệ thống Kiểm tra Nhanh tại Sân bay (Fast Airport Clearance Experience System - F.A.C.E.S), hệ thống nhận diện khuôn mặt tại sân bay đầu tiên của Malaysia, ở Sân bay Quốc tế Senai.",
      ];
      break;
    default:
      break;
  }

  var quoteAnime;
  var progressAnime;
  var current = 0;

  function loopQuoteAnime() {
    quoteAnime.complete = function() {
      var max = 7;

      if (current < max) {
        current++;
      } else {
        current = 0;
      }

      $('.page-loading .questions').html(quote_prefix + ' ' + quotes[current]);

      $('.page-loading .questions').each(function () {
        $(this).html($(this).text().replace(/[^\s]/g, "<span class='letter'>$&</span>"));
      });

      initQuoteAnime();
      loopQuoteAnime();
    }
  }

  function initQuoteAnime() {
    quoteAnime = anime.timeline()
      .add({
        targets: '.questions',
        opacity: 1,
        duration: 0,
        delay: 0
      })
      .add({
        targets: '.questions .letter',
        scale: [0.3,1],
        opacity: [0,1],
        translateZ: 0,
        easing: "easeOutExpo",
        duration: 600,
        delay: function(el, i) {
          return 30 * (i+1)
        }
      })
      .add({
        targets: '.questions',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
      });
  }

  function startQuoteAnime() {
    // $('.page-loading .questions').css("height", $('.page-loading .questions').height());
    // $('.page-loading .questions').css("height", 100);
    $('.page-loading .questions').each(function () {
      $(this).html($(this).text().replace(/[^\s]/g, "<span class='letter'>$&</span>"));
      // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
    });

    initQuoteAnime();
    loopQuoteAnime();
  }

  startQuoteAnime();

  var loadingMsg = [
    "Gathering Data",
    "Sorting Data",
    "Generating Image",
    "Preparing Preview",
    "Almost There",
    "We're Done",
  ];

  switch (lang) {
    case 'id':
      loadingMsg = [
        "Mengumpulkan Data",
        "Memilah Data",
        "Memuat gambar",
        "Menyiapkan Pratinjau",
        "Hampir Selesai",
        "Selesai",
      ];
      break;
    case 'my':
      loadingMsg = [
        "Mengumpulkan data",
        "Memilah Data",
        "Memuat gambar",
        "Menyiapkan Pratinjau",
        "Hampir Selesai",
        "Selesai",
      ];
      break;
    case 'th':
      loadingMsg = [
        "กำลังรวบรวมข้อมูล",
        "กำลังจัดลำดับข้อมูล",
        "กำลังสร้างรูปภาพ",
        "กำลังเตรียมแสดงตัวอย่าง",
        "อีกนิดเดียววิดีโอก็จะเสร็จแล้ว",
        "วิดีโอเสร็จเรียบร้อย",
      ];
      break;
    case 'cn-zh':
      loadingMsg = [
        "收集数据",
        "数据分类",
        "图像生成中",
        "准备预览",
        "马上就好",
        "大功告成",
      ];
      break;
    case 'my-zh':
      loadingMsg = [
        "收集数据",
        "数据分类",
        "图像生成中",
        "准备预览",
        "马上就好",
        "大功告成",
      ];
      break;
    case 'tw-zh':
      loadingMsg = [
        "收集資料",
        "篩選資料",
        "圖片製作中",
        "準備預覽",
        "快好嘞 ~",
        "完成",
      ];
      break;
    case 'hk-zh':
      loadingMsg = [
        "收集資料中",
        "整理資料中",
        "圖片製作中",
        "預覽準備中",
        "快將完成",
        "大功告成",
      ];
      break;
    case 'kr':
      loadingMsg = [
        "데이터수집중",
        "데이터정렬중",
        "이미지 생성중",
        "미리보기 준비 중",
        "잠시만기다려주세요",
        "완료되었습니다",
      ];
      break;
    case 'jp':
      loadingMsg = [
        "データ収集中",
        "データ分析中",
        "イメージ画像作成中",
        "試写準備中",
        "完成まであと少しです",
        "完成です",
      ];
      break;
    case 'vn':
      loadingMsg = [
        "Thu thập dữ liệu",
        "Phân loại dữ liệu",
        "Dựng Hình ảnh",
        "Chuẩn bị nội dung xem trước",
        "Sắp xong rồi",
        "Vậy là xong",
      ];
      break;
    default:
      break;
  }

  function initProgressAnime() {
    $('#loading-section .progress-lead').each(function () {
      $(this).html($(this).text().replace(/[^\s]/g, "<span class='letter'>$&</span>"));
    });

    progressAnime = anime.timeline()
      .add({
        targets: '#loading-section .progress-lead .letter',
        translateY: ["1.1em", 0],
        translateZ: 0,
        duration: 750,
        delay: function(el, i) {
          return 50 * i;
        }
      }).add({
        targets: '#loading-section .progress-lead .progress-title',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
      });
  }
})