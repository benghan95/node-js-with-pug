(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
    }


(lib.txtB = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AA8EOIkFAAQgQAAAAgQIAAn7QAAgQAQAAIDwAAQA+gBAvAvQAvAwgBA/QAAA3ghAlQAbAYAPAhQAPAiAAAjQACBAgxAzQgvAxg9AAIgDAAgAhNCiIBgAAQAWAAARgSQARgTAAgYQAAgWgQgTQgRgUgXAAIhgAAgAhNg4IBNAAQAYAAAQgPQAPgPAAgXQAAgWgPgPQgQgPgYAAIhNAAg");

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txtB, new cjs.Rectangle(-21.8,-26.9,43.7,54), null);


(lib.mcBoxShadow = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#BA2120").s().p("EgbHAhpMAAAhDRMA2PAAAMAAABDRg");
    this.shape.setTransform(173.6,215.3);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mcBoxShadow, new cjs.Rectangle(0,0,347.3,430.7), null);


(lib.mcBoxBlack = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("EgbHAhpMAAAhDRMA2PAAAMAAABDRg");
    this.shape.setTransform(173.6,215.3);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mcBoxBlack, new cjs.Rectangle(0,0,347.3,430.7), null);


(lib.GuestFlown = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#231F20").s().p("A6tFsQgLAAgIgIQgHgHAAgLIAAq9MA2PAAAIAALXgA6iFGMA1FAAAIAAqLMg1FAAAg");
    this.shape.setTransform(173.6,394.3);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#EC2C2C").s().p("A6tFTIgBqlMA1dAAAIAAKlg");
    this.shape_1.setTransform(173.6,394.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.GuestFlown, new cjs.Rectangle(0,0,347.3,430.7), null);


(lib.graPlane = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#231F20").p("AiShKID+hdQAJgDAIAEQAHAFABAJIAZC/QAFAkgSAgQgRAgghAPIgYAMQgLAFgMgCQgMgDgIgJIi1jJQgHgHACgKQADgJAJgEg");
    this.shape.setTransform(96.8,58.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AAwCpQgMgDgIgJIi1jJQgHgHACgKQADgJAJgEID+hdQAJgDAIAEQAHAFABAJIAZC/QAFAkgSAgQgRAgghAPIgYAMQgHADgIAAIgIAAg");
    this.shape_1.setTransform(96.8,58.5);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#231F20").p("AIWkoQhXgFheAUQhYAUhUAoQgYAMgIADQgTAHgQABQgQACgmgJImEhgQg7gOghgCQg0gDgkAVQgVAMgKAVQgMAXAMASQAFAJATANIESCtIimBdQgRALgMAAQgJAAgQgJQg5gjgdgPQgygZgpACQgXABgRAMQgTAOABATQABAMAKARIBNCFQg3AuAMAkQAHATAXAJQASAIAaABQAxAABZgSQDZgvEFhTQDxhNCghLQCDg8BGhAQAmgjALgeQAIgUgCgVQgBgWgMgQQgMgOgVgJQgNgGgbgHg");
    this.shape_2.setTransform(69.7,31.9);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("ApBE/QgagBgSgIQgXgJgHgTQgMgkA3guIhNiFQgKgRgBgMQgBgTATgOQARgMAXgBQApgCAyAZQAdAPA5AjQAQAJAJAAQAMAAARgLICmhdIkSitQgTgNgFgJQgMgSAMgXQAKgVAVgMQAkgVA0ADQAhACA7AOIGEBgQAmAJAQgCQAQgBATgHQAIgDAYgMQBUgoBYgUQBegUBXAFIBMALQAbAHANAGQAVAJAMAOQAMAQABAWQACAVgIAUQgLAegmAjQhGBAiDA8QigBLjxBNQkFBTjZAvQhWASgyAAIgCAAg");
    this.shape_3.setTransform(69.7,31.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graPlane, new cjs.Rectangle(-1,-1,142.3,79.2), null);


(lib.txt1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // N
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AAXA2QgEAAgBgDIgmg+IAAA+QAAABAAAAQAAABAAAAQgBABAAAAQgBAAgBAAIgUAAQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAgBIAAhlIABgCIADgBIAXAAQAEAAACADIAjA7IAAg7QAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAIAUAAIADABIABACIAABlQAAABgBAAQAAABAAAAQgBABAAAAQgBAAgBAAg");
    this.shape.setTransform(119.8,66.8);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AA4CEQgIAAgFgIIhciZIAACZQAAAIgJAAIgxAAQgIAAAAgIIAAj3QAAgDACgDQACgCAEAAIA5AAQAIAAAEAIIBYCPIAAiPQAAgIAIAAIAyAAQADAAACACQADADAAADIAAD3QAAAIgIAAg");
    this.shape_1.setTransform(119.8,66.8);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("ABRDAQgMAAgGgLIiIjfIAADfQAAALgMAAIhJAAQgLAAABgLIAAloQAAgGACgCQADgEAFAAIBUAAQAMAAAHAMICBDRIAAjRQAAgMALAAIBIAAQAEAAAEAEQAFADgBAFIAAFoQAAALgMAAg");
    this.shape_2.setTransform(119.8,66.8);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("ABkDrQgOAAgKgOIilkRIAAERQAAAOgPAAIhZAAQgNAAAAgOIAAm5QAAgGADgEQAEgEAGAAIBnAAQAOAAAIAOICeEAIAAkAQAAgOAOAAIBZAAQAEAAAFAEQAFAEABAGIAAG5QAAAOgPAAg");
    this.shape_3.setTransform(119.8,66.8);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFFFFF").s().p("ABvEFQgQAAgKgPIi3kwIAAEwQgBAPgRAAIhiAAQgPAAAAgPIAAnrQAAgGADgFQAFgEAHAAIByAAQAPAAAJAPICwEcIAAkcQAAgPAPAAIBiAAQAGAAAGAEQAFAFAAAGIAAHrQABAPgSAAg");
    this.shape_4.setTransform(119.8,66.8);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#FFFFFF").s().p("AByEOQgQAAgKgQIi+k6IAAE6QAAAQgSAAIhlAAQgQAAAAgQIAAn7QAAgHAEgEQAEgFAIAAIB1AAQARAAAJAQIC1EmIAAkmQAAgQARAAIBkAAQAGAAAGAFQAGAFAAAGIAAH7QAAAQgSAAg");
    this.shape_5.setTransform(119.8,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).wait(39));

    // O
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbAwQgNgHgHgNQgIgNAAgPQAAgOAIgMQAHgOANgHQANgIAOABQAPgBANAIQANAHAHAOQAIAMAAAOQAAAPgIANQgHANgNAHQgNAHgPABQgOgBgNgHgAgTgUQgHAJgBALQABAMAHAIQAJAJAKAAQAMAAAIgJQAIgIAAgMQAAgLgIgJQgIgIgMABIAAgBQgLAAgIAIg");
    this.shape_6.setTransform(61.1,66.8);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#FFFFFF").s().p("AhCB1QgggSgTgfQgSggAAgkQAAgjASggQATgeAggTQAegSAkAAQAlAAAfASQAfASASAfQASAgAAAjQAAAlgSAfQgSAfgfASQgfATglAAQgkAAgegTgAAxAyQATgVAAgdQAAgcgTgVQgVgVgcABQgbgBgUAVQgUAVABAcQgBAdAUAVQAUAVAbAAQAcAAAVgVg");
    this.shape_7.setTransform(61.1,66.8);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#FFFFFF").s().p("AhiCrQgtgagbguQgbgtAAg2QAAg0AbguQAagtAugbQAtgbA1ABQA2gBAtAbQAtAbAbAtQAaAuAAA0QAAA2gaAtQgbAugtAaQgtAbg2AAQg1AAgtgbgABGBJQAdgegBgrQABgqgdgdQgdgfgpAAQgpAAgcAfQgdAdABAqQgBArAdAeQAcAeApgBQApABAdgeg");
    this.shape_8.setTransform(61.1,66.8);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#FFFFFF").s().p("Ah4DSQg3gggig5Qggg3AAhCQAAhBAgg3QAgg3A5giQA4ggBAAAQBCAAA3AgQA3AiAiA3QAgA3AABBQAABCggA3QgiA5g3AgQg3AghCAAQhAAAg4gggABXBZQAiglAAg0QAAgzgiglQglglgyABQgygBgjAlQgjAlAAAzQAAA0AjAlQAjAmAygBQAyABAlgmg");
    this.shape_9.setTransform(61.1,66.8);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#FFFFFF").s().p("AiGDpQg9gkglg+Qgkg+ABhJQgBhIAkg9QAkg+A+gkQA/glBHABQBJgBA9AlQA+AkAkA+QAlA9gBBIQABBJglA+QglA+g9AkQg9AjhJABQhHgBg/gjgABgBjQAmgqgBg5QABg4gmgpQgogqg4AAQg3AAgnAqQgoApACA4QgCA5AoAqQAnApA3gBQA4ABAogpg");
    this.shape_10.setTransform(61.1,66.8);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#FFFFFF").s().p("AiKDwQhAglglhAQglhAAAhLQAAhKAlhAQAkg/BBgmQBAglBKAAQBLAABAAlQBAAlAlBAQAlBAAABKQAABMglA/QgmBAg/AlQhAAmhLAAQhKAAhAgmgABjBmQAogrgBg7QABg6gogrQgpgrg6ABQg5gBgpArQgoArABA6QgBA7AoArQApArA5gBQA6ABApgrg");
    this.shape_11.setTransform(61.1,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},23).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(40));

    // I
    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#FFFFFF").s().p("AgKA2QgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBIAAhlQAAAAAAgBQAAgBABAAQAAgBABAAQAAAAABAAIAVAAQAAAAABAAQABAAAAABQABAAAAABQAAABAAAAIAABlQAAABAAAAQAAABgBAAQAAABgBAAQgBAAAAAAg");
    this.shape_12.setTransform(18.9,66.8);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#FFFFFF").s().p("AgZCEQgIAAAAgIIAAj3QAAgIAIAAIAzAAQAIAAAAAIIAAD3QAAAIgIAAg");
    this.shape_13.setTransform(18.9,66.8);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#FFFFFF").s().p("AgmDAQgLAAAAgLIAAloQAAgMALAAIBNAAQALAAAAAMIAAFoQAAALgLAAg");
    this.shape_14.setTransform(18.9,66.8);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#FFFFFF").s().p("AguDrQgOAAAAgOIAAm5QAAgOAOAAIBeAAQANAAAAAOIAAG5QAAAOgNAAg");
    this.shape_15.setTransform(18.9,66.8);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#FFFFFF").s().p("AgzEFQgQAAAAgPIAAnrQAAgPAQAAIBnAAQAQAAAAAPIAAHrQAAAPgQAAg");
    this.shape_16.setTransform(18.9,66.8);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#FFFFFF").s().p("Ag1EOQgQAAAAgQIAAn7QAAgQAQAAIBsAAQAPAAAAAQIAAH7QAAAQgPAAg");
    this.shape_17.setTransform(18.9,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_12}]},22).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).wait(41));

    // L
    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#FFFFFF").s().p("AgfA2QgBAAgBAAQAAAAgBgBQAAAAgBgBQAAAAAAgBIAAhlQAAAAAAgBQABgBAAAAQABgBAAAAQABAAABAAIAVAAQABAAABAAQAAAAABABQAAAAAAABQAAABAAAAIAABPIAoAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAIAAAUQAAABAAAAQgBABAAAAQAAABgBAAQAAAAgBAAg");
    this.shape_18.setTransform(-12.6,66.8);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#FFFFFF").s().p("AhOCEQgIAAAAgIIAAj3QAAgIAIAAIA1AAQAHAAAAAIIAADAIBiAAQAHAAAAAHIAAAwQAAAIgHAAg");
    this.shape_19.setTransform(-12.6,66.8);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#FFFFFF").s().p("AhzDAQgLAAAAgLIAAloQAAgMALAAIBNAAQALAAAAAMIAAEYICPAAQALAAAAALIAABFQAAALgLAAg");
    this.shape_20.setTransform(-12.5,66.8);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#FFFFFF").s().p("AiNDrQgNAAAAgOIAAm5QAAgOANAAIBfAAQANAAAAAOIAAFXICvAAQANAAAAAOIAABUQAAAOgNAAg");
    this.shape_21.setTransform(-12.5,66.8);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#FFFFFF").s().p("AidEFQgPAAAAgPIAAnrQAAgPAPAAIBpAAQAPAAAAAPIAAF9IDDAAQAPAAAAAQIAABeQAAAPgPAAg");
    this.shape_22.setTransform(-12.5,66.8);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("#FFFFFF").s().p("AiiEOQgQAAAAgQIAAn7QAAgQAQAAIBsAAQAQAAAAAQIAAGKIDIAAQAQAAABAQIAABhQgBAQgQAAg");
    this.shape_23.setTransform(-12.5,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_18}]},21).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).wait(42));

    // L
    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.f("#FFFFFF").s().p("AggA2QAAAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAgBIAAhlQAAAAABgBQAAgBAAAAQABgBAAAAQABAAAAAAIAWAAQABAAAAAAQABAAAAABQABAAAAABQAAABAAAAIAABPIAoAAQAAAAABAAQAAAAABAAQAAABAAAAQAAABAAAAIAAAUQAAABAAAAQAAABAAAAQgBABAAAAQgBAAAAAAg");
    this.shape_24.setTransform(-53.5,66.8);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.f("#FFFFFF").s().p("AhPCEQgHAAAAgIIAAj3QAAgIAHAAIA1AAQAIAAAAAIIAADAIBiAAQAHAAAAAHIAAAwQAAAIgHAAg");
    this.shape_25.setTransform(-53.5,66.8);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.f("#FFFFFF").s().p("AhzDAQgLAAAAgLIAAloQAAgMALAAIBNAAQALAAAAAMIAAEYICPAAQALAAAAALIAABFQAAALgLAAg");
    this.shape_26.setTransform(-53.5,66.8);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.f("#FFFFFF").s().p("AiNDrQgNAAAAgOIAAm5QAAgOANAAIBfAAQANAAAAAOIAAFXICvAAQANAAAAAOIAABUQAAAOgNAAg");
    this.shape_27.setTransform(-53.5,66.8);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.f("#FFFFFF").s().p("AidEFQgPAAAAgPIAAnrQAAgPAPAAIBpAAQAPAAAAAPIAAF9IDDAAQAPAAAAAQIAABeQAAAPgPAAg");
    this.shape_28.setTransform(-53.5,66.8);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.f("#FFFFFF").s().p("AiiEOQgQAAAAgQIAAn7QAAgQAQAAIBsAAQAQAAAAAQIAAGKIDIAAQAQAAABAQIAABhQgBAQgQAAg");
    this.shape_29.setTransform(-53.5,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_24}]},20).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).wait(43));

    // I
    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.f("#FFFFFF").s().p("AgKA2QgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBIAAhlQAAAAAAgBQAAgBABAAQAAgBABAAQAAAAABAAIAVAAQABAAAAAAQABAAAAABQABAAAAABQAAABAAAAIAABlQAAABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAg");
    this.shape_30.setTransform(-87,66.8);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.f("#FFFFFF").s().p("AgaCEQgHAAAAgIIAAj3QAAgIAHAAIA0AAQAIAAAAAIIAAD3QAAAIgIAAg");
    this.shape_31.setTransform(-87,66.8);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.f("#FFFFFF").s().p("AgmDAQgLAAAAgLIAAloQAAgMALAAIBNAAQALAAAAAMIAAFoQAAALgLAAg");
    this.shape_32.setTransform(-87,66.8);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.f("#FFFFFF").s().p("AguDrQgOAAAAgOIAAm5QAAgOAOAAIBeAAQANAAAAAOIAAG5QAAAOgNAAg");
    this.shape_33.setTransform(-87,66.8);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.f("#FFFFFF").s().p("Ag0EFQgPAAAAgPIAAnrQAAgPAPAAIBpAAQAPAAAAAPIAAHrQAAAPgPAAg");
    this.shape_34.setTransform(-87,66.8);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.f("#FFFFFF").s().p("Ag1EOQgQAAAAgQIAAn7QAAgQAQAAIBrAAQAQAAAAAQIAAH7QAAAQgQAAg");
    this.shape_35.setTransform(-87,66.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_30}]},19).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).wait(44));

    // B
    this.instance = new lib.txtB();
    this.instance.parent = this;
    this.instance.setTransform(-121.7,66.8,0.2,0.2,0,0,0,-0.2,0);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({_off:false},0).to({regX:0,scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(45));

    // -
    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.f("#FFFFFF").s().p("AAcAQIAAgfIBKAAIAAAfgAhlAQIAAgfIBKAAIAAAfg");
    this.shape_36.setTransform(0.1,5.2);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.f("#FFFFFF").s().p("AC7ArIAAhWIHyAAIAABWgAqsArIAAhWIHyAAIAABWg");
    this.shape_37.setTransform(0.1,5.2);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.f("#FFFFFF").s().p("AEtA/IAAh9IMhAAIAAB9gAxNA/IAAh9IMhAAIAAB9g");
    this.shape_38.setTransform(0,5.2);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.f("#FFFFFF").s().p("AFxBLIAAiVIPXAAIAACVgA1HBLIAAiVIPXAAIAACVg");
    this.shape_39.setTransform(0,5.2);

    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.f("#FFFFFF").s().p("AGIBPIAAidIQUAAIAACdgA2bBPIAAidIQUAAIAACdg");
    this.shape_40.setTransform(0,5.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_36}]},12).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).wait(52));

    // A
    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.f("#FFFFFF").s().p("AAXArQAAAAgBAAQAAAAgBgBQAAAAAAAAQgBgBAAgBIgEgLIgfAAIgEALQAAABAAABQgBAAAAAAQgBABAAAAQgBAAAAAAIgSAAQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAAAAAIAfhSQABAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAIANAAQABAAAAAAQABAAAAABQABAAAAAAQAAABABAAIAfBQIAAACQAAAAAAAAQAAABAAAAQAAAAgBAAQAAAAgBAAgAgJAKIASAAIgJgZg");
    this.shape_41.setTransform(0,5.3);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.f("#FFFFFF").s().p("AA6BpIgBAAQgGAAgCgFIgBgEIgJgYIgCgBIhKAAIgCACIgJAaIAAABQgCAFgGAAIgDAAIgnAAIgCAAQgBAAAAgBQgBAAAAAAQAAAAgBgBQAAAAAAgBIABgDIBNjFIAAAAQACgFAFgBIACAAIAeAAIAEAAQAFAAADAGIAAABIBNDEIAAAAIABADIAAAAQAAADgFAAgAgWAaIAsAAIgWhAg");
    this.shape_42.setTransform(0,5.3);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.f("#FFFFFF").s().p("ABVCaIgBAAQgKAAgDgJIgCgFIgMgkIhwAAIgCAEIgMAlIgBABQgDAIgJAAIgFAAIg4AAIgEAAQgDgBAAgEIAAAAIABgEIBwkfIAAgBQADgIAGgBIAEAAIAtAAIAFAAQAIAAAEAJIABABIBvEeIAAABIABADIAAABQABAEgHABgAggAlIBAAAIgghcg");
    this.shape_43.setTransform(0,5.3);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.f("#FFFFFF").s().p("ABnC8IgBAAQgLAAgEgLIgCgGIgQgrIiJAAIgRAxIgBACQgDAJgMAAIgGAAIhFAAIgEAAQgEgCAAgDIAAgBIABgFICJlgIAAgBQAEgJAIgCIAEAAIA3AAIAGAAQALAAAEALIABACICJFeIAAABIABAFIAAAAQABAGgJAAgAgoAuIBQAAIgohyg");
    this.shape_44.setTransform(0,5.3);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.f("#FFFFFF").s().p("ABzDRIgCAAQgNAAgEgMIgCgHIgRgwIiZAAIgCAEIgRAzIgBACQgEAKgMAAIgHAAIhNAAIgEgBQgFgBAAgEIABgGICZmHIAAgBQAEgKAJgCIAEgBIA9AAIAHAAQAMAAAFANIABACICYGFIAAABIACAFIAAABQAAAGgJAAgAgsAzIBYAAIgsh/g");
    this.shape_45.setTransform(0,5.3);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.f("#FFFFFF").s().p("AB1DYQgNAAgFgNIgUg5IidAAIgUA5QgEANgOAAIhWAAQgKAAAAgHIACgGICdmVQAFgNANAAIBGAAQANAAAFANICeGVIACAGQAAAHgKAAgAguA1IBcAAIguiDg");
    this.shape_46.setTransform(0,5.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_41}]},5).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).wait(58));

    // F
    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.f("#FFFFFF").s().p("AgVAjQgBAAAAgBQAAAAgBAAQAAAAAAgBQAAAAAAgBIAAhAQAAAAAAAAQAAgBAAAAQABAAAAAAQAAgBABAAIArAAQAAAAABABQAAAAAAAAQABAAAAABQAAAAAAAAIAAANQAAAAAAAAQAAABgBAAQAAAAAAAAQgBABAAAAIgbAAIAAAOIATAAQABAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAAAKQAAABAAAAQAAABAAAAQAAAAgBAAQAAAAgBAAIgTAAIAAAVQAAABAAAAQAAABgBAAQAAAAAAAAQgBABAAAAg");
    this.shape_47.setTransform(120.5,-60);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.f("#FFFFFF").s().p("AhcCSQgKAAAAgIIAAkSQAAgJAKAAIC6AAQAJAAAAAJIAAAyQAAAIgJAAIh3AAIAAA+IBXAAQAIAAAAAIIAAAvQAAAJgIAAIhXAAIAABaQAAAIgIAAg");
    this.shape_48.setTransform(120.5,-60);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.f("#FFFFFF").s().p("AiVDqQgNgBAAgOIAAm2QAAgOANABIErAAQANgBAAAOIAABRQAAAOgNAAIi/AAIAABhICMAAQANAAAAAOIAABLQAAAOgNAAIiMAAIAACPQAAAOgOABg");
    this.shape_49.setTransform(120.5,-60);

    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.f("#FFFFFF").s().p("Ai8EoQgSAAAAgSIAAorQAAgSASAAIF6AAQARAAAAASIAABmQAAARgRAAIjyAAIAAB8ICxAAQARAAAAARIAABgQAAASgRAAIixAAIAAC1QAAASgRAAg");
    this.shape_50.setTransform(120.5,-60);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.f("#FFFFFF").s().p("AjUFNQgUAAAAgTIAApyQAAgUAUAAIGqAAQATAAAAAUIAABzQAAAUgTgBIkRAAIAACLIDIAAQATAAAAAUIAABsQAAATgTABIjIAAIAADNQAAATgTAAg");
    this.shape_51.setTransform(120.5,-60);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.f("#FFFFFF").s().p("AjcFZQgVABAAgVIAAqIQAAgWAVAAIG6AAQAUAAAAAWIAAB2QAAAVgUAAIkbAAIAACPIDPAAQAUAAAAAVIAABwQAAAUgUAAIjPAAIAADVQAAAVgUgBg");
    this.shape_52.setTransform(120.5,-60);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_47}]},4).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).wait(59));

    // L
    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.f("#FFFFFF").s().p("AgUAjQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAgBIAAhAIABgBIABgBIAOAAQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAIAAAyIAZAAQAAAAABAAQAAAAAAAAQABABAAAAQAAABAAAAIAAAMQAAABAAAAQAAABgBAAQAAAAAAAAQgBABAAAAg");
    this.shape_53.setTransform(64.8,-60);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.f("#FFFFFF").s().p("AhXCSQgJAAAAgIIAAkSQAAgEADgDQACgCAEAAIA7AAQAIAAAAAJIAADUIBsAAQAJAAAAAJIAAA1QAAAIgJAAg");
    this.shape_54.setTransform(64.8,-60);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.f("#FFFFFF").s().p("AiNDqQgNgBABgOIAAm2QAAgGADgEQAEgDAFAAIBfAAQANgBAAAOIAAFVICuAAQANAAAAAOIAABTQAAAOgNABg");
    this.shape_55.setTransform(64.9,-60);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.f("#FFFFFF").s().p("AiyEoQgRAAAAgSIAAorQAAgIAFgFQAFgEAHgBIB3AAQARAAAAASIAAGwIDdAAQAQAAABARIAABqQgBASgQAAg");
    this.shape_56.setTransform(64.9,-60);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.f("#FFFFFF").s().p("AjIFNQgUAAABgTIAApyQgBgJAGgGQAFgFAJAAICGAAQATAAABAUIAAHmID3AAQATAAABAUIAAB4QgBATgTAAg");
    this.shape_57.setTransform(64.9,-60);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.f("#FFFFFF").s().p("AjQFZQgTABAAgVIAAqIQgBgKAGgGQAGgGAIAAICLAAQAUAAABAWIAAH4IEAAAQAVAAgBAUIAAB8QABAVgVgBg");
    this.shape_58.setTransform(64.9,-60);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_53}]},3).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).wait(60));

    // A
    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.f("#FFFFFF").s().p("AATAjQgBAAAAgBQgBAAAAAAQAAAAgBgBQAAAAAAgBIgDgJIgZAAIgCAJQgBABAAAAQAAABgBAAQAAAAgBAAQAAABgBAAIgNAAQgBAAAAgBQAAAAgBAAQAAAAAAAAQAAAAAAAAIABgCIAZhAQAAAAAAAAQAAgBABAAQAAAAABAAQAAgBABAAIAKAAQABAAAAABQAAAAABAAQAAAAAAABQAAAAAAAAIAaBAIAAACQAAAAAAAAQAAAAAAAAQgBAAAAAAQAAABgBAAgAgGAJIANAAIgHgVg");
    this.shape_59.setTransform(3.2,-60);

    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.f("#FFFFFF").s().p("ABPCSQgJAAgDgIIgNgmIhqAAIgOAmQgDAIgJAAIg7AAQgHAAAAgEIACgEIBqkSQAFgJAIAAIAvAAQAJAAADAJIBrESIACAEQAAAEgHAAgAgfAkIA+AAIgfhYg");
    this.shape_60.setTransform(3.2,-60);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.f("#FFFFFF").s().p("AB+DqQgOgBgEgOIgWg9IirAAIgVA9QgFAOgOABIheAAQgKAAAAgIIABgHICrm2QAFgOAOABIBMAAQAOgBAFAOICsG2IABAHQAAAIgKAAgAgyA5IBjAAIgxiOg");
    this.shape_61.setTransform(3.2,-60);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.f("#FFFFFF").s().p("ACgEoQgTAAgFgSIgbhOIjZAAIgbBOQgGASgSAAIh2AAQgOAAAAgJIACgJIDYorQAHgSASAAIBgAAQASAAAGASIDaIrIACAJQAAAJgOAAgAg/BJIB+AAIhAi0g");
    this.shape_62.setTransform(3.2,-60);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.f("#FFFFFF").s().p("AC0FNQgVAAgFgTIgghYIj0AAIgeBYQgHATgUAAIiGAAQgOAAAAgKIACgJIDzpyQAIgUAUAAIBsAAQAUAAAHAUID1JyIACAJQAAAKgOAAgAhHBSICOAAIhIjMg");
    this.shape_63.setTransform(3.2,-60);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.f("#FFFFFF").s().p("AC7FZQgWABgGgVIgghbIj9AAIggBbQgHAVgVgBIiKAAQgQAAAAgKIADgKID8qIQAIgWAVAAIBwAAQAVAAAHAWID+KIIADAKQAAAKgQAAgAhKBUICTAAIhKjSg");
    this.shape_64.setTransform(3.2,-60);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_59}]},2).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).wait(61));

    // H
    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.f("#FFFFFF").s().p("AANAjQAAAAAAgBQgBAAAAAAQAAAAAAgBQgBAAAAgBIAAgaIgWAAIAAAaQAAABAAAAQAAABAAAAQgBAAAAAAQAAABgBAAIgNAAQgBAAAAgBQgBAAAAAAQAAAAAAgBQAAAAAAgBIAAhAIAAgBIACgBIANAAQABAAAAABQAAAAABAAQAAAAAAABQAAAAAAAAIAAAXIAWAAIAAgXIABgBIABgBIAPAAQAAAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAIAABAQAAABAAAAQAAABAAAAQAAAAgBAAQAAABgBAAg");
    this.shape_65.setTransform(-67.6,-60);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.f("#FFFFFF").s().p("AA6CSQgJAAAAgIIAAhwIhiAAIAABwQAAAIgJAAIg6AAQgIAAAAgIIAAkSQAAgEABgDQADgCAEAAIA7AAQAIgBAAAJIAABeIBiAAIAAhdQAAgEADgDQACgCAEAAIA7AAQAJgBAAAJIAAETQgBAIgJAAg");
    this.shape_66.setTransform(-67.6,-60);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.f("#FFFFFF").s().p("ABcDqQgNgBAAgOIAAiyIidAAIAACyQAAAOgPABIhdAAQgNgBAAgOIAAm2QAAgFADgFQAEgDAGAAIBfAAQANAAAAANIAACWICdAAIAAiWQAAgFADgFQAEgDAGAAIBfAAQANAAAAANIAAG2QAAAOgPABg");
    this.shape_67.setTransform(-67.6,-60);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.f("#FFFFFF").s().p("AB1EoQgRAAAAgSIAAjiIjHAAIAADiQAAASgTAAIh1AAQgRAAAAgSIAAorQgBgHAFgFQAEgGAJAAIB4AAQAQABAAARIAAC9IDHAAIAAi9QAAgHAEgFQAFgGAIAAIB3AAQARABAAARIAAIrQABASgUAAg");
    this.shape_68.setTransform(-67.6,-60);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.f("#FFFFFF").s().p("ACEFNQgTAAgBgTIAAkAIjgAAIAAEAQAAATgVAAIiEAAQgUAAAAgTIAApyQAAgIAGgHQAFgFAJAAICHAAQASAAAAATIAADWIDgAAIAAjVQABgIAFgHQAFgFAJAAICGAAQAUAAAAATIAAJzQAAATgXAAg");
    this.shape_69.setTransform(-67.6,-60);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.f("#FFFFFF").s().p("ACJFZQgVABABgVIAAkIIjpAAIAAEIQAAAVgWgBIiJAAQgUABAAgVIAAqIQAAgJAFgHQAFgFAKgBICMAAQATAAAAAVIAADdIDpAAIAAjcQgBgJAGgHQAFgFAKgBICMAAQATAAAAAVIAAKJQAAAVgXgBg");
    this.shape_70.setTransform(-67.6,-60);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).wait(62));

    // #
    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.f("#FFFFFF").s().p("AAEAbQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAgBAAAAIAAgLIgEAAIAAALQAAAAAAABQAAAAgBAAQAAAAAAAAQAAAAgBAAIgHAAQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAIAAgLIgFAAIgBgBIAAgBIAAgHIAAgBIABAAIAFAAIAAgIIgFAAIgBAAIAAgBIAAgIIAAgBIABAAIAFAAIAAgMQAAAAAAgBQAAAAAAAAQAAAAABAAQAAgBAAAAIAHAAQABAAAAABQAAAAAAAAQABAAAAAAQAAABAAAAIAAAMIAEAAIAAgMQAAAAAAgBQAAAAABAAQAAAAAAAAQAAgBABAAIAHAAQAAAAAAABQABAAAAAAQAAAAAAAAQAAABAAAAIAAAMIAGAAQAAAAAAAAQABAAAAAAQAAAAAAAAQAAABAAAAIAAAIIAAABIgBAAIgGAAIAAAIIAGAAQAAAAAAAAQABAAAAAAQAAAAAAAAQAAABAAAAIAAAHIAAABIgBABIgGAAIAAALQAAAAAAABQAAAAAAAAQAAAAgBAAQAAAAAAAAgAgCAFIAEAAIAAgIIgEAAg");
    this.shape_71.setTransform(-125.3,-52.9);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.f("#FFFFFF").s().p("AATCIQgHAAAAgIIAAg5IgbAAIAAA5QAAAIgHAAIgiAAQgIAAAAgIIAAg5IgYAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQgBAAAAgBQgDgCAAgDIAAgjQAAgDACgCQACgDAEAAIAXAAIAAgpIgYAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQgDgCAAgDIAAgkQAAgDACgCQACgDAEAAIAXAAIAAg4QAAgIAIAAIAiAAQAHAAAAAIIAAA4IAbAAIAAg4QAAgIAHAAIAiAAQAIAAAAAIIAAA4IAcAAQAHAAAAAIIAAAkQAAADgDACQgCACgDAAIgbAAIAAApIAcAAQAHAAAAAIIAAAjQAAADgDACQgCACgDAAIgbAAIAAA5QAAAIgIAAgAgJAbIAFAGIACABIAHAAIACgBIAFgGIAGAAIABAAIABgBIABgCIAAgEIgBgDIgDgDIAAgIIADgDIABgCIAAgHIgBgCIgDgDIAAgLIgBgDIgDgDIgBgBIgBADIgGAAIgHgHIgHAAIgOAHIgBABIAAAIIABABIAAABIADACIAAAJIgDACIAAABIgBABIAAAGIABABIADACIAAAMIABACIAEAEIAAAAg");
    this.shape_72.setTransform(-125.3,-52.9);

    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.f("#FFFFFF").s().p("AAeDVQgMAAAAgMIAAhZIgqAAIAABZQAAAMgLAAIg2AAQgMAAAAgMIAAhZIgmAAQgDAAgEgDQgDgDAAgFIAAg4QAAgFADgDQADgDAGAAIAkAAIAAhCIgmAAQgDAAgEgDQgDgEAAgFIAAg3QAAgFADgEQADgDAGAAIAkAAIAAhZQAAgMAMAAIA2AAQALAAAAAMIAABZIAqAAIAAhZQAAgMAMAAIA2AAQAMAAAAAMIAABZIArAAQALAAAAALIAAA4QAAAFgDAEQgEADgEAAIgrAAIAABCIArAAQALAAAAALIAAA4QAAAFgDADQgEADgEAAIgrAAIAABZQAAAMgMAAgAgbgjIgBABIAAAIIABABIAAABIABAEIAAAJIgBAEIAAAAIgBABIAAAGIABACIABAEIAAAMIAAADIACAFIAAACIAAAHIABABIAFAAIAJADIADAAIAHAAIACAAIAJgDIAGAAIACAAIABAAIACgBIAAgGIgBgDIgBgEIAAgJIABgFIABgCIAAgHIgBgDIgBgEIAAgMIAAgDIgCgFIgBgDIgBgEIgGAAIgLgDIgIAAIgTADg");
    this.shape_73.setTransform(-125.3,-52.9);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.f("#FFFFFF").s().p("AAlEDQgPAAAAgOIAAhtIgzAAIAABtQAAAOgOAAIhCAAQgOAAAAgOIAAhtIguAAQgEAAgFgEQgEgDAAgGIAAhEQAAgGAEgEQAEgEAGAAIAtAAIAAhQIguAAQgEAAgFgEQgEgEAAgGIAAhEQAAgGAEgEQAEgFAGAAIAtAAIAAhrQAAgPAOAAIBCAAQAOAAAAAPIAABrIAzAAIAAhrQAAgPAPAAIBBAAQAPAAAAAPIAABrIA1AAQANAAAAAPIAABEQAAAGgEAEQgFAEgFAAIg0AAIAABQIA1AAQANAAAAAOIAABEQAAAGgEADQgFAEgFAAIg0AAIAABtQAAAOgPAAgAgegfIAAABIAAATIAAABIAAABIAAAHIAAABIAAARIABADIAAAGIAAAEIAAAMIAFAAIALABIAEAAIAIAAIACAAIARgBIACAAIABAAIACAAIAAgGIAAgEIAAgUIAAgDIAAgIIAAgCIAAgRIAAgDIAAgHIgBgDIgBgIIgcgBIgWABIgBAAg");
    this.shape_74.setTransform(-125.3,-52.9);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.f("#FFFFFF").s().p("AAnETQgPAAAAgQIAAhzIg3AAIAABzQAAAQgPAAIhFAAQgPAAAAgQIAAhzIgxAAQgFAAgEgEQgFgEAAgGIAAhIQAAgGAEgEQAFgFAGAAIAwAAIAAhVIgxAAQgFAAgEgEQgFgEAAgHIAAhHQAAgHAEgEQAFgFAGAAIAwAAIAAhyQAAgQAPAAIBFAAQAPAAAAAQIAAByIA3AAIAAhyQAAgQAPAAIBFAAQAQAAAAAQIAAByIA4AAQAOAAAAAPIAABIQAAAHgFAEQgEAEgGAAIg3AAIAABVIA4AAQAOAAAAAPIAABIQAAAGgFAEQgEAEgGAAIg3AAIAABzQAAAQgQAAgAgfArIA3AAIAAhVIg3AAg");
    this.shape_75.setTransform(-125.3,-52.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_71}]}).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).wait(64));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-127.2,-55.7,3.9,5.5);


(lib.mcPlane = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.instance = new lib.graPlane();
    this.instance.parent = this;
    this.instance.setTransform(0.8,0.9,1,1,0,0,0,70.4,38.6);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({y:1.9},1).to({y:-0.1},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-70.1,-38.7,141.8,79.2);


(lib.mcBox2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().p("EgbHAhpMAAAhDRMA2PAAAMAAABDRg");

    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:173.6,y:215.3}).wait(50).to({graphics:null,x:0,y:0}).wait(61));

    // side red
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#C5281C").s().p("Aj2MjIHt5FIAAZFg");
    this.shape.setTransform(-24.7,350.4);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#C5281C").s().p("AkvNsIHu7XQCtQWhcLBg");
    this.shape_1.setTransform(-19.1,343);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#C5281C").s().p("AlXOgIHt8/QEqTDifJ8g");
    this.shape_2.setTransform(-15,337.8);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#C5281C").s().p("AlwPAIHu9/QF0UtjHJSg");
    this.shape_3.setTransform(-12.6,334.7);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#C5281C").s().p("Al4PKIHu+UQGNVQjUJEg");
    this.shape_4.setTransform(-11.8,333.6);

    var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[]},37).wait(61));

    // shadow
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#BA2120").s().p("AAYEPQgggpgehzQgdhugWibQgJhFgHhIIAKBHQAYCpAiBsQAiBxAkATQAnAUAihiIgYCbIgDAPQgLALgLAAQgRAAgQgVg");
    this.shape_5.setTransform(-10.9,294.5);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#BA2120").s().p("ABqEPQiSgpiJhzQiEhuhgibQgrhFgfhIQAUAkAXAjQBwCpCXBsQCdBxCjATQCyAUCbhiQguA7hBBgIgKAPQgzALg0AAQhKAAhMgVg");
    this.shape_6.setTransform(45.7,294.5);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#BA2120").s().p("AClEPQjkgpjVhzQjNhuiWibQhDhFgwhIQAfAkAkAjQCuCpDrBsQD2BxD9ATQEVAUDyhiQhIA7hkBgIgRAPQhPALhRAAQhzAAh3gVg");
    this.shape_7.setTransform(86.2,294.5);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#BA2120").s().p("ADJEPQkWgpkChzQj6hui1ibQhRhFg8hIQAnAkArAjQDUCpEcBsQErBxE0ATQFRAUElhiQhXA7h6BgIgUAPQhgALhjAAQiLAAiQgVg");
    this.shape_8.setTransform(110.4,294.5);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#BA2120").s().p("ADVEPQkmgpkShzQkIhujBibQhVhFg/hIQAoAkAvAjQDfCpEuBsQE8BxFGATQFlAUE2hiQhcA7iBBgIgVAPQhmALhpAAQiTAAiYgVg");
    this.shape_9.setTransform(118.5,294.5);
    this.shape_9._off = true;

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#BA2120").s().p("ADVEIQkmglkShuQkIhrjBiYQhVhEg/hHQAoAjAvAjQDfCmEuBnQE8BtFGAOQFlAPE2hmQhcA9iBBhIgVAQQh0AOh3AAQiFAAiKgSg");
    this.shape_10.setTransform(118.5,294.7);
    this.shape_10._off = true;

    var maskedShapeInstanceList = [this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},9).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[]},1).wait(61));
    this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(61));
    this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).wait(1).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).wait(1).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(62));

    // cut
    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#C5281C").s().p("Ah2PKIAA+UQAKC8AZCrQAYCpAiBsQAiByAkATQAnATAjhiIgZCbQgcC+gVCyIhRLXg");
    this.shape_11.setTransform(-12.1,333.6);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#C5281C").s().p("AoXPKIAA+UQArC8BwCrQBwCpCXBsQCcByCkATQCyATCbhiQguA8hBBfQiBC+hdCyIlvLXg");
    this.shape_12.setTransform(40.1,333.6);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#C5281C").s().p("AtCPKIAA+UQBDC8CvCrQCuCpDrBsQD1ByD+ATQEVATDyhiQhIA8hkBfQjKC+iRCyIo7LXg");
    this.shape_13.setTransform(77.5,333.6);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#C5281C").s().p("Av1PKIAA+UQBSC8DUCrQDTCpEdBsQEqByE1ATQFRATElhiQhXA8h7BfQj0C+iwCyIq2LXg");
    this.shape_14.setTransform(99.9,333.6);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#C5281C").s().p("AwwPKIAA+UQBWC8DhCrQDfCpEuBsQE7ByFHATQFlATE2hiQhcA8iBBfQkDC+i6CyIrfLXg");
    this.shape_15.setTransform(107.3,333.6);
    this.shape_15._off = true;

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#C5281C").s().p("AwwvJQBWC6DhCnQDfCnEuBoQE7BtFHAOQFlAPE2hnQhcA+iBBhQkDDAi6C2IrfLhIroALg");
    this.shape_16.setTransform(107.3,335.1);
    this.shape_16._off = true;

    var maskedShapeInstanceList = [this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},9).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[]},1).wait(61));
    this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(61));
    this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).wait(1).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).wait(1).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(62));

    // black
    this.instance = new lib.mcBoxBlack("single",0);
    this.instance.parent = this;
    this.instance.setTransform(173.6,215.3,1,1,0,0,0,173.6,215.3);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#000000").s().p("EgbHAhpMAAAhDRMA2PAAAMAAABDRg");
    this.shape_17.setTransform(173.6,215.3);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#000000").s().p("EgZfAhpIgRgRQBJrNiguGQAQi0gQhrMAAAglOMA18AAAIATAZMAAABCNIg5Arg");
    this.shape_18.setTransform(173.6,215.3);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#000000").s().p("EgZCAhpIgEgCIgHgQQBrqKjKvLQgBilgZh5IgBgJMAAAglDMA13AAAIAYARIAAAAMAAABCHIgmA0IgKAFg");
    this.shape_19.setTransform(173.6,215.3);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#000000").s().p("EgYmAhpIgEgBIAAgRQCNpIj0wQQgSiXgiiFIgCgMMAAAgk/MA1zAAAIAcAIIAAABMAAABCAIgTA+IgFAKg");
    this.shape_20.setTransform(173.6,215.3);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#000000").s().p("EgYOAhpQDUpEmN1RMAAAgk8MA2PAAAMAAABDRg");
    this.shape_21.setTransform(173.6,215.3);

    var maskedShapeInstanceList = [this.instance,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape_17}]},9).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},37).wait(61));

    // shadow
    this.instance_1 = new lib.mcBoxShadow("single",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(183.6,225.3,1,1,0,0,0,173.6,215.3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},50).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,357.3,440.7);


(lib.mcBox = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // GuestsFlown
    this.instance = new lib.GuestFlown();
    this.instance.parent = this;
    this.instance.setTransform(193.6,235.3,1,1,0,0,0,173.6,215.3);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({x:163.6,y:205.3},4,cjs.Ease.get(1)).to({x:173.6,y:215.3},2).wait(59));

    // black
    this.instance_1 = new lib.mcBoxBlack("single",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(173.6,215.3,1,1,0,0,0,173.6,215.3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).to({startPosition:0},1,cjs.Ease.get(1)).to({x:113.6,y:155.3},11,cjs.Ease.get(1)).wait(3).to({startPosition:0},0).to({x:178.6,y:220.3},4,cjs.Ease.get(1)).to({x:173.6,y:215.3},2).wait(59));

    // GuestsFlown
    this.instance_2 = new lib.GuestFlown();
    this.instance_2.parent = this;
    this.instance_2.setTransform(169.6,186.3,1,1,0,0,0,173.6,215.3);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off:false},0).to({x:193.6,y:235.3},11).to({_off:true},3).wait(65));

    // shadow
    this.instance_3 = new lib.mcBoxShadow("single",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(173.6,215.3,1,1,0,0,0,173.6,215.3);
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({_off:false},0).to({x:233.6,y:275.3},11,cjs.Ease.get(1)).wait(3).to({startPosition:0},0).to({x:178.6,y:220.3},4,cjs.Ease.get(1)).to({x:183.6,y:225.3},2).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,347.3,430.7);


(lib.mcLogo = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // timeline functions:
    this.frame_117 = function() {
        this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(117).call(this.frame_117).wait(1));

    // PLANE
    this.instance = new lib.mcPlane();
    this.instance.parent = this;
    this.instance.setTransform(-236,417.8,1,1,0,0,0,0.8,0.9);
    this.instance.alpha = 0;
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({_off:false},0).wait(1).to({regX:0,regY:0,x:-159.6,y:394.2,alpha:0.2},0).wait(1).to({x:-82.4,y:371.5,alpha:0.4},0).wait(1).to({x:-5.2,y:348.8,alpha:0.6},0).wait(1).to({x:72.1,y:326.2,alpha:0.8},0).wait(1).to({x:149.3,y:303.5,alpha:1},0).wait(1).to({x:192.4,y:301.7},0).wait(1).to({x:235.7,y:300.7},0).wait(1).to({x:242.2},0).wait(1).to({x:248.7,y:300.5},0).wait(1).to({x:252.3,y:298.9},0).wait(3).to({regX:0.8,regY:0.9,x:253.6,y:299.8},0).wait(31).to({mode:"single",startPosition:0},0).wait(42));

    // txt1
    this.instance_1 = new lib.txt1("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(144.2,82.9);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).to({x:194.2,y:152.9,startPosition:11},11,cjs.Ease.get(-1)).to({x:174.2,y:132.9,startPosition:18},7,cjs.Ease.get(-1)).wait(95));

    // Layer 16
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#EC2C2C").s().p("A6iFGIAAqLMA1FAAAIAAKLg");
    this.shape.setTransform(173.6,394.3);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#EC2C2C").s().p("A10FGIAAqLMArpAAAIAAKLg");
    this.shape_1.setTransform(202,394.3);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#EC2C2C").s().p("AxlFGIAAqLMAjLAAAIAAKLg");
    this.shape_2.setTransform(227.3,394.3);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#EC2C2C").s().p("At3FGIAAqLIbvAAIAAKLg");
    this.shape_3.setTransform(249.7,394.3);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#EC2C2C").s().p("AqoFGIAAqLIVRAAIAAKLg");
    this.shape_4.setTransform(269,394.3);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#EC2C2C").s().p("An6FGIAAqLIP1AAIAAKLg");
    this.shape_5.setTransform(285.4,394.3);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#EC2C2C").s().p("AlrFGIAAqLILXAAIAAKLg");
    this.shape_6.setTransform(298.8,394.3);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#EC2C2C").s().p("Aj7FGIAAqLIH3AAIAAKLg");
    this.shape_7.setTransform(309.3,394.3);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#EC2C2C").s().p("AisFGIAAqLIFZAAIAAKLg");
    this.shape_8.setTransform(316.7,394.3);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#EC2C2C").s().p("Ah8FGIAAqLID5AAIAAKLg");
    this.shape_9.setTransform(321.2,394.3);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#EC2C2C").s().p("AhsFGIAAqLIDZAAIAAKLg");
    this.shape_10.setTransform(322.7,394.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},27).to({state:[{t:this.shape}]},7).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).wait(74));

    // txt2
    this.txtGuests = new cjs.Text("GUESTS FLOWN", "bold 30px 'Gotham Black'", "#FFFFFF");
    this.txtGuests.name = "txtGuests";
    this.txtGuests.textAlign = "center";
    this.txtGuests.lineHeight = 27;
    this.txtGuests.lineWidth = 321;
    this.txtGuests.parent = this;
    this.txtGuests.setTransform(173.6,382.3);
    this.txtGuests._off = true;

    this.timeline.addTween(cjs.Tween.get(this.txtGuests).wait(27).to({_off:false},0).wait(91));

    // FlownGuest
    this.instance_2 = new lib.GuestFlown();
    this.instance_2.parent = this;
    this.instance_2.setTransform(173.6,215.3,1,1,0,0,0,173.6,215.3);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(27).to({_off:false},0).wait(91));

    // mcBox
    this.instance_3 = new lib.mcBox();
    this.instance_3.parent = this;
    this.instance_3.setTransform(16.6,21.4,0.1,0.1,0,0,0,178.5,220.5);
    this.instance_3._off = true;

    this.instance_4 = new lib.mcBox2("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(178.6,220.3,1,1,0,0,0,178.6,220.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},5).to({state:[{t:this.instance_3}]},11).to({state:[{t:this.instance_3}]},7).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4,p:{mode:"synched",startPosition:0,loop:false}}]},2).to({state:[{t:this.instance_4,p:{mode:"single",startPosition:49,loop:undefined}}]},49).wait(42));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off:false},0).to({regX:178.7,regY:220.2,scaleX:1.1,scaleY:1.1,rotation:386,x:178.8,y:220.3},11,cjs.Ease.get(-1)).to({regX:178.5,regY:220.3,scaleX:1,scaleY:1,rotation:353.6,x:178.7,y:220.4},7,cjs.Ease.get(-1)).to({regX:178.6,rotation:360,x:178.6,y:220.3},2,cjs.Ease.get(-1)).to({_off:true},2).wait(91));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


// stage content:
(lib.logo = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // timeline functions:
    this.frame_0 = function() {
        var inputTxt = "GUESTS FLOWN"; //The varieble for text input, Kristen Khong
        this.mcLogo.txtGuests.text = inputTxt; // This is the dynamic text, Kristen Khong
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

    // Isolation Mode
    this.mcLogo = new lib.mcLogo();
    this.mcLogo.parent = this;
    this.mcLogo.setTransform(221.4,179.7);

    this.timeline.addTween(cjs.Tween.get(this.mcLogo).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
    id: '51540C5377B641B7A6B703659A25C199',
    width: 800,
    height: 800,
    fps: 24,
    color: "#454545",
    opacity: 0.00,
    manifest: [],
    preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};

an.compositions = an.compositions || {};
an.compositions['51540C5377B641B7A6B703659A25C199'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}

an.getComposition = function(id) {
    return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;