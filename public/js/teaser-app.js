$(document).ready(function() {
  var lang    = $('input[name="lang"]').val();
  var days    = 'DAYS';
  var hours   = 'HOURS';
  var minutes = 'MINUTES';
  var seconds = 'SECONDS';

  switch (lang) {
    case 'id': 
      days    = 'hari';
      hours   = 'jam';
      minutes = 'menit';
      seconds = 'detik';
      break;
    case 'th':
      days    = 'วัน';
      hours   = 'ชั่วโมง';
      minutes = 'นาที';
      seconds = 'ที่สอง';
      break;
    case 'cn-zh': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    case 'tw-zh': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    case 'hk-zh': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    case 'kr': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    case 'jp': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    case 'vn': 
      days    = '天';
      hours   = '小时';
      minutes = '分钟';
      seconds = '秒';
      break;
    default: 
      break;
  }

  $('#time-countdown .flipclock-wrap:nth-child(1) .flipclock-divider .flipclock-label').html(days);
  $('#time-countdown .flipclock-wrap:nth-child(2) .flipclock-divider .flipclock-label').html(hours);
  $('#time-countdown .flipclock-wrap:nth-child(3) .flipclock-divider .flipclock-label').html(minutes);
  $('#time-countdown .flipclock-wrap:nth-child(4) .flipclock-divider .flipclock-label').html(seconds);
})