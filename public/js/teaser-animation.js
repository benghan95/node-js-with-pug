(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
    }


(lib.txt5 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-18.5,9).lineTo(-19.2,8.5).curveTo(-19.5,8.1,-19.5,7.6).curveTo(-19.5,7,-19.1,6.6).curveTo(-18.7,6.2,-18.3,6.2).lineTo(-17.7,6.3).lineTo(-17,6.4).curveTo(-16.5,6.4,-16.3,6.3).curveTo(-16,6.1,-15.8,5.7).lineTo(-15.6,5.5).lineTo(-19.7,-3.9).lineTo(-19.9,-4.6).curveTo(-19.8,-5.3,-19.4,-5.8).curveTo(-18.9,-6.2,-18.3,-6.2).curveTo(-17.6,-6.2,-17.2,-5.9).curveTo(-16.9,-5.6,-16.6,-5).lineTo(-14,1.9).lineTo(-11.6,-5).curveTo(-11.4,-5.6,-11,-5.9).curveTo(-10.6,-6.2,-10,-6.2).curveTo(-9.3,-6.2,-8.9,-5.8).curveTo(-8.4,-5.4,-8.4,-4.7).curveTo(-8.4,-4.3,-8.5,-3.9).lineTo(-12.7,6).curveTo(-13.1,6.9,-13.5,7.5).curveTo(-13.9,8.1,-14.3,8.5).curveTo(-14.7,8.9,-15.4,9.1).curveTo(-15.9,9.3,-16.6,9.3).curveTo(-17.6,9.3,-18.5,9).closePath().moveTo(-3.1,5.5).curveTo(-4.3,5.1,-5.1,4.2).curveTo(-5.9,3.4,-6.4,2.3).lineTo(-6.7,1.1).lineTo(-6.8,-0.2).lineTo(-6.8,-0.2).curveTo(-6.9,-1.4,-6.4,-2.6).curveTo(-6,-3.7,-5.2,-4.5).curveTo(-4.4,-5.4,-3.3,-5.9).lineTo(-2.2,-6.2).lineTo(-1,-6.3).curveTo(-0.2,-6.3,0.4,-6.2).curveTo(1,-6.1,1.6,-5.8).curveTo(2.3,-5.5,3.3,-4.4).curveTo(4,-3.6,4.3,-2.6).curveTo(4.7,-1.6,4.7,-0.7).curveTo(4.7,0.1,4.2,0.5).curveTo(3.8,0.9,3.1,0.9).lineTo(-3.5,0.9).curveTo(-3.4,1.5,-3.1,1.9).curveTo(-2.8,2.4,-2.5,2.7).curveTo(-2.2,3,-1.7,3.1).curveTo(-1.2,3.3,-0.6,3.3).curveTo(0.1,3.3,0.6,3.1).lineTo(1.7,2.5).curveTo(2.1,2.2,2.5,2.2).curveTo(3.1,2.2,3.5,2.6).curveTo(3.9,3,3.9,3.5).curveTo(3.9,4.1,3.4,4.5).curveTo(2.6,5.2,1.6,5.6).curveTo(0.6,5.9,-0.6,5.9).curveTo(-2.4,5.8,-3.1,5.5).closePath().moveTo(1.5,-1.1).lineTo(1.2,-2.2).lineTo(0.7,-3).curveTo(0.4,-3.3,-0.1,-3.5).curveTo(-0.5,-3.6,-1,-3.6).curveTo(-1.5,-3.6,-2,-3.5).curveTo(-2.3,-3.3,-2.7,-3).lineTo(-3.2,-2.2).lineTo(-3.6,-1.1).lineTo(1.5,-1.1).lineTo(1.5,-1.1).closePath().moveTo(9.8,5.7).curveTo(9.2,5.5,8.7,5.1).curveTo(8.3,4.7,8,4).curveTo(7.7,3.3,7.8,2.3).lineTo(7.8,-3.3).lineTo(7.7,-3.3).curveTo(7,-3.3,6.6,-3.7).curveTo(6.2,-4.1,6.2,-4.7).curveTo(6.2,-5.3,6.6,-5.7).curveTo(7,-6.1,7.7,-6.1).lineTo(7.8,-6.1).lineTo(7.8,-7.6).curveTo(7.8,-8.3,8.2,-8.8).curveTo(8.8,-9.3,9.4,-9.3).curveTo(10.1,-9.3,10.6,-8.8).curveTo(11.1,-8.3,11.1,-7.6).lineTo(11.1,-6.1).lineTo(12.5,-6.1).curveTo(13.2,-6.1,13.5,-5.7).curveTo(14,-5.3,14,-4.7).curveTo(14,-4.1,13.5,-3.7).curveTo(13.2,-3.3,12.5,-3.3).lineTo(11.1,-3.3).lineTo(11.1,1.8).curveTo(11.1,2.4,11.4,2.6).curveTo(11.6,2.9,12.2,2.9).lineTo(12.5,2.9).curveTo(13.2,2.9,13.5,3.3).curveTo(14,3.7,14,4.3).curveTo(13.9,4.7,13.7,5.1).curveTo(13.5,5.4,13.1,5.5).curveTo(12.3,5.9,11.2,5.9).curveTo(10.4,5.9,9.8,5.7).closePath().moveTo(16.6,5.3).curveTo(16,4.7,16,3.9).lineTo(16,3.9).curveTo(16,3.1,16.6,2.6).curveTo(17.1,2,17.9,2).curveTo(18.8,2,19.3,2.6).curveTo(19.9,3.1,19.8,3.9).lineTo(19.8,3.9).curveTo(19.9,4.7,19.3,5.3).curveTo(18.8,5.8,17.9,5.8).curveTo(17.1,5.8,16.6,5.3).closePath();
    this.shape.setTransform(194.2,0.9);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt5, new cjs.Rectangle(-217.3,-13,434.8,26), null);


(lib.txt4 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(45.7,7.8).curveTo(44.5,7.4,43.7,6.5).curveTo(42.8,5.7,42.4,4.6).lineTo(42.1,3.4).lineTo(41.9,2.1).lineTo(41.9,2.1).curveTo(41.9,0.9,42.3,-0.3).curveTo(42.7,-1.4,43.5,-2.2).curveTo(44.3,-3.1,45.4,-3.6).lineTo(46.5,-3.9).lineTo(47.7,-4).curveTo(48.5,-4,49.2,-3.9).curveTo(49.7,-3.8,50.3,-3.5).curveTo(51,-3.2,52.1,-2.1).curveTo(52.7,-1.3,53.1,-0.3).curveTo(53.4,0.7,53.4,1.6).curveTo(53.5,2.4,53,2.8).curveTo(52.5,3.2,51.9,3.2).lineTo(45.2,3.2).curveTo(45.4,3.8,45.7,4.2).curveTo(45.9,4.7,46.2,5).curveTo(46.6,5.3,47.1,5.4).curveTo(47.5,5.6,48.2,5.6).curveTo(48.8,5.6,49.4,5.4).lineTo(50.5,4.8).curveTo(50.8,4.5,51.3,4.5).curveTo(51.9,4.5,52.2,4.9).curveTo(52.6,5.3,52.6,5.8).curveTo(52.6,6.4,52.1,6.8).curveTo(51.3,7.5,50.4,7.9).curveTo(49.4,8.2,48.1,8.2).curveTo(46.3,8.1,45.7,7.8).closePath().moveTo(50.3,1.2).lineTo(50,0.1).lineTo(49.5,-0.7).curveTo(49.2,-1,48.7,-1.2).curveTo(48.2,-1.3,47.7,-1.3).curveTo(47.3,-1.3,46.8,-1.2).curveTo(46.4,-1,46,-0.7).lineTo(45.5,0.1).lineTo(45.2,1.2).lineTo(50.3,1.2).lineTo(50.3,1.2).closePath().moveTo(18.7,8.1).lineTo(17.4,7.8).curveTo(16,7,15.4,6.5).curveTo(14.6,5.6,14.1,4.5).lineTo(13.7,3.4).lineTo(13.6,2.1).lineTo(13.6,2.1).curveTo(13.6,1.3,14.1,-0.3).curveTo(14.6,-1.4,15.4,-2.2).curveTo(16,-2.8,17.5,-3.6).lineTo(18.7,-3.9).lineTo(20,-4).curveTo(21,-4,22.5,-3.6).curveTo(23.3,-3.3,24.5,-2.3).curveTo(25.5,-1.4,25.9,-0.3).curveTo(26.4,1.2,26.4,2.1).lineTo(26.2,3.3).lineTo(25.9,4.5).curveTo(25.5,5.6,24.5,6.4).curveTo(23.3,7.4,22.5,7.8).curveTo(20.9,8.2,20,8.2).closePath().moveTo(18.7,-0.9).curveTo(18.2,-0.7,17.8,-0.2).curveTo(17.3,0.2,17.1,0.8).curveTo(16.9,1.4,16.9,2.1).curveTo(16.9,2.8,17.1,3.3).curveTo(17.3,3.9,17.8,4.4).curveTo(18.2,4.8,18.8,5.1).curveTo(19.3,5.4,20,5.4).curveTo(20.7,5.4,21.3,5.1).curveTo(21.8,4.9,22.3,4.4).curveTo(22.6,4,22.8,3.4).curveTo(23.1,2.8,23.1,2.1).lineTo(23.1,2.1).curveTo(23.1,1.5,22.8,0.9).curveTo(22.6,0.3,22.3,-0.2).curveTo(21.8,-0.6,21.3,-0.9).curveTo(20.6,-1.2,20,-1.2).curveTo(19.3,-1.2,18.7,-0.9).closePath().moveTo(-16.2,7.8).curveTo(-17.3,7.4,-18.2,6.5).curveTo(-19,5.7,-19.5,4.6).lineTo(-19.8,3.4).lineTo(-19.9,2.1).lineTo(-19.9,2.1).curveTo(-19.9,0.9,-19.5,-0.3).curveTo(-19.1,-1.4,-18.3,-2.2).curveTo(-17.5,-3.1,-16.4,-3.6).lineTo(-15.4,-3.9).lineTo(-14.1,-4).curveTo(-13.4,-4,-12.7,-3.9).curveTo(-12.1,-3.8,-11.6,-3.5).curveTo(-10.8,-3.2,-9.8,-2.1).curveTo(-9.1,-1.3,-8.8,-0.3).curveTo(-8.4,0.7,-8.4,1.6).curveTo(-8.4,2.4,-8.9,2.8).curveTo(-9.3,3.2,-10,3.2).lineTo(-16.6,3.2).curveTo(-16.5,3.8,-16.2,4.2).curveTo(-15.9,4.7,-15.6,5).curveTo(-15.3,5.3,-14.7,5.4).curveTo(-14.3,5.6,-13.7,5.6).curveTo(-13,5.6,-12.5,5.4).lineTo(-11.4,4.8).curveTo(-11,4.5,-10.6,4.5).curveTo(-9.9,4.5,-9.6,4.9).curveTo(-9.3,5.3,-9.2,5.8).curveTo(-9.3,6.4,-9.7,6.8).curveTo(-10.5,7.5,-11.5,7.9).curveTo(-12.5,8.2,-13.7,8.2).curveTo(-15.5,8.1,-16.2,7.8).closePath().moveTo(-11.6,1.2).lineTo(-11.9,0.1).lineTo(-12.4,-0.7).curveTo(-12.7,-1,-13.2,-1.2).curveTo(-13.6,-1.3,-14.1,-1.3).curveTo(-14.6,-1.3,-15.1,-1.2).curveTo(-15.5,-1,-15.8,-0.7).lineTo(-16.3,0.1).lineTo(-16.6,1.2).lineTo(-11.6,1.2).lineTo(-11.6,1.2).closePath().moveTo(7.7,8).curveTo(7.1,7.8,6.6,7.4).curveTo(6.2,7,5.9,6.3).curveTo(5.6,5.6,5.6,4.6).lineTo(5.6,-1).lineTo(5.5,-1).curveTo(5,-1,4.5,-1.4).curveTo(4.1,-1.8,4.1,-2.4).curveTo(4.1,-3,4.5,-3.4).curveTo(5,-3.8,5.5,-3.8).lineTo(5.6,-3.8).lineTo(5.6,-5.3).curveTo(5.7,-6,6.2,-6.5).curveTo(6.6,-7,7.3,-7).curveTo(8,-7,8.5,-6.5).curveTo(9,-6,9,-5.3).lineTo(9,-3.8).lineTo(10.5,-3.8).curveTo(11,-3.8,11.5,-3.4).curveTo(11.9,-3,11.9,-2.4).curveTo(11.9,-1.8,11.5,-1.4).curveTo(11,-1,10.5,-1).lineTo(9,-1).lineTo(9,4.1).curveTo(9,4.7,9.2,4.9).curveTo(9.5,5.2,10,5.2).lineTo(10.5,5.2).curveTo(11,5.2,11.5,5.6).curveTo(11.8,6,11.8,6.6).curveTo(11.9,7,11.6,7.4).curveTo(11.3,7.7,11,7.8).curveTo(10.1,8.2,9.1,8.2).curveTo(8.3,8.2,7.7,8).closePath().moveTo(-4,7.9).curveTo(-5.2,7.5,-6.2,6.9).curveTo(-6.9,6.4,-6.9,5.7).curveTo(-6.8,5.1,-6.4,4.8).curveTo(-6.1,4.4,-5.5,4.4).curveTo(-5.2,4.4,-4.8,4.6).curveTo(-4.1,5.1,-3.2,5.4).curveTo(-2.4,5.6,-1.7,5.6).curveTo(-1,5.6,-0.6,5.4).curveTo(-0.3,5.1,-0.2,4.7).lineTo(-0.2,4.7).curveTo(-0.2,4.2,-1,3.9).lineTo(-2.7,3.3).curveTo(-4.4,2.8,-5.1,2.2).curveTo(-5.7,1.8,-5.9,1.2).curveTo(-6.2,0.6,-6.3,-0.2).lineTo(-6.3,-0.3).curveTo(-6.2,-1.1,-5.9,-1.9).curveTo(-5.6,-2.5,-4.9,-3).curveTo(-4.4,-3.5,-3.6,-3.7).curveTo(-2.8,-4,-1.9,-4).curveTo(-1,-4,-0,-3.8).curveTo(0.8,-3.5,1.7,-3.1).curveTo(2.1,-2.9,2.3,-2.6).curveTo(2.5,-2.3,2.6,-1.8).curveTo(2.6,-1.3,2.1,-0.9).curveTo(1.7,-0.5,1.2,-0.5).curveTo(0.9,-0.5,0.6,-0.7).lineTo(-0.8,-1.2).curveTo(-1.4,-1.4,-2,-1.4).curveTo(-2.6,-1.4,-2.9,-1.2).curveTo(-3.2,-1,-3.2,-0.6).lineTo(-3.2,-0.6).curveTo(-3.2,-0.1,-2.5,0.2).lineTo(-0.9,0.8).lineTo(0.5,1.3).lineTo(1.6,2).curveTo(2.2,2.4,2.4,3).curveTo(2.7,3.6,2.7,4.3).lineTo(2.7,4.4).curveTo(2.7,5.3,2.4,6.1).curveTo(2,6.8,1.5,7.3).curveTo(0.8,7.7,0,8).curveTo(-0.8,8.2,-1.8,8.2).curveTo(-2.9,8.2,-4,7.9).closePath().moveTo(36.8,7.6).curveTo(36.3,7.1,36.2,6.4).lineTo(36.2,1.4).curveTo(36.2,0.8,36.1,0.4).curveTo(36,-0.1,35.7,-0.4).curveTo(35.5,-0.7,35.1,-0.8).curveTo(34.7,-1,34.2,-1).curveTo(33.7,-1,33.3,-0.8).curveTo(33,-0.7,32.7,-0.4).curveTo(32.5,-0.1,32.3,0.4).curveTo(32.1,0.8,32.2,1.4).lineTo(32.2,6.4).curveTo(32.2,7.1,31.6,7.6).curveTo(31.1,8.1,30.5,8.1).curveTo(29.7,8.1,29.3,7.6).curveTo(28.8,7.1,28.8,6.4).lineTo(28.8,-2.3).curveTo(28.8,-3,29.3,-3.5).curveTo(29.7,-3.9,30.5,-3.9).curveTo(31.1,-3.9,31.6,-3.5).curveTo(32.2,-3,32.2,-2.3).lineTo(32.2,-2.1).curveTo(32.8,-3,33.5,-3.5).lineTo(34.4,-3.9).curveTo(35,-4,35.6,-4).curveTo(36.5,-4,37.3,-3.7).curveTo(38,-3.4,38.6,-2.9).curveTo(39.1,-2.3,39.4,-1.5).curveTo(39.6,-0.7,39.6,0.3).lineTo(39.6,6.4).curveTo(39.6,7.1,39.1,7.6).curveTo(38.6,8.1,37.9,8.1).curveTo(37.2,8.1,36.8,7.6).closePath().moveTo(-25.3,7.6).curveTo(-25.8,7.1,-25.8,6.4).lineTo(-25.8,-6.5).curveTo(-25.8,-7.2,-25.3,-7.7).curveTo(-24.8,-8.2,-24.2,-8.2).curveTo(-23.5,-8.2,-22.9,-7.7).curveTo(-22.5,-7.2,-22.5,-6.5).lineTo(-22.5,6.4).curveTo(-22.5,7.1,-22.9,7.6).curveTo(-23.5,8.1,-24.2,8.1).curveTo(-24.8,8.1,-25.3,7.6).closePath().moveTo(-31.9,7.6).curveTo(-32.4,7.1,-32.4,6.4).lineTo(-32.4,-2.3).curveTo(-32.4,-3,-31.9,-3.5).curveTo(-31.4,-3.9,-30.7,-3.9).curveTo(-30.1,-3.9,-29.6,-3.5).curveTo(-29,-3,-29,-2.3).lineTo(-29,6.4).curveTo(-29,7.1,-29.6,7.6).curveTo(-30.1,8.1,-30.7,8.1).curveTo(-31.4,8.1,-31.9,7.6).closePath().moveTo(-38.3,7.6).curveTo(-38.8,7.1,-38.8,6.4).lineTo(-38.8,1.4).curveTo(-38.8,0.8,-38.9,0.4).curveTo(-39,-0.1,-39.3,-0.4).curveTo(-39.6,-0.7,-39.9,-0.8).curveTo(-40.3,-1,-40.7,-1).curveTo(-41.2,-1,-41.6,-0.8).curveTo(-42,-0.7,-42.3,-0.4).curveTo(-42.5,-0.1,-42.6,0.4).curveTo(-42.8,0.8,-42.8,1.4).lineTo(-42.8,6.4).curveTo(-42.8,7.1,-43.3,7.6).curveTo(-43.7,8.1,-44.4,8.1).curveTo(-45.1,8.1,-45.7,7.6).curveTo(-46.1,7.1,-46.1,6.4).lineTo(-46.1,1.4).lineTo(-46.2,0.4).curveTo(-46.4,-0.1,-46.7,-0.4).curveTo(-46.9,-0.7,-47.2,-0.8).curveTo(-47.6,-1,-48.1,-1).curveTo(-48.5,-1,-48.9,-0.8).curveTo(-49.3,-0.7,-49.5,-0.4).curveTo(-49.8,-0.1,-49.9,0.4).curveTo(-50.1,0.8,-50.1,1.4).lineTo(-50.1,6.4).curveTo(-50.1,7.1,-50.6,7.6).curveTo(-51.1,8.1,-51.8,8.1).curveTo(-52.5,8.1,-53,7.6).curveTo(-53.4,7.1,-53.4,6.4).lineTo(-53.4,-2.3).curveTo(-53.4,-3,-53,-3.5).curveTo(-52.5,-3.9,-51.8,-3.9).curveTo(-51.1,-3.9,-50.6,-3.5).curveTo(-50.1,-3,-50.1,-2.3).lineTo(-50.1,-2.1).curveTo(-49.5,-3,-48.7,-3.5).lineTo(-47.8,-3.9).curveTo(-47.2,-4,-46.7,-4).curveTo(-45.5,-4,-44.7,-3.6).curveTo(-43.8,-3.1,-43.3,-2.2).curveTo(-42.5,-3.1,-41.6,-3.6).lineTo(-40.6,-3.9).lineTo(-39.5,-4).curveTo(-38.5,-4,-37.8,-3.8).curveTo(-37,-3.5,-36.5,-2.9).curveTo(-36,-2.4,-35.8,-1.6).curveTo(-35.4,-0.8,-35.4,0.3).lineTo(-35.4,6.4).curveTo(-35.5,7.1,-36,7.6).curveTo(-36.5,8.1,-37.1,8.1).curveTo(-37.9,8.1,-38.3,7.6).closePath().moveTo(-32.1,-5.4).curveTo(-32.6,-5.9,-32.6,-6.6).lineTo(-32.6,-6.6).curveTo(-32.6,-7.3,-32.1,-7.8).curveTo(-31.6,-8.2,-30.7,-8.2).curveTo(-29.9,-8.2,-29.4,-7.8).curveTo(-28.9,-7.3,-28.9,-6.6).lineTo(-28.9,-6.6).curveTo(-28.9,-5.9,-29.4,-5.4).curveTo(-29.9,-5,-30.7,-5).curveTo(-31.6,-5,-32.1,-5.4).closePath();
    this.shape.setTransform(112.6,-1.4);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt4, new cjs.Rectangle(-217.3,-13,434.8,26), null);


(lib.txt3 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-1.9,9.7).curveTo(-2.9,9.5,-4,9.1).curveTo(-4.3,8.9,-4.7,8.6).curveTo(-4.9,8.2,-4.9,7.8).curveTo(-4.9,7.2,-4.5,6.8).curveTo(-4.1,6.4,-3.5,6.4).lineTo(-3,6.5).curveTo(-2.2,6.9,-1.4,7).curveTo(-0.6,7.2,0.3,7.2).curveTo(1.2,7.2,1.7,7).curveTo(2.4,6.8,2.9,6.4).curveTo(3.2,6,3.4,5.4).curveTo(3.6,4.8,3.7,4).lineTo(3.7,3.4).curveTo(2.9,4.4,2,4.8).curveTo(1.6,5.1,1,5.2).curveTo(0.5,5.4,-0.2,5.4).curveTo(-1.3,5.4,-2.2,5).curveTo(-3.1,4.6,-3.9,3.9).curveTo(-4.6,3.2,-5,2.2).curveTo(-5.5,1.1,-5.5,-0.1).lineTo(-5.5,-0.2).curveTo(-5.5,-1.1,-5,-2.5).curveTo(-4.6,-3.5,-3.9,-4.3).curveTo(-3.1,-5,-2.2,-5.3).curveTo(-1.2,-5.7,-0.2,-5.7).lineTo(1.1,-5.6).lineTo(2.1,-5.2).curveTo(2.8,-4.8,3.6,-3.9).curveTo(3.6,-4.6,4.1,-5.1).curveTo(4.6,-5.6,5.3,-5.6).curveTo(6,-5.6,6.5,-5.1).curveTo(6.9,-4.6,6.9,-3.9).lineTo(6.9,3.7).curveTo(6.8,5.6,6.6,6.4).lineTo(6.1,7.5).lineTo(5.5,8.3).curveTo(5,8.7,4.5,9).curveTo(4,9.3,3.3,9.5).curveTo(1.5,9.9,0.4,9.9).curveTo(-0.8,9.9,-1.9,9.7).closePath().moveTo(-0.4,-2.7).curveTo(-0.9,-2.5,-1.3,-2.2).curveTo(-1.6,-1.8,-1.9,-1.3).curveTo(-2.1,-0.8,-2.1,-0.2).lineTo(-2.1,-0.1).curveTo(-2.1,0.5,-1.9,1).curveTo(-1.6,1.5,-1.3,1.8).curveTo(-0.9,2.2,-0.4,2.4).curveTo(0.1,2.6,0.8,2.6).curveTo(1.4,2.6,1.9,2.4).curveTo(2.5,2.2,2.9,1.8).curveTo(3.2,1.5,3.4,1).curveTo(3.6,0.4,3.7,-0.1).lineTo(3.7,-0.2).curveTo(3.6,-0.8,3.4,-1.3).lineTo(2.9,-2.2).curveTo(2.5,-2.5,1.9,-2.7).curveTo(1.4,-2.9,0.8,-2.9).curveTo(0.1,-2.9,-0.4,-2.7).closePath().moveTo(-16.7,9.7).curveTo(-17.7,9.5,-18.8,9.1).curveTo(-19.2,8.9,-19.5,8.6).curveTo(-19.7,8.2,-19.7,7.8).curveTo(-19.7,7.2,-19.4,6.8).curveTo(-18.9,6.4,-18.4,6.4).lineTo(-17.8,6.5).curveTo(-17.1,6.9,-16.3,7).curveTo(-15.4,7.2,-14.6,7.2).curveTo(-13.7,7.2,-13.1,7).curveTo(-12.5,6.8,-12,6.4).curveTo(-11.6,6,-11.4,5.4).curveTo(-11.2,4.8,-11.2,4).lineTo(-11.2,3.4).curveTo(-12,4.4,-12.9,4.8).curveTo(-13.3,5.1,-13.9,5.2).curveTo(-14.4,5.4,-15,5.4).curveTo(-16.1,5.4,-17,5).curveTo(-17.9,4.6,-18.7,3.9).curveTo(-19.4,3.2,-19.9,2.2).curveTo(-20.3,1.1,-20.3,-0.1).lineTo(-20.3,-0.2).curveTo(-20.3,-1.1,-19.9,-2.5).curveTo(-19.4,-3.5,-18.7,-4.3).curveTo(-18,-5,-17,-5.3).curveTo(-16,-5.7,-15,-5.7).lineTo(-13.8,-5.6).lineTo(-12.8,-5.2).curveTo(-12,-4.8,-11.3,-3.9).curveTo(-11.3,-4.6,-10.7,-5.1).curveTo(-10.3,-5.6,-9.5,-5.6).curveTo(-8.9,-5.6,-8.4,-5.1).curveTo(-7.9,-4.6,-7.9,-3.9).lineTo(-7.9,3.7).curveTo(-8.1,5.6,-8.3,6.4).lineTo(-8.7,7.5).lineTo(-9.4,8.3).curveTo(-9.8,8.7,-10.4,9).curveTo(-10.8,9.3,-11.5,9.5).curveTo(-13.3,9.9,-14.5,9.9).curveTo(-15.6,9.9,-16.7,9.7).closePath().moveTo(-15.2,-2.7).curveTo(-15.8,-2.5,-16.1,-2.2).curveTo(-16.5,-1.8,-16.7,-1.3).curveTo(-16.9,-0.8,-16.9,-0.2).lineTo(-16.9,-0.1).curveTo(-16.9,0.5,-16.7,1).curveTo(-16.5,1.5,-16.1,1.8).curveTo(-15.8,2.2,-15.2,2.4).curveTo(-14.7,2.6,-14,2.6).curveTo(-13.5,2.6,-13,2.4).curveTo(-12.4,2.2,-12,1.8).curveTo(-11.7,1.5,-11.4,1).curveTo(-11.2,0.4,-11.2,-0.1).lineTo(-11.2,-0.2).curveTo(-11.2,-0.8,-11.4,-1.3).lineTo(-12,-2.2).curveTo(-12.4,-2.5,-13,-2.7).curveTo(-13.5,-2.9,-14,-2.9).curveTo(-14.7,-2.9,-15.2,-2.7).closePath().moveTo(13.1,6.1).curveTo(12,5.7,11.1,4.9).curveTo(10.3,4.1,9.8,3).lineTo(9.5,1.8).lineTo(9.4,0.5).lineTo(9.4,0.4).curveTo(9.4,-0.8,9.8,-1.9).curveTo(10.2,-3,11,-3.9).curveTo(11.8,-4.7,12.9,-5.2).lineTo(13.9,-5.6).lineTo(15.2,-5.7).curveTo(15.9,-5.7,16.6,-5.5).curveTo(17.2,-5.4,17.7,-5.1).curveTo(18.5,-4.8,19.5,-3.8).curveTo(20.2,-3,20.5,-1.9).curveTo(20.9,-1,20.9,-0).curveTo(20.9,0.7,20.4,1.2).curveTo(20,1.6,19.3,1.6).lineTo(12.7,1.6).curveTo(12.8,2.2,13.1,2.6).curveTo(13.4,3,13.7,3.3).curveTo(14,3.6,14.6,3.8).curveTo(15,3.9,15.6,3.9).curveTo(16.3,3.9,16.8,3.7).lineTo(17.9,3.1).curveTo(18.3,2.8,18.7,2.8).curveTo(19.4,2.8,19.7,3.2).curveTo(20,3.6,20.1,4.2).curveTo(20,4.8,19.6,5.2).curveTo(18.8,5.8,17.8,6.2).curveTo(16.8,6.6,15.6,6.6).curveTo(13.8,6.4,13.1,6.1).closePath().moveTo(17.7,-0.5).lineTo(17.4,-1.5).lineTo(16.9,-2.3).curveTo(16.6,-2.6,16.1,-2.8).curveTo(15.7,-3,15.2,-3).curveTo(14.7,-3,14.2,-2.8).curveTo(13.8,-2.7,13.5,-2.3).lineTo(13,-1.5).lineTo(12.7,-0.5).lineTo(17.7,-0.5).lineTo(17.7,-0.5).closePath().moveTo(37,6.4).curveTo(36.4,6.2,35.9,5.8).curveTo(35.5,5.4,35.2,4.7).curveTo(34.9,4,34.9,3).lineTo(34.9,-2.6).lineTo(34.8,-2.6).curveTo(34.3,-2.6,33.8,-3).curveTo(33.4,-3.4,33.4,-4).curveTo(33.4,-4.6,33.8,-5.1).curveTo(34.3,-5.5,34.8,-5.5).lineTo(34.9,-5.5).lineTo(34.9,-6.9).curveTo(35,-7.6,35.5,-8.1).curveTo(35.9,-8.6,36.6,-8.6).curveTo(37.3,-8.6,37.8,-8.1).curveTo(38.3,-7.6,38.3,-6.9).lineTo(38.3,-5.5).lineTo(39.8,-5.5).curveTo(40.3,-5.5,40.8,-5.1).curveTo(41.2,-4.6,41.2,-4).curveTo(41.2,-3.4,40.8,-3).curveTo(40.3,-2.6,39.8,-2.6).lineTo(38.3,-2.6).lineTo(38.3,2.4).curveTo(38.3,3,38.5,3.3).curveTo(38.8,3.6,39.3,3.6).lineTo(39.8,3.6).curveTo(40.3,3.6,40.8,4).curveTo(41.1,4.4,41.1,4.9).curveTo(41.2,5.4,40.9,5.7).curveTo(40.6,6.1,40.3,6.2).curveTo(39.4,6.5,38.4,6.5).curveTo(37.6,6.5,37,6.4).closePath().moveTo(25.3,6.2).curveTo(24.1,5.9,23.1,5.2).curveTo(22.4,4.8,22.4,4.1).curveTo(22.5,3.5,22.9,3.1).curveTo(23.2,2.7,23.8,2.7).curveTo(24.1,2.7,24.5,2.9).curveTo(25.2,3.4,26.1,3.7).curveTo(26.9,4,27.6,4).curveTo(28.3,4,28.7,3.7).curveTo(29,3.5,29.1,3.1).lineTo(29.1,3).curveTo(29.1,2.6,28.3,2.2).lineTo(26.6,1.7).curveTo(24.9,1.1,24.2,0.6).curveTo(23.6,0.2,23.4,-0.4).curveTo(23.1,-1,23,-1.9).lineTo(23,-1.9).curveTo(23.1,-2.8,23.4,-3.5).curveTo(23.7,-4.2,24.4,-4.7).curveTo(24.9,-5.1,25.7,-5.4).curveTo(26.5,-5.6,27.4,-5.6).curveTo(28.3,-5.6,29.2,-5.4).curveTo(30.1,-5.2,31,-4.7).curveTo(31.4,-4.6,31.6,-4.2).curveTo(31.8,-3.9,31.9,-3.5).curveTo(31.9,-2.9,31.4,-2.5).curveTo(31,-2.2,30.5,-2.2).curveTo(30.2,-2.2,29.9,-2.3).lineTo(28.5,-2.9).curveTo(27.9,-3.1,27.3,-3.1).curveTo(26.7,-3.1,26.4,-2.8).curveTo(26.1,-2.6,26.1,-2.3).lineTo(26.1,-2.2).curveTo(26.1,-1.8,26.8,-1.4).lineTo(28.4,-0.8).lineTo(29.8,-0.3).lineTo(30.9,0.3).curveTo(31.5,0.8,31.7,1.3).curveTo(32,1.9,32,2.7).lineTo(32,2.7).curveTo(32,3.7,31.7,4.4).curveTo(31.3,5.1,30.8,5.6).curveTo(30.1,6.1,29.3,6.3).curveTo(28.5,6.5,27.5,6.5).curveTo(26.4,6.5,25.3,6.2).closePath().moveTo(-35.4,6.4).lineTo(-36.4,6).curveTo(-37.2,5.6,-37.9,4.8).lineTo(-37.9,4.8).curveTo(-37.9,5.5,-38.3,6).curveTo(-38.8,6.5,-39.5,6.5).curveTo(-40.3,6.5,-40.7,6).curveTo(-41.2,5.5,-41.2,4.8).lineTo(-41.2,-8.2).curveTo(-41.2,-8.9,-40.7,-9.4).curveTo(-40.3,-9.9,-39.5,-9.9).curveTo(-38.8,-9.9,-38.3,-9.4).curveTo(-37.9,-8.9,-37.9,-8.2).lineTo(-37.9,-3.8).curveTo(-37.2,-4.7,-36.4,-5.1).curveTo(-35.9,-5.4,-35.4,-5.5).curveTo(-34.8,-5.7,-34.2,-5.7).curveTo(-33.1,-5.7,-32.2,-5.3).curveTo(-31.2,-4.9,-30.5,-4.1).curveTo(-29.7,-3.3,-29.3,-2.2).curveTo(-28.8,-0.6,-28.8,0.4).lineTo(-29,1.8).lineTo(-29.3,3.1).curveTo(-29.7,4.2,-30.4,5).curveTo(-31.2,5.7,-32.1,6.1).curveTo(-33.1,6.5,-34.2,6.5).lineTo(-35.4,6.4).closePath().moveTo(-36.2,-2.6).curveTo(-36.6,-2.4,-37.1,-2).curveTo(-37.4,-1.5,-37.6,-0.9).curveTo(-37.9,-0.3,-37.9,0.4).curveTo(-37.9,1.2,-37.6,1.8).curveTo(-37.4,2.4,-37.1,2.8).curveTo(-36.6,3.2,-36.2,3.5).curveTo(-35.6,3.7,-35,3.7).curveTo(-34.5,3.7,-33.9,3.5).curveTo(-33.4,3.3,-33,2.8).curveTo(-32.6,2.4,-32.4,1.8).curveTo(-32.2,1.2,-32.1,0.4).curveTo(-32.2,-0.3,-32.4,-0.9).curveTo(-32.6,-1.5,-33,-2).curveTo(-33.4,-2.4,-33.9,-2.6).curveTo(-34.5,-2.8,-35,-2.8).curveTo(-35.6,-2.8,-36.2,-2.6).closePath().moveTo(-25.7,6).curveTo(-26.2,5.5,-26.2,4.8).lineTo(-26.2,-3.9).curveTo(-26.2,-4.6,-25.7,-5.1).curveTo(-25.2,-5.6,-24.6,-5.6).curveTo(-23.9,-5.6,-23.3,-5.1).curveTo(-22.9,-4.6,-22.9,-3.9).lineTo(-22.9,4.8).curveTo(-22.9,5.5,-23.3,6).curveTo(-23.9,6.5,-24.6,6.5).curveTo(-25.2,6.5,-25.7,6).closePath().moveTo(-25.8,-7.1).curveTo(-26.4,-7.5,-26.4,-8.2).lineTo(-26.4,-8.3).curveTo(-26.4,-9,-25.8,-9.4).curveTo(-25.3,-9.8,-24.6,-9.8).curveTo(-23.7,-9.8,-23.2,-9.4).curveTo(-22.6,-9,-22.7,-8.3).lineTo(-22.7,-8.2).curveTo(-22.6,-7.5,-23.2,-7.1).curveTo(-23.7,-6.7,-24.6,-6.7).curveTo(-25.3,-6.7,-25.8,-7.1).closePath();
    this.shape.setTransform(9,0.3);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt3, new cjs.Rectangle(-217.3,-13,434.8,26), null);


(lib.txt2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-13,6).lineTo(-14.2,5.7).curveTo(-15.7,4.9,-16.2,4.4).curveTo(-17.1,3.5,-17.6,2.4).lineTo(-17.9,1.3).lineTo(-18.1,0).lineTo(-18.1,-0).curveTo(-18.1,-0.8,-17.6,-2.4).curveTo(-17.1,-3.5,-16.2,-4.3).curveTo(-15.7,-4.9,-14.2,-5.7).lineTo(-13,-6).lineTo(-11.6,-6.1).curveTo(-10.7,-6.1,-9.1,-5.7).curveTo(-8.4,-5.4,-7.1,-4.4).curveTo(-6.2,-3.5,-5.8,-2.4).curveTo(-5.3,-0.9,-5.3,-0).lineTo(-5.4,1.2).lineTo(-5.8,2.4).curveTo(-6.2,3.5,-7.1,4.3).curveTo(-8.4,5.3,-9.1,5.7).curveTo(-10.8,6.1,-11.7,6.1).closePath().moveTo(-13,-3).curveTo(-13.5,-2.8,-13.9,-2.3).curveTo(-14.3,-1.9,-14.5,-1.3).curveTo(-14.8,-0.7,-14.8,-0).curveTo(-14.8,0.7,-14.5,1.2).curveTo(-14.3,1.8,-13.9,2.3).curveTo(-13.5,2.7,-12.9,3).curveTo(-12.3,3.3,-11.6,3.3).curveTo(-11,3.3,-10.4,3).curveTo(-9.8,2.8,-9.4,2.3).curveTo(-9,1.9,-8.8,1.3).curveTo(-8.6,0.7,-8.6,0).lineTo(-8.6,-0).curveTo(-8.6,-0.6,-8.8,-1.2).curveTo(-9,-1.8,-9.4,-2.3).curveTo(-9.9,-2.7,-10.4,-3).curveTo(-11,-3.3,-11.7,-3.3).curveTo(-12.4,-3.3,-13,-3).closePath().moveTo(-0.7,5.8).curveTo(-1.4,5.5,-1.9,4.9).curveTo(-2.4,4.3,-2.7,3.5).curveTo(-3,2.7,-3,1.7).lineTo(-3,-4.4).curveTo(-3,-5.1,-2.5,-5.6).curveTo(-2,-6,-1.3,-6).curveTo(-0.6,-6,-0.1,-5.6).curveTo(0.4,-5.1,0.4,-4.4).lineTo(0.4,0.7).curveTo(0.4,1.2,0.5,1.7).lineTo(0.9,2.5).curveTo(1.2,2.8,1.5,2.9).curveTo(1.9,3.1,2.4,3.1).curveTo(2.9,3.1,3.3,2.9).lineTo(3.9,2.5).lineTo(4.3,1.7).curveTo(4.5,1.2,4.5,0.7).lineTo(4.5,-4.4).curveTo(4.5,-5.1,5,-5.6).curveTo(5.5,-6,6.2,-6).curveTo(6.9,-6,7.3,-5.6).curveTo(7.8,-5.1,7.8,-4.4).lineTo(7.8,4.3).curveTo(7.8,5,7.3,5.5).curveTo(6.9,6,6.2,6).curveTo(5.5,6,5,5.5).curveTo(4.5,5,4.5,4.3).lineTo(4.5,4.2).curveTo(3.8,5.1,3.1,5.5).lineTo(2.2,5.9).curveTo(1.7,6.1,1,6.1).curveTo(0.1,6.1,-0.7,5.8).closePath().moveTo(11.3,5.5).curveTo(10.8,5,10.8,4.3).lineTo(10.8,-4.4).curveTo(10.8,-5.1,11.3,-5.6).curveTo(11.8,-6,12.5,-6).curveTo(13.2,-6,13.7,-5.6).curveTo(14.2,-5.1,14.2,-4.4).lineTo(14.2,-3.8).curveTo(14.6,-4.8,15.1,-5.4).curveTo(15.7,-6,16.4,-6).curveTo(17.2,-6,17.6,-5.6).curveTo(18.1,-5.1,18.1,-4.4).curveTo(18.1,-3.8,17.7,-3.4).curveTo(17.3,-3,16.8,-2.9).curveTo(16.2,-2.7,15.7,-2.4).curveTo(15.2,-2.1,14.8,-1.6).curveTo(14.5,-1.1,14.3,-0.4).curveTo(14.2,0.3,14.2,1.2).lineTo(14.2,4.3).curveTo(14.2,5,13.7,5.5).curveTo(13.2,6,12.5,6).curveTo(11.8,6,11.3,5.5).closePath();
    this.shape.setTransform(-58.8,0.7);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt2, new cjs.Rectangle(-217.3,-13,434.8,26), null);


(lib.txt1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(55.3,9.7).curveTo(54.3,9.5,53.2,9.1).curveTo(52.8,8.9,52.5,8.6).curveTo(52.3,8.2,52.3,7.8).curveTo(52.3,7.2,52.7,6.8).curveTo(53.1,6.4,53.7,6.4).lineTo(54.2,6.5).curveTo(55,6.9,55.8,7).curveTo(56.6,7.2,57.5,7.2).curveTo(58.4,7.2,58.9,7).curveTo(59.6,6.8,60.1,6.4).curveTo(60.4,6,60.6,5.4).curveTo(60.8,4.8,60.8,4).lineTo(60.8,3.4).curveTo(60.1,4.4,59.2,4.8).curveTo(58.7,5.1,58.2,5.2).curveTo(57.7,5.4,57,5.4).curveTo(55.9,5.4,55,5).curveTo(54.1,4.6,53.3,3.9).curveTo(52.6,3.2,52.2,2.2).curveTo(51.7,1.1,51.7,-0.1).lineTo(51.7,-0.2).curveTo(51.7,-1.1,52.2,-2.5).curveTo(52.6,-3.5,53.3,-4.3).curveTo(54.1,-5,55,-5.3).curveTo(56,-5.7,57,-5.7).lineTo(58.3,-5.6).lineTo(59.3,-5.2).curveTo(60,-4.8,60.8,-3.9).curveTo(60.8,-4.6,61.3,-5.1).curveTo(61.8,-5.6,62.5,-5.6).curveTo(63.2,-5.6,63.7,-5.1).curveTo(64.1,-4.6,64.1,-3.9).lineTo(64.1,3.7).curveTo(64,5.6,63.8,6.4).lineTo(63.3,7.5).lineTo(62.6,8.3).curveTo(62.2,8.7,61.7,9).curveTo(61.2,9.3,60.5,9.5).curveTo(58.7,9.9,57.6,9.9).curveTo(56.4,9.9,55.3,9.7).closePath().moveTo(56.8,-2.7).curveTo(56.3,-2.5,55.9,-2.2).curveTo(55.5,-1.8,55.3,-1.3).curveTo(55.1,-0.8,55.1,-0.2).lineTo(55.1,-0.1).curveTo(55.1,0.5,55.3,1).curveTo(55.5,1.5,55.9,1.8).curveTo(56.3,2.2,56.8,2.4).curveTo(57.3,2.6,58,2.6).curveTo(58.6,2.6,59.1,2.4).curveTo(59.6,2.2,60.1,1.8).curveTo(60.4,1.5,60.6,1).curveTo(60.8,0.4,60.8,-0.1).lineTo(60.8,-0.2).curveTo(60.8,-0.8,60.6,-1.3).lineTo(60.1,-2.2).curveTo(59.6,-2.5,59.1,-2.7).curveTo(58.6,-2.9,58,-2.9).curveTo(57.3,-2.9,56.8,-2.7).closePath().moveTo(-24.4,6.1).curveTo(-25.5,5.7,-26.4,4.9).curveTo(-27.2,4.1,-27.7,3).lineTo(-28,1.8).lineTo(-28.1,0.5).lineTo(-28.1,0.4).curveTo(-28.2,-0.8,-27.7,-1.9).curveTo(-27.3,-3,-26.5,-3.9).curveTo(-25.7,-4.7,-24.7,-5.2).lineTo(-23.5,-5.6).lineTo(-22.3,-5.7).curveTo(-21.5,-5.7,-20.9,-5.5).curveTo(-20.3,-5.4,-19.7,-5.1).curveTo(-19,-4.8,-18,-3.8).curveTo(-17.3,-3,-17,-1.9).curveTo(-16.6,-1,-16.6,-0).curveTo(-16.6,0.7,-17,1.2).curveTo(-17.5,1.6,-18.2,1.6).lineTo(-24.8,1.6).curveTo(-24.7,2.2,-24.4,2.6).curveTo(-24.2,3,-23.8,3.3).curveTo(-23.5,3.6,-23,3.8).curveTo(-22.5,3.9,-21.9,3.9).curveTo(-21.3,3.9,-20.6,3.7).lineTo(-19.6,3.1).curveTo(-19.2,2.8,-18.8,2.8).curveTo(-18.2,2.8,-17.8,3.2).curveTo(-17.5,3.6,-17.5,4.2).curveTo(-17.4,4.8,-17.9,5.2).curveTo(-18.7,5.8,-19.7,6.2).curveTo(-20.7,6.6,-22,6.6).curveTo(-23.7,6.4,-24.4,6.1).closePath().moveTo(-19.8,-0.5).lineTo(-20.1,-1.5).lineTo(-20.6,-2.3).curveTo(-20.9,-2.6,-21.4,-2.8).curveTo(-21.8,-3,-22.3,-3).curveTo(-22.8,-3,-23.3,-2.8).curveTo(-23.6,-2.7,-24,-2.3).lineTo(-24.5,-1.5).lineTo(-24.9,-0.5).lineTo(-19.8,-0.5).lineTo(-19.8,-0.5).closePath().moveTo(-44.5,6.1).curveTo(-45.6,5.7,-46.5,4.9).curveTo(-47.2,4.1,-47.7,3).lineTo(-48.1,1.8).lineTo(-48.2,0.5).lineTo(-48.2,0.4).curveTo(-48.2,-0.8,-47.7,-1.9).curveTo(-47.3,-3,-46.6,-3.9).curveTo(-45.7,-4.7,-44.7,-5.2).lineTo(-43.6,-5.6).lineTo(-42.3,-5.7).curveTo(-41.6,-5.7,-41,-5.5).curveTo(-40.3,-5.4,-39.8,-5.1).curveTo(-39.1,-4.8,-38,-3.8).curveTo(-37.4,-3,-37,-1.9).curveTo(-36.7,-1,-36.7,-0).curveTo(-36.7,0.7,-37.1,1.2).curveTo(-37.5,1.6,-38.3,1.6).lineTo(-44.9,1.6).curveTo(-44.8,2.2,-44.5,2.6).curveTo(-44.2,3,-43.9,3.3).curveTo(-43.5,3.6,-43,3.8).curveTo(-42.5,3.9,-42,3.9).curveTo(-41.3,3.9,-40.7,3.7).lineTo(-39.6,3.1).curveTo(-39.2,2.8,-38.8,2.8).curveTo(-38.2,2.8,-37.8,3.2).curveTo(-37.5,3.6,-37.5,4.2).curveTo(-37.5,4.8,-37.9,5.2).curveTo(-38.8,5.8,-39.7,6.2).curveTo(-40.8,6.6,-42,6.6).curveTo(-43.8,6.4,-44.5,6.1).closePath().moveTo(-39.8,-0.5).lineTo(-40.2,-1.5).lineTo(-40.6,-2.3).curveTo(-41,-2.6,-41.4,-2.8).curveTo(-41.9,-3,-42.3,-3).curveTo(-42.8,-3,-43.3,-2.8).curveTo(-43.7,-2.7,-44,-2.3).lineTo(-44.6,-1.5).lineTo(-44.9,-0.5).lineTo(-39.8,-0.5).lineTo(-39.8,-0.5).closePath().moveTo(-57.9,6.4).lineTo(-59.4,6).lineTo(-60.7,5.3).lineTo(-61.9,4.3).lineTo(-62.9,3.1).lineTo(-63.6,1.8).curveTo(-63.9,1.1,-64,0.3).lineTo(-64.1,-1.3).lineTo(-64.1,-1.4).lineTo(-64,-3).curveTo(-63.9,-3.8,-63.6,-4.5).lineTo(-62.9,-5.8).lineTo(-61.9,-7).lineTo(-60.7,-8).curveTo(-60.1,-8.4,-59.3,-8.7).curveTo(-58.6,-9,-57.8,-9.2).lineTo(-56.1,-9.3).curveTo(-55,-9.3,-53.1,-8.9).curveTo(-52,-8.5,-51,-7.8).curveTo(-50.7,-7.6,-50.5,-7.2).curveTo(-50.3,-6.9,-50.3,-6.4).curveTo(-50.3,-5.7,-50.8,-5.3).curveTo(-51.2,-4.8,-52,-4.8).curveTo(-52.4,-4.8,-53,-5.1).curveTo(-53.7,-5.7,-54.5,-5.9).curveTo(-55.2,-6.2,-56.2,-6.2).curveTo(-57.1,-6.2,-57.9,-5.8).curveTo(-58.8,-5.5,-59.3,-4.8).curveTo(-60,-4.1,-60.2,-3.3).curveTo(-60.6,-2.4,-60.6,-1.4).lineTo(-60.6,-1.4).curveTo(-60.6,-0.4,-60.2,0.5).curveTo(-60,1.4,-59.3,2).curveTo(-58.8,2.7,-58,3.1).curveTo(-57.1,3.5,-56.2,3.5).curveTo(-55.1,3.5,-54.3,3.1).curveTo(-53.5,2.9,-52.8,2.3).curveTo(-52.3,1.9,-51.8,1.9).curveTo(-51.1,1.9,-50.6,2.3).curveTo(-50.2,2.8,-50.2,3.4).curveTo(-50.2,4.2,-50.8,4.6).curveTo(-52.2,5.7,-53.1,6.1).lineTo(-54.6,6.5).lineTo(-56.3,6.6).lineTo(-57.9,6.4).closePath().moveTo(25.5,6.4).curveTo(24.9,6.2,24.4,5.8).curveTo(24,5.4,23.7,4.7).curveTo(23.4,4,23.5,3).lineTo(23.5,-2.6).lineTo(23.4,-2.6).curveTo(22.7,-2.6,22.3,-3).curveTo(21.9,-3.4,21.9,-4).curveTo(21.9,-4.6,22.3,-5.1).curveTo(22.7,-5.5,23.4,-5.5).lineTo(23.5,-5.5).lineTo(23.5,-6.9).curveTo(23.5,-7.6,23.9,-8.1).curveTo(24.5,-8.6,25.1,-8.6).curveTo(25.8,-8.6,26.3,-8.1).curveTo(26.8,-7.6,26.8,-6.9).lineTo(26.8,-5.5).lineTo(28.2,-5.5).curveTo(28.9,-5.5,29.2,-5.1).curveTo(29.7,-4.6,29.7,-4).curveTo(29.7,-3.4,29.2,-3).curveTo(28.9,-2.6,28.2,-2.6).lineTo(26.8,-2.6).lineTo(26.8,2.4).curveTo(26.8,3,27.1,3.3).curveTo(27.3,3.6,27.9,3.6).lineTo(28.2,3.6).curveTo(28.9,3.6,29.2,4).curveTo(29.7,4.4,29.7,4.9).curveTo(29.6,5.4,29.4,5.7).curveTo(29.2,6.1,28.8,6.2).curveTo(28,6.5,26.9,6.5).curveTo(26.1,6.5,25.5,6.4).closePath().moveTo(11.7,6.3).curveTo(10.9,6.1,10.4,5.6).curveTo(9.8,5.1,9.5,4.5).curveTo(9.2,3.8,9.2,2.9).lineTo(9.2,2.9).curveTo(9.2,2,9.5,1.2).curveTo(9.9,0.5,10.5,0.1).curveTo(11.1,-0.4,12,-0.6).curveTo(12.9,-0.9,13.9,-0.9).curveTo(15.2,-0.9,16.8,-0.4).lineTo(16.8,-0.6).curveTo(16.8,-1.1,16.6,-1.5).curveTo(16.5,-1.9,16.2,-2.2).curveTo(15.9,-2.5,15.4,-2.6).lineTo(14.3,-2.7).curveTo(13,-2.7,12,-2.4).lineTo(11.5,-2.3).curveTo(10.9,-2.3,10.5,-2.7).curveTo(10.1,-3.1,10.1,-3.7).curveTo(10.1,-4.1,10.3,-4.5).curveTo(10.6,-4.8,11,-5).lineTo(12.7,-5.4).curveTo(13.6,-5.6,14.8,-5.6).curveTo(16.1,-5.6,17.2,-5.3).curveTo(18.1,-4.9,18.8,-4.3).curveTo(19.4,-3.6,19.8,-2.7).curveTo(20,-1.7,20,-0.5).lineTo(20,4.9).curveTo(20,5.5,19.6,6).curveTo(19.1,6.5,18.4,6.5).curveTo(17.7,6.5,17.2,6).curveTo(16.8,5.6,16.8,5.1).lineTo(16.8,5).curveTo(16.1,5.8,15.4,6.1).curveTo(14.4,6.5,13.3,6.5).curveTo(12.4,6.5,11.7,6.3).closePath().moveTo(13,1.6).curveTo(12.5,2,12.5,2.8).lineTo(12.5,2.8).curveTo(12.5,3.5,12.9,3.9).curveTo(13.5,4.2,14.3,4.2).curveTo(14.8,4.2,15.3,4.1).curveTo(15.8,3.9,16.2,3.7).lineTo(16.7,3).curveTo(16.9,2.6,16.9,2.2).lineTo(16.9,1.6).curveTo(15.9,1.1,14.7,1.1).curveTo(13.7,1.1,13,1.6).closePath().moveTo(-8.3,6.4).lineTo(-9.3,6).curveTo(-10.1,5.6,-10.7,4.8).lineTo(-10.7,4.8).curveTo(-10.7,5.5,-11.3,6).curveTo(-11.7,6.5,-12.5,6.5).curveTo(-13.1,6.5,-13.6,6).curveTo(-14.1,5.5,-14.1,4.8).lineTo(-14.1,-8.2).curveTo(-14.1,-8.9,-13.6,-9.4).curveTo(-13.1,-9.9,-12.5,-9.9).curveTo(-11.7,-9.9,-11.3,-9.4).curveTo(-10.7,-8.9,-10.7,-8.2).lineTo(-10.7,-3.8).curveTo(-10.1,-4.7,-9.2,-5.1).curveTo(-8.8,-5.4,-8.2,-5.5).curveTo(-7.7,-5.7,-7.1,-5.7).curveTo(-6.1,-5.7,-5.1,-5.3).curveTo(-4.1,-4.9,-3.4,-4.1).curveTo(-2.6,-3.3,-2.2,-2.2).curveTo(-1.7,-0.6,-1.7,0.4).lineTo(-1.8,1.8).lineTo(-2.2,3.1).curveTo(-2.6,4.2,-3.4,5).curveTo(-4.1,5.7,-5.1,6.1).curveTo(-6.1,6.5,-7.1,6.5).lineTo(-8.3,6.4).closePath().moveTo(-9,-2.6).curveTo(-9.6,-2.4,-9.9,-2).curveTo(-10.4,-1.5,-10.6,-0.9).curveTo(-10.8,-0.3,-10.8,0.4).curveTo(-10.8,1.2,-10.6,1.8).curveTo(-10.4,2.4,-9.9,2.8).curveTo(-9.6,3.2,-9,3.5).curveTo(-8.5,3.7,-8,3.7).curveTo(-7.4,3.7,-6.9,3.5).curveTo(-6.3,3.3,-5.9,2.8).curveTo(-5.5,2.4,-5.3,1.8).curveTo(-5,1.2,-5.1,0.4).curveTo(-5,-0.3,-5.3,-0.9).curveTo(-5.5,-1.5,-5.9,-2).curveTo(-6.3,-2.4,-6.9,-2.6).curveTo(-7.4,-2.8,-8,-2.8).curveTo(-8.5,-2.8,-9,-2.6).closePath().moveTo(46.6,6).curveTo(46,5.5,46,4.8).lineTo(46,-0.2).curveTo(46,-0.8,45.9,-1.3).curveTo(45.8,-1.7,45.5,-2).curveTo(45.2,-2.3,44.9,-2.5).curveTo(44.5,-2.7,44.1,-2.7).curveTo(43.5,-2.7,43.2,-2.5).curveTo(42.8,-2.3,42.5,-2).curveTo(42.2,-1.7,42.1,-1.3).curveTo(41.9,-0.8,41.9,-0.2).lineTo(41.9,4.8).curveTo(41.9,5.5,41.5,6).curveTo(41,6.5,40.3,6.5).curveTo(39.5,6.5,39.1,6).curveTo(38.6,5.5,38.6,4.8).lineTo(38.6,-3.9).curveTo(38.6,-4.6,39.1,-5.1).curveTo(39.5,-5.6,40.3,-5.6).curveTo(41,-5.6,41.5,-5.1).curveTo(41.9,-4.6,41.9,-3.9).lineTo(41.9,-3.8).curveTo(42.6,-4.7,43.3,-5.1).lineTo(44.2,-5.5).curveTo(44.8,-5.7,45.4,-5.7).curveTo(46.3,-5.7,47.1,-5.4).curveTo(47.8,-5.1,48.4,-4.5).curveTo(48.8,-3.9,49.1,-3.1).curveTo(49.4,-2.3,49.4,-1.3).lineTo(49.4,4.8).curveTo(49.4,5.5,48.9,6).curveTo(48.4,6.5,47.7,6.5).curveTo(47.1,6.5,46.6,6).closePath().moveTo(32.6,6).curveTo(32.2,5.5,32.2,4.8).lineTo(32.2,-3.9).curveTo(32.2,-4.6,32.6,-5.1).curveTo(33.1,-5.6,33.8,-5.6).curveTo(34.5,-5.6,35,-5.1).curveTo(35.5,-4.6,35.5,-3.9).lineTo(35.5,4.8).curveTo(35.5,5.5,35,6).curveTo(34.5,6.5,33.8,6.5).curveTo(33.1,6.5,32.6,6).closePath().moveTo(1.2,6).curveTo(0.8,5.5,0.8,4.8).lineTo(0.8,-3.9).curveTo(0.8,-4.6,1.2,-5.1).curveTo(1.7,-5.6,2.4,-5.6).curveTo(3.1,-5.6,3.6,-5.1).curveTo(4.1,-4.6,4.1,-3.9).lineTo(4.1,-3.4).curveTo(4.5,-4.4,5,-4.9).curveTo(5.6,-5.6,6.4,-5.6).curveTo(7.1,-5.6,7.5,-5.1).curveTo(8,-4.7,8,-4).curveTo(8,-3.3,7.6,-2.9).curveTo(7.3,-2.5,6.7,-2.4).curveTo(6.1,-2.3,5.6,-1.9).curveTo(5.1,-1.6,4.7,-1.1).curveTo(4.5,-0.6,4.3,0.1).curveTo(4.1,0.8,4.1,1.7).lineTo(4.1,4.8).curveTo(4.1,5.5,3.6,6).curveTo(3.1,6.5,2.4,6.5).curveTo(1.7,6.5,1.2,6).closePath().moveTo(-33.5,6).curveTo(-34,5.5,-34,4.8).lineTo(-34,-8.2).curveTo(-34,-8.9,-33.5,-9.4).curveTo(-33,-9.9,-32.3,-9.9).curveTo(-31.7,-9.9,-31.2,-9.4).curveTo(-30.6,-8.9,-30.6,-8.2).lineTo(-30.6,4.8).curveTo(-30.6,5.5,-31.2,6).curveTo(-31.7,6.5,-32.3,6.5).curveTo(-33,6.5,-33.5,6).closePath().moveTo(32.5,-7.1).curveTo(31.9,-7.5,31.9,-8.2).lineTo(31.9,-8.3).curveTo(31.9,-9,32.5,-9.4).curveTo(33,-9.8,33.8,-9.8).curveTo(34.7,-9.8,35.2,-9.4).curveTo(35.7,-9,35.7,-8.3).lineTo(35.7,-8.2).curveTo(35.7,-7.5,35.2,-7.1).curveTo(34.7,-6.7,33.8,-6.7).curveTo(33,-6.7,32.5,-7.1).closePath();
    this.shape.setTransform(-150,0.3);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt1, new cjs.Rectangle(-239.9,-13,479.9,26), null);


(lib.Tween1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2).moveTo(22.8,-12.7).curveTo(19,-12.9,15.1,-12).curveTo(11.3,-11.2,7.7,-9.5).curveTo(6.7,-9,6.3,-8.8).curveTo(5.5,-8.5,4.9,-8.5).curveTo(4.2,-8.4,2.6,-8.8).lineTo(-14,-12.9).curveTo(-16.5,-13.5,-18,-13.6).curveTo(-20.2,-13.7,-21.7,-12.8).curveTo(-22.6,-12.3,-23,-11.4).curveTo(-23.5,-10.4,-23,-9.6).curveTo(-22.8,-9.3,-22,-8.7).lineTo(-10.3,-1.3).lineTo(-17.4,2.7).curveTo(-18.1,3.1,-18.6,3.1).curveTo(-19,3.1,-19.7,2.8).curveTo(-22.1,1.3,-23.3,0.6).curveTo(-25.5,-0.5,-27.2,-0.4).curveTo(-28.2,-0.3,-28.9,0.2).curveTo(-29.8,0.8,-29.7,1.6).curveTo(-29.7,2.1,-29.2,2.8).lineTo(-25.9,8.5).curveTo(-28.3,10.5,-27.8,12).curveTo(-27.5,12.8,-26.5,13.2).curveTo(-25.7,13.6,-24.6,13.6).curveTo(-22.5,13.6,-18.8,12.8).curveTo(-9.5,10.8,1.7,7.3).curveTo(11.9,4,18.8,0.8).curveTo(24.3,-1.8,27.3,-4.5).curveTo(28.9,-6,29.4,-7.3).curveTo(29.8,-8.2,29.7,-9).curveTo(29.6,-10,29.1,-10.6).curveTo(28.6,-11.3,27.7,-11.6).curveTo(27.1,-11.9,26,-12.2).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.3,-15.3,67.9,33.3);


(lib.star = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-5.9).lineTo(0,-5.9).lineTo(0,-5.8).lineTo(0,-5.9).curveTo(1.2,-0.9,5.9,0).curveTo(1.2,1.7,0.1,5.9).curveTo(-1.3,1.4,-5.9,0).curveTo(-0.8,-2,0,-5.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.star, new cjs.Rectangle(-6.9,-6.9,13.8,13.8), null);


(lib.square = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-6,-6).lineTo(6,-6).lineTo(6,6).lineTo(-6,6).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.square, new cjs.Rectangle(-7,-7,14,14), null);


(lib.o = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-4.8).curveTo(1.9,-4.8,3.4,-3.4).curveTo(4.8,-1.9,4.8,0).curveTo(4.8,1.9,3.4,3.4).curveTo(1.9,4.8,0,4.8).curveTo(-1.9,4.8,-3.4,3.4).curveTo(-4.8,1.9,-4.8,0).curveTo(-4.8,-1.9,-3.4,-3.4).curveTo(-1.9,-4.8,0,-4.8).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.o, new cjs.Rectangle(-5.7,-5.7,11.5,11.5), null);


(lib.mountains = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7.8,13.6).lineTo(-1.9,9.6).lineTo(3.6,13.6).moveTo(-20.1,2.2).lineTo(-7.3,-6.8).lineTo(11.4,6.5).moveTo(-20.1,2.2).lineTo(-14.2,6).lineTo(-6.8,0.9).lineTo(-3.8,2.5).lineTo(1.2,-0.4).moveTo(3.9,13.6).lineTo(19.3,2.3).lineTo(35.1,13.6).moveTo(113.8,13).lineTo(93.7,-1.7).lineTo(72.9,13).moveTo(145.1,-1.9).lineTo(137.4,3.1).lineTo(127.7,-3.6).lineTo(123.8,-1.5).lineTo(117.3,-5.2).moveTo(129,13).lineTo(121.4,7.9).lineTo(114.2,13).moveTo(145.1,-1.9).lineTo(128.3,-13.6).lineTo(104,3.8).moveTo(166.5,13).lineTo(145.1,-1.9).moveTo(196.1,12.9).lineTo(172.7,-4.9).lineTo(157.8,6.5).moveTo(-196.1,12.9).lineTo(-172.7,-4.9).lineTo(-157.8,6.5).moveTo(-166.5,13).lineTo(-145.1,-1.9).lineTo(-137.4,3.1).lineTo(-127.7,-3.6).lineTo(-123.8,-1.5).lineTo(-117.3,-5.2).moveTo(-129,13).lineTo(-121.4,7.9).lineTo(-114.2,13).moveTo(-145.1,-1.9).lineTo(-128.3,-13.6).lineTo(-104,3.8).moveTo(-113.8,13).lineTo(-93.7,-1.7).lineTo(-72.9,13).moveTo(-59,13.5).lineTo(-41.1,-0.1).lineTo(-29.9,8.6).moveTo(-36.5,13.6).lineTo(-20.1,2.2);
    this.shape.setTransform(21,-16.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mountains, new cjs.Rectangle(-176.1,-31.1,394.2,29.1), null);


(lib.graSteam = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(4.9,43.6).curveTo(4.9,36.4,-0.6,31.5).curveTo(-4.9,27.6,-4.9,21.8).curveTo(-4.9,16.1,-0.6,12.2).curveTo(4.9,7.2,4.9,0).curveTo(4.9,-7.2,-0.6,-12.1).curveTo(-4.9,-16,-4.9,-21.8).curveTo(-4.9,-27.5,-0.6,-31.4).curveTo(4.9,-36.4,4.9,-43.6);
    this.shape.setTransform(8.5,43.7);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(5,43.6).curveTo(5,36.4,-0.6,31.5).curveTo(-4.9,27.6,-4.9,21.8).curveTo(-4.9,16.1,-0.6,12.2).curveTo(5,7.2,5,0).curveTo(5,-7.2,-0.6,-12.1).curveTo(-4.9,-16,-4.9,-21.8).curveTo(-4.9,-27.5,-0.6,-31.4).curveTo(5,-36.4,5,-43.6);
    this.shape_1.setTransform(-8.5,43.7);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(4.9,43.6).curveTo(4.9,36.4,-0.6,31.5).curveTo(-4.9,27.6,-4.9,21.8).curveTo(-4.9,16.1,-0.6,12.2).curveTo(4.9,7.2,4.9,0).curveTo(4.9,-7.2,-0.6,-12.1).curveTo(-4.9,-16,-4.9,-21.9).curveTo(-4.9,-27.6,-0.6,-31.4).curveTo(4.9,-36.4,4.9,-43.6);
    this.shape_2.setTransform(8.5,-43.6);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(5,43.6).curveTo(5,36.4,-0.6,31.5).curveTo(-4.9,27.6,-4.9,21.8).curveTo(-4.9,16.1,-0.6,12.2).curveTo(5,7.2,5,0).curveTo(5,-7.2,-0.6,-12.1).curveTo(-4.9,-16,-4.9,-21.9).curveTo(-4.9,-27.6,-0.6,-31.4).curveTo(5,-36.4,5,-43.6);
    this.shape_3.setTransform(-8.5,-43.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graSteam, new cjs.Rectangle(-14.5,-88.3,29,176.6), null);


(lib.graRice = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.1,0.7).curveTo(2.7,-2.1,7.1,0.5).curveTo(1.2,3.7,-3.1,0.7).lineTo(-7.1,2).lineTo(-6.4,-2.2).closePath();
    this.shape.setTransform(-28.9,-37.6);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(3,-0.6).lineTo(7.1,-1).lineTo(5.6,3).lineTo(3,-0.6).curveTo(-3.3,1,-7,-2.4).curveTo(-0.8,-4.4,3,-0.6).closePath();
    this.shape_1.setTransform(-21.8,-26.5);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.6,0).lineTo(6.6,0.7).lineTo(4.2,4.2).lineTo(2.6,0).curveTo(-3.9,-0,-6.6,-4.2).curveTo(-0.1,-4.6,2.6,0).closePath();
    this.shape_2.setTransform(-19.5,-17.6);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.5,-0).curveTo(2.5,1.4,1.7,2.3).curveTo(1.1,3.4,0,3.4).curveTo(-1,3.4,-1.8,2.3).curveTo(-2.5,1.4,-2.5,-0).curveTo(-2.5,-1.4,-1.8,-2.4).curveTo(-1,-3.3,0,-3.4).curveTo(1.1,-3.4,1.8,-2.4).curveTo(2.5,-1.4,2.5,-0).closePath();
    this.shape_3.setTransform(-58.4,-39.1);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(1.7,1.8).curveTo(0.8,2.8,-0.4,2.9).curveTo(-1.7,3.2,-2.4,2.4).curveTo(-3.2,1.7,-3,0.4).curveTo(-2.8,-0.8,-1.7,-1.8).curveTo(-0.8,-2.8,0.4,-2.9).curveTo(1.6,-3.1,2.4,-2.3).curveTo(3.1,-1.6,2.9,-0.4).curveTo(2.8,0.8,1.7,1.8).closePath();
    this.shape_4.setTransform(-38.7,-26.5);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.4,-0.7).curveTo(2.8,0.7,2.3,1.8).curveTo(1.9,3,0.9,3.2).curveTo(-0.1,3.5,-1.1,2.7).curveTo(-2.1,1.9,-2.4,0.6).curveTo(-2.8,-0.8,-2.3,-1.9).curveTo(-1.9,-3,-0.8,-3.3).curveTo(0.2,-3.5,1.1,-2.8).curveTo(2.1,-2,2.4,-0.7).closePath();
    this.shape_5.setTransform(-31.8,-17.6);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.2,-1.3).curveTo(2.9,-0.1,2.7,1.2).curveTo(2.6,2.4,1.7,2.9).curveTo(0.8,3.5,-0.4,2.9).curveTo(-1.4,2.4,-2.1,1.2).curveTo(-2.8,0,-2.7,-1.2).curveTo(-2.6,-2.4,-1.7,-2.9).curveTo(-0.8,-3.4,0.3,-3).curveTo(1.5,-2.5,2.2,-1.3).closePath();
    this.shape_6.setTransform(-49.2,-34.4);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.2,-1.3).curveTo(2.9,-0.1,2.7,1.2).curveTo(2.6,2.4,1.7,2.9).curveTo(0.8,3.4,-0.3,2.9).curveTo(-1.4,2.4,-2.1,1.2).curveTo(-2.8,0,-2.8,-1.2).curveTo(-2.6,-2.4,-1.7,-2.9).curveTo(-0.8,-3.4,0.3,-3).curveTo(1.4,-2.5,2.2,-1.3).closePath();
    this.shape_7.setTransform(-40.8,-15.9);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(1.7,1.8).curveTo(0.8,2.8,-0.4,2.9).curveTo(-1.7,3.2,-2.4,2.4).curveTo(-3.2,1.7,-3,0.4).curveTo(-2.8,-0.8,-1.7,-1.8).curveTo(-0.8,-2.8,0.4,-2.9).curveTo(1.6,-3.1,2.4,-2.3).curveTo(3.1,-1.6,2.9,-0.4).curveTo(2.8,0.8,1.7,1.8).closePath();
    this.shape_8.setTransform(-48,-25);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.4,-0.7).curveTo(2.8,0.7,2.3,1.8).curveTo(1.9,3,0.9,3.2).curveTo(-0.1,3.5,-1.1,2.7).curveTo(-2.1,1.9,-2.4,0.6).curveTo(-2.8,-0.8,-2.3,-1.9).curveTo(-1.9,-3,-0.8,-3.3).curveTo(0.2,-3.5,1.1,-2.8).curveTo(2.1,-2,2.4,-0.7).closePath();
    this.shape_9.setTransform(-49.5,-15.4);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.2,-1.3).curveTo(2.9,-0.1,2.7,1.2).curveTo(2.6,2.4,1.7,2.9).curveTo(0.8,3.5,-0.4,2.9).curveTo(-1.4,2.4,-2.1,1.2).curveTo(-2.8,0,-2.7,-1.2).curveTo(-2.6,-2.4,-1.7,-2.9).curveTo(-0.8,-3.4,0.3,-3).curveTo(1.5,-2.5,2.2,-1.3).closePath();
    this.shape_10.setTransform(-57.9,-28.2);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2.4,-0.7).curveTo(2.8,0.7,2.3,1.8).curveTo(1.9,3,0.9,3.3).curveTo(-0.1,3.5,-1.1,2.7).curveTo(-2,2,-2.4,0.6).curveTo(-2.8,-0.7,-2.4,-1.9).curveTo(-1.9,-3,-0.9,-3.2).curveTo(0.1,-3.5,1.1,-2.8).curveTo(2,-2,2.4,-0.7).closePath();
    this.shape_11.setTransform(-58.1,-17.2);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-8.7,11.5).curveTo(-4.6,0.7,-7.6,-12.3).curveTo(-18,3.6,-8.7,11.5).curveTo(3.9,15.3,13.1,-0).curveTo(1.8,1.5,-8.7,11.5).closePath();
    this.shape_12.setTransform(7.3,-57.1);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-10.7,-15.3).curveTo(-5.7,-16.8,6.1,-17.3).curveTo(17.8,-17.8,22.4,-7.6).curveTo(25.5,5.3,2.8,15.3).curveTo(1.4,15.5,-3.3,16.9).curveTo(-8,18.3,-14.2,15.2).curveTo(-20.4,12,-22.2,5.4).curveTo(-24,-1.3,-19.9,-7.5).curveTo(-15.8,-13.7,-10.7,-15.3).closePath().moveTo(-8.9,-10.5).curveTo(-4.2,-11.8,0.1,-9.3).curveTo(4.3,-6.9,5.6,-2.1).curveTo(6.9,2.7,4.5,6.9).curveTo(2,11.2,-2.8,12.5).curveTo(-7.6,13.8,-11.8,11.3).curveTo(-16,8.8,-17.3,4.1).curveTo(-18.6,-0.7,-16.2,-4.9).curveTo(-13.7,-9.2,-8.9,-10.5).closePath();
    this.shape_13.setTransform(-40.4,-74.1);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(24.6,-47.2).lineTo(59.5,-47.2).curveTo(69.5,-47.2,69.5,-37.2).lineTo(69.5,-8.8).moveTo(61.4,-21.8).lineTo(63.1,-22.5).moveTo(42.1,-20.8).lineTo(45.1,-22.8).moveTo(52.6,-16.5).lineTo(54.6,-18).moveTo(44.6,-12.3).lineTo(46.6,-13.5).moveTo(51.1,-25.3).lineTo(52.9,-26.5).moveTo(57.4,-31.3).lineTo(59.1,-32.5).moveTo(48.4,-34.3).lineTo(50.9,-36.3).moveTo(24.6,-47.2).curveTo(35.6,-43.2,30.1,-34.8).curveTo(35.1,-25.8,30.1,-22.8).curveTo(35,-16.2,29.6,-10.3).curveTo(31.4,-5.7,31,-2.8).curveTo(30.6,-0.1,28.4,1.2).curveTo(32.2,5.1,29.4,9.2).curveTo(30.4,11.5,30.8,13.2).curveTo(31.1,14.8,30.8,16).curveTo(30.4,17.4,29,18.2).curveTo(30.2,21,30.3,23.1).curveTo(30.4,25.8,28.5,27.4).curveTo(30.6,30.7,30.1,33.2).curveTo(29.7,36.1,26.2,37.9).curveTo(29.2,39.8,27.1,42.8).curveTo(24.8,45.9,24.6,46).curveTo(23.8,46.6,22.6,47.2).lineTo(-60.1,47.2).curveTo(-69.5,47,-69.5,37.2).lineTo(-69.5,-37.2).curveTo(-69.5,-47,-60.1,-47.2).closePath().moveTo(36,0.2).lineTo(31,-2.8).moveTo(69.5,19.7).lineTo(69.5,37.2).curveTo(69.5,47.2,59.5,47.2).lineTo(46.6,47.2).lineTo(22.6,47.2).moveTo(69.5,19.7).lineTo(61.2,29.7).lineTo(54,38.5).lineTo(46.6,47.2).moveTo(69.5,19.7).lineTo(36,0.2).curveTo(55.8,-1.8,69.5,-8.8).closePath().moveTo(65.5,-52).curveTo(76.5,-52,76.5,-41).lineTo(76.5,41).curveTo(76.5,52,65.5,52).lineTo(-66.1,52).curveTo(-76.5,51.7,-76.5,41).lineTo(-76.5,-41).curveTo(-76.5,-51.6,-66.1,-52).closePath().moveTo(28.5,27.4).lineTo(54,38.5).moveTo(61.2,29.7).lineTo(30.8,13.2).moveTo(39.4,-31).lineTo(41.9,-32.5);
    this.shape_14.setTransform(0,-51.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graRice, new cjs.Rectangle(-77.4,-104.9,154.9,105.9), null);


(lib.graHeart = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.5,28.5).curveTo(-61.9,2.6,-60.4,-17.9).curveTo(-59,-38.5,-48.4,-46.3).curveTo(-37.9,-54.2,-26,-52.9).curveTo(-14.1,-51.5,-5.3,-42.8).curveTo(-2,-39.5,0,-35.7).curveTo(2.1,-39.5,5.3,-42.8).curveTo(14.1,-51.5,26,-52.8).curveTo(37.9,-54.2,48.4,-46.3).curveTo(59,-38.5,60.4,-17.9).curveTo(61.9,2.6,33.5,28.6).curveTo(9.5,50.5,1.2,52.8).curveTo(0.6,53,0,53.1).curveTo(-0.5,53,-1.2,52.8).curveTo(-9.5,50.5,-33.5,28.5).closePath();
    this.shape.setTransform(-0.3,-0.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.graHeart, new cjs.Rectangle(-61.7,-54.8,123,108.1), null);


(lib.graGPS = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(18.7,-7.2).curveTo(18.7,-4.4,18,-1.9).curveTo(16.4,3.2,14.3,7.6).curveTo(12,12.5,9.2,16.6).curveTo(5.6,21.9,1.1,25.9).curveTo(-2.2,23.2,-5,20.4).curveTo(-15.2,10.3,-17.9,-1.8).curveTo(-18.7,-4.4,-18.7,-7.2).curveTo(-18.7,-14.9,-13.2,-20.4).curveTo(-7.8,-25.9,0,-25.9).curveTo(7.8,-25.9,13.2,-20.4).curveTo(18.7,-14.9,18.7,-7.2).closePath().moveTo(9.4,-7).curveTo(9.4,-3.1,6.7,-0.4).curveTo(3.9,2.4,0,2.4).curveTo(-3.9,2.4,-6.7,-0.4).curveTo(-9.4,-3.1,-9.4,-7).curveTo(-9.4,-10.9,-6.7,-13.7).curveTo(-3.9,-16.4,0,-16.4).curveTo(3.9,-16.4,6.7,-13.7).curveTo(9.4,-10.9,9.4,-7).closePath();
    this.shape.setTransform(0,-25.9);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#EC2C2C").beginStroke().moveTo(-5,20.4).curveTo(-15.2,10.3,-17.9,-1.8).curveTo(-18.7,-4.4,-18.7,-7.2).curveTo(-18.7,-14.9,-13.2,-20.4).curveTo(-7.8,-25.9,0,-25.9).curveTo(7.8,-25.9,13.2,-20.4).curveTo(18.7,-14.9,18.7,-7.2).curveTo(18.7,-4.4,18,-1.9).curveTo(16.4,3.2,14.3,7.6).curveTo(12,12.5,9.2,16.6).curveTo(5.6,21.9,1.2,25.9).curveTo(-2.3,23.2,-5,20.4).closePath().moveTo(-9.4,-7).curveTo(-9.4,-3.1,-6.7,-0.4).curveTo(-3.9,2.4,0,2.4).curveTo(3.9,2.4,6.7,-0.4).curveTo(9.4,-3.1,9.4,-7).curveTo(9.4,-10.9,6.7,-13.7).curveTo(3.9,-16.4,0,-16.4).curveTo(-3.9,-16.4,-6.7,-13.7).curveTo(-9.4,-10.9,-9.4,-7).lineTo(-9.4,-7).closePath().moveTo(-6.7,-0.4).curveTo(-9.4,-3.1,-9.4,-7).curveTo(-9.4,-10.9,-6.7,-13.7).curveTo(-3.9,-16.4,0,-16.4).curveTo(3.9,-16.4,6.7,-13.7).curveTo(9.4,-10.9,9.4,-7).curveTo(9.4,-3.1,6.7,-0.4).curveTo(3.9,2.4,0,2.4).curveTo(-3.9,2.4,-6.7,-0.4).closePath().moveTo(9.4,-7).lineTo(9.4,-7).closePath();
    this.shape_1.setTransform(0,-25.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graGPS, new cjs.Rectangle(-19.7,-52.7,39.4,53.8), null);


(lib.graCultery = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(9.8,-23.2).curveTo(9.8,-20.3,8.6,-18).curveTo(8.1,-17,7.3,-16.2).curveTo(5.2,-13.6,2.3,-13.3).lineTo(2.3,41.5).curveTo(2.3,42.5,1.6,43.2).curveTo(1,44,0.2,44).curveTo(-0.7,44,-1.3,43.2).curveTo(-1.9,42.5,-1.9,41.5).lineTo(-1.9,-13.2).curveTo(-5.1,-13.5,-7.4,-16.2).curveTo(-9.9,-19.1,-9.9,-23.2).lineTo(-9.9,-44).lineTo(-6.4,-44).lineTo(-6.4,-23.2).lineTo(-1.7,-23.2).lineTo(-1.7,-44).lineTo(1.6,-44).lineTo(1.6,-23.2).lineTo(6.1,-23.2).lineTo(6.1,-44).lineTo(9.8,-44).closePath();
    this.shape.setTransform(-14.2,1.4);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(11.4,-28.6).curveTo(11.4,-21.6,8,-16.8).curveTo(5.5,-12.9,2.1,-12.1).lineTo(2.1,42.9).curveTo(2.1,43.9,1.5,44.6).curveTo(0.9,45.4,0,45.4).curveTo(-0.8,45.4,-1.5,44.6).curveTo(-2.1,43.9,-2.1,42.9).lineTo(-2.1,-12.1).curveTo(-5.5,-13,-8.1,-16.8).curveTo(-11.4,-21.6,-11.4,-28.6).curveTo(-11.4,-35.5,-8.1,-40.5).curveTo(-4.7,-45.4,0,-45.4).curveTo(4.7,-45.4,8,-40.5).curveTo(11.4,-35.5,11.4,-28.6).closePath();
    this.shape_1.setTransform(12.7,0);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.graCultery, new cjs.Rectangle(-25,-46.3,50.2,92.7), null);


(lib.bagHandle = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(26.4,-22.9).lineTo(17.6,-22.9).lineTo(-17.7,-22.9).lineTo(-17.7,37.6).moveTo(-26.5,-22.9).lineTo(-33.8,-22.9).lineTo(-33.8,-37.6).lineTo(33.8,-37.6).lineTo(33.8,-22.9).lineTo(26.4,-22.9).lineTo(26.4,37.6).moveTo(-17.7,-22.9).lineTo(-26.5,-22.9).moveTo(-26.5,37.6).lineTo(-26.5,-22.9).moveTo(17.6,37.6).lineTo(17.6,-22.9);
    this.shape.setTransform(0,-37.6);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bagHandle, new cjs.Rectangle(-34.8,-76.1,69.6,77.2), null);


(lib.bag2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.7,-50.4).lineTo(-13.7,-62.6).lineTo(13.6,-62.6).lineTo(13.6,-50.4).lineTo(17.5,-50.4).moveTo(-17.5,-50.4).lineTo(-17.5,-66.5).lineTo(17.5,-66.5).lineTo(17.5,-50.4).lineTo(35.7,-50.4).lineTo(50.7,-50.4).lineTo(50.7,-28.8).lineTo(57.1,-28.8).lineTo(57.1,-14.2).lineTo(50.7,-14.2).lineTo(35.7,-14.2).lineTo(29.8,-14.2).lineTo(29.8,-28.8).lineTo(35.7,-28.8).lineTo(50.7,-28.8).moveTo(35.7,-50.4).lineTo(35.7,-28.8).moveTo(50.7,-50.4).lineTo(84.7,-50.4).lineTo(84.7,66.5).lineTo(50.7,66.5).lineTo(35.7,66.5).lineTo(-35.7,66.5).lineTo(-50.7,66.5).lineTo(-84.7,66.5).lineTo(-84.7,-50.4).lineTo(-50.7,-50.4).lineTo(-50.7,-28.8).moveTo(50.7,-14.2).lineTo(50.7,66.5).moveTo(35.7,-14.2).lineTo(35.7,66.5).moveTo(-35.7,-50.4).lineTo(-17.5,-50.4).lineTo(-13.7,-50.4).lineTo(13.6,-50.4).moveTo(-50.7,-50.4).lineTo(-35.7,-50.4).lineTo(-35.7,-28.8).lineTo(-30.4,-28.8).lineTo(-30.4,-14.2).lineTo(-35.7,-14.2).lineTo(-50.7,-14.2).lineTo(-57.7,-14.2).lineTo(-57.7,-28.8).lineTo(-50.7,-28.8).lineTo(-35.7,-28.8).moveTo(-35.7,-14.2).lineTo(-35.7,66.5).moveTo(-50.7,-14.2).lineTo(-50.7,66.5);
    this.shape.setTransform(0,-66.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bag2, new cjs.Rectangle(-85.6,-134,171.4,135.1), null);


(lib.aniSmack = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 2
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.6,1.6).lineTo(1.6,-1.6);
    this.shape.setTransform(-4.3,-19.3);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.5,2.7).lineTo(2.5,-2.6);
    this.shape_1.setTransform(-2.5,-21.4);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.5,3.7).lineTo(3.5,-3.7);
    this.shape_2.setTransform(-0.6,-23.5);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4.4,4.8).lineTo(4.4,-4.8);
    this.shape_3.setTransform(1.3,-25.7);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,5.8).lineTo(5.3,-5.8);
    this.shape_4.setTransform(3.2,-27.8);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.4,3.7).lineTo(3.4,-3.7);
    this.shape_5.setTransform(7,-31.8);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.5,1.6).lineTo(1.5,-1.6);
    this.shape_6.setTransform(10.8,-35.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[]},1).wait(1));

    // Layer 3
    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4.1,0).lineTo(4.1,0);
    this.shape_7.setTransform(-1.8,-13.2);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.1,0).lineTo(5.1,0);
    this.shape_8.setTransform(1.1,-13.2);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-6.2,0).lineTo(6.2,0);
    this.shape_9.setTransform(4,-13.2);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7.2,0).lineTo(7.2,0);
    this.shape_10.setTransform(6.9,-13.2);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-8.2,0).lineTo(8.2,0);
    this.shape_11.setTransform(9.8,-13.2);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,0).lineTo(5,0);
    this.shape_12.setTransform(15.5,-13.2);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.7,0).lineTo(1.7,0);
    this.shape_13.setTransform(21.3,-13.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7}]}).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[]},1).wait(1));

    // Layer 4
    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.6,-1.6).lineTo(1.6,1.6);
    this.shape_14.setTransform(-4.3,-6.9);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.5,-2.7).lineTo(2.5,2.6);
    this.shape_15.setTransform(-2.5,-4.7);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.5,-3.7).lineTo(3.5,3.7);
    this.shape_16.setTransform(-0.6,-2.6);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4.4,-4.8).lineTo(4.4,4.8);
    this.shape_17.setTransform(1.3,-0.5);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.3,-5.8).lineTo(5.3,5.8);
    this.shape_18.setTransform(3.2,1.6);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.4,-3.7).lineTo(3.4,3.7);
    this.shape_19.setTransform(7,5.7);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.5,-1.6).lineTo(1.5,1.6);
    this.shape_20.setTransform(10.8,9.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14}]}).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.9,-21.9,10.2,17.6);


(lib.aniPhone = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 7
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape.setTransform(46.2,74.4);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_1.setTransform(49,77);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_2.setTransform(57.4,84.6);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_3.setTransform(71.3,97.3);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.9,-3.8).curveTo(1.5,-4.2,2.1,-4.3).curveTo(3.4,-4.3,4,-3.8).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.2,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.2,0.1,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_4.setTransform(72,98);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,4,-3.9).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_5.setTransform(72.7,98.7);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.8).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.2,0.2,4.4).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_6.setTransform(73.4,99.4);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_7.setTransform(74.1,100);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.6,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.2,4.4).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_8.setTransform(74.8,100.7);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.5,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_9.setTransform(75.5,101.4);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_10.setTransform(76.2,102);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.3,-4.3,2,-4.3).curveTo(3.3,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_11.setTransform(76.9,102.7);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_12.setTransform(77.6,103.4);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.8,2.4,2.6,2.5).curveTo(0.9,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_13.setTransform(78.3,104);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-4).curveTo(4.1,-3.8,4.2,-3.7).curveTo(4.9,-3,5,-1.7).curveTo(5,-1.5,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.4,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_14.setTransform(79,104.7);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.7).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4,-3.8,4.2,-3.7).curveTo(4.9,-3,5,-1.7).curveTo(5,-1.5,5,-1.2).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.4,2.6,2.5).curveTo(0.9,4.1,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_15.setTransform(79.7,105.4);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.9,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.9,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_16.setTransform(80.4,106.1);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_17.setTransform(81.2,106.7);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.8).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.2,2.9,2.1).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_18.setTransform(81.9,107.4);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,4.9,-1.3).curveTo(5,0.2,2.9,2.1).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_19.setTransform(82.6,108.1);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,4.9,-1.3).curveTo(5,0.2,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_20.setTransform(83.3,108.7);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.3).curveTo(0.3,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_21.setTransform(84,109.4);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.3,-3.5,0.5,-3.7).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_22.setTransform(84.7,110.1);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.2,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_23.setTransform(85.4,110.7);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.7).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.2,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_24.setTransform(86.1,111.4);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5.1,0.2,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_25.setTransform(86.8,112.1);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.1,-4.2,-0.4,-3.5).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5.1,0.2,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.4,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_26.setTransform(87.5,112.7);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.3,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-4,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_27.setTransform(88.2,113.4);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0,-3,0.1,-3.1).curveTo(0.3,-3.3,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.8,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_28.setTransform(88.9,114.1);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.3,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.4,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(2.9,-4.5,3.7,-4.1).curveTo(3.8,-4,4,-3.9).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2).curveTo(2.9,2.2,2.8,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_29.setTransform(89.6,114.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:46.2,y:74.4}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape,p:{x:90.3,y:115.4}}]},1).wait(16));

    // Layer 6
    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_30.setTransform(46.2,-68.9);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.8,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.9).curveTo(5,-1.7,4.9,-1.4).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_31.setTransform(49.8,-71.2);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_32.setTransform(60.7,-78.3);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_33.setTransform(78.8,-90.3);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4,-0.4,-3.6).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.4).curveTo(3.4,-4.3,4,-3.9).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,5,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.8,-2.7,2.3).curveTo(-2.8,2.3,-2.8,2.2).curveTo(-5,0.1,-5,-1.5).closePath();
    this.shape_34.setTransform(79.2,-91.2);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4,-0.4,-3.6).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,4,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_35.setTransform(79.7,-92.1);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,4,-3.9).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,5,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.2,4.4).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_36.setTransform(80.1,-93);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.5).curveTo(4.9,-2.9,5,-1.5).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.3).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_37.setTransform(80.5,-94);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.4).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.3).curveTo(-2.8,2.3,-2.8,2.2).curveTo(-5,0.1,-5,-1.5).closePath();
    this.shape_38.setTransform(80.9,-94.9);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_39.setTransform(81.4,-95.9);

    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.4,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_40.setTransform(81.8,-96.8);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,4,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_41.setTransform(82.2,-97.8);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.7).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_42.setTransform(82.6,-98.7);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_43.setTransform(83.1,-99.6);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.4,2.6,2.5).curveTo(0.9,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_44.setTransform(83.5,-100.6);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.4,-3.6,0.7,-3.8).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.3,2.6,2.4).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_45.setTransform(83.9,-101.5);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.4,-3.6,0.7,-3.8).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_46.setTransform(84.3,-102.4);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_47.setTransform(84.8,-103.4);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_48.setTransform(85.2,-104.3);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.8).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.6,4.9,-1.3).curveTo(5,0.2,2.9,2.1).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_49.setTransform(85.6,-105.2);

    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_50.setTransform(86,-106.2);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.3,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_51.setTransform(86.5,-107.1);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.4,-3.4,0.6,-3.7).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_52.setTransform(86.9,-108.1);

    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_53.setTransform(87.3,-109);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.1,-4.2,-0.4,-3.5).curveTo(0.1,-3.1,0.2,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5.1,0.2,3,2.1).curveTo(2.9,2.3,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.4,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_54.setTransform(87.7,-110);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.9).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.1,3,2.1).curveTo(2.8,2.2,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_55.setTransform(88.2,-110.9);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.9).curveTo(5,-1.7,5,-1.4).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_56.setTransform(88.6,-111.8);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.8,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,4.9,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_57.setTransform(89,-112.8);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.1,-3,0.1,-3.1).curveTo(0.3,-3.3,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2.1).curveTo(2.9,2.2,2.8,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.4,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_58.setTransform(89.4,-113.7);

    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.3,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.4,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(2.9,-4.5,3.7,-4.1).curveTo(3.8,-4,4,-3.9).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,4.9,-1.5).curveTo(5.1,0.1,3,2).curveTo(2.9,2.2,2.7,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_59.setTransform(89.9,-114.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30,p:{x:46.2,y:-68.9}}]}).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_30,p:{x:90.3,y:-115.6}}]},1).wait(16));

    // Layer 4
    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_60.setTransform(-47.2,-68.1);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.3,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-4,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2).curveTo(2.9,2.2,2.7,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_61.setTransform(-50.5,-70.5);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.3,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.7,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_62.setTransform(-60.6,-77.9);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_63.setTransform(-77.3,-90.3);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.5,0.4,-3.5).curveTo(0.6,-3.8,0.9,-3.9).curveTo(1.5,-4.3,2.1,-4.3).curveTo(3.4,-4.3,4,-3.8).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.4).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.2,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_64.setTransform(-77.7,-91.1);

    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.8,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_65.setTransform(-78.1,-92.1);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4,-0.4,-3.6).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_66.setTransform(-78.6,-93);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.8).curveTo(4.1,-3.8,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.2,0.2,4.4).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_67.setTransform(-79,-93.9);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.6,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.9,0.4,2.8,2.3).curveTo(2.7,2.5,2.5,2.6).curveTo(0.8,4.1,0.2,4.4).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_68.setTransform(-79.5,-94.8);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.8,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.3,4.9,-1.1).curveTo(4.8,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.6).curveTo(0.8,4.1,0.1,4.3).curveTo(0,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_69.setTransform(-79.9,-95.7);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-2.9,5,-1.6).curveTo(5,-1.4,4.9,-1.1).curveTo(4.9,0.4,2.8,2.3).curveTo(2.7,2.4,2.5,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_70.setTransform(-80.3,-96.6);

    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_71.setTransform(-80.8,-97.5);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.7).curveTo(5,-1.4,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_72.setTransform(-81.2,-98.4);

    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_73.setTransform(-81.6,-99.3);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_74.setTransform(-82.1,-100.3);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-3.9).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,5,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.4,2.6,2.5).curveTo(0.9,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_75.setTransform(-82.5,-101.2);

    this.shape_76 = new cjs.Shape();
    this.shape_76.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_76.setTransform(-83,-102.1);

    this.shape_77 = new cjs.Shape();
    this.shape_77.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,4.9,-1.3).curveTo(4.9,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_77.setTransform(-83.4,-103);

    this.shape_78 = new cjs.Shape();
    this.shape_78.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,4.9,-1.3).curveTo(5,0.2,2.9,2.1).curveTo(2.8,2.3,2.6,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_78.setTransform(-83.8,-103.9);

    this.shape_79 = new cjs.Shape();
    this.shape_79.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_79.setTransform(-84.3,-104.8);

    this.shape_80 = new cjs.Shape();
    this.shape_80.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.2).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.7,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_80.setTransform(-84.7,-105.7);

    this.shape_81 = new cjs.Shape();
    this.shape_81.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.3,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_81.setTransform(-85.1,-106.6);

    this.shape_82 = new cjs.Shape();
    this.shape_82.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.3,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_82.setTransform(-85.6,-107.5);

    this.shape_83 = new cjs.Shape();
    this.shape_83.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.1,-4.2,-0.4,-3.5).curveTo(0.1,-3.1,0.2,-3.2).curveTo(0.3,-3.4,0.6,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.9,2.3,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.4,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_83.setTransform(-86,-108.5);

    this.shape_84 = new cjs.Shape();
    this.shape_84.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.2,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5.1,0.2,3,2.1).curveTo(2.9,2.3,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_84.setTransform(-86.5,-109.4);

    this.shape_85 = new cjs.Shape();
    this.shape_85.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_85.setTransform(-86.9,-110.3);

    this.shape_86 = new cjs.Shape();
    this.shape_86.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_86.setTransform(-87.3,-111.2);

    this.shape_87 = new cjs.Shape();
    this.shape_87.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.3,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-4,4,-3.9).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2).curveTo(2.9,2.2,2.8,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_87.setTransform(-87.8,-112.1);

    this.shape_88 = new cjs.Shape();
    this.shape_88.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3,0.1,-3.1).curveTo(0.3,-3.3,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2.1).curveTo(2.9,2.2,2.8,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_88.setTransform(-88.2,-113);

    this.shape_89 = new cjs.Shape();
    this.shape_89.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.3,-0.4,-3.6).curveTo(0,-3,0.1,-3.1).curveTo(0.2,-3.3,0.4,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(2.9,-4.5,3.7,-4.1).curveTo(3.8,-4,4,-3.9).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2).curveTo(2.9,2.2,2.8,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_89.setTransform(-88.6,-113.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_60,p:{x:-47.2,y:-68.1}}]}).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_60,p:{x:-89.1,y:-114.9}}]},1).wait(16));

    // Layer 5
    this.shape_90 = new cjs.Shape();
    this.shape_90.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-3.1,-4.4,-2.1,-4.3).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(-0.2,-3.2,0,-2.9).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).lineTo(0,4.4).curveTo(-0,4.4,-0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).closePath();
    this.shape_90.setTransform(-45.6,75);

    this.shape_91 = new cjs.Shape();
    this.shape_91.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3,2.1).curveTo(2.9,2.2,2.7,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_91.setTransform(-48.4,77.7);

    this.shape_92 = new cjs.Shape();
    this.shape_92.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3.1,5,-1.7).curveTo(5,-1.5,5,-1.3).curveTo(5,0.3,2.9,2.2).curveTo(2.8,2.3,2.6,2.5).curveTo(0.9,4,0.2,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_92.setTransform(-56.8,85.5);

    this.shape_93 = new cjs.Shape();
    this.shape_93.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1.2,-4.2,-0.4,-3.5).curveTo(0.2,-3.2,0.4,-3.5).curveTo(1.2,-4.2,2.1,-4.3).curveTo(3.1,-4.4,4,-3.8).curveTo(4.9,-3.2,5,-1.5).curveTo(5.1,0.2,2.8,2.4).curveTo(0.8,4.2,0.1,4.4).curveTo(-0.8,4.2,-2.8,2.4).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_93.setTransform(-70.8,98.5);

    this.shape_94 = new cjs.Shape();
    this.shape_94.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-0.9,-4,-0.4,-3.5).curveTo(0.4,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.9,-3.8).curveTo(1.5,-4.3,2.1,-4.3).curveTo(3.4,-4.3,4,-3.8).curveTo(4.2,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.2,4.9,-1).curveTo(4.8,0.5,2.8,2.4).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.2,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_94.setTransform(-71.5,99.1);

    this.shape_95 = new cjs.Shape();
    this.shape_95.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.5,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.9).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.9,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.3).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_95.setTransform(-72.2,99.7);

    this.shape_96 = new cjs.Shape();
    this.shape_96.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.5).curveTo(4.8,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.2).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_96.setTransform(-72.9,100.4);

    this.shape_97 = new cjs.Shape();
    this.shape_97.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.1,-4,-3.8).curveTo(-1,-4,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.5,-3.7,0.8,-3.8).curveTo(1.4,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.7,4.3,-3.5).curveTo(4.8,-2.8,5,-1.5).curveTo(5,-1.3,4.9,-1).curveTo(4.8,0.5,2.8,2.3).curveTo(2.6,2.5,2.5,2.6).curveTo(0.7,4.1,0.1,4.4).curveTo(0,4.4,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-0.3,4.3,-0.3,4.3).curveTo(-1.1,3.9,-2.8,2.4).curveTo(-2.8,2.3,-2.9,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_97.setTransform(-73.6,101);

    this.shape_98 = new cjs.Shape();
    this.shape_98.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-0.9,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.4,-3.5).curveTo(0.6,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2.1,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_98.setTransform(-74.3,101.6);

    this.shape_99 = new cjs.Shape();
    this.shape_99.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.3,-3.4,0.4,-3.4).curveTo(0.6,-3.6,0.8,-3.8).curveTo(1.4,-4.2,2,-4.3).curveTo(3.3,-4.3,3.9,-3.9).curveTo(4.1,-3.8,4.3,-3.6).curveTo(4.9,-2.9,5,-1.6).curveTo(5,-1.3,5,-1.1).curveTo(4.9,0.4,2.9,2.3).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.2,4.3,-0.3,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_99.setTransform(-75,102.2);

    this.shape_100 = new cjs.Shape();
    this.shape_100.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.5).curveTo(0.5,-3.7,0.8,-3.8).curveTo(1.4,-4.3,2,-4.4).curveTo(3.3,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.3).curveTo(-2.8,2.3,-2.8,2.2).curveTo(-5.1,0.1,-5,-1.5).closePath();
    this.shape_100.setTransform(-75.7,102.8);

    this.shape_101 = new cjs.Shape();
    this.shape_101.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.4,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.9,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_101.setTransform(-76.4,103.4);

    this.shape_102 = new cjs.Shape();
    this.shape_102.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.3,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4.1,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_102.setTransform(-77.1,104);

    this.shape_103 = new cjs.Shape();
    this.shape_103.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.8).curveTo(-1,-4.1,-0.4,-3.5).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,2,-4.3).curveTo(3.2,-4.4,3.9,-3.9).curveTo(4,-3.8,4.2,-3.6).curveTo(4.8,-3,5,-1.6).curveTo(5,-1.4,4.9,-1.2).curveTo(4.9,0.4,2.9,2.2).curveTo(2.7,2.4,2.6,2.5).curveTo(0.8,4.1,0.2,4.3).curveTo(0.1,4.4,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_103.setTransform(-77.8,104.6);

    this.shape_104 = new cjs.Shape();
    this.shape_104.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.4).curveTo(0.5,-3.6,0.7,-3.8).curveTo(1.3,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.3,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,3.9,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_104.setTransform(-78.5,105.3);

    this.shape_105 = new cjs.Shape();
    this.shape_105.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.4,-3.6,0.7,-3.8).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.8,4.2,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.3,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_105.setTransform(-79.2,105.9);

    this.shape_106 = new cjs.Shape();
    this.shape_106.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.1,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.8,4.1,-3.7).curveTo(4.8,-3,5,-1.7).curveTo(5,-1.5,4.9,-1.2).curveTo(4.9,0.3,2.9,2.2).curveTo(2.7,2.3,2.6,2.5).curveTo(0.8,4,0.2,4.3).curveTo(0.1,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.2,4.3,-0.2,4.3).curveTo(-1,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_106.setTransform(-79.9,106.5);

    this.shape_107 = new cjs.Shape();
    this.shape_107.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.3,-3.3).curveTo(0.4,-3.6,0.7,-3.8).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.2,-4.4,3.8,-4).curveTo(4,-3.9,4.2,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_107.setTransform(-80.7,107.1);

    this.shape_108 = new cjs.Shape();
    this.shape_108.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1,-4.2,-0.4,-3.6).curveTo(0.2,-3.3,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_108.setTransform(-81.4,107.7);

    this.shape_109 = new cjs.Shape();
    this.shape_109.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.2,1.9,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_109.setTransform(-82.1,108.3);

    this.shape_110 = new cjs.Shape();
    this.shape_110.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.2,-3.2,0.2,-3.2).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.2,-4.1,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(4,-3.9,4.1,-3.7).curveTo(4.8,-3.1,5,-1.8).curveTo(5,-1.5,5,-1.3).curveTo(5,0.2,3,2.2).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_110.setTransform(-82.8,108.9);

    this.shape_111 = new cjs.Shape();
    this.shape_111.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.3).curveTo(0.4,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_111.setTransform(-83.5,109.6);

    this.shape_112 = new cjs.Shape();
    this.shape_112.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.2,0.2,-3.2).curveTo(0.3,-3.5,0.6,-3.7).curveTo(1.1,-4.2,1.8,-4.3).curveTo(3.1,-4.4,3.8,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_112.setTransform(-84.2,110.2);

    this.shape_113 = new cjs.Shape();
    this.shape_113.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.2,-3.2).curveTo(0.3,-3.4,0.5,-3.7).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4.1,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,5,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.1,4.3,-0.2,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_113.setTransform(-84.9,110.8);

    this.shape_114 = new cjs.Shape();
    this.shape_114.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.8).curveTo(5,-1.6,4.9,-1.4).curveTo(5,0.2,3,2.1).curveTo(2.8,2.3,2.7,2.4).curveTo(0.9,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.4,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_114.setTransform(-85.6,111.4);

    this.shape_115 = new cjs.Shape();
    this.shape_115.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.7).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.5,3.7,-4.1).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.2,5,-1.9).curveTo(5,-1.7,4.9,-1.4).curveTo(5,0.1,3,2.1).curveTo(2.8,2.2,2.7,2.4).curveTo(0.9,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0,4.3,-0,4.3).curveTo(-0.1,4.3,-0.1,4.3).curveTo(-0.9,4.1,-2.7,2.4).curveTo(-2.8,2.3,-2.8,2.3).curveTo(-5.1,0.2,-5,-1.5).closePath();
    this.shape_115.setTransform(-86.3,112);

    this.shape_116 = new cjs.Shape();
    this.shape_116.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.2).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1.1,-4.1,1.8,-4.3).curveTo(3,-4.5,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2.1).curveTo(2.9,2.2,2.8,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_116.setTransform(-87,112.6);

    this.shape_117 = new cjs.Shape();
    this.shape_117.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3.1,0.1,-3.1).curveTo(0.3,-3.4,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2.1).curveTo(2.9,2.2,2.8,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_117.setTransform(-87.7,113.2);

    this.shape_118 = new cjs.Shape();
    this.shape_118.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.1,-4.2,-0.4,-3.6).curveTo(0.1,-3,0.1,-3.1).curveTo(0.3,-3.3,0.5,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(3,-4.4,3.7,-4).curveTo(3.9,-3.9,4,-3.8).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2.1).curveTo(2.9,2.2,2.8,2.4).curveTo(1,4,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_118.setTransform(-88.4,113.8);

    this.shape_119 = new cjs.Shape();
    this.shape_119.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5,-1.5).curveTo(-4.9,-3.2,-4,-3.9).curveTo(-1.2,-4.3,-0.4,-3.6).curveTo(0,-3.1,0.1,-3.1).curveTo(0.2,-3.4,0.4,-3.6).curveTo(1,-4.1,1.7,-4.3).curveTo(2.9,-4.5,3.7,-4.1).curveTo(3.8,-4,4,-3.9).curveTo(4.8,-3.3,5,-1.9).curveTo(5,-1.7,5,-1.5).curveTo(5.1,0.1,3.1,2).curveTo(2.9,2.2,2.8,2.3).curveTo(1,3.9,0.3,4.3).curveTo(0.2,4.3,0.1,4.3).curveTo(0.1,4.3,0,4.3).curveTo(-0,4.3,-0.1,4.3).curveTo(-0.8,4.1,-2.7,2.4).curveTo(-2.7,2.4,-2.8,2.3).curveTo(-5,0.2,-5,-1.5).closePath();
    this.shape_119.setTransform(-89.1,114.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_90,p:{x:-45.6,y:75}}]}).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_90,p:{x:-89.8,y:115}}]},1).wait(16));

    // Layer 3
    this.shape_120 = new cjs.Shape();
    this.shape_120.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-0.5,0.3).lineTo(0.5,-0.4);
    this.shape_120.setTransform(-10.3,6.4);

    this.shape_121 = new cjs.Shape();
    this.shape_121.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.4,3.2).lineTo(3.4,-3.2);
    this.shape_121.setTransform(-7.4,3.5);

    this.shape_122 = new cjs.Shape();
    this.shape_122.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-6.2,6.1).lineTo(6.3,-6.1);
    this.shape_122.setTransform(-4.5,0.7);

    this.shape_123 = new cjs.Shape();
    this.shape_123.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.1,8.9).lineTo(9.1,-8.9);
    this.shape_123.setTransform(-1.7,-2.2);

    this.shape_124 = new cjs.Shape();
    this.shape_124.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-12,11.8).lineTo(12,-11.8);
    this.shape_124.setTransform(1.2,-5.1);

    this.shape_125 = new cjs.Shape();
    this.shape_125.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-14.9,14.7).lineTo(14.9,-14.7);
    this.shape_125.setTransform(4.1,-7.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_120}]},4).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).wait(37));

    // Layer 2
    this.shape_126 = new cjs.Shape();
    this.shape_126.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-0.8,-1).lineTo(0.8,1);
    this.shape_126.setTransform(-18.1,-3.4);

    this.shape_127 = new cjs.Shape();
    this.shape_127.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-1.6,-2.2).lineTo(1.6,2.2);
    this.shape_127.setTransform(-17.3,-2.3);

    this.shape_128 = new cjs.Shape();
    this.shape_128.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-2.4,-3.3).lineTo(2.4,3.3);
    this.shape_128.setTransform(-16.4,-1.1);

    this.shape_129 = new cjs.Shape();
    this.shape_129.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-3.2,-4.5).lineTo(3.2,4.5);
    this.shape_129.setTransform(-15.6,0);

    this.shape_130 = new cjs.Shape();
    this.shape_130.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4,-5.6).lineTo(4,5.6);
    this.shape_130.setTransform(-14.8,1.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_126}]}).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).wait(42));

    // Layer 1
    this.shape_131 = new cjs.Shape();
    this.shape_131.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-28.9,-62.2).lineTo(28.9,-62.2).curveTo(35,-62.2,35,-56.9).lineTo(35,45).curveTo(35,50.4,28.9,50.4).lineTo(-28.9,50.4).curveTo(-35,50.4,-35,45).lineTo(-35,-56.9).curveTo(-35,-62.2,-28.9,-62.2).closePath().moveTo(-33.2,-67.6).lineTo(33.2,-67.6).curveTo(40.2,-67.6,40.2,-61.1).lineTo(40.2,61.2).curveTo(40.2,67.6,33.2,67.6).lineTo(-33.2,67.6).curveTo(-40.2,67.6,-40.2,61.2).lineTo(-40.2,-61.1).curveTo(-40.2,-67.6,-33.2,-67.6).closePath().moveTo(4.8,59).curveTo(4.8,61,3.4,62.3).curveTo(1.9,63.7,0,63.7).curveTo(-1.9,63.7,-3.4,62.3).curveTo(-4.7,61,-4.7,59).curveTo(-4.7,57,-3.4,55.6).curveTo(-1.9,54.2,0,54.2).curveTo(1.9,54.2,3.4,55.6).curveTo(4.8,57,4.8,59).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape_131).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.1,-74.2,105.3,154.6);


(lib.Symbol = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.9,0).lineTo(0,0).lineTo(5.9,0).moveTo(0,-5.9).lineTo(0,0).lineTo(0,5.9);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol, new cjs.Rectangle(-6.9,-6.9,13.8,13.8), null);


(lib.bag1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 4 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(-58.3,39.8).lineTo(-58.3,-39.8).lineTo(58.3,-39.8).lineTo(58.3,39.8).closePath();
    mask.setTransform(1.2,-214.9);

    // Layer 3
    this.instance = new lib.bagHandle();
    this.instance.parent = this;
    this.instance.setTransform(-0.1,-152.6,1,1,0,0,0,-0.1,-37.6);

    var maskedShapeInstanceList = [this.instance];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).to({y:-212.6},7,cjs.Ease.get(-1)).to({y:-202.6},3,cjs.Ease.get(1)).wait(5));

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-53.3,-87.5).lineTo(53.3,-87.5).curveTo(61.7,-87.5,61.7,-79.1).lineTo(61.7,69.1).curveTo(61.7,77.5,53.3,77.5).lineTo(-53.3,77.5).curveTo(-61.7,77.5,-61.7,69.1).lineTo(-61.7,-79.1).curveTo(-61.7,-87.5,-53.3,-87.5).closePath().moveTo(28.5,78).lineTo(46.6,78).lineTo(46.6,87.5).lineTo(28.5,87.5).closePath().moveTo(42.6,-67.9).lineTo(42.6,61.9).moveTo(21.2,-67.9).lineTo(21.2,61.9).moveTo(-46.6,78).lineTo(-28.6,78).lineTo(-28.6,87.5).lineTo(-46.6,87.5).closePath().moveTo(-0.1,-67.9).lineTo(-0.1,61.9).moveTo(-21.4,-67.9).lineTo(-21.4,61.9).moveTo(-42.7,-67.9).lineTo(-42.7,61.9);
    this.shape.setTransform(0,-87.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-62.7,-191.2,125.5,192.2);


(lib.aniWaves = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(-46.5,27).curveTo(-47,25.2,-47,23).lineTo(-47,-27).lineTo(47,-27).lineTo(47,23).curveTo(47,25.2,46.5,27).closePath();
    mask.setTransform(40.3,3);

    // Layer 1
    this.instance = new lib.graSteam();
    this.instance.parent = this;
    this.instance.setTransform(0,0,1,1,90);

    var maskedShapeInstanceList = [this.instance];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).to({x:80.6},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-14.5,94,29);


(lib.aniTitle = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // txt5
    this.instance = new lib.txt5();
    this.instance.parent = this;
    this.instance.setTransform(194.5,180.3,1,1,0,0,0,194.5,0);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({scaleX:2.2,scaleY:2.2,x:116.1,y:117.8},7,cjs.Ease.get(-1)).to({scaleX:2,scaleY:2,x:116,y:117.9},3,cjs.Ease.get(1)).to({scaleX:2.04,scaleY:2.04,x:118.4,y:119.8},24).to({scaleX:2.2,scaleY:2.2,x:127.7,y:127.4},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:194.5,y:180.3},5,cjs.Ease.get(-1)).wait(1));

    // txt4
    this.instance_1 = new lib.txt4();
    this.instance_1.parent = this;
    this.instance_1.setTransform(112.4,180.3,1,1,0,0,0,112.4,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(26).to({scaleX:2.2,scaleY:2.2,x:-48.2,y:117.8},7,cjs.Ease.get(-1)).to({scaleX:2,scaleY:2,y:117.9},3,cjs.Ease.get(1)).to({scaleX:2.04,scaleY:2.04,x:-49.1,y:119.8},32).to({scaleX:2.2,scaleY:2.2,x:-53,y:127.4},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:112.4,y:180.3},5,cjs.Ease.get(-1)).wait(1));

    // txt3
    this.instance_2 = new lib.txt3();
    this.instance_2.parent = this;
    this.instance_2.setTransform(8.8,180.3,1,1,0,0,0,8.8,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(16).to({scaleX:5.58,scaleY:5.58,x:0.4,y:17},8,cjs.Ease.get(-1)).to({scaleX:5.07,scaleY:5.07},4,cjs.Ease.get(1)).to({scaleX:5.17,scaleY:5.17,y:16.9},40).to({scaleX:5.58,scaleY:5.58,y:16.5},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:8.8,y:180.3},5,cjs.Ease.get(-1)).wait(1));

    // txt2
    this.instance_3 = new lib.txt2();
    this.instance_3.parent = this;
    this.instance_3.setTransform(-59,180.3,1,1,0,0,0,-59,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(8).to({scaleX:2.2,scaleY:2.2,x:137.4,y:-73.7},7,cjs.Ease.get(-0.89)).to({scaleX:2,scaleY:2},3,cjs.Ease.get(1)).to({scaleX:2.04,scaleY:2.04,x:140.2,y:-75.6},50).to({scaleX:2.2,scaleY:2.2,x:151.1,y:-83.3},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:-59,y:180.3},5,cjs.Ease.get(-1)).wait(1));

    // txt1
    this.instance_4 = new lib.txt1();
    this.instance_4.parent = this;
    this.instance_4.setTransform(-149.8,180.3,1,1,0,0,0,-149.8,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:2.2,scaleY:2.2,x:-44.2,y:-73.7},7,cjs.Ease.get(-1)).to({scaleX:2,scaleY:2},3,cjs.Ease.get(1)).to({scaleX:2.04,scaleY:2.04,x:-45.1,y:-75.6},58).to({scaleX:2.2,scaleY:2.2,x:-48.6,y:-83.3},3,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:-149.8,y:180.3},5,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-239.9,167.3,479.9,26);


(lib.aniStar = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.star();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,rotation:-360,x:-118.5},5,cjs.Ease.get(-1)).to({rotation:-5040,x:-127.5},24).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-92,2,6.9,6.9);


(lib.anio = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.o();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,x:-128.5},4,cjs.Ease.get(-1)).to({x:-137.5},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.4,2.6,5.8,5.8);


(lib.aniGPS = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 1
    this.instance = new lib.graGPS();
    this.instance.parent = this;
    this.instance.setTransform(-1.2,-1.3,0.1,0.1,0,0,0,0,-0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:0,scaleX:1.1,scaleY:1.1,y:-1.2},4,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1,y:-1.3},2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-6.5,3.9,5.4);


(lib.aniGlobe = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // aniGPS
    this.instance = new lib.aniGPS("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-47.5,24.1,0.849,0.849,-15);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(7).to({_off:false},0).wait(52));

    // aniGPS
    this.instance_1 = new lib.aniGPS("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(19.6,25,0.569,0.569,45,0,0,0.5,0.1);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).wait(54));

    // aniGPS
    this.instance_2 = new lib.aniGPS("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-4.3,-3.3,0.849,0.849,15);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({_off:false},0).wait(55));

    // aniGPS
    this.instance_3 = new lib.aniGPS("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-36.5,-39.1);
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off:false},0).wait(57));

    // globe
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(26.3,-92.8).lineTo(19.4,-78).curveTo(46.5,-66,58.1,-40).curveTo(62,-31.2,63.4,-22.6).curveTo(66.4,-3.6,58,15.1).curveTo(57.8,15.6,57.5,16.2).curveTo(49.1,34,33.5,45.2).curveTo(24.6,51.5,13.5,55.5).curveTo(1.5,60,-10.6,60.9).curveTo(-25,62,-39.4,58).lineTo(-45.6,71.2).moveTo(12.1,-62.5).lineTo(12.9,-62.1).curveTo(22.7,-57.5,23.7,-40.1).curveTo(24,-34.5,23.4,-28.7).lineTo(45.6,-19.3).curveTo(47.6,-5.1,41.9,8.9).curveTo(41.8,9.3,41.6,9.8).curveTo(35.9,23.1,25.2,31.5).moveTo(11.8,-62.6).lineTo(12.1,-62.5).lineTo(1.7,-37.9).lineTo(23.4,-28.7).curveTo(22,-16,16.1,-2).curveTo(16,-1.6,15.8,-1.2).curveTo(10.1,12.2,2.5,21.9).lineTo(25.2,31.5).curveTo(19.2,36.2,11.6,39.2).curveTo(-9.2,47.7,-30,39.1).lineTo(-30.8,38.8).curveTo(-40.8,34.3,-41.8,16.6).curveTo(-42.2,10.2,-41.2,3.4).curveTo(-39.6,-8.8,-33.9,-22.2).curveTo(-33.7,-22.7,-33.6,-23.1).lineTo(-9,-12.7).lineTo(-19.7,12.5).lineTo(-30.8,38.8).curveTo(-51.5,30,-60,8.9).curveTo(-63.1,1.3,-63.9,-6.2).curveTo(-65.4,-19.8,-59.7,-33.1).curveTo(-59.5,-33.6,-59.4,-34).curveTo(-56.1,-41.4,-51.2,-47.3).curveTo(-47,-52.3,-41.6,-56.2).curveTo(-36.2,-60,-29.6,-62.7).curveTo(-27.6,-63.5,-25.6,-64.2).curveTo(-6.9,-70.3,11.8,-62.6).closePath().moveTo(12.9,-62.1).curveTo(33.5,-53.2,42,-32.4).curveTo(44.6,-25.8,45.6,-19.3).moveTo(16.1,-2).lineTo(41.9,8.9).moveTo(-41.6,-56.2).lineTo(-19.4,-46.8).curveTo(-15.7,-51.4,-11.5,-55).curveTo(1.7,-66.3,11.8,-62.6).moveTo(-33.6,-23.1).curveTo(-27.6,-37,-19.4,-46.8).lineTo(1.7,-37.9).lineTo(-9,-12.7).lineTo(16.1,-2).moveTo(-59.4,-34).lineTo(-33.6,-23.1).moveTo(2.5,21.9).curveTo(-1.8,27.3,-6.6,31.5).curveTo(-19.9,43,-30,39.1).moveTo(-41.2,3.4).lineTo(-19.7,12.5).lineTo(2.5,21.9).moveTo(-10.6,82).lineTo(-21.9,82).moveTo(-10.6,60.9).lineTo(-10.6,82).lineTo(-10.6,92.8).lineTo(-39.4,92.8).moveTo(-63.9,-6.2).lineTo(-41.2,3.4).moveTo(1.7,82).lineTo(-10.6,82).moveTo(18.9,92.8).lineTo(-10.6,92.8);
    this.shape.setTransform(2.5,7.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-62.7,-86,130.4,187.6);


(lib.aniGlasses = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 7
    this.instance = new lib.aniStar("synched",4,false);
    this.instance.parent = this;
    this.instance.setTransform(3.7,46.3,1,1,45);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).to({x:-6.3,y:41.3,startPosition:8},4).wait(26));

    // frame
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(15.9,-23.4).curveTo(20.9,-27.4,30.9,-28.7).curveTo(80.8,-35,82.5,-1.2).curveTo(81,26.6,61.7,29.5).curveTo(26,32,11.8,-8.2).curveTo(9.6,-15.5,13,-20.3).curveTo(14.1,-22,15.9,-23.4).closePath().moveTo(28.4,-33.2).curveTo(60.5,-37.3,96.8,-26.3).curveTo(98.2,-21.4,96.2,-13.7).curveTo(87.8,-8.7,88.2,-1.4).curveTo(86.4,30.8,64.1,34.1).curveTo(22.7,37.1,6.3,-9.5).curveTo(4.6,-15,5.8,-19.4).curveTo(-0,-21.9,-5.8,-19.4).curveTo(-4.7,-15,-6.3,-9.5).curveTo(-22.8,37.1,-64.1,34.1).curveTo(-86.4,30.8,-88.2,-1.4).curveTo(-87.9,-8.7,-96.3,-13.7).curveTo(-98.2,-21.4,-96.9,-26.3).curveTo(-60.5,-37.3,-28.5,-33.2).curveTo(-26,-32.9,-23.8,-32.4).lineTo(1,-29.5).lineTo(26,-32.9).curveTo(26.3,-32.9,26.6,-32.9).curveTo(27.5,-33.1,28.4,-33.2).closePath().moveTo(86.6,-23).lineTo(93.9,-23).lineTo(93.9,-19.4).lineTo(86.6,-19.4).closePath().moveTo(-86.7,-23).lineTo(-93.9,-23).lineTo(-93.9,-19.4).lineTo(-86.7,-19.4).closePath().moveTo(-16,-23.4).curveTo(-20.9,-27.4,-31,-28.7).curveTo(-80.9,-35,-82.6,-1.2).curveTo(-81,26.6,-61.8,29.5).curveTo(-26.1,32,-11.8,-8.2).curveTo(-9.7,-15.5,-13,-20.3).curveTo(-14.2,-22,-16,-23.4).closePath();

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(46));

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(11.8,-8.2).curveTo(9.7,-15.4,13,-20.3).curveTo(14.2,-22,15.9,-23.4).curveTo(20.9,-27.4,31,-28.6).curveTo(80.9,-35,82.5,-1.2).curveTo(81,26.6,61.7,29.5).lineTo(57.6,29.7).curveTo(25.2,29.7,11.8,-8.2).closePath().moveTo(-61.7,29.5).curveTo(-81,26.6,-82.5,-1.2).curveTo(-80.9,-35,-31,-28.6).curveTo(-20.8,-27.4,-15.9,-23.4).curveTo(-14.2,-22,-13,-20.3).curveTo(-9.7,-15.4,-11.8,-8.2).curveTo(-25.2,29.7,-57.6,29.7).lineTo(-61.7,29.5).closePath();
    mask.setTransform(0,-0.1);

    // 2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(27.1,-55).lineTo(33.5,-49.8).lineTo(-26.6,55).lineTo(-33.4,49.8).closePath();
    this.shape_1.setTransform(131.5,9.5);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.4,49.8).lineTo(27.1,-55).lineTo(33.4,-49.8).lineTo(-26.6,55).closePath();
    this.shape_2.setTransform(131.5,9.5);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.4,-49.8).lineTo(-26.6,55).closePath();
    this.shape_3.setTransform(130.8,9.5);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.5,-49.9).lineTo(-26.6,55).closePath();
    this.shape_4.setTransform(128.9,9.5);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.5,-49.8).lineTo(-26.6,55).closePath();
    this.shape_5.setTransform(128.9,9.5);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.5,-50).lineTo(-26.7,55).closePath();
    this.shape_6.setTransform(125.6,9.5);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.5,-49.9).lineTo(-26.7,55).closePath();
    this.shape_7.setTransform(125.6,9.5);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.6,-50).lineTo(-26.7,55).closePath();
    this.shape_8.setTransform(121.1,9.5);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.5,49.8).lineTo(27.1,-55).lineTo(33.5,-50.1).lineTo(-26.7,55).closePath();
    this.shape_9.setTransform(121.1,9.5);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.6,49.8).lineTo(27.1,-55).lineTo(33.6,-50.1).lineTo(-26.7,55).closePath();
    this.shape_10.setTransform(115.3,9.5);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.6,49.8).lineTo(27.1,-55).lineTo(33.6,-50.2).lineTo(-26.7,55).closePath();
    this.shape_11.setTransform(115.3,9.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.7,49.8).lineTo(27.1,-55).lineTo(33.7,-50.3).lineTo(-26.8,55).closePath();
    this.shape_12.setTransform(108.2,9.5);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.7,49.8).lineTo(27.1,-55).lineTo(33.7,-50.3).lineTo(-26.8,55).closePath();
    this.shape_13.setTransform(108.2,9.5);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.8,49.8).lineTo(27,-55).lineTo(33.8,-50.5).lineTo(-26.9,55).closePath();
    this.shape_14.setTransform(99.8,9.5);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.8,49.8).lineTo(27,-55).lineTo(33.8,-50.5).lineTo(-26.9,55).closePath();
    this.shape_15.setTransform(99.8,9.5);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-33.9,49.8).lineTo(27,-55).lineTo(33.9,-50.8).lineTo(-27,55).closePath();
    this.shape_16.setTransform(90.1,9.5);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-33.9,49.8).lineTo(27,-55).lineTo(33.9,-50.7).lineTo(-27,55).closePath();
    this.shape_17.setTransform(90.1,9.5);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34,49.8).lineTo(27,-55).lineTo(34,-51).lineTo(-27.1,55).closePath();
    this.shape_18.setTransform(79.1,9.5);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34,49.8).lineTo(27.1,-55).lineTo(34,-51).lineTo(-27.1,55).closePath();
    this.shape_19.setTransform(79.1,9.5);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.1,49.8).lineTo(27,-55).lineTo(34.1,-51.2).lineTo(-27.2,55).closePath();
    this.shape_20.setTransform(66.8,9.5);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34.1,49.8).lineTo(27,-55).lineTo(34.1,-51.3).lineTo(-27.2,55).closePath();
    this.shape_21.setTransform(66.8,9.5);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.2,49.8).lineTo(27,-55).lineTo(34.2,-51.5).lineTo(-27.4,55).closePath();
    this.shape_22.setTransform(53.2,9.5);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34.2,49.8).lineTo(27,-55).lineTo(34.3,-51.5).lineTo(-27.4,55).closePath();
    this.shape_23.setTransform(53.2,9.5);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.4,49.8).lineTo(27,-55).lineTo(34.4,-51.9).lineTo(-27.5,55).closePath();
    this.shape_24.setTransform(38.3,9.5);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34.4,49.8).lineTo(27,-55).lineTo(34.4,-51.9).lineTo(-27.4,55).closePath();
    this.shape_25.setTransform(38.3,9.5);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.6,49.8).lineTo(27,-55).lineTo(34.6,-52.2).lineTo(-27.6,55).closePath();
    this.shape_26.setTransform(22.1,9.5);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34.6,49.8).lineTo(27,-55).lineTo(34.6,-52.3).lineTo(-27.6,55).closePath();
    this.shape_27.setTransform(22.1,9.5);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.8,49.8).lineTo(26.9,-55).lineTo(34.8,-52.6).lineTo(-27.8,55).closePath();
    this.shape_28.setTransform(4.7,9.5);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-34.8,49.8).lineTo(26.9,-55).lineTo(34.8,-52.6).lineTo(-27.8,55).closePath();
    this.shape_29.setTransform(4.7,9.5);

    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-35,49.8).lineTo(26.9,-55).lineTo(35,-53).lineTo(-27.9,55).closePath();
    this.shape_30.setTransform(-14.1,9.5);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-35,49.8).lineTo(27,-55).lineTo(35,-53.1).lineTo(-27.9,55).closePath();
    this.shape_31.setTransform(-14.1,9.5);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-35.1,49.8).lineTo(26.9,-55).lineTo(35.1,-53.5).lineTo(-28.1,55).closePath();
    this.shape_32.setTransform(-34.1,9.5);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-35.2,49.8).lineTo(26.9,-55).lineTo(35.1,-53.5).lineTo(-28.1,55).closePath();
    this.shape_33.setTransform(-34.1,9.5);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-35.4,49.8).lineTo(26.9,-55).lineTo(35.4,-54).lineTo(-28.4,55).closePath();
    this.shape_34.setTransform(-55.5,9.5);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-35.4,49.8).lineTo(26.9,-55).lineTo(35.4,-54).lineTo(-28.4,55).closePath();
    this.shape_35.setTransform(-55.5,9.5);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-35.6,49.8).lineTo(26.8,-55).lineTo(35.6,-54.5).lineTo(-28.6,55).closePath();
    this.shape_36.setTransform(-78.1,9.5);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-35.6,49.8).lineTo(26.8,-55).lineTo(35.6,-54.5).lineTo(-28.6,55).closePath();
    this.shape_37.setTransform(-78.1,9.5);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(26.8,-55).lineTo(35.9,-55).lineTo(-28.8,55).lineTo(-35.8,49.8).closePath();
    this.shape_38.setTransform(-102,9.5);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-35.8,49.8).lineTo(26.8,-55).lineTo(35.8,-55).lineTo(-28.8,55).closePath();
    this.shape_39.setTransform(-102,9.5);

    var maskedShapeInstanceList = [this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2,p:{x:131.5}},{t:this.shape_1}]}).to({state:[{t:this.shape_2,p:{x:130.8}},{t:this.shape_3}]},1).to({state:[{t:this.shape_5},{t:this.shape_4}]},1).to({state:[{t:this.shape_7},{t:this.shape_6}]},1).to({state:[{t:this.shape_9},{t:this.shape_8}]},1).to({state:[{t:this.shape_11},{t:this.shape_10}]},1).to({state:[{t:this.shape_13},{t:this.shape_12}]},1).to({state:[{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_17},{t:this.shape_16}]},1).to({state:[{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_21},{t:this.shape_20}]},1).to({state:[{t:this.shape_23},{t:this.shape_22}]},1).to({state:[{t:this.shape_25},{t:this.shape_24}]},1).to({state:[{t:this.shape_27},{t:this.shape_26}]},1).to({state:[{t:this.shape_29},{t:this.shape_28}]},1).to({state:[{t:this.shape_31},{t:this.shape_30}]},1).to({state:[{t:this.shape_33},{t:this.shape_32}]},1).to({state:[{t:this.shape_35},{t:this.shape_34}]},1).to({state:[{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_39},{t:this.shape_38}]},1).wait(27));

    // 1
    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(21.8,-57.3).lineTo(38.7,-47.5).lineTo(-21.9,57.3).lineTo(-38.7,47.5).closePath();
    this.shape_40.setTransform(101.7,11.8);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-38.7,47.6).lineTo(21.9,-57.3).lineTo(38.7,-47.6).lineTo(-21.9,57.3).closePath();
    this.shape_41.setTransform(101.7,11.8);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-38.8,47.5).lineTo(21.8,-57.3).lineTo(38.8,-47.5).lineTo(-21.8,57.3).closePath();
    this.shape_42.setTransform(101,11.8);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-38.8,47.6).lineTo(21.8,-57.3).lineTo(38.8,-47.6).lineTo(-21.8,57.3).closePath();
    this.shape_43.setTransform(101,11.8);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-39,47.5).lineTo(21.8,-57.3).lineTo(39,-47.5).lineTo(-21.8,57.3).closePath();
    this.shape_44.setTransform(99,11.7);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-38.9,47.5).lineTo(21.8,-57.3).lineTo(38.9,-47.5).lineTo(-21.7,57.3).closePath();
    this.shape_45.setTransform(99,11.7);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-39.3,47.5).lineTo(21.9,-57.4).lineTo(39.3,-47.6).lineTo(-21.7,57.4).closePath();
    this.shape_46.setTransform(95.5,11.7);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-39.3,47.5).lineTo(21.9,-57.4).lineTo(39.3,-47.6).lineTo(-21.6,57.4).closePath();
    this.shape_47.setTransform(95.5,11.7);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-39.8,47.5).lineTo(21.9,-57.5).lineTo(39.8,-47.7).lineTo(-21.5,57.5).closePath();
    this.shape_48.setTransform(90.7,11.6);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-39.8,47.5).lineTo(21.9,-57.4).lineTo(39.8,-47.7).lineTo(-21.5,57.4).closePath();
    this.shape_49.setTransform(90.7,11.6);

    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-40.4,47.5).lineTo(22,-57.5).lineTo(40.4,-47.8).lineTo(-21.3,57.5).closePath();
    this.shape_50.setTransform(84.5,11.5);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-40.4,47.5).lineTo(22,-57.6).lineTo(40.4,-47.8).lineTo(-21.3,57.5).closePath();
    this.shape_51.setTransform(84.5,11.5);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-41.1,47.4).lineTo(22,-57.6).lineTo(41.1,-47.9).lineTo(-21.1,57.6).closePath();
    this.shape_52.setTransform(76.9,11.4);

    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-41.1,47.4).lineTo(22,-57.7).lineTo(41.1,-47.9).lineTo(-21,57.6).closePath();
    this.shape_53.setTransform(76.9,11.4);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-42,47.3).lineTo(22.1,-57.8).lineTo(42,-48).lineTo(-20.7,57.8).closePath();
    this.shape_54.setTransform(67.9,11.3);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-42,47.3).lineTo(22.1,-57.8).lineTo(42,-48).lineTo(-20.7,57.8).closePath();
    this.shape_55.setTransform(67.9,11.3);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-43,47.3).lineTo(22.2,-57.9).lineTo(43,-48.1).lineTo(-20.4,57.9).closePath();
    this.shape_56.setTransform(57.6,11.1);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-43,47.3).lineTo(22.2,-57.9).lineTo(43,-48.1).lineTo(-20.4,57.9).closePath();
    this.shape_57.setTransform(57.6,11.1);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-44.1,47.2).lineTo(22.3,-58.1).lineTo(44.1,-48.2).lineTo(-20,58.1).closePath();
    this.shape_58.setTransform(45.9,11);

    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-44.1,47.2).lineTo(22.3,-58.1).lineTo(44.1,-48.2).lineTo(-20,58.1).closePath();
    this.shape_59.setTransform(45.9,11);

    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-45.4,47.1).lineTo(22.4,-58.3).lineTo(45.4,-48.4).lineTo(-19.7,58.3).closePath();
    this.shape_60.setTransform(32.8,10.8);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-45.4,47.1).lineTo(22.4,-58.3).lineTo(45.4,-48.4).lineTo(-19.7,58.3).closePath();
    this.shape_61.setTransform(32.8,10.8);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-46.8,47).lineTo(22.5,-58.5).lineTo(46.8,-48.6).lineTo(-19.2,58.5).closePath();
    this.shape_62.setTransform(18.3,10.6);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-46.8,47).lineTo(22.5,-58.5).lineTo(46.8,-48.6).lineTo(-19.2,58.5).closePath();
    this.shape_63.setTransform(18.3,10.6);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-48.3,47).lineTo(22.6,-58.7).lineTo(48.3,-48.8).lineTo(-18.6,58.7).closePath();
    this.shape_64.setTransform(2.5,10.4);

    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-48.3,47).lineTo(22.6,-58.7).lineTo(48.3,-48.8).lineTo(-18.6,58.7).closePath();
    this.shape_65.setTransform(2.5,10.4);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-50,46.9).lineTo(22.8,-59).lineTo(50,-49).lineTo(-18.1,59).closePath();
    this.shape_66.setTransform(-14.7,10.1);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-50,46.9).lineTo(22.8,-59).lineTo(50,-49).lineTo(-18.1,58.9).closePath();
    this.shape_67.setTransform(-14.7,10.1);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-51.8,46.8).lineTo(22.9,-59.2).lineTo(51.8,-49.2).lineTo(-17.5,59.2).closePath();
    this.shape_68.setTransform(-33.3,9.9);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-51.8,46.8).lineTo(22.9,-59.2).lineTo(51.8,-49.2).lineTo(-17.5,59.2).closePath();
    this.shape_69.setTransform(-33.3,9.9);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-53.8,46.6).lineTo(23.1,-59.5).lineTo(53.7,-49.4).lineTo(-16.8,59.5).closePath();
    this.shape_70.setTransform(-53.3,9.6);

    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-53.8,46.6).lineTo(23.1,-59.5).lineTo(53.7,-49.4).lineTo(-16.8,59.5).closePath();
    this.shape_71.setTransform(-53.3,9.6);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-55.8,46.5).lineTo(23.2,-59.8).lineTo(55.8,-49.7).lineTo(-16.2,59.8).closePath();
    this.shape_72.setTransform(-74.7,9.3);

    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-55.8,46.5).lineTo(23.2,-59.8).lineTo(55.8,-49.7).lineTo(-16.2,59.8).closePath();
    this.shape_73.setTransform(-74.7,9.3);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-58,46.3).lineTo(23.4,-60.1).lineTo(58,-50).lineTo(-15.4,60.1).closePath();
    this.shape_74.setTransform(-97.4,9);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-58,46.3).lineTo(23.4,-60.1).lineTo(58,-50).lineTo(-15.4,60.1).closePath();
    this.shape_75.setTransform(-97.4,9);

    this.shape_76 = new cjs.Shape();
    this.shape_76.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-60.4,46.2).lineTo(23.6,-60.5).lineTo(60.4,-50.2).lineTo(-14.6,60.5).closePath();
    this.shape_76.setTransform(-121.6,8.6);

    this.shape_77 = new cjs.Shape();
    this.shape_77.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-60.4,46.2).lineTo(23.6,-60.4).lineTo(60.4,-50.3).lineTo(-14.6,60.4).closePath();
    this.shape_77.setTransform(-121.6,8.6);

    this.shape_78 = new cjs.Shape();
    this.shape_78.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(23.8,-60.8).lineTo(62.9,-50.5).lineTo(-13.8,60.8).lineTo(-62.8,46).closePath();
    this.shape_78.setTransform(-147,8.3);

    this.shape_79 = new cjs.Shape();
    this.shape_79.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-62.8,46).lineTo(23.8,-60.8).lineTo(62.8,-50.6).lineTo(-13.8,60.8).closePath();
    this.shape_79.setTransform(-147,8.3);

    var maskedShapeInstanceList = [this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68,this.shape_69,this.shape_70,this.shape_71,this.shape_72,this.shape_73,this.shape_74,this.shape_75,this.shape_76,this.shape_77,this.shape_78,this.shape_79];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_41},{t:this.shape_40}]}).to({state:[{t:this.shape_43},{t:this.shape_42}]},1).to({state:[{t:this.shape_45},{t:this.shape_44}]},1).to({state:[{t:this.shape_47},{t:this.shape_46}]},1).to({state:[{t:this.shape_49},{t:this.shape_48}]},1).to({state:[{t:this.shape_51},{t:this.shape_50}]},1).to({state:[{t:this.shape_53},{t:this.shape_52}]},1).to({state:[{t:this.shape_55},{t:this.shape_54}]},1).to({state:[{t:this.shape_57},{t:this.shape_56}]},1).to({state:[{t:this.shape_59},{t:this.shape_58}]},1).to({state:[{t:this.shape_61},{t:this.shape_60}]},1).to({state:[{t:this.shape_63},{t:this.shape_62}]},1).to({state:[{t:this.shape_65},{t:this.shape_64}]},1).to({state:[{t:this.shape_67},{t:this.shape_66}]},1).to({state:[{t:this.shape_69},{t:this.shape_68}]},1).to({state:[{t:this.shape_71},{t:this.shape_70}]},1).to({state:[{t:this.shape_73},{t:this.shape_72}]},1).to({state:[{t:this.shape_75},{t:this.shape_74}]},1).to({state:[{t:this.shape_77},{t:this.shape_76}]},1).to({state:[{t:this.shape_79},{t:this.shape_78}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-98.4,-59,196.8,94.3);


(lib.aniCoffee = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // lines
    this.instance = new lib.square();
    this.instance.parent = this;
    this.instance.setTransform(-125.9,-70);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({y:70.5},11,cjs.Ease.get(-1)).to({rotation:-30,x:-128.9,y:65.5},2,cjs.Ease.get(1)).to({rotation:-90,x:-132.9,y:70.5},3,cjs.Ease.get(1)).wait(27));

    // lines
    this.instance_1 = new lib.square();
    this.instance_1.parent = this;
    this.instance_1.setTransform(-92.4,-70);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:70.5},11,cjs.Ease.get(-1)).to({x:-93.4,y:65.5},2,cjs.Ease.get(1)).to({x:-94.4,y:70.5},3,cjs.Ease.get(1)).wait(27));

    // lines
    this.instance_2 = new lib.square();
    this.instance_2.parent = this;
    this.instance_2.setTransform(105,-70);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({x:96,y:70.5},11,cjs.Ease.get(-1)).to({x:92,y:65.5},2,cjs.Ease.get(1)).to({x:91,y:70.5},3,cjs.Ease.get(1)).to({x:90.5,y:69},2).to({x:89.5,y:70.5},4).wait(19));

    // lines
    this.instance_3 = new lib.square();
    this.instance_3.parent = this;
    this.instance_3.setTransform(125,-70);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:70.5},11,cjs.Ease.get(-1)).to({x:124,y:65.5},2,cjs.Ease.get(1)).to({x:123,y:70.5},3,cjs.Ease.get(1)).wait(27));

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.moveTo(-40.5,44.8).lineTo(-40.5,-44.8).lineTo(40.5,-44.8).lineTo(40.5,44.8).closePath();
    mask.setTransform(0,-32.8);

    // steam
    this.instance_4 = new lib.graSteam();
    this.instance_4.parent = this;
    this.instance_4.setTransform(-5.7,11);

    var maskedShapeInstanceList = [this.instance_4];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:-69.5},42).wait(1));

    // Layer 1
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(26.1,-18.3).curveTo(26,-15.6,25.8,-13.2).curveTo(34.2,-13.2,31.9,-0.9).curveTo(29.1,8.5,20.9,7).curveTo(19.7,9.6,18.3,11.9).curveTo(13.6,19.6,6.2,24.6).lineTo(-18,24.6).curveTo(-34.7,11.3,-36.1,-24.6).lineTo(26.1,-24.6).curveTo(26.2,-21.4,26.1,-18.3).curveTo(38.8,-18.4,35.4,0.1).curveTo(31.1,14.6,18.3,11.9).moveTo(25.8,-13.2).curveTo(24.8,-1.7,20.9,7);
    this.shape.setTransform(0,51.7);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(43));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-132.9,-77.3,265,154.6);


(lib.aniCam = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 5
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(2,-7.7).curveTo(3.9,-1.1,7.6,2.1).curveTo(1.7,4.3,-2,7.7).curveTo(-3.9,2.7,-7.6,-2).curveTo(-0.7,-5,2,-7.7).closePath();
    this.shape.setTransform(37.3,-32);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-7.6,-2).curveTo(-0.7,-5,2.1,-7.7).curveTo(3.9,-1.1,7.7,2.1).curveTo(1.7,4.3,-2.1,7.7).curveTo(-3.8,2.7,-7.6,-2).closePath();
    this.shape_1.setTransform(37.3,-32);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.8,-1.6).curveTo(-4,-6.9,1.6,-13.8).curveTo(7.5,-2.8,13.8,1.7).curveTo(3.4,6.8,-1.6,13.8).curveTo(-6.6,4.8,-13.8,-1.6).closePath();
    this.shape_2.setTransform(37.3,-32);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-13.8,-1.6).curveTo(-4,-6.9,1.6,-13.8).curveTo(7.6,-2.8,13.8,1.7).curveTo(3.4,6.8,-1.6,13.8).curveTo(-6.6,4.8,-13.8,-1.6).closePath();
    this.shape_3.setTransform(37.3,-32);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-19.8,-1.2).curveTo(-7.4,-8.8,1.2,-19.8).curveTo(11.2,-4.4,19.8,1.3).curveTo(5,9.3,-1.2,19.8).curveTo(-9.4,6.9,-19.8,-1.2).closePath();
    this.shape_4.setTransform(37.3,-32);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-19.8,-1.2).curveTo(-7.3,-8.8,1.2,-19.8).curveTo(11.2,-4.4,19.8,1.3).curveTo(5.1,9.3,-1.2,19.8).curveTo(-9.4,6.9,-19.8,-1.2).closePath();
    this.shape_5.setTransform(37.3,-32);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-25.9,-0.9).curveTo(-10.6,-10.7,0.8,-25.9).curveTo(14.9,-6,25.9,0.8).curveTo(6.8,11.7,-0.8,25.9).curveTo(-12.1,9,-25.9,-0.9).closePath();
    this.shape_6.setTransform(37.3,-32);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-25.9,-0.9).curveTo(-10.7,-10.7,0.8,-25.9).curveTo(14.8,-6,25.9,0.8).curveTo(6.7,11.7,-0.8,25.9).curveTo(-12.1,9,-25.9,-0.9).closePath();
    this.shape_7.setTransform(37.3,-32);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-31.9,-0.5).curveTo(-14,-12.6,0.4,-32).curveTo(18.5,-7.6,32,0.4).curveTo(8.4,14.2,-0.4,32).curveTo(-14.9,11.1,-31.9,-0.5).closePath();
    this.shape_8.setTransform(37.3,-32);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-31.9,-0.5).curveTo(-14,-12.6,0.4,-32).curveTo(18.5,-7.6,32,0.4).curveTo(8.4,14.2,-0.4,32).curveTo(-15,11.1,-31.9,-0.5).closePath();
    this.shape_9.setTransform(37.3,-32);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1},{t:this.shape}]},13).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).to({state:[{t:this.shape_5},{t:this.shape_4}]},1).to({state:[{t:this.shape_7},{t:this.shape_6}]},1).to({state:[{t:this.shape_9},{t:this.shape_8}]},1).to({state:[]},1).wait(2));

    // Layer 6
    this.instance = new lib.aniSmack("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-79.6,-52.7,1,1,-150,0,0,6,-13.1);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({_off:true},6).wait(2));

    // body
    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(5.1,0).curveTo(5.1,2.1,3.6,3.6).curveTo(2.1,5.1,-0,5.1).curveTo(-2.1,5.1,-3.6,3.6).curveTo(-5.1,2.1,-5.1,0).curveTo(-5.1,-2.1,-3.6,-3.6).curveTo(-2.1,-5.1,-0,-5.1).curveTo(2.1,-5.1,3.6,-3.6).curveTo(5.1,-2.1,5.1,0).closePath().moveTo(1.7,-0.1).curveTo(1.7,0.6,1.2,1.1).curveTo(0.7,1.6,0,1.6).curveTo(-0.7,1.6,-1.2,1.1).curveTo(-1.6,0.6,-1.6,-0.1).curveTo(-1.6,-0.7,-1.2,-1.2).curveTo(-0.7,-1.7,0,-1.7).curveTo(0.7,-1.7,1.2,-1.2).curveTo(1.7,-0.7,1.7,-0.1).closePath();
    this.shape_10.setTransform(-49.5,-18.4,1,1,0,0,0,0.3,-8.7);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(25.6,6.5).curveTo(25.6,16.7,18.4,23.9).curveTo(11.2,31.1,1,31.1).curveTo(-9.2,31.1,-16.4,23.9).curveTo(-23.6,16.7,-23.6,6.5).curveTo(-23.6,-3.6,-16.4,-10.8).curveTo(-9.2,-18.1,1,-18.1).curveTo(11.2,-18.1,18.4,-10.8).curveTo(25.6,-3.6,25.6,6.5).closePath().moveTo(30.1,-39).lineTo(46.5,-39).lineTo(46.5,-26.2).lineTo(30.1,-26.2).closePath().moveTo(-25.2,-18.5).curveTo(-24.9,-18.9,-24.6,-19.1).curveTo(-14,-29.8,1,-29.8).curveTo(16,-29.8,26.6,-19.1).curveTo(26.9,-18.9,27.2,-18.5).lineTo(70,-18.5).lineTo(70,47.2).lineTo(-70,47.2).lineTo(-70,-18.5).lineTo(-70,-38.2).lineTo(-35.9,-38.2).lineTo(-35.9,-43.2).lineTo(-31.1,-43.2).moveTo(57,-38.2).lineTo(57,-43.6).lineTo(65.7,-43.6).lineTo(65.7,-38.2).lineTo(70,-38.2).lineTo(70,-18.5).moveTo(-19.9,-38.2).lineTo(-17.2,-38.2).lineTo(-13,-47.2).lineTo(50.2,-47.2).lineTo(53,-38.2).lineTo(57,-38.2).moveTo(65.7,-38.2).lineTo(57,-38.2).moveTo(-24.1,-43.2).lineTo(-19.9,-43.2).lineTo(-19.9,-38.2).lineTo(-35.9,-38.2).moveTo(-24.1,-43.2).lineTo(-31.1,-43.2).lineTo(-31.1,-46.4).lineTo(-24.1,-46.4).closePath().moveTo(-70,-18.5).lineTo(-25.2,-18.5).moveTo(27.2,-18.5).curveTo(37.2,-8.1,37.2,6.5).curveTo(37.2,21.5,26.6,32.1).curveTo(16,42.8,1,42.8).curveTo(-14,42.8,-24.6,32.1).curveTo(-35.2,21.5,-35.2,6.5).curveTo(-35.2,-8.1,-25.2,-18.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill("#EF3224").beginStroke().moveTo(27.3,14.4).lineTo(26.6,13.7).curveTo(16,3.1,1,3.1).curveTo(-14,3.1,-24.7,13.7).lineTo(-25.3,14.4).lineTo(-70,14.4).lineTo(-70,-5.4).lineTo(-35.9,-5.4).lineTo(-19.9,-5.4).lineTo(-17.3,-5.4).lineTo(-13,-14.4).lineTo(50.2,-14.4).lineTo(53,-5.4).lineTo(57.1,-5.4).lineTo(65.6,-5.4).lineTo(70,-5.4).lineTo(70,14.4).closePath().moveTo(30.1,6.6).lineTo(46.5,6.6).lineTo(46.5,-6.1).lineTo(30.1,-6.1).closePath();
    this.shape_12.setTransform(0,-32.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]}).to({state:[]},18).wait(2));

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().moveTo(-17.4,17.4).curveTo(-24.6,10.2,-24.6,0).curveTo(-24.6,-10.1,-17.4,-17.4).curveTo(-10.2,-24.6,-0,-24.6).curveTo(10.2,-24.6,17.4,-17.4).curveTo(24.6,-10.1,24.6,0).curveTo(24.6,10.2,17.4,17.4).curveTo(10.2,24.6,-0,24.6).curveTo(-10.2,24.6,-17.4,17.4).closePath();

    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:1,y:6.5}).wait(18).to({graphics:null,x:0,y:0}).wait(2));

    // flare
    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(9.9,-37.8).lineTo(22.6,-31.9).lineTo(-9.9,37.8).lineTo(-22.6,31.9).closePath();
    this.shape_13.setTransform(32.7,19.5);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-22.6,31.9).lineTo(9.9,-37.8).lineTo(22.6,-31.9).lineTo(-9.9,37.8).closePath();
    this.shape_14.setTransform(32.7,19.5);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-22.6,31.9).lineTo(9.9,-37.8).lineTo(22.6,-31.9).lineTo(-9.9,37.8).closePath();
    this.shape_15.setTransform(32.1,19.4);

    var maskedShapeInstanceList = [this.shape_13,this.shape_14,this.shape_15];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14,p:{x:32.7,y:19.5}},{t:this.shape_13,p:{x:32.7,y:19.5}}]}).to({state:[{t:this.shape_14,p:{x:32.1,y:19.4}},{t:this.shape_15,p:{x:32.1,y:19.4}}]},1).to({state:[{t:this.shape_14,p:{x:30.3,y:19}},{t:this.shape_15,p:{x:30.3,y:19}}]},1).to({state:[{t:this.shape_14,p:{x:27.3,y:18.4}},{t:this.shape_15,p:{x:27.3,y:18.4}}]},1).to({state:[{t:this.shape_14,p:{x:23.1,y:17.6}},{t:this.shape_15,p:{x:23.1,y:17.6}}]},1).to({state:[{t:this.shape_14,p:{x:17.7,y:16.5}},{t:this.shape_15,p:{x:17.7,y:16.5}}]},1).to({state:[{t:this.shape_14,p:{x:11.2,y:15.2}},{t:this.shape_15,p:{x:11.2,y:15.2}}]},1).to({state:[{t:this.shape_14,p:{x:3.4,y:13.7}},{t:this.shape_15,p:{x:3.4,y:13.7}}]},1).to({state:[{t:this.shape_14,p:{x:-5.5,y:11.9}},{t:this.shape_15,p:{x:-5.5,y:11.9}}]},1).to({state:[{t:this.shape_14,p:{x:-15.7,y:9.9}},{t:this.shape_15,p:{x:-15.7,y:9.9}}]},1).to({state:[{t:this.shape_14,p:{x:-27,y:7.7}},{t:this.shape_15,p:{x:-27,y:7.7}}]},1).to({state:[{t:this.shape_14,p:{x:-39.6,y:5.2}},{t:this.shape_13,p:{x:-39.6,y:5.2}}]},1).to({state:[]},7).wait(2));

    // btn
    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-10.6,3.6).lineTo(-10.6,-3.6).lineTo(10.6,-3.6).lineTo(10.6,3.6);
    this.shape_16.setTransform(-52,-41.9);

    this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(9).to({y:-41.6},0).wait(1).to({y:-40.9},0).wait(1).to({y:-39.6},0).wait(1).to({y:-37.9},0).wait(1).to({y:-38.4},0).wait(1).to({y:-39.4},0).wait(1).to({y:-40.3},0).wait(1).to({y:-41},0).wait(1).to({y:-41.5},0).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71,-48.2,142,96.5);


(lib.aniBaggage = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 4
    this.instance = new lib.aniSmack("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-22.6,-170.2,1,1,0,0,0,0,-13.5);

    this.instance_1 = new lib.aniSmack("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-164.5,-170.2,1,1,0,0,180,0,-13.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1},{t:this.instance}]},6).wait(18));

    // Layer 3
    this.instance_2 = new lib.aniSmack("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-23.2,-3,1,1,0,0,180,0,-13.5);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).wait(5).to({regY:-13.4,scaleX:0.8,scaleY:0.8,skewY:0,x:163.7,y:-5.3},0).wait(14));

    // bag1
    this.instance_3 = new lib.bag1("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-94,-125.1,1,1,0,0,0,-0.1,-125.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24));

    // bag2
    this.instance_4 = new lib.bag2();
    this.instance_4.parent = this;
    this.instance_4.setTransform(155.7,-5,1,1,7.2,0,0,84.7,0);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleY:0.98,rotation:0,y:0},5,cjs.Ease.get(-1)).to({regY:0.1,scaleY:1,rotation:-4,y:-11.9},2,cjs.Ease.get(1)).to({regY:0,rotation:0,y:0},4,cjs.Ease.get(-0.97)).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-156.7,-191.2,330.3,192.2);


(lib.ani = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // 11
    this.instance = new lib.Symbol();
    this.instance.parent = this;
    this.instance.setTransform(-88.5,5.5,0.5,0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:1.1,scaleY:1.1,x:-118.5},5,cjs.Ease.get(-1)).to({x:-127.5},9,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-92,2,6.9,6.9);


(lib.aniMeal = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 4
    this.instance = new lib.anio("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-24.6,-57.5,0.8,0.8,-11.5);

    this.instance_1 = new lib.anio("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-24.5,-57.5,0.8,0.8,-106.5,0,0,-0.1,0.1);

    this.instance_2 = new lib.anio("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(-44.5,-88.5,0.8,0.8,161);

    this.instance_3 = new lib.anio("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(-24.6,-57.6,0.8,0.8,71);

    this.instance_4 = new lib.ani("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(-33,-55.2,1,1,-64,0,0,-0.1,-0.1);

    this.instance_5 = new lib.ani("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(-18,-60.7,1,1,116,0,0,-0.1,-0.1);

    this.instance_6 = new lib.ani("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(-45.8,-12.6,1,1,-148.3);

    this.instance_7 = new lib.ani("synched",0,false);
    this.instance_7.parent = this;
    this.instance_7.setTransform(-28.2,-65.5,1,1,26,0,0,-0.1,-0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},8).wait(18));

    // star
    this.instance_8 = new lib.aniStar("synched",0,false);
    this.instance_8.parent = this;
    this.instance_8.setTransform(2.5,-1.2,1,1,-159.3,0,0,-0.1,0.1);

    this.instance_9 = new lib.aniStar("synched",0,false);
    this.instance_9.parent = this;
    this.instance_9.setTransform(-10.1,-56.7,1,1,149.5);

    this.instance_10 = new lib.aniStar("synched",0,false);
    this.instance_10.parent = this;
    this.instance_10.setTransform(-36.6,-22.1,1,1,-24.2);

    this.instance_11 = new lib.aniStar("synched",0,false);
    this.instance_11.parent = this;
    this.instance_11.setTransform(-33.1,-89.6,1,1,41.2);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8}]},4).wait(22));

    // cultery
    this.instance_12 = new lib.graCultery();
    this.instance_12.parent = this;
    this.instance_12.setTransform(7.1,42.1,1,1,15,0,0,-80.3,93.8);

    this.timeline.addTween(cjs.Tween.get(this.instance_12).to({regX:-80.4,rotation:-2.2,x:2.4,y:45.1},4,cjs.Ease.get(-1)).to({rotation:0,x:0,y:44.1},3).wait(19));

    // rice
    this.instance_13 = new lib.graRice();
    this.instance_13.parent = this;
    this.instance_13.setTransform(-16,44.2,1,1,-15,0,0,27.9,44.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_13).to({rotation:3.5,x:-4.1,y:44.1},4,cjs.Ease.get(-1)).to({regX:28,rotation:0,x:0},3).wait(19));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-156.3,-112.6,301.5,142.4);


(lib.aniFireworkSpark = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 6
    this.instance = new lib.ani("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(-2.2,1.5,0.5,0.5,-90,0,0,-0.1,-0.1);

    this.instance_1 = new lib.ani("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(-2.2,-2.8,0.5,0.5,0,0,0,-0.1,-0.1);

    this.instance_2 = new lib.ani("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(2.8,2.8,0.5,0.5,180,0,0,-0.1,-0.1);

    this.instance_3 = new lib.ani("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(2.8,-8.5,0.5,0.5,90,0,0,-0.1,-0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},8).to({state:[]},7).wait(15));

    // Layer 7
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(3.5,0).curveTo(3.5,1.4,2.5,2.5).curveTo(1.4,3.5,0,3.5).curveTo(-1.4,3.5,-2.5,2.5).curveTo(-3.5,1.4,-3.5,0).curveTo(-3.5,-1.4,-2.5,-2.5).curveTo(-1.4,-3.5,0,-3.5).curveTo(1.4,-3.5,2.5,-2.5).curveTo(3.5,-1.4,3.5,0).closePath();

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-4.7,0).curveTo(-4.7,-1.9,-3.3,-3.3).curveTo(-1.9,-4.7,0,-4.7).curveTo(1.9,-4.7,3.3,-3.3).curveTo(4.7,-1.9,4.7,0).curveTo(4.7,1.9,3.3,3.3).curveTo(1.9,4.7,0,4.7).curveTo(-1.9,4.7,-3.3,3.3).curveTo(-4.7,1.9,-4.7,0).closePath();

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.8,0).curveTo(-5.8,-2.5,-4.2,-4.2).curveTo(-2.5,-5.8,0,-5.8).curveTo(2.5,-5.8,4.2,-4.2).curveTo(5.8,-2.5,5.8,0).curveTo(5.8,2.5,4.2,4.2).curveTo(2.5,5.8,0,5.8).curveTo(-2.5,5.8,-4.2,4.2).curveTo(-5.8,2.5,-5.8,0).closePath();

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7,0).curveTo(-7,-3,-5,-5).curveTo(-3,-7,0,-7).curveTo(3,-7,5,-5).curveTo(7,-3,7,0).curveTo(7,3,5,5).curveTo(3,7,0,7).curveTo(-3,7,-5,5).curveTo(-7,3,-7,0).closePath();

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-8.2,0).curveTo(-8.2,-3.5,-5.8,-5.8).curveTo(-3.5,-8.2,0,-8.2).curveTo(3.5,-8.2,5.8,-5.8).curveTo(8.2,-3.5,8.2,0).curveTo(8.2,3.5,5.8,5.8).curveTo(3.5,8.2,0,8.2).curveTo(-3.5,8.2,-5.8,5.8).curveTo(-8.2,3.5,-8.2,0).closePath();

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.4,0).curveTo(-9.4,-4,-6.7,-6.7).curveTo(-4,-9.4,0,-9.4).curveTo(4,-9.4,6.8,-6.7).curveTo(9.4,-4,9.4,0).curveTo(9.4,4,6.8,6.8).curveTo(4,9.4,0,9.4).curveTo(-4,9.4,-6.7,6.8).curveTo(-9.4,4,-9.4,0).closePath();

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-10.7,0).curveTo(-10.7,-4.5,-7.6,-7.6).curveTo(-4.5,-10.7,0,-10.7).curveTo(4.5,-10.7,7.6,-7.6).curveTo(10.7,-4.5,10.7,0).curveTo(10.7,4.5,7.6,7.6).curveTo(4.5,10.7,0,10.7).curveTo(-4.5,10.7,-7.6,7.6).curveTo(-10.7,4.5,-10.7,0).closePath();

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-11.8,0).curveTo(-11.8,-5,-8.4,-8.4).curveTo(-5,-11.8,0,-11.8).curveTo(5,-11.8,8.4,-8.4).curveTo(11.8,-5,11.8,0).curveTo(11.8,5,8.4,8.4).curveTo(5,11.8,0,11.8).curveTo(-5,11.8,-8.4,8.4).curveTo(-11.8,5,-11.8,0).closePath();

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.1,0).curveTo(-13.1,-5.5,-9.3,-9.3).curveTo(-5.5,-13.1,0,-13.1).curveTo(5.5,-13.1,9.3,-9.3).curveTo(13.1,-5.5,13.1,0).curveTo(13.1,5.5,9.3,9.3).curveTo(5.5,13.1,0,13.1).curveTo(-5.5,13.1,-9.3,9.3).curveTo(-13.1,5.5,-13.1,0).closePath();

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-14.2,0).curveTo(-14.2,-6,-10.2,-10.2).curveTo(-6,-14.2,0,-14.2).curveTo(6,-14.2,10.2,-10.2).curveTo(14.2,-6,14.2,0).curveTo(14.2,6,10.2,10.2).curveTo(6,14.2,0,14.2).curveTo(-6,14.2,-10.2,10.2).curveTo(-14.2,6,-14.2,0).closePath();

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(15.4,0).curveTo(15.4,6.5,11,11).curveTo(6.5,15.4,0,15.4).curveTo(-6.5,15.4,-11,11).curveTo(-15.4,6.5,-15.4,0).curveTo(-15.4,-6.5,-11,-11).curveTo(-6.5,-15.4,0,-15.4).curveTo(6.5,-15.4,11,-11).curveTo(15.4,-6.5,15.4,0).closePath();

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},1).wait(15));

    // Layer 3
    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill("#EF3224").beginStroke().moveTo(-2.3,2.2).curveTo(-3.2,1.3,-3.2,0).curveTo(-3.2,-1.3,-2.3,-2.3).curveTo(-1.3,-3.2,0,-3.2).curveTo(1.3,-3.2,2.2,-2.3).curveTo(3.2,-1.3,3.1,0).curveTo(3.2,1.3,2.2,2.2).curveTo(1.3,3.2,0,3.1).curveTo(-1.3,3.2,-2.3,2.2).closePath();

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill("#EF3224").beginStroke().moveTo(-2.4,2.4).curveTo(-3.4,1.4,-3.4,0).curveTo(-3.4,-1.4,-2.4,-2.4).curveTo(-1.4,-3.4,0,-3.4).curveTo(1.4,-3.4,2.4,-2.4).curveTo(3.4,-1.4,3.4,0).curveTo(3.4,1.4,2.4,2.4).curveTo(1.4,3.4,0,3.4).curveTo(-1.4,3.4,-2.4,2.4).closePath();

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill("#EF3224").beginStroke().moveTo(-3,3).curveTo(-4.2,1.7,-4.1,0).curveTo(-4.2,-1.7,-3,-3).curveTo(-1.7,-4.2,0,-4.1).curveTo(1.7,-4.2,3,-3).curveTo(4.2,-1.7,4.1,0).curveTo(4.2,1.7,3,3).curveTo(1.7,4.2,0,4.1).curveTo(-1.7,4.2,-3,3).closePath();

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill("#EF3224").beginStroke().moveTo(-3.9,3.9).curveTo(-5.5,2.3,-5.5,0).curveTo(-5.5,-2.3,-3.9,-3.9).curveTo(-2.3,-5.5,0,-5.5).curveTo(2.3,-5.5,3.9,-3.9).curveTo(5.5,-2.3,5.5,0).curveTo(5.5,2.3,3.9,3.9).curveTo(2.3,5.5,0,5.5).curveTo(-2.3,5.5,-3.9,3.9).closePath();

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill("#EF3224").beginStroke().moveTo(-5.1,5.1).curveTo(-7.3,3,-7.3,0).curveTo(-7.3,-3,-5.1,-5.1).curveTo(-3,-7.3,0,-7.3).curveTo(3,-7.3,5.1,-5.1).curveTo(7.3,-3,7.3,0).curveTo(7.3,3,5.1,5.1).curveTo(3,7.3,0,7.3).curveTo(-3,7.3,-5.1,5.1).closePath();

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill("#EF3224").beginStroke().moveTo(-6.8,6.8).curveTo(-9.5,4,-9.5,0).curveTo(-9.5,-4,-6.8,-6.8).curveTo(-4,-9.5,0,-9.5).curveTo(4,-9.5,6.8,-6.8).curveTo(9.5,-4,9.5,0).curveTo(9.5,4,6.8,6.8).curveTo(4,9.5,0,9.5).curveTo(-4,9.5,-6.8,6.8).closePath();

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill("#EF3224").beginStroke().moveTo(-8.7,8.7).curveTo(-12.4,5.1,-12.3,0).curveTo(-12.4,-5.1,-8.7,-8.7).curveTo(-5.1,-12.4,0,-12.3).curveTo(5.1,-12.4,8.7,-8.7).curveTo(12.4,-5.1,12.3,0).curveTo(12.4,5.1,8.7,8.7).curveTo(5.1,12.4,0,12.3).curveTo(-5.1,12.4,-8.7,8.7).closePath();

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill("#EF3224").beginStroke().moveTo(-11.1,11.1).curveTo(-15.7,6.5,-15.7,0).curveTo(-15.7,-6.5,-11.1,-11.1).curveTo(-6.5,-15.7,0,-15.7).curveTo(6.5,-15.7,11.1,-11.1).curveTo(15.7,-6.5,15.7,0).curveTo(15.7,6.5,11.1,11.1).curveTo(6.5,15.7,0,15.7).curveTo(-6.5,15.7,-11.1,11.1).closePath();

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill("#EF3224").beginStroke().moveTo(-13.8,13.8).curveTo(-19.5,8.1,-19.5,0).curveTo(-19.5,-8.1,-13.8,-13.8).curveTo(-8.1,-19.5,0,-19.5).curveTo(8.1,-19.5,13.8,-13.8).curveTo(19.5,-8.1,19.5,0).curveTo(19.5,8.1,13.8,13.8).curveTo(8.1,19.5,0,19.5).curveTo(-8.1,19.5,-13.8,13.8).closePath();

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill("#EF3224").beginStroke().moveTo(-16.8,16.8).curveTo(-23.8,9.8,-23.9,0).curveTo(-23.8,-9.8,-16.8,-16.8).curveTo(-9.8,-23.8,0,-23.9).curveTo(9.8,-23.8,16.8,-16.8).curveTo(23.8,-9.8,23.9,0).curveTo(23.8,9.8,16.8,16.8).curveTo(9.8,23.8,0,23.9).curveTo(-9.8,23.8,-16.8,16.8).closePath();

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill("#EF3224").beginStroke().moveTo(-20.3,20.2).curveTo(-28.7,11.9,-28.7,0).curveTo(-28.7,-11.9,-20.3,-20.3).curveTo(-11.9,-28.7,0,-28.7).curveTo(11.9,-28.7,20.2,-20.3).curveTo(28.7,-11.9,28.7,0).curveTo(28.7,11.9,20.2,20.2).curveTo(11.9,28.7,0,28.7).curveTo(-11.9,28.7,-20.3,20.2).closePath();

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill("#EF3224").beginStroke().moveTo(-24,24).curveTo(-34.1,14,-34,0).curveTo(-34.1,-14,-24,-24).curveTo(-14,-34.1,0,-34).curveTo(14,-34.1,24,-24).curveTo(34.1,-14,34,0).curveTo(34.1,14,24,24).curveTo(14,34.1,0,34).curveTo(-14,34.1,-24,24).closePath();

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill("#EF3224").beginStroke().moveTo(-28.2,28.2).curveTo(-40,16.5,-40,0).curveTo(-40,-16.5,-28.2,-28.2).curveTo(-16.5,-40,0,-40).curveTo(16.5,-40,28.2,-28.2).curveTo(40,-16.5,40,0).curveTo(40,16.5,28.2,28.2).curveTo(16.5,40,0,40).curveTo(-16.5,40,-28.2,28.2).closePath();

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},2).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[]},11).wait(5));

    // Layer 5
    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(34.7,-13.5).lineTo(24.6,-9.5).moveTo(15,-34.2).lineTo(10.6,-24.2).moveTo(9.4,24.5).lineTo(13.4,34.8).moveTo(34.2,15.1).lineTo(24,10.7).moveTo(-24.2,-10.6).lineTo(-34.1,-14.9).moveTo(-13.6,-34.8).lineTo(-9.7,-24.6).moveTo(-10.6,24.1).lineTo(-15,34.1).moveTo(-24.5,9.6).lineTo(-34.7,13.5);
    this.shape_24.setTransform(0,-0.1);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0.1,0.1).lineTo(0.1,37.3).moveTo(0.1,0.1).lineTo(26.4,26.4).moveTo(0.1,0.1).lineTo(37.3,0.1).moveTo(26.5,-26.4).lineTo(0.1,0.1).moveTo(0.1,-37.3).lineTo(0.1,0.1).moveTo(-26.4,-26.4).lineTo(0.1,0.1).lineTo(-26.2,26.4).moveTo(-37.3,0.1).lineTo(0.1,0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_25},{t:this.shape_24}]},12).to({state:[]},13).wait(5));

    // mask (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().moveTo(-3.7,3.7).curveTo(-5.2,2.2,-5.2,0).curveTo(-5.2,-2.2,-3.7,-3.7).curveTo(-2.2,-5.2,0,-5.2).curveTo(2.2,-5.2,3.7,-3.7).curveTo(5.2,-2.2,5.2,0).curveTo(5.2,2.2,3.7,3.7).curveTo(2.2,5.2,0,5.2).curveTo(-2.2,5.2,-3.7,3.7).closePath();
    var mask_graphics_1 = new cjs.Graphics().moveTo(-4,4).curveTo(-5.6,2.3,-5.6,0).curveTo(-5.6,-2.3,-4,-4).curveTo(-2.3,-5.6,0,-5.6).curveTo(2.3,-5.6,4,-4).curveTo(5.6,-2.3,5.6,0).curveTo(5.6,2.3,4,4).curveTo(2.3,5.6,0,5.6).curveTo(-2.3,5.6,-4,4).closePath();
    var mask_graphics_2 = new cjs.Graphics().moveTo(-4.9,4.9).curveTo(-6.9,2.9,-6.9,0).curveTo(-6.9,-2.9,-4.9,-4.9).curveTo(-2.9,-6.9,0,-6.9).curveTo(2.9,-6.9,4.9,-4.9).curveTo(6.9,-2.9,6.9,0).curveTo(6.9,2.9,4.9,4.9).curveTo(2.9,6.9,0,6.9).curveTo(-2.9,6.9,-4.9,4.9).closePath();
    var mask_graphics_3 = new cjs.Graphics().moveTo(-6.4,6.4).curveTo(-9,3.7,-9,0).curveTo(-9,-3.7,-6.4,-6.4).curveTo(-3.7,-9,0,-9).curveTo(3.7,-9,6.4,-6.4).curveTo(9,-3.7,9,0).curveTo(9,3.7,6.4,6.4).curveTo(3.7,9,0,9).curveTo(-3.7,9,-6.4,6.4).closePath();
    var mask_graphics_4 = new cjs.Graphics().moveTo(-8.5,8.5).curveTo(-11.9,4.9,-12,0).curveTo(-11.9,-5,-8.5,-8.5).curveTo(-5,-11.9,0,-12).curveTo(4.9,-11.9,8.5,-8.5).curveTo(11.9,-5,12,0).curveTo(11.9,4.9,8.5,8.5).curveTo(4.9,11.9,0,12).curveTo(-5,11.9,-8.5,8.5).closePath();
    var mask_graphics_5 = new cjs.Graphics().moveTo(-11.2,11.2).curveTo(-15.7,6.5,-15.8,0).curveTo(-15.7,-6.5,-11.2,-11.2).curveTo(-6.5,-15.7,0,-15.8).curveTo(6.5,-15.7,11.2,-11.2).curveTo(15.7,-6.5,15.8,0).curveTo(15.7,6.5,11.2,11.2).curveTo(6.5,15.7,0,15.8).curveTo(-6.5,15.7,-11.2,11.2).closePath();
    var mask_graphics_6 = new cjs.Graphics().moveTo(-14.4,14.4).curveTo(-20.3,8.5,-20.3,0).curveTo(-20.3,-8.4,-14.4,-14.4).curveTo(-8.4,-20.3,0,-20.3).curveTo(8.5,-20.3,14.4,-14.4).curveTo(20.3,-8.4,20.3,0).curveTo(20.3,8.5,14.4,14.4).curveTo(8.5,20.3,0,20.3).curveTo(-8.4,20.3,-14.4,14.4).closePath();
    var mask_graphics_7 = new cjs.Graphics().moveTo(-18.3,18.3).curveTo(-25.9,10.7,-25.8,0).curveTo(-25.9,-10.7,-18.3,-18.3).curveTo(-10.7,-25.9,0,-25.8).curveTo(10.7,-25.9,18.3,-18.3).curveTo(25.9,-10.7,25.8,0).curveTo(25.9,10.7,18.3,18.3).curveTo(10.7,25.9,0,25.8).curveTo(-10.7,25.9,-18.3,18.3).closePath();
    var mask_graphics_8 = new cjs.Graphics().moveTo(-22.8,22.8).curveTo(-32.2,13.3,-32.1,0).curveTo(-32.2,-13.3,-22.8,-22.8).curveTo(-13.3,-32.2,0,-32.1).curveTo(13.3,-32.2,22.8,-22.8).curveTo(32.2,-13.3,32.1,0).curveTo(32.2,13.3,22.8,22.8).curveTo(13.3,32.2,0,32.1).curveTo(-13.3,32.2,-22.8,22.8).closePath();
    var mask_graphics_9 = new cjs.Graphics().moveTo(-27.8,27.8).curveTo(-39.3,16.2,-39.3,0).curveTo(-39.3,-16.2,-27.8,-27.8).curveTo(-16.2,-39.3,0,-39.3).curveTo(16.2,-39.3,27.8,-27.8).curveTo(39.3,-16.2,39.3,0).curveTo(39.3,16.2,27.8,27.8).curveTo(16.2,39.3,0,39.3).curveTo(-16.2,39.3,-27.8,27.8).closePath();
    var mask_graphics_10 = new cjs.Graphics().moveTo(-33.5,33.5).curveTo(-47.3,19.6,-47.3,0).curveTo(-47.3,-19.6,-33.5,-33.5).curveTo(-19.6,-47.3,0,-47.3).curveTo(19.6,-47.3,33.5,-33.5).curveTo(47.3,-19.6,47.3,0).curveTo(47.3,19.6,33.5,33.5).curveTo(19.6,47.3,0,47.3).curveTo(-19.6,47.3,-33.5,33.5).closePath();
    var mask_graphics_11 = new cjs.Graphics().moveTo(-39.7,39.7).curveTo(-56.1,23.2,-56.2,0).curveTo(-56.1,-23.2,-39.7,-39.7).curveTo(-23.2,-56.1,0,-56.2).curveTo(23.2,-56.1,39.7,-39.7).curveTo(56.1,-23.2,56.2,0).curveTo(56.1,23.2,39.7,39.7).curveTo(23.2,56.1,0,56.2).curveTo(-23.2,56.1,-39.7,39.7).closePath();
    var mask_graphics_12 = new cjs.Graphics().moveTo(-46.5,46.5).curveTo(-65.9,27.2,-65.9,0).curveTo(-65.9,-27.2,-46.5,-46.5).curveTo(-27.2,-65.9,0,-65.9).curveTo(27.2,-65.9,46.5,-46.5).curveTo(65.9,-27.2,65.9,0).curveTo(65.9,27.2,46.5,46.5).curveTo(27.2,65.9,0,65.9).curveTo(-27.2,65.9,-46.5,46.5).closePath();

    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:0,y:0}).wait(1).to({graphics:mask_graphics_1,x:0,y:0}).wait(1).to({graphics:mask_graphics_2,x:0,y:0}).wait(1).to({graphics:mask_graphics_3,x:0,y:0}).wait(1).to({graphics:mask_graphics_4,x:0,y:0}).wait(1).to({graphics:mask_graphics_5,x:0,y:0}).wait(1).to({graphics:mask_graphics_6,x:0,y:0}).wait(1).to({graphics:mask_graphics_7,x:0,y:0}).wait(1).to({graphics:mask_graphics_8,x:0,y:0}).wait(1).to({graphics:mask_graphics_9,x:0,y:0}).wait(1).to({graphics:mask_graphics_10,x:0,y:0}).wait(1).to({graphics:mask_graphics_11,x:0,y:0}).wait(1).to({graphics:mask_graphics_12,x:0,y:0}).wait(13).to({graphics:null,x:0,y:0}).wait(5));

    // Layer 1
    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(34.7,-13.5).lineTo(24.6,-9.5).moveTo(15,-34.2).lineTo(10.6,-24.2).moveTo(9.4,24.5).lineTo(13.4,34.8).moveTo(34.2,15.1).lineTo(24,10.7).moveTo(-24.2,-10.6).lineTo(-34.1,-14.9).moveTo(-13.6,-34.8).lineTo(-9.7,-24.6).moveTo(-10.6,24.1).lineTo(-15,34.1).moveTo(-24.5,9.6).lineTo(-34.7,13.5);
    this.shape_26.setTransform(0,-0.1);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0.1,0.1).lineTo(0.1,37.3).moveTo(0.1,0.1).lineTo(26.4,26.4).moveTo(0.1,0.1).lineTo(37.3,0.1).moveTo(26.5,-26.4).lineTo(0.1,0.1).moveTo(0.1,-37.3).lineTo(0.1,0.1).moveTo(-26.4,-26.4).lineTo(0.1,0.1).lineTo(-26.2,26.4).moveTo(-37.3,0.1).lineTo(0.1,0.1);

    var maskedShapeInstanceList = [this.shape_26,this.shape_27];

    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).to({state:[]},25).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.2,-5.2,10.4,10.4);


(lib.ani_1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // Layer 29
    this.shape = new cjs.Shape();
    this.shape.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-6,0).lineTo(6,0);
    this.shape.setTransform(-111,0);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7.9,0).lineTo(7.9,0);
    this.shape_1.setTransform(-118.1,0);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.7,0).lineTo(9.7,0);
    this.shape_2.setTransform(-125.1,0);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-11.6,0).lineTo(11.6,0);
    this.shape_3.setTransform(-132.2,0);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.4,0).lineTo(13.4,0);
    this.shape_4.setTransform(-139.3,0);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-15.3,0).lineTo(15.3,0);
    this.shape_5.setTransform(-146.3,0);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-17.1,0).lineTo(17.1,0);
    this.shape_6.setTransform(-153.4,0);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-19,0).lineTo(19,0);
    this.shape_7.setTransform(-160.5,0);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-20.9,0).lineTo(20.9,0);
    this.shape_8.setTransform(-167.6,0);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-22.7,0).lineTo(22.7,0);
    this.shape_9.setTransform(-174.6,0);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-24.6,0).lineTo(24.6,0);
    this.shape_10.setTransform(-181.7,0);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-26.4,0).lineTo(26.4,0);
    this.shape_11.setTransform(-188.8,0);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-28.3,0).lineTo(28.3,0);
    this.shape_12.setTransform(-195.8,0);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-30.1,0).lineTo(30.1,0);
    this.shape_13.setTransform(-202.9,0);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-32,0).lineTo(32,0);
    this.shape_14.setTransform(-210,0);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},226).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[]},1).wait(24));

    // Layer 28
    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-6,0).lineTo(6,0);
    this.shape_15.setTransform(111,0);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-7.8,0).lineTo(7.9,0);
    this.shape_16.setTransform(117.9,0);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-9.7,0).lineTo(9.7,0);
    this.shape_17.setTransform(124.7,0);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-11.6,0).lineTo(11.6,0);
    this.shape_18.setTransform(131.6,0);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-13.4,0).lineTo(13.4,0);
    this.shape_19.setTransform(138.4,0);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-15.3,0).lineTo(15.3,0);
    this.shape_20.setTransform(145.3,0);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-17.2,0).lineTo(17.2,0);
    this.shape_21.setTransform(152.2,0);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-19,0).lineTo(19,0);
    this.shape_22.setTransform(159,0);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-20.8,0).lineTo(20.8,0);
    this.shape_23.setTransform(165.9,0);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-22.7,0).lineTo(22.7,0);
    this.shape_24.setTransform(172.7,0);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-24.6,0).lineTo(24.6,0);
    this.shape_25.setTransform(179.6,0);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-26.4,0).lineTo(26.4,0);
    this.shape_26.setTransform(186.4,0);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-28.3,0).lineTo(28.3,0);
    this.shape_27.setTransform(193.3,0);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-30.2,0).lineTo(30.2,0);
    this.shape_28.setTransform(200.2,0);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-32,0).lineTo(32,0);
    this.shape_29.setTransform(207,0);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_15}]},226).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[]},1).wait(24));

    // aniLike2
    this.instance = new lib.anio("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(1.1,0,0.8,0.8,-37.4);

    this.instance_1 = new lib.anio("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(1,0,0.8,0.8,-132.5);

    this.instance_2 = new lib.anio("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(1.1,0,0.8,0.8,135);

    this.instance_3 = new lib.anio("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(1,0,0.8,0.8,45);

    this.instance_4 = new lib.ani("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(-5.5,5.7,1,1,-90);

    this.instance_5 = new lib.ani("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(5.5,-5.7,1,1,90);

    this.instance_6 = new lib.ani("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(5.7,5.5,1,1,180);

    this.instance_7 = new lib.ani("synched",0,false);
    this.instance_7.parent = this;
    this.instance_7.setTransform(-5.7,-5.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},226).to({state:[]},15).wait(24));

    // aniLike
    this.instance_8 = new lib.graHeart();
    this.instance_8.parent = this;
    this.instance_8.setTransform(1.5,1.5,0.5,0.5);
    this.instance_8._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(222).to({_off:false},0).to({scaleX:1,scaleY:1},4,cjs.Ease.get(-1)).to({scaleX:1.1,scaleY:1.1},14,cjs.Ease.get(1)).to({_off:true},1).wait(24));

    // aniCam
    this.instance_9 = new lib.aniCam("single",0);
    this.instance_9.parent = this;
    this.instance_9.setTransform(0,35.1,1,1,-0.5,0,0,0,0.1);
    this.instance_9._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(241).to({_off:false},0).to({regX:0.1,regY:-0.1,rotation:-0.3,x:0.1,y:-4},4,cjs.Ease.get(-1)).to({regX:0,regY:0,rotation:0,x:0,y:0,mode:"synched",loop:false},2).wait(18));

    // aniGlasses
    this.instance_10 = new lib.aniGlasses();
    this.instance_10.parent = this;
    this.instance_10.setTransform(0,0,0.96,0.96);
    this.instance_10._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(198).to({_off:false},0).to({scaleX:1,scaleY:1},23).to({_off:true},1).wait(43));

    // aniBags
    this.instance_11 = new lib.aniBaggage("synched",0,false);
    this.instance_11.parent = this;
    this.instance_11.setTransform(0,0,1,1,0,0,0,0,-125.1);
    this.instance_11._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(181).to({_off:false},0).to({_off:true},17).wait(67));

    // aniCoffee
    this.instance_12 = new lib.aniCoffee();
    this.instance_12.parent = this;
    this.instance_12.setTransform(0,0,0.96,0.96);
    this.instance_12._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(161).to({_off:false},0).to({scaleX:1,scaleY:1},19).to({_off:true},1).wait(84));

    // aniMeal
    this.instance_13 = new lib.aniMeal("synched",0,false);
    this.instance_13.parent = this;
    this.instance_13.setTransform(0,0,1,1,0,0,0,0,-52);
    this.instance_13._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(139).to({_off:false},0).to({_off:true},22).wait(104));

    // plane
    this.instance_14 = new lib.Tween1("synched",0);
    this.instance_14.parent = this;
    this.instance_14.setTransform(103.2,0.1);
    this.instance_14._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(122).to({_off:false},0).to({x:208.7,y:-38.4},16).to({_off:true},1).wait(126));

    // plane
    this.instance_15 = new lib.Tween1("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(-243.8,-0.9);
    this.instance_15._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(122).to({_off:false},0).to({x:-138.3,y:-39.4},16).to({_off:true},1).wait(126));

    // globe
    this.instance_16 = new lib.aniGlobe();
    this.instance_16.parent = this;
    this.instance_16.setTransform(0,0,0.7,0.7);
    this.instance_16._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(122).to({_off:false},0).to({scaleX:1.1,scaleY:1.1},4,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},3).to({_off:true},10).wait(126));

    // phase3
    this.instance_17 = new lib.anio("synched",0,false);
    this.instance_17.parent = this;
    this.instance_17.setTransform(1.1,0,0.8,0.8,-37.4);

    this.instance_18 = new lib.anio("synched",0,false);
    this.instance_18.parent = this;
    this.instance_18.setTransform(1,0,0.8,0.8,-132.5);

    this.instance_19 = new lib.anio("synched",0,false);
    this.instance_19.parent = this;
    this.instance_19.setTransform(1.1,0,0.8,0.8,135);

    this.instance_20 = new lib.anio("synched",0,false);
    this.instance_20.parent = this;
    this.instance_20.setTransform(1,0,0.8,0.8,45);

    this.instance_21 = new lib.ani("synched",0,false);
    this.instance_21.parent = this;
    this.instance_21.setTransform(-5.5,5.7,1,1,-90);

    this.instance_22 = new lib.ani("synched",0,false);
    this.instance_22.parent = this;
    this.instance_22.setTransform(5.5,-5.7,1,1,90);

    this.instance_23 = new lib.ani("synched",0,false);
    this.instance_23.parent = this;
    this.instance_23.setTransform(5.7,5.5,1,1,180);

    this.instance_24 = new lib.ani("synched",0,false);
    this.instance_24.parent = this;
    this.instance_24.setTransform(-5.7,-5.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_24},{t:this.instance_23},{t:this.instance_22},{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17}]},124).to({state:[]},15).wait(126));

    // lines
    this.instance_25 = new lib.aniWaves();
    this.instance_25.parent = this;
    this.instance_25.setTransform(89.2,0);

    this.instance_26 = new lib.aniWaves();
    this.instance_26.parent = this;
    this.instance_26.setTransform(-169.8,0);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_26},{t:this.instance_25}]},105).to({state:[]},17).wait(143));

    // phase2
    this.instance_27 = new lib.aniPhone();
    this.instance_27.parent = this;
    this.instance_27.setTransform(0,0,0.88,0.88);
    this.instance_27._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(105).to({_off:false},0).to({scaleX:0.99,scaleY:0.99},14).to({scaleX:1,scaleY:1},2).to({_off:true},1).wait(143));

    // firework3
    this.instance_28 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_28.parent = this;
    this.instance_28.setTransform(-127.5,25.5);
    this.instance_28._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(88).to({_off:false},0).to({_off:true},17).wait(160));

    // firework2
    this.instance_29 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_29.parent = this;
    this.instance_29.setTransform(282.5,25.5);
    this.instance_29._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(36).to({_off:false},0).to({_off:true},20).wait(29).to({_off:false,x:122.5},0).to({_off:true},20).wait(160));

    // firework1
    this.instance_30 = new lib.aniFireworkSpark("synched",0,false);
    this.instance_30.parent = this;
    this.instance_30.setTransform(-280,-82.4);
    this.instance_30._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(8).to({_off:false},0).to({_off:true},25).wait(47).to({_off:false,x:0},0).to({_off:true},25).wait(160));

    // fireworkLine3
    this.shape_30 = new cjs.Shape();
    this.shape_30.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_30.setTransform(-127,152);

    this.shape_31 = new cjs.Shape();
    this.shape_31.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-12.8).lineTo(0,12.8);
    this.shape_31.setTransform(-127,137.5);

    this.shape_32 = new cjs.Shape();
    this.shape_32.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.5).lineTo(0,18.5);
    this.shape_32.setTransform(-127,123);

    this.shape_33 = new cjs.Shape();
    this.shape_33.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.2).lineTo(0,24.2);
    this.shape_33.setTransform(-127,108.5);

    this.shape_34 = new cjs.Shape();
    this.shape_34.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,30).lineTo(0,-30);
    this.shape_34.setTransform(-127,94);

    this.shape_35 = new cjs.Shape();
    this.shape_35.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.4).lineTo(0,24.4);
    this.shape_35.setTransform(-127,80.6);

    this.shape_36 = new cjs.Shape();
    this.shape_36.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.8).lineTo(0,18.8);
    this.shape_36.setTransform(-127,67.2);

    this.shape_37 = new cjs.Shape();
    this.shape_37.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_37.setTransform(-127,53.8);

    this.shape_38 = new cjs.Shape();
    this.shape_38.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-7.6).lineTo(0,7.6);
    this.shape_38.setTransform(-127,40.4);

    this.shape_39 = new cjs.Shape();
    this.shape_39.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_39.setTransform(-127,27);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_30}]},80).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[]},1).wait(175));

    // fireworkLine2
    this.shape_40 = new cjs.Shape();
    this.shape_40.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_40.setTransform(283,152);

    this.shape_41 = new cjs.Shape();
    this.shape_41.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-12.8).lineTo(0,12.8);
    this.shape_41.setTransform(283,137.5);

    this.shape_42 = new cjs.Shape();
    this.shape_42.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.5).lineTo(0,18.5);
    this.shape_42.setTransform(283,123);

    this.shape_43 = new cjs.Shape();
    this.shape_43.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.2).lineTo(0,24.2);
    this.shape_43.setTransform(283,108.5);

    this.shape_44 = new cjs.Shape();
    this.shape_44.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,30).lineTo(0,-30);
    this.shape_44.setTransform(283,94);

    this.shape_45 = new cjs.Shape();
    this.shape_45.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-24.4).lineTo(0,24.4);
    this.shape_45.setTransform(283,80.6);

    this.shape_46 = new cjs.Shape();
    this.shape_46.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-18.8).lineTo(0,18.8);
    this.shape_46.setTransform(283,67.2);

    this.shape_47 = new cjs.Shape();
    this.shape_47.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-13.2).lineTo(0,13.2);
    this.shape_47.setTransform(283,53.8);

    this.shape_48 = new cjs.Shape();
    this.shape_48.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-7.6).lineTo(0,7.6);
    this.shape_48.setTransform(283,40.4);

    this.shape_49 = new cjs.Shape();
    this.shape_49.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_49.setTransform(283,27);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_40,p:{x:283}}]},28).to({state:[{t:this.shape_41,p:{x:283}}]},1).to({state:[{t:this.shape_42,p:{x:283}}]},1).to({state:[{t:this.shape_43,p:{x:283}}]},1).to({state:[{t:this.shape_44,p:{x:283}}]},1).to({state:[{t:this.shape_45,p:{x:283}}]},1).to({state:[{t:this.shape_46,p:{x:283}}]},1).to({state:[{t:this.shape_47,p:{x:283}}]},1).to({state:[{t:this.shape_48,p:{x:283}}]},1).to({state:[{t:this.shape_49,p:{x:283}}]},1).to({state:[]},1).to({state:[{t:this.shape_40,p:{x:123}}]},39).to({state:[{t:this.shape_41,p:{x:123}}]},1).to({state:[{t:this.shape_42,p:{x:123}}]},1).to({state:[{t:this.shape_43,p:{x:123}}]},1).to({state:[{t:this.shape_44,p:{x:123}}]},1).to({state:[{t:this.shape_45,p:{x:123}}]},1).to({state:[{t:this.shape_46,p:{x:123}}]},1).to({state:[{t:this.shape_47,p:{x:123}}]},1).to({state:[{t:this.shape_48,p:{x:123}}]},1).to({state:[{t:this.shape_49,p:{x:123}}]},1).to({state:[]},1).wait(178));

    // fireworkLine1
    this.shape_50 = new cjs.Shape();
    this.shape_50.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,7).lineTo(0,-7);
    this.shape_50.setTransform(-280,152);

    this.shape_51 = new cjs.Shape();
    this.shape_51.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-17).lineTo(0,17);
    this.shape_51.setTransform(-280,114.5);

    this.shape_52 = new cjs.Shape();
    this.shape_52.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-27).lineTo(0,27);
    this.shape_52.setTransform(-280,77);

    this.shape_53 = new cjs.Shape();
    this.shape_53.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-37).lineTo(0,37);
    this.shape_53.setTransform(-280,39.5);

    this.shape_54 = new cjs.Shape();
    this.shape_54.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,47).lineTo(0,-47);
    this.shape_54.setTransform(-280,2);

    this.shape_55 = new cjs.Shape();
    this.shape_55.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-38).lineTo(0,38);
    this.shape_55.setTransform(-280,-15);

    this.shape_56 = new cjs.Shape();
    this.shape_56.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-29).lineTo(0,29);
    this.shape_56.setTransform(-280,-32);

    this.shape_57 = new cjs.Shape();
    this.shape_57.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-20).lineTo(0,20);
    this.shape_57.setTransform(-280,-49);

    this.shape_58 = new cjs.Shape();
    this.shape_58.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,-11).lineTo(0,11);
    this.shape_58.setTransform(-280,-66);

    this.shape_59 = new cjs.Shape();
    this.shape_59.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0,2).lineTo(0,-2);
    this.shape_59.setTransform(-280,-83);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_50,p:{x:-280}}]}).to({state:[{t:this.shape_51,p:{x:-280}}]},1).to({state:[{t:this.shape_52,p:{x:-280}}]},1).to({state:[{t:this.shape_53,p:{x:-280}}]},1).to({state:[{t:this.shape_54,p:{x:-280}}]},1).to({state:[{t:this.shape_55,p:{x:-280}}]},1).to({state:[{t:this.shape_56,p:{x:-280}}]},1).to({state:[{t:this.shape_57,p:{x:-280}}]},1).to({state:[{t:this.shape_58,p:{x:-280}}]},1).to({state:[{t:this.shape_59,p:{x:-280}}]},1).to({state:[]},1).to({state:[{t:this.shape_50,p:{x:0}}]},62).to({state:[{t:this.shape_51,p:{x:0}}]},1).to({state:[{t:this.shape_52,p:{x:0}}]},1).to({state:[{t:this.shape_53,p:{x:0}}]},1).to({state:[{t:this.shape_54,p:{x:0}}]},1).to({state:[{t:this.shape_55,p:{x:0}}]},1).to({state:[{t:this.shape_56,p:{x:0}}]},1).to({state:[{t:this.shape_57,p:{x:0}}]},1).to({state:[{t:this.shape_58,p:{x:0}}]},1).to({state:[{t:this.shape_59,p:{x:0}}]},1).to({state:[]},1).wait(183));

    // phase1
    this.instance_31 = new lib.mountains();
    this.instance_31.parent = this;
    this.instance_31.setTransform(-23.7,167.9,1,0.071);
    this.instance_31._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(72).to({_off:false},0).to({scaleY:1.1},3,cjs.Ease.get(-1)).to({scaleY:1},2).to({_off:true},28).wait(160));

    // Layer 3
    this.shape_60 = new cjs.Shape();
    this.shape_60.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(0.5,0).lineTo(-0.5,0);
    this.shape_60.setTransform(0,169);

    this.shape_61 = new cjs.Shape();
    this.shape_61.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-5.4,0).lineTo(5.4,0);
    this.shape_61.setTransform(0,168.9);

    this.shape_62 = new cjs.Shape();
    this.shape_62.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-10.3,0).lineTo(10.3,0);
    this.shape_62.setTransform(0,168.9);

    this.shape_63 = new cjs.Shape();
    this.shape_63.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-15.2,0).lineTo(15.2,0);
    this.shape_63.setTransform(0,168.9);

    this.shape_64 = new cjs.Shape();
    this.shape_64.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-20.1,0).lineTo(20.1,0);
    this.shape_64.setTransform(0,168.9);

    this.shape_65 = new cjs.Shape();
    this.shape_65.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-24.9,0).lineTo(24.9,0);
    this.shape_65.setTransform(0,168.8);

    this.shape_66 = new cjs.Shape();
    this.shape_66.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-29.9,0).lineTo(29.9,0);
    this.shape_66.setTransform(0,168.8);

    this.shape_67 = new cjs.Shape();
    this.shape_67.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-34.8,0).lineTo(34.8,0);
    this.shape_67.setTransform(0,168.8);

    this.shape_68 = new cjs.Shape();
    this.shape_68.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-39.6,0).lineTo(39.6,0);
    this.shape_68.setTransform(0,168.8);

    this.shape_69 = new cjs.Shape();
    this.shape_69.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-44.5,0).lineTo(44.5,0);
    this.shape_69.setTransform(0,168.7);

    this.shape_70 = new cjs.Shape();
    this.shape_70.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-49.4,0).lineTo(49.4,0);
    this.shape_70.setTransform(0,168.7);

    this.shape_71 = new cjs.Shape();
    this.shape_71.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-54.3,0).lineTo(54.3,0);
    this.shape_71.setTransform(0,168.7);

    this.shape_72 = new cjs.Shape();
    this.shape_72.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-59.2,0).lineTo(59.2,0);
    this.shape_72.setTransform(0,168.7);

    this.shape_73 = new cjs.Shape();
    this.shape_73.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-64,0).lineTo(64,0);
    this.shape_73.setTransform(0,168.6);

    this.shape_74 = new cjs.Shape();
    this.shape_74.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-69,0).lineTo(69,0);
    this.shape_74.setTransform(0,168.6);

    this.shape_75 = new cjs.Shape();
    this.shape_75.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-73.8,0).lineTo(73.8,0);
    this.shape_75.setTransform(0,168.6);

    this.shape_76 = new cjs.Shape();
    this.shape_76.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-78.8,0).lineTo(78.8,0);
    this.shape_76.setTransform(0,168.6);

    this.shape_77 = new cjs.Shape();
    this.shape_77.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-83.7,0).lineTo(83.7,0);
    this.shape_77.setTransform(0,168.5);

    this.shape_78 = new cjs.Shape();
    this.shape_78.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-88.5,0).lineTo(88.5,0);
    this.shape_78.setTransform(0,168.5);

    this.shape_79 = new cjs.Shape();
    this.shape_79.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-93.4,0).lineTo(93.4,0);
    this.shape_79.setTransform(0,168.5);

    this.shape_80 = new cjs.Shape();
    this.shape_80.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-98.3,0).lineTo(98.3,0);
    this.shape_80.setTransform(0,168.5);

    this.shape_81 = new cjs.Shape();
    this.shape_81.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-103.2,0).lineTo(103.2,0);
    this.shape_81.setTransform(0,168.4);

    this.shape_82 = new cjs.Shape();
    this.shape_82.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-108.1,0).lineTo(108.1,0);
    this.shape_82.setTransform(0,168.4);

    this.shape_83 = new cjs.Shape();
    this.shape_83.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-113,0).lineTo(113,0);
    this.shape_83.setTransform(0,168.4);

    this.shape_84 = new cjs.Shape();
    this.shape_84.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-117.8,0).lineTo(117.8,0);
    this.shape_84.setTransform(0,168.3);

    this.shape_85 = new cjs.Shape();
    this.shape_85.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-122.8,0).lineTo(122.8,0);
    this.shape_85.setTransform(0,168.3);

    this.shape_86 = new cjs.Shape();
    this.shape_86.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-127.7,0).lineTo(127.7,0);
    this.shape_86.setTransform(0,168.3);

    this.shape_87 = new cjs.Shape();
    this.shape_87.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-132.6,0).lineTo(132.6,0);
    this.shape_87.setTransform(0,168.3);

    this.shape_88 = new cjs.Shape();
    this.shape_88.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-137.4,0).lineTo(137.4,0);
    this.shape_88.setTransform(0,168.2);

    this.shape_89 = new cjs.Shape();
    this.shape_89.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-142.3,0).lineTo(142.3,0);
    this.shape_89.setTransform(0,168.2);

    this.shape_90 = new cjs.Shape();
    this.shape_90.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-147.2,0).lineTo(147.2,0);
    this.shape_90.setTransform(0,168.2);

    this.shape_91 = new cjs.Shape();
    this.shape_91.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-152.1,0).lineTo(152.1,0);
    this.shape_91.setTransform(0,168.2);

    this.shape_92 = new cjs.Shape();
    this.shape_92.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-157,0).lineTo(157,0);
    this.shape_92.setTransform(0,168.1);

    this.shape_93 = new cjs.Shape();
    this.shape_93.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-161.8,0).lineTo(161.8,0);
    this.shape_93.setTransform(0,168.1);

    this.shape_94 = new cjs.Shape();
    this.shape_94.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-166.8,0).lineTo(166.8,0);
    this.shape_94.setTransform(0,168.1);

    this.shape_95 = new cjs.Shape();
    this.shape_95.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-171.7,0).lineTo(171.7,0);
    this.shape_95.setTransform(0,168.1);

    this.shape_96 = new cjs.Shape();
    this.shape_96.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-176.6,0).lineTo(176.6,0);
    this.shape_96.setTransform(0,168);

    this.shape_97 = new cjs.Shape();
    this.shape_97.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-181.4,0).lineTo(181.4,0);
    this.shape_97.setTransform(0,168);

    this.shape_98 = new cjs.Shape();
    this.shape_98.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-186.3,0).lineTo(186.3,0);
    this.shape_98.setTransform(0,168);

    this.shape_99 = new cjs.Shape();
    this.shape_99.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(-191.2,0).lineTo(191.2,0);
    this.shape_99.setTransform(0,168);

    this.shape_100 = new cjs.Shape();
    this.shape_100.graphics.beginFill().beginStroke("#FFFFFF").setStrokeStyle(2,1,1).moveTo(196.1,0).lineTo(-196.1,0);
    this.shape_100.setTransform(0,167.9);

    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_60}]},28).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_100}]},4).to({state:[]},1).wait(192));

    // txtTitleLayer
    this.instance_32 = new lib.aniTitle("synched",0,false);
    this.instance_32.parent = this;

    this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(265));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-281,144,495.1,46.5);


// stage content:
(lib.teaser = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});

    // aniLayer
    this.instance = new lib.ani_1();
    this.instance.parent = this;
    this.instance.setTransform(350,181);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(419,525,495.1,46.5);
// library properties:
lib.properties = {
    id: '2A9F3D2A489349BFA6576CCDE5DDDE1F',
    width: 700,
    height: 400,
    fps: 24,
    color: "#EF3224",
    opacity: 1.00,
    manifest: [],
    preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};

an.compositions = an.compositions || {};
an.compositions['2A9F3D2A489349BFA6576CCDE5DDDE1F'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}

an.getComposition = function(id) {
    return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;