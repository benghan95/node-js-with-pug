var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function logo_init() {
    canvas = document.getElementById("masthead_canvas");
    anim_container = document.getElementById("masthead_animation_container");
    dom_overlay_container = document.getElementById("masthead_dom_overlay_container");
    var comp=AdobeAn.getComposition("51540C5377B641B7A6B703659A25C199");
    var lib=comp.getLibrary();
    handleComplete({},comp);
}
function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.logo();
    stage = new lib.Stage(canvas);
    stage.addChild(exportRoot);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    // Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
              var w = lib.properties.width, h = lib.properties.height;
              var iw = window.innerWidth, ih=window.innerHeight;
              var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
              if(isResp) {
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                  sRatio = lastS;
                }
                else if(!isScale) {
                  if(iw<w || ih<h)
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==1) {
                  sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==2) {
                  sRatio = Math.max(xRatio, yRatio);
                }
              }
              canvas.width = w*pRatio*sRatio;
              canvas.height = h*pRatio*sRatio;
              canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
              canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
              stage.scaleX = pRatio*sRatio;
              stage.scaleY = pRatio*sRatio;
              lastW = iw; lastH = ih; lastS = sRatio;
        }
    }
    if (window.matchMedia("(max-width: 399px)").matches) {
      makeResponsive(true,'both',true,1);
    }
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}