$(document).ready(function() {
  var lang        = $('input[name="lang"]').val();
  var hexhash     = $('input[name="hexhash"]').val();
  var thumbnailId = $('input[name="thumbnail_id"]').val();
  var videoId     = $('input[name="video_id"]').val();

  $('.btn-fbshare').on('click', function() {
    var post_text = "This is my travel personality! #halfabillion.";
    switch (lang) {
      case 'id':
        post_text = "Ini Karakter Petualangan Saya! #halfabillion";
        break;
      case 'th':
        post_text = "นี่คือลักษณะการเดินทางในแบบของฉัน! #halfabillion";
        break;
      case 'cn-zh':
        post_text = "这是我的#5亿乘客 旅行态度";
        break;
      case 'tw-zh':
        post_text = "這是我的精彩飛行紀錄！#halfabillion ";
        break;
      case 'hk-zh':
        post_text = "這是我的精彩飛行紀錄！#halfabillion ";
        break;
      case 'kr':
        post_text = "여러분의 여행 스타일 입니다 #halfabillion";
        break;
      case 'jp':
        post_text = "これが私らしい旅！#エアアジア5億人達成　#halfabillion";
        break;
      case 'vn':
        post_text = "Đây là Tính cách du lịch của tôi! #halfabillion";
        break;
      default:
        break;
    }

    FB.ui({
      method: 'share',
      href: 'https://halfabillion.airasia.com/' + lang + '/my-journey/' + hexhash,
      hashtag: '#halfabillion',
      caption: post_text,
    }, function(response){
      console.log(response);
      if (Array.isArray(response)) {
        $.ajax({
          url: '/' + lang + '/shared-facebook/' + hexhash,
          success: function(res) {
            if (res.status === 'success') {
              console.log(res.message);
            } else {
            }
          },
          error: function() {
            console.error("Error occurred. Please try again later.");
          }
        });
      } else {
      }
    });
  })

  $('#send-email').on('click', function(e) {
    e.preventDefault();

    $('#send-email').attr('disabled', true);
    $.ajax({
      url: '/' + lang + '/send-email',
      method: 'POST',
      data: {
        "language": lang,
        "hexhash": hexhash,
      },
      success: function(res) {
        if (res.status == 'success') {
          $('#send-email .fa-check').removeClass('hide');
          $('#send-email .fa-exclamation-triangle').addClass('hide');
        } else {
          $('#send-email .fa-exclamation-triangle').removeClass('hide');
          $('#send-email .fa-check').addClass('hide');
          $('#send-email').attr('disabled', false);
        }
        // alert("Email has been sent! Please check your inbox.")
      },
      error: function(err) {
        $('#send-email .fa-exclamation-triangle').removeClass('hide');
        $('#send-email .fa-check').addClass('hide');
        $('#send-email').attr('disabled', false);
        console.error("Error occurred. Please try again later.");
        console.error(err);
      }
    });
  })

  var meta_url         = $('input[name="meta_url"]').val();
  var meta_title       = $('input[name="meta_title"]').val();
  var meta_description = $('input[name="meta_description"]').val();
  var meta_image       = $('input[name="meta_image"]').val();

  var share_url = 'https://profitquery.com/add-to/weibo/?url=' + encodeURIComponent(meta_url) + '&title=' + encodeURIComponent(meta_title) + '&description=' + encodeURIComponent(meta_description) + '&img=' + encodeURIComponent(meta_image);
  // $('.btn-weiboshare').attr('href', share_url);
  $('.btn-weiboshare').attr('href', 'https://service.weibo.com/share/share.php?title=' + encodeURIComponent("@亚航之家 #5亿乘客# 这是我的旅行态度，你的呢？点击这里查看：") + '&url=' + encodeURIComponent("https://halfabillion.airasia.com/cn-zh") + '&text=' + encodeURIComponent(meta_description) + '&pic=' + encodeURIComponent(meta_image));

  $('.btn-download').on('click', function() {
    $.ajax({
      url: '/' + lang + '/downloaded/' + hexhash,
      success: function(res) {
        if (res.status === 'success') {
          console.log(res.message);
        } else {
        }
      },
      error: function() {
        console.error("Error occurred. Please try again later.");
      }
    });
  })

  // if( (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) ) {
  //   $('.btn-download').addClass('hide');
  // }

  // $('#personalised-section .video-container').on('click', function() {
  //   $(this).find('.img-wrapper').addClass('hide');
  //   $(this).find('video').get(0).play();
  // })

  // $(window).on("load resize scroll",function(e){
  //     $(".video-container video").css({'height': $(".video-container img").height()+'px'});
  // })

  // var video = document.getElementById("halfabillion-video");

  // $(video).on({
  //   mouseenter: function () {
  //     video.setAttribute("controls","controls")
  //   },
  //   mouseleave: function () {
  //     video.removeAttribute("controls");
  //   }
  // });
})