$(document).ready(function(){

  var lang = $('input[name="lang"]').val();

  var owl = $('.partners-carousel,.carousel-question');
  owl.owlCarousel({
      autoplay: true,
      items: 1,
      loop: true,
      center: true,
      onRefresh: function () {
          owl.find('div.owl-item').height('');
      },
      onRefreshed: function () {
          owl.find('div.owl-item').height(owl.height());
      }
  });

  var lineDrawing = anime({
    targets: [
      '#cloud-1 path',
      '#cloud-2 path',
      '#cloud-3 path',
      '#mountain-1 polyline',
      '#mountain-2 polyline',
      '#star-1 polygon',
      '#star-2 polygon',
      '#trees-1 line',
      '#trees-1 polyline',
      '#trees-2 line',
      '#trees-2 polyline',
      '#hill-1 path',
      '#hill-2 path',
      '#building-1 path',
      '#building-2 path',
      // '#landmark-a .shape path',
      // '#landmark-b .shape path',
    ],
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'easeInOutSine',
    duration: 2000,
    direction: 'alternate',
    loop: false
  });

  var windlineDrawing = anime({
    targets: [
      '#wind-1 #line-1',
      '#wind-2 #line-1',
    ],
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'easeInOutSine',
    duration: 3000,
    direction: 'alternate',
    loop: true
  });

  var fillPath = anime({
    targets: [
    '#hot-air-ballon-1 path',
    '#hot-air-ballon-2 path',
    '#building-1 path',
    '#building-2 path'
    ],
    opacity: {
      value: 1,
      duration: 2000
    },
    fill: '#FFFFFF',
    duration: 2000,
    easing: 'easeInOutSine',
    direction: 'alternate',
    loop: false
  });

  var wavefillPath = anime({
    targets: [
    '#stars-1 path',
    '#stars-2 path',
    '#wave-md-2 path',
    '#wave-md-1 path',
    '#wave-sm-2 path',
    '#wave-sm-1 path'
    ],
    fillOpacity: 1,
    duration: 5000,
    easing: 'easeInOutSine',
    loop: true
  });


  var star = anime ({
    targets: [
      '#star-1',
      '#star-2',
    ],
    rotate: 360,
    duration: 15000,
    loop: true,
    easing: 'linear'
  });

  var star1 = anime ({
    targets: [
      '#star-1',
      '#star-2',
    ],
    rotate: 360,
    duration: 4000,
    scale: 1.2,
    direction: 'alternate',
    loop: true
  });

  var ballon1Timeline = anime.timeline({
    duration: 2500,
    loop: true
  });

  ballon1Timeline
    .add({
      targets: ['#hot-air-ballon-1'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-1'],
      translateX: -30,
      translateY: -20,
      rotate: '5deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-1'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-1'],
      translateX: +30,
      translateY: +20,
      rotate: '-5deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-1'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    });

  var ballon2Timeline = anime.timeline({
    duration: 3000,
    loop: true
  });

  ballon2Timeline
    .add({
      targets: ['#hot-air-ballon-2'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-2'],
      translateX: 40,
      translateY: 20,
      rotate: '5deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-2'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-2'],
      translateY: -20,
      translateX: -40,
      rotate: '-5deg',
      easing: 'easeInOutSine',
    })
    .add({
      targets: ['#hot-air-ballon-2'],
      translateX: 0,
      translateY: 0,
      rotate: '0deg',
      easing: 'easeInOutSine',
    });

  // var bubble1 = anime({
  //   targets: '#bubble-1',
  //   scale: {
  //     value: 1.2,
  //     duration: 4000,
  //     delay: 800,
  //     easing: 'easeInOutQuart'
  //   },
  //   delay: 250, // All properties except 'scale' inherit 250ms delay
  //   loop: true
  // });

// var tree = anime({
//   targets: '#trees-1 polyline',
//   rotate: 10,
//   easing: 'easeOutQuad',
//   duration: 2000,
//   direction: 'alternate',
//   loop: true
// });

  // function confetti() {
  //    $.each($('.masthead-confetti'), function(){
  //       var confetticount = ($(this).width()/50)*10;
  //       for(var i = 0; i <= confetticount; i++) {
  //          $(this).append('<span class="particle c' + $.rnd(1,2) + '" style="top:' + $.rnd(10,50) + '%; left:' + $.rnd(0,100) + '%;width:' + $.rnd(6,8) + 'px; height:' + $.rnd(3,4) + 'px;animation-delay: ' + ($.rnd(0,30)/10) + 's;"></span>');
  //       }
  //    });
  // }

  // jQuery.rnd = function(m,n) {
  //       m = parseInt(m);
  //       n = parseInt(n);
  //       return Math.floor( Math.random() * (n - m + 1) ) + m;
  // }

  // setTimeout(function(){
  //   confetti();
  // }, 3000);

  if ($('#bubble-1').length) {
    var $cloud_1    = $('#bubble-1');
    var $cloud_2    = $('#bubble-2');
    var $container  = $('body');
    var container_w = $container.width();
    var container_h = $container.height();

    $(window).on('mousemove.parallax', function(event) {
      var pos_x = event.pageX,
          pos_y = event.pageY,
          left  = 0,
          top   = 0;

      left = container_w / 2 - pos_x;
      top  = container_h / 2 - pos_y;

      TweenMax.to(
        $cloud_2, 1,
        {
          css: {
            transform: 'translateX(' + left / 16 + 'px) translateY(' + top / 24 + 'px)'
          },
          overwrite: 'all'
        });

      TweenMax.to(
        $cloud_1, 1,
        {
          css: {
            transform: 'translateX(' + left / 8 + 'px) translateY(' + top / 12 + 'px)'
          },
          ease:Expo.easeOut,
          overwrite: 'all'
        });
    });
  }
});