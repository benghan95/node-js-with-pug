var jsonfile_b = "/public/landmark-b.json",
    fps = 60,
    width = 360,
    height = 160,
    AJAX_req_b;

AJAX_JSON_Req_b(jsonfile_b);

function handle_AJAX_Complete_b() {
if( AJAX_req_b.readyState == 4 && AJAX_req_b.status == 200 )
{
    json_b = JSON.parse(AJAX_req_b.responseText);
    // comp = new SVGAnim(
    //                json,
    //                width,
    //                height,
    //                fps
    //                );
    var container_b = document.getElementById("vector-container");
    var comp_b = new SVGAnim(json_b,width,height,fps);
    comp_b.s.node.setAttribute('id','landmark-b');
    container_b.appendChild(comp_b.s.node);

}
}

function AJAX_JSON_Req_b( url )
{
AJAX_req_b = new XMLHttpRequest();
AJAX_req_b.open("GET", url, true);
AJAX_req_b.setRequestHeader("Content-type", "application/json");

AJAX_req_b.onreadystatechange = handle_AJAX_Complete_b;
AJAX_req_b.send();
}