const i18n       = require('i18n');
const Email      = require('email-templates');
const path       = require('path');
const nodemailer = require("nodemailer");
const mg         = require('nodemailer-mailgun-transport');
const User       = require('../models/User');
const moment     = require('moment');

exports.index = (req, res) => {
  const language = 'en';
  const email = new Email({
    juice: true,
    juiceResources: {
      preserveImportant: true,
      webResources: {
        relativeTo: path.join(__dirname, '..', 'public')
      }
    },
    message: {
      from: 'benghan.ng@nagaddbtribal.com.my'
    },
    transport: {
      jsonTransport: true
    }
  });

  let subject            = "My incredible journey with AirAsia! #halfabillion.";
  let web_check_in       = 'Web Check In';
  let manage_my_bookings = 'Manage My Bookings';
  let customer_support   = 'Customer Support';
  let view_online        = 'View Online';
  let big_member_ID      = 'BIG Member ID';
  let hello              = 'Hello';
  let desc_1             = 'We have come a long way together.';
  let desc_2             = 'Thank you for being one in #halfabillion.';
  let button_text        = 'See My Journey';

  switch(language) {
    case 'id':
      subject            = "Petualangan istimewaku bersama AirAsia!#halfabillion.";
      web_check_in       = 'Check-In';
      manage_my_bookings = 'Pembelian Saya';
      customer_support   = 'Pelayanan Pelanggan';
      view_online        = 'Lihat online';
      big_member_ID      = 'ID Keanggotaan BIG';
      hello              = 'Halo';
      button_text        = 'Lihat Perjalanan Saya';
      desc_1             = 'Kita telah bersama sekian lama.';
      desc_2             = 'Terima kasih karena telah menjadi bagian dari #halfabillion.';
      break;
    case 'th':
      subject            = "สุดยอดการเดินทางของฉันกับแอร์เอเชีย#halfabillion";
      web_check_in       = 'เช็คอิน';
      manage_my_bookings = 'จัดการบุ๊คกิ้ง';
      customer_support   = 'บริการช่วยเหลือของแอร์เอเชีย';
      view_online        = 'ดูออนไลน์';
      big_member_ID      = 'หมายเลขสมาชิก BIG ID';
      hello              = 'สวัสดี';
      button_text        = 'ดูเรื่องราวการเดินทางของคุณ';
      desc_1             = 'เพราะเราได้ร่วมเดินทางด้วยกันมาอย่างยาวนานขอขอบคุณที่ได้ร่วมเดินทางเป็นส่วนหนึ่งใน';
      desc_2             = '#halfabillion กับเรา';
      break;
    case 'cn-zh':
      subject            = "我与亚航的精彩旅程（#五亿分之一）";
      web_check_in       = '值机服务';
      manage_my_bookings = '我的预订';
      customer_support   = '亚航支持';
      view_online        = '在线查看';
      big_member_ID      = 'BIG会员账户';
      hello              = '你好';
      button_text        = '查看我的旅程';
      desc_1             = '感谢一路有你';
      desc_2             = '感谢成为#五亿分之一乘客';
      break;
    case 'tw-zh':
      subject            = "AirAsia #非凡五億的精采旅程";
      web_check_in       = '報到';
      manage_my_bookings = '我的預訂';
      customer_support   = '亞航支持';
      view_online        = '在線查看';
      big_member_ID      = 'BIG會員ID';
      hello              = '你好';
      button_text        = '瀏覽我的行程';
      desc_1             = '我們一起肩並肩一起走過許多旅程。';
      desc_2             = '感謝您参與 #非凡五億旅程!';
      break;
    case 'hk-zh':
      subject            = "我與AirAsia的精彩旅程！ #halfabillion ";
      web_check_in       = '登機手續';
      manage_my_bookings = '我的預訂';
      customer_support   = '亞航支持';
      view_online        = '在線查看';
      big_member_ID      = 'BIG會員ID';
      hello              = '你好';
      button_text        = '參閱我的旅程';
      desc_1             = '感謝您一路相伴！成為我們';
      desc_2             = '#halfabillion 中的一份子。';
      break;
    case 'kr':
      subject            = "에어아시아와 함께 멋진 여정을 떠나보세요! #halfabillion.";
      web_check_in       = '체크인';
      manage_my_bookings = '나의 예약';
      customer_support   = '고객지원';
      view_online        = '온라인보기';
      big_member_ID      = 'BIG 회원 ID';
      hello              = '안녕하세요';
      button_text        = '나의 여정 보기';
      desc_1             = '여러분 덕분에 에어아시아는 더 높이 날 수 있었습니다.';
      desc_2             = '#halfabillion에 함께 해 주셔서 감사합니다.';
      break;
    case 'jp':
      subject            = "エアアジアと私の旅 #エアアジア5億人達成 #halfabillion";
      web_check_in       = 'Webチェックイン';
      manage_my_bookings = '予約する';
      customer_support   = 'エアアジア　サポート';
      view_online        = 'ブラウザで表示';
      big_member_ID      = 'BIGメンバーID';
      hello              = 'こんにちは,';
      name_title        = 'さん',
      button_text        = '"私の旅"を見る';
      desc_1             = 'エアアジアをご利用いただき、ありがとうございます。';
      desc_2             = '搭乗者5億人を達成することができました。';
      break;
    case 'vn':
      subject            = "Hành trình tuyệt vời của tôi cùng AirAsia! #halfabillion.";
      web_check_in       = 'Thủ tục lên máy bay';
      manage_my_bookings = 'Quản lý đặt vé';
      customer_support   = 'Hỗ trợ AirAsia';
      view_online        = 'Xem trực tuyến';
      big_member_ID      = 'ID Thành viên BIG';
      hello              = 'Chào';
      button_text        = 'Xem Hành trình của tôi';
      desc_1             = 'Chúng ta đã cùng nhau đi qua chặng đường dài.';
      desc_2             = 'Cảm ơn bạn là một trong nửa tỷ #halfabillion hành khách.';
      break;
    default:
      break;
  }

  email
    .send({
      template: 'promo',
      message: {
        to: 'benghan.ng@nagaddbtribal.com.my'
      },
      locals: {
        subject           : subject,
        lang              : language,
        name              : "Peter",
        bigID             : "2147260405",
        web_check_in      : web_check_in,
        manage_my_bookings: manage_my_bookings,
        customer_support  : customer_support,
        view_online       : view_online,
        big_member_ID     : big_member_ID,
        hello             : hello,
        button_text       : button_text,
        desc_1            : desc_1,
        desc_2            : desc_2,
        button_text       : button_text,
        current_timestamp : moment().format('D MMM YYYY'),
      }
    })
    .then(console.log)
    .catch(console.error);
};

exports.sendEmail = async (req, res) => {
  const hexhash  = req.body.hexhash;
  const language = req.body.language;

  let userName     = "";
  let userEmail    = "";
  let userBigId    = "";
  let thumbnailUrl = "";

  let subject            = "My incredible journey with AirAsia! #halfabillion.";
  let web_check_in       = 'Web Check In';
  let manage_my_bookings = 'Manage My Bookings';
  let customer_support   = 'Customer Support';
  let view_online        = 'View Online';
  let big_member_ID      = 'BIG Member ID';
  let hello              = 'Hello';
  let desc_1             = 'We have come a long way together.';
  let desc_2             = 'Thank you for being one in #halfabillion.';
  let desc_3             = 'My travel personality from the journeys I had with AirAsia!';
  let button_text        = 'See My Journey';

  switch(language) {
    case 'id':
      subject            = "Petualangan istimewaku bersama AirAsia!#halfabillion.";
      web_check_in       = 'Check-In';
      manage_my_bookings = 'Pembelian Saya';
      customer_support   = 'Pelayanan Pelanggan';
      view_online        = 'Lihat online';
      big_member_ID      = 'ID Keanggotaan BIG';
      hello              = 'Halo';
      button_text        = 'Lihat Perjalanan Saya';
      desc_1             = 'Kita telah bersama sekian lama.';
      desc_2             = 'Terima kasih karena telah menjadi bagian dari #halfabillion.';
      desc_3             = 'Kepribadian petualangan saya dari perjalanan yang telah saya lakukan bersama AirAsia! ';
      break;
    case 'th':
      subject            = "สุดยอดการเดินทางของฉันกับแอร์เอเชีย#halfabillion";
      web_check_in       = 'เช็คอิน';
      manage_my_bookings = 'จัดการบุ๊คกิ้ง';
      customer_support   = 'บริการช่วยเหลือของแอร์เอเชีย';
      view_online        = 'ดูออนไลน์';
      big_member_ID      = 'หมายเลขสมาชิก BIG ID';
      hello              = 'สวัสดี';
      button_text        = 'ดูเรื่องราวการเดินทางของคุณ';
      desc_1             = 'เพราะเราได้ร่วมเดินทางด้วยกันมาอย่างยาวนานขอขอบคุณที่ได้ร่วมเดินทางเป็นส่วนหนึ่งใน';
      desc_2             = '#halfabillion กับเรา';
      desc_3             = 'ลักษณะเดินทางของฉันจากการเดินทางร่วมกับแอร์เอเชีย';
      break;
    case 'cn-zh':
      subject            = "我与亚航的精彩旅程（#五亿乘客）";
      web_check_in       = '值机服务';
      manage_my_bookings = '我的预订';
      customer_support   = '亚航支持';
      view_online        = '在线查看';
      big_member_ID      = 'BIG会员账户';
      hello              = '你好';
      button_text        = '查看我的旅程';
      desc_1             = '感谢一路有你';
      desc_2             = '感谢成为#五亿乘客';
      desc_3             = '作为亚航的#5亿乘客之一，这是专属于我的旅行态度';
      break;
    case 'tw-zh':
      subject            = "AirAsia #非凡五億的精采旅程";
      web_check_in       = '報到';
      manage_my_bookings = '我的預訂';
      customer_support   = '亞航支持';
      view_online        = '在線查看';
      big_member_ID      = 'BIG會員ID';
      hello              = '你好';
      button_text        = '瀏覽我的行程';
      desc_1             = '我們一起肩並肩一起走過許多旅程。';
      desc_2             = '感謝您参與 #非凡五億旅程!';
      desc_3             = '這是我與AirAsia的精彩飛行紀錄！';
      break;
    case 'hk-zh':
      subject            = "我與AirAsia的精彩旅程！ #halfabillion ";
      web_check_in       = '登機手續';
      manage_my_bookings = '我的預訂';
      customer_support   = '亞航支持';
      view_online        = '在線查看';
      big_member_ID      = 'BIG會員ID';
      hello              = '你好';
      button_text        = '參閱我的旅程';
      desc_1             = '感謝您一路相伴！成為我們';
      desc_2             = '#halfabillion 中的一份子。';
      desc_3             = '這是我與AirAsia的精彩飛行紀錄';
      break;
    case 'kr':
      subject            = "에어아시아와 함께 멋진 여정을 떠나보세요! #halfabillion.";
      web_check_in       = '체크인';
      manage_my_bookings = '나의 예약';
      customer_support   = '고객지원';
      view_online        = '온라인보기';
      big_member_ID      = 'BIG 회원 ID';
      hello              = '안녕하세요';
      button_text        = '나의 여정 보기';
      desc_1             = '여러분 덕분에 에어아시아는 더 높이 날 수 있었습니다.';
      desc_2             = '#halfabillion에 함께 해 주셔서 감사합니다.';
      desc_3             = '에어아시아와 함께 떠난 나의 여행 스타일';
      break;
    case 'jp':
      subject            = "エアアジアと私の旅 #エアアジア5億人達成 #halfabillion";
      web_check_in       = 'Webチェックイン';
      manage_my_bookings = '予約する';
      customer_support   = 'エアアジア　サポート';
      view_online        = 'ブラウザで表示';
      big_member_ID      = 'BIGメンバーID';
      hello              = 'こんにちは,';
      name_title        = 'さん',
      button_text        = '"私の旅"を見る';
      desc_1             = 'エアアジアをご利用いただき、ありがとうございます。';
      desc_2             = '搭乗者5億人を達成することができました。';
      desc_3             = '私の旅は、エアアジア　#エアアジア5億人達成　で行った旅の履歴です';
      break;
    case 'vn':
      subject            = "Hành trình tuyệt vời của tôi cùng AirAsia! #halfabillion.";
      web_check_in       = 'Thủ tục lên máy bay';
      manage_my_bookings = 'Quản lý đặt vé';
      customer_support   = 'Hỗ trợ AirAsia';
      view_online        = 'Xem trực tuyến';
      big_member_ID      = 'ID Thành viên BIG';
      hello              = 'Chào';
      button_text        = 'Xem Hành trình của tôi';
      desc_1             = 'Chúng ta đã cùng nhau đi qua chặng đường dài.';
      desc_2             = 'Cảm ơn bạn là một trong nửa tỷ #halfabillion hành khách.';
      desc_3             = 'Tính cách du lịch của tôi thể hiện qua hành trình với AirAsia!';
      break;
    default:
      break;
  }

  User.findOne({ hexhash }, (err, user) => {
    if (err) {
      console.error(err);
      res.status(200).send({
        "status" : "error",
        "message": "Please ensure you have logged in to your account.",
      });
      return;
    }
    if (!user) {
      console.error("user not found for email");
      res.status(200).send({
        "status" : "error",
        "message": "Please ensure you have logged in to your account.",
      });
      return;
    }

    user.hasEmailed = true;
    user.save();
    
    userName  = user.info.name;
    userEmail = user.email;
    userBigId = user.bigId;

    const transporter = nodemailer.createTransport({
      host: 'smtpdm-ap-southeast-1.aliyun.com',
      port: 465,
      secure: true,
      auth: {
        user: 'donotreply@halfabillion.airasia.com',
        pass: 'RgtkvqKHwYEj54npashZ'
      }
    })

    const email = new Email({
      juice: true,
      juiceResources: {
        preserveImportant: true,
        webResources: {
          images: false
        }
      },
      message: {
        from: 'donotreply@halfabillion.airasia.com',
        to  : user.email
      },
      send: true,
      transport: transporter,
    });

    let url = 'halfabillion.airasia.com';
    switch (user.thumbnail.serverIP) {
      case '20.191.143.59':
        url = 'halfabillion.airasia.com';
        break;
      case '52.231.66.227':
        url = 'a1-halfabillion.airasia.com';
        break;
      case '104.215.182.222':
        url = 'a4-halfabillion.airasia.com';
        break;
      case '13.67.53.185':
        url = 'a6-halfabillion.airasia.com';
        break;
      case '137.116.140.88':
        url = 'a7-halfabillion.airasia.com';
        break;
      case '137.116.142.123':
        url = 'a8-halfabillion.airasia.com';
        break;
    }

    email
      .send({
        template: 'promo',
        message: {
          to: user.email
        },
        locals: {
          subject           : subject,
          lang              : language,
          name              : userName,
          total_destinations: user.info.total_destinations,
          first_visited     : user.info.first_visited,
          bigID             : userBigId,
          web_check_in      : web_check_in,
          manage_my_bookings: manage_my_bookings,
          customer_support  : customer_support,
          view_online       : view_online,
          big_member_ID     : big_member_ID,
          hello             : hello,
          button_text       : button_text,
          desc_1            : desc_1,
          desc_2            : desc_2,
          button_text       : button_text,
          current_timestamp : moment().format('D MMM YYYY'),
          thumbnail_url         : `https://${url}/results/${user.thumbnail.projectId}/result_00000.jpg`,
          journey_url         : `https://halfabillion.airasia.com/${language}/my-journey/${user.hexhash}`,
        }
      })
      .then((response) => {
        res.status(200).send({
          "status": "success",
          "message": "Email has been sent!",
        });
        return;
      })
      .catch((err) => {
        res.status(200).send({
          "status": "error",
          "message": err,
        });
        return;
      });
  })
};
