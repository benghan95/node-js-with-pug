const fs     = require('fs');
const fetch  = require('node-fetch');
const Render = require('../models/Render');
const User = require('../models/User');

const resultPath = 'C:/inetpub/wwwroot/results';

exports.checkStatus = (req, res, next) => {
  User.findOne({ hexhash: req.body.hexhash}, (err, user) => {
    if (user) {
      const thumbnail = user.thumbnail;
      if (thumbnail.status != 'pending' && thumbnail.serverIP) {
        res.status(200).send({
          "status" : "success",
          "message": user.thumbnail.serverIP
        });
        return;
      } else {
        res.status(200).send({
          "status" : "pending",
          "message": 15,
        });
        return;
      }
    } else {
      res.status(200).send({
        "status" : "error",
        "message": err,
      });
      return;
    }
  })
  
}

exports.logging = async (req, res, next) => {
  const renderId     = req.body.renderId;
  const thumbnailId  = req.body.thumbnailId;
  const hexhash      = req.body.hexhash;
  const progressPath = `${resultPath}/${thumbnailId}/progress.txt`;

  Render.findOne({ _id: renderId }, (err, render) => {
    if (err) {
      console.error("Logging error: " + JSON.stringify(err));
      res.status(200).send({
        'status': 'error',
        'message': err
      })
      return;
    }

    if (render) {
      if (render.status == 'pending') {
        res.status(200).send({
          'status': 'pending',
          'message': 0
        })
        return;
      } else if (render.status == 'rendering') {
        const progressPath = `${resultPath}/${render.projectId}/progress.txt`;

        fs.access(progressPath, fs.constants.F_OK, (err) => {
          if (!err) {
            fs.readFile(progressPath, async (err, data) => {
              if (err) {
                render.status      = 'error';
                render.errMessage  = err;
                render.completedAt = null;
                render.save();
                console.error("Logging Read File Error: " + JSON.stringify(err));
                res.status(200).send({
                  'status': 'error',
                  'message': err
                });
                return;
              }

              const progressData  = data.toString();
              const thumbnailPath = `${resultPath}/${thumbnailId}/result_00000.jpg`;
              const progress      = await getProgress(progressData, thumbnailPath);

              if (progress == 'failed') {
                render.status      = 'error';
                render.errMessage  = 'Render failed.';
                render.completedAt = null;
                render.save();
                res.status(200).send({
                  'status': 'failed',
                  'message': err
                });
                return;
              } else if (progress == 'rendering') {
                render.status      = 'error';
                render.errMessage  = 'Still rendering.';
                render.completedAt = null;
                render.save();
                res.status(200).send({
                  'status': 'rendering',
                  'message': 30
                });
                return;
              } else if (progress == 'completed') {
                render.status      = 'completed';
                render.completedAt = new Date();
                const duration  = render.completedAt - render.startedAt;
                render.duration = Math.round(duration / 1000);
                render.save();
                res.status(200).send({
                  'status': 'completed',
                  'message': 100
                });
                return true;
              }
            });
          } else {
            render.status      = 'error';
            render.errMessage  = err;
            render.completedAt = null;
            render.save();
            console.error("Logging Read File Error: " + JSON.stringify(err));
            res.status(200).send({
              'status': 'error',
              'message': err
            });
            return;
          }
        });
      } else if (render.status == 'completed') {
        res.status(200).send({
          'status': 'completed',
          'message': 100
        });
        return;
      } else {
        console.error("Logging Error: Render status others");
        res.status(200).send({
          'status': 'error',
          'message': 'Render status others'
        });
        return;
      }
    } else {
      console.error("Logging Error: Render not found!");
      res.status(200).send({
        'status': 'error',
        'message': 'Render not found!'
      });
      return;
    }
  })
};

const getProgress = (progressData, path) => {
  const regex = /\((.*?)\)/gm;
  const exists   = fs.existsSync(path);
  const maxFrame = 1;

  let currentFrame = 0;
  let m;

  while ((m = regex.exec(progressData)) !== null) {
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    let value = parseInt(m[1]);
    if (value && value > currentFrame) {
      currentFrame = value;
    }
  }

  if ((maxFrame - currentFrame) <= 0) {
    if(progressData.indexOf('Rendering error') >= 0) {
      return 'failed';
    } else {
      if (progressData.indexOf('project finished') >= 0
      && !(progressData.indexOf('Rendering error') >= 0) ) {
        return 'completed';
      } else {
        return 'rendering';
      }
    }
  } else {
    if (progressData.indexOf('Rendering error') >= 0) {
      return 'failed';
    } else {
      return 'rendering';
    }
  }
}