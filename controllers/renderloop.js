
const Render                = require('../models/Render');
const { Project, renderer } = require('nexrender');
const fs                    = require('fs-extra');
const fetch                 = require('node-fetch');
const path                  = require('path');
const publicIp = require('public-ip');
var currentPublicIP = null;

const maxAerender = 1;
const oriLoopmiliSeconds = 2000;

let loopmiliSeconds = 1000;
let firstRoundPurge = true;
let stillRendering = false;
const datascript  = 'data.js';
const aepxPath    = 'C:/inetpub/wwwroot/public/assets';
const scriptPath  = 'C:/inetpub/wwwroot/public/uploads';
const resultPath  = 'C:/inetpub/wwwroot/results';
const aebinary    = "C:\\Program Files\\Adobe\\Adobe After Effects CC 2017\\Support Files\\aerender";

console.log = function (d) {
  process.stdout.write(d + '\n');
};

const getCount = () => {
    stillRendering = true;
    publicIp.v4().then(ip => {
        console.log("Your public IP is:"+ip);
        currentPublicIP = ip;
        
        autoRender();
    });

  Render.count({status: 'pending', type: 'video'}, (err, count) => {
    console.log("Count:"+ count);

    if(count == 0){
      stillRendering = false;
      // loopmiliSeconds = 60000;
    }else{
      // loopmiliSeconds = oriLoopmiliSeconds;
    }
  });
}

exports.checkStatus = (req, res) => {
  setInterval(function(){
    if(stillRendering)
      return;
      
    getCount();
  },loopmiliSeconds);
};

const autoRender = () => {
  Render.count({status: 'rendering', type: 'video', serverIP: currentPublicIP}, (err, count) => {
    if (count < maxAerender || firstRoundPurge) {
      let purgeQuery = {status: 'pending', type: 'video'};
      if(firstRoundPurge && count != 0)
        purgeQuery = {status: 'rendering', type: 'video', serverIP: currentPublicIP};

      Render.findOne(purgeQuery, {}, { sort: { 'created_at' : 1 } }, async (err, render) => {

        if (render) {
          let startTime = new Date();
          let package = await renderProject(render);
          let project;
          let data;

          if (render.type == 'thumbnail') {
            stillRendering = false;
            return;
          } else if (render.type == 'video') {
            data = [
              {
                type: 'project',
                name: 'project.aepx',
                src : `${aepxPath}/${render.aepx}`
              },
              {
                type: 'script',
                name: 'user.js',
                src : `${scriptPath}/${render.userHexhash}/${datascript}`
              }
            ];

            project = new Project({
              template: 'project.aepx',
              composition: render.composition,
              settings: {
                outputModule: 'H.264',
                outputExt: 'mov',
              },
              assets: data,
            });

            render.projectId = project.uid;
            render.save();
          }

          renderer.render(aebinary, project).then((result) => {
            render.status      = 'completed';
            render.completedAt = Date.now();
            render.save();

            let endTime = new Date();
            let diff = endTime.getTime() - startTime.getTime();            
            console.log("Rendering Elapsed:"+ round(diff/1000));

            fetch('https://halfabillion.airasia.com/en/update-status', {
              method: 'POST',
              body  : JSON.stringify({ render }),
              headers: { 'Content-Type': 'application/json' },
            });

            if (render.type == 'video') {
              User.findOne({ hexhash: render.userHexhash }, (err, res) => {
                
              })
            }

            if (render.type == 'thumbnail') {
              fs.copy(`${resultPath}/${result.uid}/result_00000.jpg`, `${resultPath}/${result.uid}_result.jpg`, err => {
                if (err) { console.error(err); return null; }
              })
            }
            
            stillRendering = false;
          }).catch((err) => {
            console.error(err);
            console.log("Error:"+ JSON.stringify(err));

            render.status     = 'error';
            render.errMessage = JSON.stringify(err);
            render.save();
            fetch('https://halfabillion.airasia.com/en/update-status', {
              method: 'POST',
              body  : JSON.stringify({ render }),
              headers: { 'Content-Type': 'application/json' },
            });

            stillRendering = false;

            render.status = 'pending';
            render.save();
          });
        }
      });
    }else{
      stillRendering = false;
    }
  });
}

const renderVideo = (render) => {
  let aepxfile = 'airasia-halfabillion-en.aepx';
  let language = 'en';

  aepx = render.aepx;
  composition = render.composition;
  const data = [
    {
      type: 'project',
      name: 'project.aepx',
      src : `${aepxPath}/${aepxfile}`
    },
    {
      type: 'script',
      name: 'user.js',
      src : `${scriptPath}/${render.userHexhash}/${datascript}`
    }
  ];

  const project = new Project({
    template: 'project.aepx',
    composition: composition,
    settings: {
      outputModule: 'H.264',
      outputExt: 'mov',
    },
    assets: data,
  });

  // const render = new Render({
  //   renderId   : project.uid,
  //   status     : 'pending',
  //   type       : 'video',
  //   aepx       : aepxfile,
  //   composition: composition,
  //   userHexhash: render.userHexhash,
  //   receivedAt : Date.now(),
  // });

  render.status = 'rendering';
  render.startedAt = Date.now();
  render.serverId = currentPublicIP;
  render.save();

  return {
    data: data,
    project: project
  };
}

const renderProject = async (render) => {
  const dataPath = path.join(__dirname, '../public/uploads/', render.userHexhash, 'data.js');
  
  await fs.outputFile(dataPath, `var user = ${JSON.stringify(render.info)};`, async (err) => {
    if (!err) {
      // const thumbnailProj = await renderThumbnail(user);
      const videoProj     = await renderVideo(render);
      return {
        "status"   : "success",
        // "thumbnail": thumbnailProj,
        "package"    : videoProj,
      };
    } else {
      return {
        "status"   : "error",
        // "thumbnail": null,
        "package"    : null,
      };
    }
  })
}


exports.renderFailed = (req, res) => {
  const hexhash = req.body.hexhash;

  User.findOne({ hexhash }, (err, user) => {
    if (err) {
      console.error("Render Failed Error: ");
      console.error(err);
      res.status(200).send({ "status": "success" });
      return;
    }

    if (user) {
      user.thumbnail = {};
      user.video = {};
      user.save();
      res.status(200).send({ "status": "success" });
      return;
    } else {
      console.error("Render Failed Error: ");
      console.error("User not found!");
      res.status(200).send({ "status": "success" });
      return;
    }
  })
}