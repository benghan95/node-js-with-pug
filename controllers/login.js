const SHA256 = require('crypto-js/sha256');
const User   = require('../models/User');
const Render = require('../models/Render');
const Server = require('../models/Server');
const moment = require('moment');
const fetch  = require('node-fetch');

exports.postLogin = async (req, res, next) => {
  try {
    const body = req.body;
    if (body.accessToken) {
      let info = {};

      info.name            = body.name;
      info.gender          = body.gender;
      info.lang            = body.lang;
      info.favoriteFood    = '';
      info.bookingsMade    = 0;
      info.tier            = 'Occasional Traveler';
      info.bookingsMsg     = '';
      info.firstLanded     = '2018';
      info.firstVisited    = '2018';
      info.preferredDevice = '';
      info.thumbnailTitle  = `${body.name}, you are an on and off traveller!`;
      info.thumbnailDesc   = `You’re a super busy person who takes very few breaks. We think you deserve more down time to relax and unwind.`;
      info.thumbnailFooter = `Book now @ airasia.com`;

      fetch('https://k.apiairasia.com/custprof?hitme=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJTaUx2MWRjZmwyMGltMGxEcENmczRBRUZRTnVreEhRSSIsImlhdCI6MTUyNDczNjExMiwiZXhwIjoxODQwMzU1MzEyLCJhdWQiOiIiLCJzdWIiOiJoZWV6aGVuZG9uZ0BhaXJhc2lhLmNvbSJ9.lo_yp95MndV1nxTT2Ha2tiw6HJWxHzDprDwOZkBrPoA', {
        headers: {
          'Authorization': body.accessToken,
        }
      })
        .then(resp => resp.json())
        .then(async (response) => {
          info = populateUserInfo(info, body, response);

          User.findOne({ userId: body.userId }, (err, existingUser) => {
            if (err) {
              res.status(200).send({
                "status" : "error",
                "message": err,
              });
              return;
            }

            let thumbnailRender = {
              status     : 'error',
              type       : 'thumbnail',
              aepx       : 'social-share-en.aepx',
              composition: 'social-share',
              info       : info,
            };

            if (existingUser) {
              existingUser.name   = body.name;
              existingUser.gender = body.gender;
              existingUser.email  = body.email;
              existingUser.info   = info;
              existingUser.save();

              generateThumbnailRender(existingUser, (err, thumbnailRender) => {
                if (thumbnailRender) {
                  existingUser.thumbnail  = {
                    renderId: thumbnailRender._id,
                    status  : thumbnailRender.status,
                    sentAt  : new Date(),
                  }

                  existingUser.save();

                  res.status(200).send({
                    "status": "success",
                    "user"  : existingUser,
                  });
                  return;
                }
              });
            } else {
              const user = new User({
                name       : body.name,
                gender     : body.gender,
                userId     : body.userId,
                bigId      : body.bigId,
                accessToken: body.accessToken,
                email      : body.email,
                hexhash    : SHA256(body.bigId),
                info       : info,
                thumbnail  : {},
              });
              user.save();
  
              generateThumbnailRender(user, (err, thumbnailRender) => {
                console.log(thumbnailRender);
                if (thumbnailRender) {
                  user.thumbnail  = {
                    renderId: thumbnailRender._id,
                    status  : thumbnailRender.status,
                    sentAt  : new Date(),
                  }
                  user.save();
  
                  res.status(200).send({
                    "status" : "success",
                    "user": user,
                  });
                }
              });
            }

          });
        });
    } else {
      res.status(200).send({
        "status" : "error",
        "message": "Access Token not found!",
      });
      return;
    }
  } catch (err) {
    console.error('Login failed: ');
    console.error(err);
    res.status(200).send({
      "status" : "error",
      "message": err,
    });
    return;
  }
};

exports.trigger = (req, res) => {
  const language = req.params.lang;

  User.findOne({}, {}, { sort: { 'createdAt' : 1 } }, (err, user) => {
    if (user) {

      user.info.bookingsMade = 2;
      user.info.lang = language;
      user.save();
      user.info = generateTier(user.info);
      user.save();
      generateThumbnailRender(user, (err, thumbnailRender) => {
        if (thumbnailRender) {
          user.thumbnail  = {
            renderId   : thumbnailRender._id,
            status     : thumbnailRender.status,
            sentAt     : new Date(),
            completedAt: null,
            projectId  : '',
            serverIP   : thumbnailRender.serverIP,
          }

          console.error("Testing: " + thumbnailRender._id);
  
          user.save();

        }
      });

      setTimeout(function() {
        user.info.bookingsMade = 9;
        user.save();
        user.info = generateTier(user.info);
        user.save();
        generateThumbnailRender(user, (err, thumbnailRender) => {
          if (thumbnailRender) {
            user.thumbnail  = {
              renderId   : thumbnailRender._id,
              status     : thumbnailRender.status,
              sentAt     : new Date(),
              completedAt: null,
              projectId  : '',
              serverIP   : thumbnailRender.serverIP,
            }

            console.error("Testing: " + thumbnailRender._id);
    
            user.save();

          }
        });
      }, 10000)

      setTimeout(function() {
        user.info.bookingsMade = 15;
        user.save();
        user.info = generateTier(user.info);
        user.save();
        generateThumbnailRender(user, (err, thumbnailRender) => {
          if (thumbnailRender) {
            user.thumbnail  = {
              renderId   : thumbnailRender._id,
              status     : thumbnailRender.status,
              sentAt     : new Date(),
              completedAt: null,
              projectId  : '',
              serverIP   : thumbnailRender.serverIP,
            }
  
            console.error("Testing: " + thumbnailRender._id);
    
            user.save();
  
          }
        });
      }, 20000);

      setTimeout(function() {
        user.info.bookingsMade = 30;
        user.save();
        user.info = generateTier(user.info);
        user.save();
        generateThumbnailRender(user, (err, thumbnailRender) => {
          if (thumbnailRender) {
            user.thumbnail  = {
              renderId   : thumbnailRender._id,
              status     : thumbnailRender.status,
              sentAt     : new Date(),
              completedAt: null,
              projectId  : '',
              serverIP   : thumbnailRender.serverIP,
            }

            console.error("Testing: " + thumbnailRender._id);
    
            user.save();

          }
        });
      }, 30000);

      setTimeout(function() {
        user.info.bookingsMade = 100;
        user.save();
        user.info = generateTier(user.info);
        user.save();
        generateThumbnailRender(user, (err, thumbnailRender) => {
          if (thumbnailRender) {
            user.thumbnail  = {
              renderId   : thumbnailRender._id,
              status     : thumbnailRender.status,
              sentAt     : new Date(),
              completedAt: null,
              projectId  : '',
              serverIP   : thumbnailRender.serverIP,
            }

            console.error("Testing: " + thumbnailRender._id);
    
            user.save();

          }
        });
      }, 40000);

    }
  });


  // for (let i = 0; i < 10; i ++) {
  //   User.findOne({}, {}, { sort: { 'createdAt' : 1 } }, (err, user) => {
  //     if (user) {
  //       generateThumbnailRender(user, (err, thumbnailRender) => {
  //         if (thumbnailRender) {
  //           user.thumbnail  = {
  //             renderId: thumbnailRender._id,
  //             status  : thumbnailRender.status,
  //             sentAt  : new Date(),
  //           }

  //           user.save();
  //           return;
  //         }
  //       });
  //     }
  //   });
  // }

  res.status(200).send({
    "status" : "success",
  });
}

const populateUserInfo = (info, body, response) => {
  let tier            = 1;
  let tierName        = `Occasional Traveler`;
  let bookingsMade    = 0;
  let bookingsMsg     = `Bookings since 2016`;
  let thumbnailName   = body.name;
  let thumbnailTitle  = `You're a high flyer!`;
  let thumbnailDesc   = `Your home is basically in the sky and you live to conquer them all. Conquer the rest of our growing network with over 130 destinations to choose from!`;
  let thumbnailFooter = `Thank you for being one in #halfabillion.\r\nBook now @ airasia.com`;

  info.name            = body.name;
  info.gender          = body.gender;
  info.lang            = body.lang;
  info.firstLanded     = '2018';
  info.firstVisited    = '2018';
  info.tier            = tier;
  info.tierName        = tierName;
  info.bookingsMade    = bookingsMade;
  info.bookingsMsg     = bookingsMsg;
  info.thumbnailTitle  = thumbnailTitle;
  info.thumbnailDesc   = thumbnailDesc;
  info.thumbnailFooter = thumbnailFooter;

  if ('data' in response) {
    if (response.data.length > 0){
      const data = response.data[0];

      info.favoriteFood    = data.fav_meal;
      info.bookingsMade    = data.booking_count;
      info.preferredDevice = data.prefer_device;

      if (data.first_landed) {
        info.firstLanded  = moment(data.first_landed).format();
        info.firstVisited = moment(data.first_landed).format('D.M.YYYY');
      }
    }
  }

  return generateTier(info);
}

const generateTier = (info) => {
  if (info.bookingsMade <= 3) {
    info.tier = 1;
    info.tierName = "Occasional Traveler";
    switch (info.lang) {
      case 'en':
        info.thumbnailTitle = "You're an on and off traveller!";
        info.thumbnailDesc = "You’re a super busy person who takes very few breaks. We think you deserve more down time to relax and unwind. Reward yourself with a quick weekend trip!";
        break;
      case 'id':
        info.thumbnailTitle = "Anda seorang petualang pemula!";
        info.thumbnailDesc = "Anda mungkin orang yang sangat sibuk dan hanya mempunyai waktu sedikit untuk beristirahat. Bagaimana kalau Anda meluangkan lebih banyak waktu untuk bersantai? Ayo, hadiahi diri Anda dengan perjalanan akhir pekan yang singkat!";
        break;
      case 'th':
        info.thumbnailTitle = "คุณคือนักเดินทางสมัครเล่น!";
        info.thumbnailDesc = " คุณอาจจะยุ่งมากและมีเวลาพักผ่อนน้อย \r\n เราขอแนะนำให้คุณหาเวลาพักผ่อน \r\n หรือออกเดินทางไปเที่ยวบ้าง ให้รางวัลแก่ \r\n ตัวเองด้วยการจองทริปสั้นๆ สุดสัปดาห์นี้!";
        break;
      case 'cn-zh':
        info.thumbnailTitle = "你平时一定很忙碌吧！";
        info.thumbnailDesc = "你已经忙到很少休息啦。\r\n对自己好一点吧！\r\n快快奖励自己一个周末旅行~";
        break;
      case 'tw-zh':
        info.thumbnailTitle = "三不五時就會旅行的你";
        info.thumbnailDesc = "生活忙碌的你，我們認為是時候擁有更多時間休息和放鬆。獎勵自己一個週末的旅行吧！";
        break;
      case 'hk-zh':
        info.thumbnailTitle = "您是一個忙碌的旅行者！";
        info.thumbnailDesc = "您實在是太忙碌了！我們認為您值得擁有更多時間來休息和放鬆。給自己獎勵一個週末旅行吧！";
        break;
      case 'kr':
        info.thumbnailTitle = "여행을 가끔씩 떠나는 여행자시군요";
        info.thumbnailDesc = "잠시 휴식을 가질 시간이 없을 정도로 바쁘시군요. 머리를 식힐 시간이 필요해 보입니다. 주말 여행을 통해 스스로에게 선물을 해보세요!";
        break;
      case 'jp':
        info.thumbnailTitle = "回数は少ないですが、中身の濃い旅を経験されていますね。";
        info.thumbnailDesc = "忙しい時こそ、リラックスできる時間を持てる旅に出かけてはいかがでしょうか。自分へのご褒美に、週末旅をしませんか？";
        break;
      case 'vn':
        info.thumbnailTitle = "bạn không thường xuyên đi du lịch lắm!";
        info.thumbnailDesc = "Hẳn bạn rất bận với công việc mà không có ngày nghỉ. Hãy tin chúng tôi, bạn xứng đáng được thư giãn sau những ngày lao động vất vả. Hãy tự thưởng cho mình chuyến đi cuối tuần!";
        break;
      default:
        break;
    }
  } else if (info.bookingsMade <= 9) {
    info.tier = 2;
    info.tierName = "Casual Holidaymaker";
    switch (info.lang) {
      case 'en':
        info.thumbnailTitle = "You're a curious traveller!";
        info.thumbnailDesc = "Your passion for travel is real! You’re a true world lover – people, cities and culture intrigue you to no end. Discover more exotic charms in Asean!";
        break;
      case 'id':
        info.thumbnailTitle = "Anda seorang petualang yang penuh dengan rasa ingin tahu!";
        info.thumbnailDesc = "Anda memiliki semangat seorang petualang! Bertemu dengan banyak orang, menjelajahi sudut kota, dan mengenal budaya menarik perhatian Anda. Temukan lebih banyak pesona eksotis di Asia Tenggara!";
        break;
      case 'th':
        info.thumbnailTitle = "คุณคือนักเดินทางที่รักการเรียนรู้!";
        info.thumbnailDesc = " ความรักในการเดินทางของคุณไม่เป็นรองใคร! \r\n คุณคือผู้ที่หลงรักการเรียนรู้ผู้คนและวัฒนธรรม \r\n มนต์เสน่ห์แห่งอาเซียนอีกมากมาย \r\n ยังรอให้คุณไปสัมผัส";
        break;
      case 'cn-zh':
        info.thumbnailTitle = "你对世界充满好奇！";
        info.thumbnailDesc = "这个世界的风土人情\r\n都深深吸引着你。\r\n继续和我们一起,\r\n探索来自东盟的异国魅力吧！";
        break;
      case 'tw-zh':
        info.thumbnailTitle = "對世界充滿好奇的你";
        info.thumbnailDesc = "你對旅遊充滿熱情！體驗不同的人、城市和文化都能深深吸引你，你是一個熱愛探索世界的人。繼續在其他充滿異國情調的東協發掘不同的魅力吧！";
        break;
      case 'hk-zh':
        info.thumbnailTitle = "您是一個好奇旅行者！";
        info.thumbnailDesc = "您真的很熱愛旅遊呢！您是一個真正的世界愛好者，各地的人們，不同的城市和文化都深深吸引著您。繼續在其他充滿異國情調的東盟國家發掘他們的魅力吧！";
        break;
      case 'kr':
        info.thumbnailTitle = "무척이나 호기심 많은 여행자 이시군요";
        info.thumbnailDesc = "여행에 대한 열정이 정말 가득하시군요! 세계 여러 사람, 도시, 문화를 끝없이 탐구하는 진짜 여행가입니다. 아세안의 더 많은 매력을 탐험해 보세요!";
        break;
      case 'jp':
        info.thumbnailTitle = "好奇心旺盛な旅人ですね。";
        info.thumbnailDesc = "あなたは本物の旅人です。世界中の人、街、文化があなたを待っています。アセアンのエキゾチックな魅力を探しに行こう！";
        break;
      case 'vn':
        info.thumbnailTitle = "bạn là người thích khám phá!";
        info.thumbnailDesc = "Bạn rất đam mê du lịch. Tìm hiểu thế giới, con người và văn hóa có sức hút khó cưỡng đối với bạn. Hãy khám phá thêm vẻ quyến rũ của Asean!";
        break;
      default:
        break;
    }
  } else if (info.bookingsMade <= 15) {
    info.tier = 3;
    info.tierName = "Eager Adventurer";
    switch (info.lang) {
      case 'en':
        info.thumbnailTitle = "You're an eager beaver!";
        info.thumbnailDesc = "Your passport has more stamps than a seasoned tour guide. You’ve covered the basics, now go explore hidden gems off the beaten path. Come explore our unique destinations!";
        break;
      case 'id':
        info.thumbnailTitle = "Anda seorang petualang yang sangat antusias!";
        info.thumbnailDesc = "Paspor Anda memiliki lebih banyak cap daripada seorang pemandu wisata. Anda telah melakukan banyak perjalanan ke berbagai tempat wisata. Kini saatnya menjelajah lebih jauh ke destinasi unik kami!";
        break;
      case 'th':
        info.thumbnailTitle = "คุณคือนักเดินทางที่อยากค้นหาสิ่งแปลกใหม่!";
        info.thumbnailDesc = " หนังสือเดินทางของคุณน่าจะมีตราประทับมากกว่าไกด์ \r\n นำเที่ยวบางคนซะอีก การเที่ยวแบบธรรมดาไม่ใช่สิ่งที่ \r\n คุณต้องการอีกแล้ว ลองค้นหาสถานที่ใหม่ๆ ที่ยังไม่เคย \r\n มีใครได้สัมผัส บินสู่ปลายทางที่แตกต่าง!";
        break;
      case 'cn-zh':
        info.thumbnailTitle = "你是一个旅行狂热分子！";
        info.thumbnailDesc = "你几乎已经打卡走遍所有热门景点了。\r\n是时候去发掘更多地道、小众的地方了。\r\n快来探索亚航独一无二的目的地吧!";
        break;
      case 'tw-zh':
        info.thumbnailTitle = "您是一個渴求出走的旅行者！";
        info.thumbnailDesc = "你護照裹的簽證蓋章比經驗豐富的導遊還要多！你已完成入門班，現在去探索更多被蘊藏的寶石吧。快來探索我們的獨家秘境吧！";
        break;
      case 'hk-zh':
        info.thumbnailTitle = "您是一個渴求出走的旅行者！";
        info.thumbnailDesc = "您護照裹的簽證蓋章比經驗豐富的導遊還要多！您已經完成入門班，現在去探索更多被蘊藏的寶石吧。快來探索我們的獨家目的地吧！";
        break;
      case 'kr':
        info.thumbnailTitle = "여행을 정말로 사랑하는 여행자이시군요!";
        info.thumbnailDesc = "당신의 여권에 도장은 여행 가이드보다 더 많이 찍혀 있을거 같아요. 여행의 기본은 충분히 다지셨으니, 이제 숨겨진 보석 같은 여행지를 탐험해 보세요! 에어아시아의 특별한 취항지들을 탐험해 보세요!";
        break;
      case 'jp':
        info.thumbnailTitle = "旅行の楽しみがわかる上級者ですね。";
        info.thumbnailDesc = "きっとあなたのパスポートは経験豊かなツアーガイドよりもたくさんの国のスタンプが押されていることでしょう。すでに基本的な観光地は押さえているあなた、今度はガイドブックにはない新たな場所を見つけに出かけてみませんか？";
        break;
      case 'vn':
        info.thumbnailTitle = "bạn cực kỳ thích du lịch!";
        info.thumbnailDesc = "Hộ chiếu của bạn còn nhiều dấu mộc hơn hướng dẫn viên du lịch. Bạn đã đi đến những nơi phổ biến, giờ hãy tìm kiếm những nơi ít người biết. Hãy khám phá những điểm đến độc đáo của AirAsia!";
        break;
      default:
        break;
    }
  } else if (info.bookingsMade <= 30) {
    info.tier = 4;
    info.tierName = "Habitual Tourist";
    switch (info.lang) {
      case 'en':
        info.thumbnailTitle = "You're almost a local!";
        info.thumbnailDesc = "You do as the locals do. You’ve grasped the language and learned to love local cuisine. But, have you tried all of our 35 tantalising meals?";
        break;
      case 'id':
        info.thumbnailTitle = "Anda sudah seperti penduduk lokal!";
        info.thumbnailDesc = "Anda sudah melakukan seperti yang dilakukan penduduk lokal. Anda telah memahami bahasa dan belajar mencintai masakan lokal. Tapi, sudahkah Anda mencoba 35 makanan lezat kami?";
        break;
      case 'th':
        info.thumbnailTitle = "คุณคือนักเดินทางระดับผู้เชี่ยวชาญ!";
        info.thumbnailDesc = " คุณมีความรู้ในการท่องเที่ยวราวกับเป็นเจ้าถิ่น \r\n ฟังภาษาท้องถิ่นได้บ้างและมีความรักในอาหาร \r\n ของแต่ละที่ ว่าแต่... คุณลองชิมอาหารกว่า 35 เมนู \r\n บนเที่ยวบินของเราหรือยังคะ?";
        break;
      case 'cn-zh':
        info.thumbnailTitle = "你已经是个当地人了！";
        info.thumbnailDesc = "你像当地人一样，\r\n会讲当地语言，热爱地方美食。\r\n不过，我们的35款机上美食你都试过了吗？";
        break;
      case 'tw-zh':
        info.thumbnailTitle = "你根本已經是一個當地人了！";
        info.thumbnailDesc = "你就像一個當地人，掌握了不同的語言及深深地愛上不同的美食。不過，我們的35款飛機餐都試過了嗎？";
        break;
      case 'hk-zh':
        info.thumbnailTitle = "您已經是一個當地人了！";
        info.thumbnailDesc = "您就像一個當地人，掌握了不同的語言及深深地愛上不同的美食。不過，我們的35款飛機餐您都試過了嗎？";
        break;
      case 'kr':
        info.thumbnailTitle = "거의 현지인 수준이시군요!";
        info.thumbnailDesc = "현지인 보다 더 현지인 같아요. 현지 언어와 음식들은 이미 섭렵하셨죠? 그렇다면 다양한 에어아시아의 35가지 기내식은 모두 드셔보셨나요?";
        break;
      case 'jp':
        info.thumbnailTitle = "旅行者というよりはローカル人！";
        info.thumbnailDesc = "現地の人が生活するように言葉を学び、ローカルフードを楽しんでいることでしょう。しかし、エアアジアの35種類の美味しい機内食は制覇しましたか？";
        break;
      case 'vn':
        info.thumbnailTitle = "bạn sành sõi như người địa phương!";
        info.thumbnailDesc = "Bạn như thể là người địa phương. Bạn thành thạo ngôn ngữ và yêu thích ẩm thực địa phương. Vậy bạn đã thử 35 món ngon của AirAsia chưa nhỉ?";
        break;
      default:
        break;
    }
  } else {
    info.tier = 5;
    info.tierName = "Day Tripper";
    switch (info.lang) {
      case 'en':
        info.thumbnailTitle = "You're a high flyer!";
        info.thumbnailDesc = "Your home is basically in the sky and you live to conquer them all. Conquer the rest of our growing network with over 130 destinations to choose from!";
        break;
      case 'id':
        info.thumbnailTitle = "Anda seorang petualang sejati!";
        info.thumbnailDesc = "Rumah Anda pada dasarnya berada di langit dan Anda hidup untuk menaklukkan itu semua. Ayo, taklukkan destinasi lainnya dengan lebih dari 130 destinasi menarik untuk dipilih!";
        break;
      case 'th':
        info.thumbnailTitle = "คุณคือสุดยอดนักเดินทาง!";
        info.thumbnailDesc = " เครื่องบินเปรียบเหมือนบ้านของคุณและคุณรัก \r\n การท่องเที่ยวเป็นชีวิตจิตใจ ยังมีปลายทาง \r\n มากมายที่รอคุณไปเยือน ผ่านโครงข่ายการบิน \r\n ของเราที่มีเส้นทางกว่า 130 ปลายทาง!";
        break;
      case 'cn-zh':
        info.thumbnailTitle = "你是一个空中飞人！";
        info.thumbnailDesc = "你的人生基本都在飞行里度过,\r\n而征服世界就是你的使命。\r\n继续来挑战我们不断增多的航线吧！\r\n超过130个精彩目的地等你来征服！";
        break;
      case 'tw-zh':
        info.thumbnailTitle = "你是一個專業的空中飛人！";
        info.thumbnailDesc = "天空是你的家，征服世界就是你的人生目標。繼續征服我們不斷增加的航點吧，有130多個目的地任你翱翔！";
        break;
      case 'hk-zh':
        info.thumbnailTitle = "您是一個職業旅行者！";
        info.thumbnailDesc = "天空是您的家，征服世界就是您的生命。繼續征服我們不斷增長的網絡吧，有130多個目的地任君選擇！";
        break;
      case 'kr':
        info.thumbnailTitle = "여행 마스터 이시군요!";
        info.thumbnailDesc = "이미 하늘을 집 삼아, 다양한 나라들을 정복하고 계시군요. 에어아시아 130여개의 여행지 중 아직 정복하지 못한 곳이 있는지 찾아보세요!";
        break;
      case 'jp':
        info.thumbnailTitle = "旅行マスター！";
        info.thumbnailDesc = "国々をめぐる旅の中で、空が家、といっても過言ではありません。エアアジア130以上の就航地でまだ制覇していない国や地域はありませんか？";
        break;
      case 'vn':
        info.thumbnailTitle = "bạn là cao thủ du lịch!";
        info.thumbnailDesc = "Bầu trời chính là nhà và bạn sống để chinh phục những đỉnh cao. Hãy chinh phục mạng lưới bay hơn 130 điểm đến của AirAsia!";
        break;
      default:
        break;
    }
  }

  info.thumbnailName   = info.name;
  info.thumbnailFooter = `Thank you for being one in #halfabillion.\r\nBook now @ airasia.com`;
  switch (info.lang) {
    case 'en':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "Bookings since 2016";
      info.thumbnailFooter   = `Thank you for being one in #halfabillion.\r\nBook now @ airasia.com`;
      break;
    case 'id':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "Pemesanan sejak 2016";
      info.thumbnailFooter   = `Terima kasih karena telah menjadi bagian dari #halfabillion.\r\nPesan sekarang di airasia.com!`;
      break;
    case 'th':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade} ครั้ง`;
      info.bookingsMsg       = "คือจำนวนที่คุณสำรองที่นั่งกับเราตั้งแต่ปี 2559";
      info.thumbnailFooter   = `ขอขอบคุณที่ได้ร่วมเดินทางเป็นส่วนหนึ่งใน #halfabillion\r\nกับเรา จองด่วนที่ airasia.com`;
      break;
    case 'cn-zh':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "次航班预定从2016年起";
      info.thumbnailFooter   = `感谢您成为#5亿乘客中的一员。\r\n即刻登录airasia.com预订机票！`;
      break;
    case 'tw-zh':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "次航班預訂從2016年起";
      info.thumbnailFooter   = `感謝您參與 #非凡五億旅程！\r\n登入@airasia.com預訂機票！`;
      break;
    case 'hk-zh':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "是你從2016年起預訂航班的次數";
      info.thumbnailFooter   = `成為我們 #halfabillion 中的一份子。\r\n即上@airasia.com預訂至抵機票！`;
      break;
    case 'kr':
      info.thumbnailName     = `${info.name}님,`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "2016년 이후 예약 횟수";
      info.thumbnailFooter   = `#halfabillion에 함께 해 주셔서 감사합니다. \r\n지금 바로 airasia.com에서 예약하세요`;
      break;
    case 'jp':
      info.thumbnailName     = `${info.name} さんは、`;
      info.thumbnailBookings = `${info.bookingsMade} 回`;
      info.bookingsMsg       = "2016年以降の予約数";
      info.thumbnailFooter   = `エアアジアをご利用いただき、\r\nありがとうございます。\r\n今すぐairasia.comへ`;
      break;
    case 'vn':
      info.thumbnailName     = `${info.name},`;
      info.thumbnailBookings = `${info.bookingsMade}`;
      info.bookingsMsg       = "lần đặt vé từ 2016";
      info.thumbnailFooter   = `Cảm ơn bạn là một trong nửa tỷ #halfabillion hành khách. \r\nĐặt vé ngay @ airasia.com`;
      break;
    default:
      break;
  }

  return info;
}

const generateThumbnailRender = (user, callback) => {
  let aepxfile = 'social-share-en.aepx';

  switch (user.info.lang) {
    case 'id':
      aepxfile = 'social-share-id.aepx';
      break;
    case 'th':
      aepxfile = 'social-share-th.aepx';
      break;
    case 'cn-zh':
      aepxfile = 'social-share-cn-zh.aepx';
      break;
    case 'tw-zh':
      aepxfile = 'social-share-tw-zh.aepx';
      break;
    case 'hk-zh':
      aepxfile = 'social-share-hk-zh.aepx';
      break;
    case 'kr':
      aepxfile = 'social-share-kr.aepx';
      break;
    case 'jp':
      aepxfile = 'social-share-jp.aepx';
      break;
    case 'vn':
      aepxfile = 'social-share-vn.aepx';
      break;
    default:
      break;
  }

  const pipeline = [
    {
      "$match": {
        "status": "online",
      }
    },
    {
      "$project": {
        "serverIP": 1,
        "serverName": 1,
        "status": 1,
        "pending": 1,
        "rendering": 1,
        "total": 1,
        "processing": { "$add": [ "$pending", "$rendering" ] }
      }
    }, {
      "$sort": { "processing": 1, "total": 1 }
    }
  ];

  Server.find({ 'status': 'online' }, (err, servers) => {
    servers.forEach((server) => {
      Render.count({status: 'pending', serverIP: server.serverIP}, (err, count) => {
        server.pending = count;
      });
      Render.count({status: 'rendering', serverIP: server.serverIP}, (err, count) => {
        server.rendering = count;
      });
      Render.count({status: 'completed', serverIP: server.serverIP}, (err, count) => {
        server.completed = count;
      });
      Render.count({status: 'error', serverIP: server.serverIP}, (err, count) => {
        server.error = count;
      });
      server.save();
    });
  });

  const render = new Render({
    status     : 'pending',
    type       : 'thumbnail',
    aepx       : aepxfile,
    serverIP   : null,
    receivedAt : null,
    composition: 'social-share',
    userHexhash: user.hexhash,
    info       : user.info,
  });
  render.save();

  Server.aggregate(pipeline, (err, servers) => {
    if (err) {
      console.error(err); 
      callback(null, render);
    }
    if (servers.length > 0) {
      const chosenServer = servers[0];

      render.serverIP   = chosenServer.serverIP;
      render.receivedAt = new Date();
      render.save();

      Server.findOne({serverIP: chosenServer.serverIP}, (err, server) => {
        if (server) {
          server.pending = server.pending + 1;
          server.total = server.total + 1;
          server.save();
        }
      });

      callback(null, render);
    } else {
      callback(null, render);
    }
  });
}
