const i18n                  = require('i18n');
const User                  = require('../models/User');
const Render                = require('../models/Render');

/**
 * GET /
 * Loading page.
 */
exports.index = async (req, res, next) => {
  i18n.setLocale(req, req.params.lang);

  const { lang } = req.params;
  const { hexhash } = req.params;

  User.findOne({ hexhash }, async (err, user) => {
    if (err) {
      res.redirect('/' + lang);
      return;
    }

    if (!user) {
      res.redirect('/' + lang);
      return;
    }

    if ('thumbnail' in user) {
      Render.findOne({ _id: user.thumbnail.renderId }, (err, render) => {
        if (render) {
          if (render.status == 'completed' && user.thumbnail.status == 'completed') {
            res.redirect('/' + lang + '/my-journey/' + hexhash);
            return;
          } else {
            res.render('loading', {
              title           : 'Loading Your #halfabillion Journey - AirAsia',
              slug            : 'loading/' + hexhash,
              lang            : lang,
              hexhash         : hexhash,
              meta_title      : 'AirAsia #halfabillion',
              meta_description: "With your support, we have increased our fleet of 2 aircraft in 2001, to over 230 aircraft in 2017. And by constantly improving our services, we're able to fly over 189,000 guests to over 130 destinations across Asia and beyond. Every day!",
              meta_image      : 'https://halfabillion.airasia.com/public/img/masthead_og.png',
              render          : render._id,
              thumbnail       : render.projectId,
              thumbnailServer : render.serverIP,
            });
          }
        } else {
          res.redirect('/' + lang);
          return;
        }
      })
    }
  });
};