const i18n  = require('i18n');
const fs    = require('fs');
const User  = require('../models/User');
const fetch = require('node-fetch');

exports.sharedFacebook = async (req, res) => {
  i18n.setLocale(req, req.params.lang);

  const { hexhash } = req.params;

  if (!hexhash) {
    res.status(200).send({
      "status": "error",
      "message": "Hexhash not found!",
    });
    return;
  }

  User.findOne({ hexhash }, (err, user) => {
    if (err) {
      res.status(200).send({
        "status": "error",
        "message": err,
      });
      return;
    }

    if (user) {
      user.hasShared = true;
      user.save();
      res.status(200).send({
        "status": "success",
        "message": "User has shared through Facebook",
      });
      return;
    }

    res.status(200).send({
      "status": "error",
      "message": err,
    });
    return;
  })
};

exports.downloaded = async (req, res) => {
  i18n.setLocale(req, req.params.lang);

  const { hexhash } = req.params;

  if (!hexhash) {
    res.status(200).send({
      "status": "error",
      "message": "Hexhash not found!",
    });
    return;
  }

  User.findOne({ hexhash }, (err, user) => {
    if (err) {
      res.status(200).send({
        "status": "error",
        "message": err,
      });
      return;
    }

    if (user) {
      user.hasDownloaded = true;
      user.save();
      res.status(200).send({
        "status": "success",
        "message": "User has downloaded",
      });
      return;
    }

    res.status(200).send({
      "status": "error",
      "message": err,
    });
    return;
  })
};

