
const Server                = require('../models/Server');
const Render                = require('../models/Render');
const User                  = require('../models/User');
const { Project, renderer } = require('nexrender');
const fs                    = require('fs-extra');
const fetch                 = require('node-fetch');
const path                  = require('path');
const publicIp              = require('public-ip');

const datascript   = 'data.js';
const aepxPath     = 'C:/inetpub/wwwroot/public/assets';
const scriptPath   = 'C:/inetpub/wwwroot/public/uploads';
const resultPath   = 'C:/inetpub/wwwroot/results';
const aebinary     = "C:\\Program Files\\Adobe\\Adobe After Effects CC 2017\\Support Files\\aerender";
const loopmiliSeconds = 1000;
const clearmilliSeconds = 5000;
const calcTotalmilliSeconds = 60000;

let currentServer   = null;
let maxAerender     = 10;
let isMain          = false;
let currentPublicIP = null;

exports.checkStatus = (req, res) => {
  publicIp.v4().then(ip => {
    currentPublicIP = ip;
    switch (currentPublicIP) {
      case '20.191.143.59':
        maxAerender = 10;
        isMain = true;
        break;
      case '52.231.66.227':
        maxAerender = 10;
        break;
      case '104.215.182.222':
        maxAerender = 10;
        break;
      case '13.67.53.185':
        maxAerender = 10;
        break;
      case '137.116.140.88':
        maxAerender = 10;
        break;
      case '137.116.142.123':
        maxAerender = 10;
        break;
    }

    setInterval(function(){
      getCount();
    }, loopmiliSeconds);

    setInterval(function(){
      findEmpty();
    }, clearmilliSeconds);

    setInterval(function(){
      calcTotal();
    }, calcTotalmilliSeconds);
  });
};

const calcTotal = () => {
  Server.find({ 'status': 'online' }, (err, servers) => {
    servers.forEach((server) => {
      Render.count({serverIP: server.serverIP}, (err, count) => {
        server.total = count;
        server.save();
      });
      Render.count({status: 'pending', serverIP: server.serverIP}, (err, count) => {
        server.pending = count;
        server.save();
      });
      Render.count({status: 'rendering', serverIP: server.serverIP}, (err, count) => {
        server.rendering = count;
        server.save();
      });
      Render.count({status: 'completed', serverIP: server.serverIP}, (err, count) => {
        server.completed = count;
        server.save();
      });
      Render.count({status: 'error', serverIP: server.serverIP}, (err, count) => {
        server.error = count;
        server.save();
      });
    });
  });
}

const findEmpty = () => {
  Render.find({ 'status': 'pending', 'serverIP': null}, (err, renders) => {
    if (!err) {
      if (renders && renders.length > 0) {
        renders.forEach((render) => {
          render.serverIP   = currentPublicIP;
          render.receivedAt = new Date();
          render.save();
        });
      }
    }
  })
}

const getCount = () => {
  autoRender(currentPublicIP);

  // Render.count({status: 'pending', serverIP: currentPublicIP}, (err, count) => {
  //   if (count == 0) {
  //     if (isMain) {
  //       Render.find({ status: 'rendering' }, {}, { sort: { createdAt : 1 } }, (err, renders) => {
  //         if (renders.length > 0) {
  //           renders.forEach((render) => {
  //             const current = new Date();
  //             if (render.startedAt && render.status == 'rendering') {
  //               const duration = current.getTime() - render.startedAt.getTime();
  //               if (Math.round(duration / 1000) > 90 && render.status == 'rendering') {
  //                 console.log("Too Long! " + duration);
  //                 console.log(render._id);
  //                 render.status   = 'pending';
  //                 render.serverIP = currentPublicIP;
  //                 render.save();

  //                 Server.findOne({serverIP: currentPublicIP}, (err, server) => {
  //                   if (server) {
  //                     server.pending += 1;
  //                     server.total += 1;
  //                     server.save();
  //                   }
  //                 });
  //               }
  //             }
  //           })
  //         }
  //       });

  //       Render.find({ status: 'pending', serverIP: null, receivedAt: null }, {}, { sort: { createdAt : 1 } }, (err, renders) => {
  //         if (renders.length > 0) {
  //           renders.forEach((render) => {
  //             const current = new Date();
  //             if (render.status == 'pending') {
  //               render.serverIP   = currentPublicIP;
  //               render.receivedAt = new Date();
  //               render.save();

  //               Server.findOne({serverIP: currentPublicIP}, (err, server) => {
  //                 if (server) {
  //                   server.pending += 1;
  //                   server.total += 1;
  //                   server.save();
  //                 }
  //               });
  //             }
  //           })
  //         }
  //       });
  //     }
  //   }
  // });
}

const createDataFile = (render) => {
  const dataPath = path.join(__dirname, '../public/uploads/', render.userHexhash, 'data.js');

  return fs.outputFile(dataPath, `var user = ${JSON.stringify(render.info)};`).then((err) => {
    if (!err) {
      return true;
    } else {
      return false;
    }
  });
}

const autoRender = (serverIP) => {
  Render.count({ status: 'rendering', serverIP: serverIP }, (err, count) => {
    if (err) {
      console.error("Auto Render: " + JSON.stringify(err));
      return;
    }

    if (count < maxAerender) {
      Render.findOne({ status: 'pending', serverIP: serverIP }, {}, { sort: { 'createdAt' : 1 } }, async (err, render) => {
        if (err) {
          console.error("Auto Render find render: " + JSON.stringify(err));
          return;
        }
        if (render) {
          Server.findOne({serverIP: currentPublicIP}, (err, server) => {
            if (server) {
              server.pending = server.pending + 1;
              server.total += 1;
              server.save();
            }
          });

          if (render.status == 'pending') {
            render.status = 'rendering';
            render.save();

            Server.findOne({serverIP: currentPublicIP}, (err, server) => {
              if (server) {
                server.pending = server.pending - 1;
                server.rendering = server.rendering + 1;
                server.save();
              }
            });

            const hasDataFile = await createDataFile(render);
            if (hasDataFile) {
              const data = [
                {
                  type: 'project',
                  name: 'project.aepx',
                  src : `${aepxPath}/${render.aepx}`
                },
                {
                  type: 'script',
                  name: 'user.js',
                  src : `${scriptPath}/${render.userHexhash}/${datascript}`
                }
              ];

              project = new Project({
                template: 'project.aepx',
                composition: render.composition,
                settings   : {
                  outputModule: 'JPEG SEQUENCE',
                  outputExt   : 'jpg',
                  startFrame  : 0,
                  endFrame    : 1
                },
                assets: data,
              });

              render.startedAt = new Date();
              render.projectId = project.uid;
              render.save();

              User.findOne({ hexhash: render.userHexhash }, (err, user) => {
                if (user) {
                  user.thumbnail.status    = 'rendering';
                  user.thumbnail.projectId = render.projectId;
                  user.thumbnail.serverIP  = serverIP;
                  user.save();
                } else {
                  render.remove();
                  return;
                }
              })

              renderer.render(aebinary, project).then(async (result) => {
                render.completedAt = new Date();
                const hasValidated = await validateThumbnail(render);
                User.findOne({ hexhash: render.userHexhash }, (err, user) => {
                  if (user) {
                    user.thumbnail.status      = 'completed';
                    user.thumbnail.completedAt = new Date();
                    user.thumbnail.projectId   = result.uid;
                    user.thumbnail.serverIP    = serverIP;
                    user.save();
                  }
                });
                if (!hasValidated) {
                  // render.status = 'error';
                  // render.save();

                  // currentServer.pending += 1;
                  // currentServer.save();
                } else {
                  // `${resultPath}/${result.uid}/result_00000.jpg`, `${resultPath}/${result.uid}_result.jpg`
                  
                }
              }).catch((err) => {
                console.error("Auto Render Error: " + JSON.stringify(err));
                render.status     = 'error';
                render.errMessage = err;
                render.save();

                // render.status = 'pending';
                // render.save();

                Server.findOne({serverIP: currentPublicIP}, (err, server) => {
                  if (server) {
                    server.rendering -= 1;
                    server.error += 1;
                    server.save();
                  }
                });
              });
            } else {
              render.status = 'pending';
              render.errMessage = 'Data file not found!';
              render.save();
              return;
            }
          }
        }
      });
    } else {
      return;
    }
  });
}

const validateThumbnail = (render) => {
  const progressPath = `${resultPath}/${render.projectId}/progress.txt`;

  return fs.access(progressPath, fs.constants.F_OK).then((err) => {
    if (!err) {
      return fs.readFile(progressPath).then((data, err) => {
        if (err) {
          render.status      = 'error';
          render.errMessage  = err;
          render.completedAt = null;
          render.save();
          console.error("Logging Read File Error: " + JSON.stringify(err));
          return false;
        }

        const progressData  = data.toString();
        const progress      = getProgress(progressData, progressPath);

        if (progress == 'failed') {
          render.status      = 'error';
          render.errMessage  = 'Render failed.';
          render.completedAt = null;
          render.save();

          Server.findOne({serverIP: currentPublicIP}, (err, server) => {
            if (server) {
              server.rendering -= 1;
              server.error += 1;
              server.save();
            }
          });
          return false;
        } else if (progress == 'rendering') {
          render.status      = 'error';
          render.errMessage  = 'Rendering.';
          render.completedAt = null;
          render.save();

          Server.findOne({serverIP: currentPublicIP}, (err, server) => {
            if (server) {
              server.rendering -= 1;
              server.error += 1;
              server.save();
            }
          });

          return false;
        } else if (progress == 'completed') {
          render.status      = 'completed';
          render.completedAt = new Date();
          render.save();

          const duration  = render.completedAt.getTime() - render.startedAt.getTime();
          render.duration = Math.round(duration / 1000);
          render.save();

          Server.findOne({serverIP: currentPublicIP}, (err, server) => {
            if (server) {
              server.rendering -= 1;
              server.completed += 1;
              server.save();
            }
          });

          return true;
        }
      })
    } else {
      render.status      = 'error';
      render.errMessage  = err;
      render.completedAt = null;
      render.save();

      Server.findOne({serverIP: currentPublicIP}, (err, server) => {
        if (server) {
          server.rendering -= 1;
          server.error += 1;
          server.save();
        }
      });
      console.error("Logging Read File Error: " + JSON.stringify(err));
      return false;
    }
  });
}

const getProgress = (progressData, path) => {
  const regex = /\((.*?)\)/gm;
  const exists   = fs.existsSync(path);
  const maxFrame = 1;

  let currentFrame = 0;
  let m;

  while ((m = regex.exec(progressData)) !== null) {
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    let value = parseInt(m[1]);
    if (value && value > currentFrame) {
      currentFrame = value;
    }
  }

  if(progressData.indexOf('error') >= 0) {
    return 'failed';
  } else {
    return 'completed';
  }

  // if ((maxFrame - currentFrame) <= 0) {
  //   if(progressData.indexOf('error') >= 0) {
  //     return 'failed';
  //   } else {
  //     return 'completed';
  //   }
  // } else {
  //   if (progressData.indexOf('Rendering error') >= 0 || progressData.indexOf('project failed') >= 0) {
  //     return 'failed';
  //   } else {
  //     return 'rendering';
  //   }
  // }
}
