const i18n = require('i18n');
/**
 * GET /
 * Teaser page.
 */
exports.index = (req, res) => {
    i18n.setLocale(req, req.params.lang);
    res.render('maintenance', {
      title: 'Maintenance',
      slug: '',
    });
};
