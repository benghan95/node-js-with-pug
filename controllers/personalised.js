const i18n  = require('i18n');
const fs    = require('fs');
const User  = require('../models/User');
const fetch = require('node-fetch');

exports.index = async (req, res) => {
  i18n.setLocale(req, req.params.lang);

  const { lang }    = req.params;
  const { hexhash } = req.params;

  if (!hexhash) {
    res.redirect(`/${lang}`);
    return;
  }

  let meta_title       = "One in #halfabillion";
  let meta_description = "My travel personality from the journeys I had with AirAsia! #halfabillion. Create your own #halfabillion personality by visiting www.halfabillion.airasia.com";

  switch (req.params.lang) {
    case 'id':
      meta_title       = "Satu dari #halfabillion";
      meta_description = "Karakter petualangan saya dari perjalanan yang telah saya lakukan bersama AirAsia! #halfabillion . Lihat karakter #halfabillion Anda dengan mengunjungi halfabillion.airasia.com";
      break;
    case 'th':
      meta_title       = "ขอบคุณที่เป็นส่วนหนึ่งใน #halfabillion";
      meta_description = "ลักษณะเดินทางของฉันจากการเดินทางร่วมกับแอร์เอเชีย#halfabillion. ตรวจสอบลักษณะนักเดินทางของคุณได้ที่ www.halfabillion.airasia.com #halfabillion";
      break;
    case 'cn-zh':
      meta_title       = "我是#5亿乘客中的一员";
      meta_description = "作为亚航的#5亿乘客之一，这是专属于我的旅行态度 登录www.halfabillion.airasia.com生成专属于你的#5亿乘客 旅行态度";
      break;
    case 'tw-zh':
      meta_title       = "成為五億乘客之一 #halfabillion ";
      meta_description = "這是我與AirAsia的精彩飛行紀錄！ #halfabillion 點擊進入www.halfabillion.airasia.com，製作屬於你的 #halfabillion 飛行紀錄。";
      break;
    case 'hk-zh':
      meta_title       = "我的 #halfabillion 旅程";
      meta_description = "這是我與AirAsia的精彩飛行紀錄！ #halfabillion 點擊進入www.halfabillion.airasia.com，創建屬於您的 #halfabillion 飛行紀錄。";
      break;
    case 'kr':
      meta_title       = "#halfabillion 중 한 명 이십니다";
      meta_description = "에어아시아와 함께 떠난 나의 여행 스타일 #halfabillion www.halfabillion.airasia.com를 방문하여 나의 여행 스타일을 알아보세요";
      break;
    case 'jp':
      meta_title       = "5億人以上の旅の中のひとつ";
      meta_description = "私の旅は、エアアジア　#エアアジア5億人達成　#halfabillion　で行った旅の履歴です。www.halfabillion.airasia.comで私の旅 #エアアジア5億人達成　#halfabillionの記録を作ろう。";
      break;
    case 'vn':
      meta_title       = "Một trong #halfabillion";
      meta_description = "Tính cách du lịch của tôi thể hiện qua hành trình với AirAsia! #halfabillion Tự tạo video Tính cách du lịch của bạn tại www.halfabillion.airasia.com";
      break;
    default:
      break;
  }

  let thumbnail_id  = null;
  let thumbnail_url = null;

  User.findOne({ hexhash }, async (err, user) => {
    if (err) {
      res.redirect(`/${lang}`);
      return;
    }

    if (!user) {
      res.redirect(`/${lang}`);
      return;
    }

    const thumbnail = user.thumbnail;

    if ('projectId' in thumbnail) {
      if (thumbnail.status == 'completed') {
        let url = 'halfabillion.airasia.com';

        switch (thumbnail.serverIP) {
          case '20.191.143.59':
            url = 'halfabillion.airasia.com';
            break;
          case '52.231.66.227':
            url = 'a1-halfabillion.airasia.com';
            break;
          case '104.215.182.222':
            url = 'a4-halfabillion.airasia.com';
            break;
          case '13.67.53.185':
            url = 'a6-halfabillion.airasia.com';
            break;
          case '137.116.140.88':
            url = 'a7-halfabillion.airasia.com';
            break;
          case '137.116.142.123':
            url = 'a8-halfabillion.airasia.com';
            break;
        }
        res.render('personalised', {
          title  : 'My #halfabillion Journey - AirAsia',
          slug   : 'my-journey/' + hexhash,
          lang   : lang,
          big_id : user.bigId,
          hexhash: hexhash,
          thumbnail_id: thumbnail.projectId,
          thumbnail_url: `https://${url}/results/${thumbnail.projectId}/result_00000.jpg`,
          meta_title,
          meta_description,
          meta_url  : `https://halfabillion.airasia.com/${lang}/my-journey/${user.hexhash}?redirect=landing`,
          meta_image: `https://${url}/results/${thumbnail.projectId}/result_00000.jpg`,
        });
        return;
      } else {
        res.redirect(`/${lang}`);
        return;
      }
    } else {
      res.redirect(`/${lang}`);
      return;
    }
  });
};

