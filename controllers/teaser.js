const i18n = require('i18n');
/**
 * GET /
 * Teaser page.
 */
exports.index = (req, res) => {
  if (!req.params.lang) {
    res.redirect('/en');
  } else {
    i18n.setLocale(req, req.params.lang);
    res.render('teaser', {
      title: 'Celebrating our biggest milestone yet. - AirAsia',
      meta_title: 'Celebrating our biggest milestone yet. - AirAsia',
      slug: 'teaser',
      lang: req.params.lang,
      meta_image: 'https://halfabillion.airasia.com/public/img/masthead_og.png',
    });
  }
};
