module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'HalfABillion',
      script    : 'apploop.js',
      instances : 1,
      exec_mode : 'cluster',
      out_file: 'pm2_out.log',
      error_file: 'pm2_err.log',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        PORT: 8080,
        NODE_ENV: 'production'
      }
    }
  ]
};
