const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  userId       : { type: String, unique: true },
  bigId        : { type: String, unique: true },
  name         : String,
  email        : String,
  gender       : String,
  hexhash      : String,
  accessToken  : String,
  hasShared    : Boolean,
  hasDownloaded: Boolean,
  hasEmailed   : Boolean,
  thumbnail    : {
    renderId   : { type: String },
    status     : String,
    sentAt     : Date,
    completedAt: Date,
    projectId  : String,
    serverIP   : String,
  },
  info: {
    name             : String,
    gender           : String,
    lang             : String,
    favoriteFood     : String,
    bookingsMade     : Number,
    tier             : Number,
    tierName         : String,
    bookingsMsg      : String,
    firstLanded      : String,
    firstVisited     : String,
    preferredDevice  : String,
    thumbnailName    : String,
    thumbnailTitle   : String,
    thumbnailDesc    : String,
    thumbnailFooter  : String,
    thumbnailBookings: String,
  },
}, { timestamps: true });

const user = mongoose.model('user', userSchema);

module.exports = user;
