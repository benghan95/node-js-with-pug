const mongoose = require('mongoose');

// _id
const renderSchema = new mongoose.Schema({
  status     : String,
  serverIP   : String,
  type       : String,
  projectId  : String,
  userHexhash: String,
  errMessage : String,
  aepx       : String,
  composition: String,
  duration   : Number,
  receivedAt : Date,
  startedAt  : Date,
  completedAt: Date,
  errorAt    : Date,
  info: {
    name             : String,
    gender           : String,
    lang             : String,
    favoriteFood     : String,
    bookingsMade     : Number,
    tier             : Number,
    tierName         : String,
    bookingsMsg      : String,
    firstLanded      : String,
    firstVisited     : String,
    preferredDevice  : String,
    thumbnailName    : String,
    thumbnailTitle   : String,
    thumbnailDesc    : String,
    thumbnailFooter  : String,
    thumbnailBookings: String,
  },
}, { timestamps: true });

const render = mongoose.model('render', renderSchema);

module.exports = render;
