const mongoose = require('mongoose');

// _id
const serverSchema = new mongoose.Schema({
  serverIP  : String,
  serverName: String,
  status    : String,
  pending   : Number,
  rendering : Number,
  completed : Number,
  error     : Number,
  total     : Number,
}, { timestamps: true });

const server = mongoose.model('server', serverSchema);

module.exports = server;
