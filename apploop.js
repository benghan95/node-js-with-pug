/**
 * Module dependencies.
 */
const express              = require('express');
const compression          = require('compression');
const cookieParser         = require('cookie-parser');
const session              = require('express-session');
const bodyParser           = require('body-parser');
const chalk                = require('chalk');
const errorHandler         = require('errorhandler');
const lusca                = require('lusca');
const dotenv               = require('dotenv');
const flash                = require('express-flash');
const path                 = require('path');
const mongoose             = require('mongoose');
const passport             = require('passport');
const expressValidator     = require('express-validator');
const expressStatusMonitor = require('express-status-monitor');
const sass                 = require('node-sass-middleware');
const postcss              = require('postcss-middleware');
const autoprefixer         = require('autoprefixer');
const i18n                 = require('i18n');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env' });

/**
 * Controllers (route handlers).
 */
const loggingController = require('./controllers/logging');
const renderController  = require('./controllers/render');
const maintenanceController  = require('./controllers/maintenance');

/**
 * API keys and Passport configuration.
 */
// const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
const app = express();

// app.use('/', httpsRedirect());
// console.log(provess.env.)

/**
 * Connect to MongoDB.
 */
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://20.191.143.59:27017/airasia');
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cookieParser());
app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src        : path.join(__dirname, 'public'),
  dist       : path.join(__dirname, 'public'),
  debug      : false,
  outputStyle: 'compressed',
  prefix     : '/public',
}));
app.use(postcss({
  plugins: [
    autoprefixer({
      browsers: 'defaults',
    })
  ],
  src: (req) => {
    if (req.path != '/') {
      return path.join(__dirname, 'public/css', req.path);
    } else {
      return "/public/css";
    }
    // const { path } = req;
  }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

const MongoStore = require('connect-mongo')(session);

app.use(session({
  resave           : true,
  saveUninitialized: true,
  secret           : process.env.SESSION_SECRET,
  cookie           : { maxAge: 1209600000 },       // two weeks in milliseconds
  store            : new MongoStore({
    url          : process.env.MONGODB_URI,
    autoReconnect: true,
  })
}));
/**
* Multi-language configuration.
*/
i18n.configure({
  // setup some locales - other locales default to en silently
  locales: ['en', 'id', 'th', 'cn-zh', 'tw-zh', 'hk-zh', 'kr', 'jp', 'vn'],

  // where to store json files - defaults to './locales' relative to modules directory
  directory: `${__dirname}/locales`,

  defaultLocale: 'en',

  // watch for changes in json files to reload locale on updates - defaults to false
  autoReload: true,

  // whether to write new locale information to disk - defaults to true
  updateFiles: false,

  // sync locale information accros all files - defaults to false
  syncFiles: true,

  cookie: 'lang',

  // sets a custom cookie name to parse locale settings from  - defaults to NULL
  // cookie: 'lang',
});
app.use(i18n.init);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use((req, res, next) => {
  if (req.path === '/api/upload') {
    next();
  } else {
    next();
    // lusca.csrf()(req, res, next);
  }
});
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the intended page
  if (!req.user &&
    req.path !== '/login' &&
    req.path !== '/signup' &&
    !req.path.match(/^\/auth/) &&
    !req.path.match(/\./)) {
    req.session.returnTo = req.originalUrl;
  } else if (req.user &&
    req.path === '/account') {
    req.session.returnTo = req.originalUrl;
  }
  next();
});
app.use('/public', express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));
app.use('/results', express.static(path.join(__dirname, 'results'), { maxAge: 31557600000 }));

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'https://halfabillion.airasia.com');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

const urlPrefix = '';
const maintenance = true;

/**
 * Primary app routes.
 */
if (maintenance) {
  app.get(`${urlPrefix}/`, maintenanceController.index);
  app.get(`${urlPrefix}/:lang`, maintenanceController.index);
  app.get(`${urlPrefix}/:lang/loading`, maintenanceController.index);
  app.get(`${urlPrefix}/:lang/loading/:hexhash`, maintenanceController.index);
  app.get(`${urlPrefix}/:lang/my-journey`, maintenanceController.index);
  app.get(`${urlPrefix}/:lang/my-journey/:hexhash`, maintenanceController.index);
} else {
  app.get(`${urlPrefix}`, landingController.redirectLang);
  app.get(`${urlPrefix}/`, landingController.redirectLang);
  app.get(`${urlPrefix}/:lang`, landingController.index);
  app.post(`${urlPrefix}/:lang/login`, loginController.postLogin);
  app.get(`${urlPrefix}/:lang/loading/:hexhash`, loadingController.index);
  app.post(`${urlPrefix}/:lang/render-thumbnail`, loadingController.renderThumbnail);
  app.get(`${urlPrefix}/:lang/my-journey/:hexhash`, personalisedController.index);
  app.get(`${urlPrefix}/:lang/trigger-rendering`, renderController.trigger);
  app.get(`${urlPrefix}/:lang/email`, emailController.index);
  app.post(`${urlPrefix}/:lang/send-email`, emailController.sendEmail);
  app.post(`${urlPrefix}/:lang/logging`, loggingController.logging);
  app.post(`${urlPrefix}/:lang/check-status`, renderController.checkRenderStatus);
  app.post(`${urlPrefix}/:lang/render-thumbnail`, renderController.renderThumbnail);
  app.post(`${urlPrefix}/:lang/shared-facebook`, shareController.sharedFacebook);
}

/**
 * Error Handler.
 */
app.use(errorHandler());
app.locals.pretty = true;

// when status is 404, error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    if( 404 === err.status  ){
      console.error(err);
      res.render('404');
      return;
    }

    // when status is 500, error handler
    if(500 === err.status) {
        return res.send({message: 'error occur'});
    }
});

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log("Check status");
  renderController.checkStatus();
});

module.exports = app;
